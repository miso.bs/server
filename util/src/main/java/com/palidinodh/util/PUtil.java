package com.palidinodh.util;

public class PUtil {
  private static final int SYSTEM_GC_MINIMUM = 10 * 60 * 1000;

  private static long lastCalledGC;

  public static void gc() {
    gc(false);
  }

  public static void gc(boolean force) {
    if (!force && PTime.currentTimeMillis() - lastCalledGC < SYSTEM_GC_MINIMUM) {
      return;
    }
    lastCalledGC = PTime.currentTimeMillis();
    System.gc();
  }

  public static int getFaceDirection(int xOffset, int yOffset) {
    return (int) (Math.atan2(-xOffset, -yOffset) * 325.949D) & 2047;
  }

  public static boolean used(int useItemId, int onItemId, int itemId1, int itemId2) {
    return useItemId == itemId1 && onItemId == itemId2
        || useItemId == itemId2 && onItemId == itemId1;
  }

  public static boolean hasMatch(int useItemId, int onItemId, int... options) {
    return getMatch(useItemId, onItemId, options) != -1;
  }

  public static int getMatch(int useItemId, int onItemId, int... options) {
    for (int option : options) {
      if (useItemId == option) {
        return option;
      }
      if (onItemId == option) {
        return option;
      }
    }
    return -1;
  }
}
