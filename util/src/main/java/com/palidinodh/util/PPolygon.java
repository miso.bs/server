package com.palidinodh.util;

import java.awt.geom.Point2D;

public class PPolygon {
  private Point2D[] points;

  public PPolygon(Point2D... points) {
    this.points = points;
  }

  public PPolygon(int[] xPoints, int[] yPoints) {
    points = new Point2D[xPoints.length];
    for (int i = 0; i < xPoints.length; i++) {
      points[i] = getPoint(xPoints[i], yPoints[i]);
    }
  }

  public boolean contains(Point2D point) {
    if (points.length < 3) {
      return false;
    }
    Point2D extreme = getPoint(Integer.MAX_VALUE, point.getY());
    int count = 0, i = 0;
    do {
      int next = (i + 1) % points.length;
      if (doIntersect(points[i], points[next], point, extreme)) {
        if (ccw(points[i], point, points[next]) == 0) {
          return onSegment(points[i], point, points[next]);
        }
        count++;
      }
      i = next;
    } while (i != 0);
    return (count % 2 == 1);
  }

  public static Point2D getPoint(double x, double y) {
    return new Point2D.Double(x, y);
  }

  private static boolean onSegment(Point2D p, Point2D q, Point2D r) {
    if (q.getX() <= Math.max(p.getX(), r.getX()) && q.getX() >= Math.min(p.getX(), r.getX())
        && q.getY() <= Math.max(p.getY(), r.getY()) && q.getY() >= Math.min(p.getY(), r.getY())) {
      return true;
    }
    return false;
  }

  private static boolean doIntersect(Point2D p1, Point2D q1, Point2D p2, Point2D q2) {
    int o1 = ccw(p1, q1, p2);
    int o2 = ccw(p1, q1, q2);
    int o3 = ccw(p2, q2, p1);
    int o4 = ccw(p2, q2, q1);
    if (o1 != o2 && o3 != o4) {
      return true;
    }
    if (o1 == 0 && onSegment(p1, p2, q1)) {
      return true;
    }
    if (o2 == 0 && onSegment(p1, q2, q1)) {
      return true;
    }
    if (o3 == 0 && onSegment(p2, p1, q2)) {
      return true;
    }
    if (o4 == 0 && onSegment(p2, q1, q2)) {
      return true;
    }
    return false;
  }

  private static int ccw(Point2D a, Point2D b, Point2D c) {
    long aX = (long) a.getX();
    long aY = (long) a.getY();
    long bX = (long) b.getX();
    long bY = (long) b.getY();
    long cX = (long) c.getX();
    long cY = (long) c.getY();
    long area = (bX - aX) * (cY - aY) - (cX - aX) * (bY - aY);
    return area == 0 ? 0 : (area > 0 ? 1 : -1);
  }
}
