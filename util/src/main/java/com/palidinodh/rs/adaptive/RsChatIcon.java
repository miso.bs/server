package com.palidinodh.rs.adaptive;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum RsChatIcon {
  NONE(0, -1),
  SILVER_CROWN(1, 0),
  GOLD_CROWN(2, 1),
  IRONMAN(3, 2),
  ULTIMATE_IRONMAN(4, 3),
  HARDCORE_IRONMAN(5, 10),
  PINK_CROWN(6, 20),
  PURPLE_CROWN(7, 21),
  GREEN_CROWN(8, 22),
  BLUE_CROWN(9, 23),
  OPAL_MEMBER(10, 24),
  JADE_MEMBER(11, 25),
  TOPAZ_MEMBER(12, 26),
  SAPPHIRE_MEMBER(13, 27),
  EMERALD_MEMBER(14, 28),
  YOUTUBE(15, 29),
  PREMIUM_MEMBER(16, 30),
  ORANGE_CROWN(17, 31),
  RUBY_MEMBER(18, 32),
  DIAMOND_MEMBER(19, 33),
  DRAGONSTONE_MEMBER(20, 34),
  ONYX_MEMBER(21, 35),
  ZENYTE_MEMBER(22, 36);

  private int osrsRankIndex;
  private int osrsSpriteIndex;
}
