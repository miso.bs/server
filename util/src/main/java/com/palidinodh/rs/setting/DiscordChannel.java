package com.palidinodh.rs.setting;

public enum DiscordChannel {
  MODERATION, ANNOUNCEMENTS, GAME_ANNOUNCEMENTS, LOCAL
}
