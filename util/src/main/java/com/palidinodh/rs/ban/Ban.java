package com.palidinodh.rs.ban;

import com.palidinodh.util.PTime;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public abstract class Ban {
  private BannedUser bannedUser;
  private BannedByUser bannedByUser;
  private long expiration;
  private String reason;

  public abstract boolean matches(BannedUser user);

  public boolean isExpired() {
    return expiration > 0 && PTime.currentTimeMillis() - expiration > 0;
  }
}
