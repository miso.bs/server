package com.palidinodh.rs.ban;

import com.palidinodh.util.PTime;
import lombok.Getter;

@Getter
public class UserBan extends Ban {
  public UserBan(BannedUser bannedUser, BannedByUser bannedByUser, String reason) {
    super(bannedUser, bannedByUser, 0, reason);
  }

  public UserBan(BannedUser bannedUser, BannedByUser bannedByUser, int hours, String reason) {
    super(bannedUser, bannedByUser,
        hours > 0 ? PTime.currentTimeMillis() + PTime.hourToMilli(hours) : 0, reason);
  }

  @Override
  public boolean matches(BannedUser user) {
    if (isExpired()) {
      return false;
    }
    BannedUser bannedUser = getBannedUser();
    return user.getUserId() == bannedUser.getUserId();
  }
}
