package com.palidinodh.rs.ban;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class BannedByUser {
  private int userId;
  private String username;
}
