# BattleScape

This is the open side of the BattleScape RSPS. Due to the private core, to fully test any changes, you'll need to request access to the beta server were you can freely deploy your changes as often as needed.

## Getting Started

These instructions will get you a copy of the project up on your local machine for development purposes.

### Prerequisites

- [Visual Studio Code](https://code.visualstudio.com) is recommended, and will be the IDE used in the installation guide.
- [Git](https://git-scm.com), needed to clone the repo and also to pull and push any changes.
- [Git LFS](https://git-lfs.github.com), needed to manage large files such as the cache.
- [Java 8 Development Kit](https://adoptopenjdk.net). In the setup, select/enable the "Set JAVA_HOME variable" option. A reboot may be needed for VSCode to find the JDK.

Required VSCode extensions:
- Java Extension Pack
- Lombok Annotations Support for VS Code

Recommended VSCode extensions:
- GitLens — Git supercharged
- GitLab Workflow (Pali Fork)
- Bracket Pair Colorizer

Creating your own fork:
- Select `Fork` in the top-right of this page.
- Once it finishes, you'll be taken to your forked copy of our repo.
- Select `Clone or download` and copy the url, you'll need this to start installing.

### Installing

- Install all of the software listed in prerequisites.
- Create your own fork, as described in prerequisites.
- Open Visual Studio Code.
- Install the extensions mentioned in prerequisites through the sidebar.
- Go to View -> Command Pallete...
- Type in `Git: Clone` and select it.
- Copy and paste the url you copied earlier from `Clone or download` and hit enter.
- Choose the location you'd like to create the repo at, such as your desktop. The directory that will be created can be renamed or moved later.
- Select open when asked if you'd like to open the cloned repo.
- Go to `Terminal -> New Terminal`.
- Type in `git lfs install` and hit enter.

## Running/Debugging

Autocompletion and error checking against the core will work, but you won't be able to deploy a test server on your local machine. You can request access to the beta server where you can upload your classes to test and debug them thoroughly.

There is an ongoing effort to port a large amount of the closed repo over to the open repo to provide better access to what can be worked on, but there are currently many limitations to what can be changed due to what is publicly available. The reason for the closed repo is to limit access to some core classes to prevent anyone from simply creating their own server from BattleScape instead of contributing to it.

## Getting the Latest Changes

Your forked repo won't automatically fetch or pull updates made to the official repo.

Setup:
- Go to `View -> Command Palette...`.
- Type in `Git: Add Remote` and select it.
- Type in `BattleScape-Server` for the name and hit enter.
- Type in `https://gitlab.com/battlescape/server.git` for the url and hit enter.
- Go to `View -> Command Palette...`.
- Type in `Git: Fetch From All Remotes`.

Pulling any changes from the official repo:
- From `Source Control` in the sidebar, select the three dots on the right-side of source control git (`More Actions...`).
- Select `Pull from...`.
- Select `Server`.
- Select `Server/master`.

## Contributing

First, make sure you've configured your username and email for git.
- Go to `Terminal -> New Terminal`.
- Type in `git config --global user.name "YOUR_USERNAME"` and hit enter.
- Type in `git config --global user.email "YOUR_EMAIL"` and hit enter.

Once you've completed change(s), push them into your forked repo.
- Go to `Source Control` in the sidebar.
- Enter a message briefly describing the changes.
- Select the checkmark (`Commit`).
- Select the `Synchronize Changes`, which saves your changes into your forked repo.

To request your changes to be added to the official repo, visit your forked repo on GitLab. Select `New pull request`, and then submit your changes from there.
