package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ ItemId.UNCHARGED_TRIDENT, ItemId.UNCHARGED_TRIDENT_E })
class UnchargedTridentItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    player.getGameEncoder()
        .sendMessage("Each charge requires 1 death rune, 1 chaos rune, 5 fire runes and 10 coins.");
  }
}
