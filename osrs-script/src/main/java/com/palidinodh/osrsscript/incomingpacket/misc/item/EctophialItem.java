package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Magic;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrscore.world.World;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.ECTOPHIAL)
class EctophialItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (!player.getController().canTeleport(true)) {
      return;
    }
    var height = 0;
    if ((player.inEdgeville() || player.getArea().inWilderness())
        && player.getClientHeight() == 0) {
      height = player.getHeight();
    }
    var ectoTile = new Tile(3603, 3528, height);
    if (height != 0) {
      ectoTile = new Tile(World.DEFAULT_TILE).randomize(2);
      ectoTile.setHeight(height);
    }
    player.getMovement().animatedTeleport(ectoTile, Magic.ECTOPHIAL_ANIMATION, -1,
        Magic.ECTOPHIAL_GRAPHIC, null, 2);
    player.getController().stopWithTeleport();
    player.getCombat().clearHitEvents();
  }
}
