package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Magic;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.FARMING_CAPE, ItemId.FARMING_CAPE_T })
class FarmingCapeItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (!player.getController().canTeleport(20, true)) {
      return;
    }
    var ardougneFarmTeleport = new Tile(2673, 3377);
    player.getMovement().animatedTeleport(ardougneFarmTeleport, Magic.NORMAL_MAGIC_ANIMATION_START,
        Magic.NORMAL_MAGIC_ANIMATION_END, Magic.NORMAL_MAGIC_GRAPHIC, null, 2);
    player.getController().stopWithTeleport();
    player.getCombat().clearHitEvents();
  }
}
