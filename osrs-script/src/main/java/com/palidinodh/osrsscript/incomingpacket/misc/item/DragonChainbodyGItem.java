package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.DRAGON_CHAINBODY_G, ItemId.DRAGON_PLATELEGS_G, ItemId.DRAGON_PLATESKIRT_G,
    ItemId.DRAGON_SQ_SHIELD_G, ItemId.DRAGON_SCIMITAR_OR, ItemId.DRAGON_DEFENDER_T,
    ItemId.DARK_INFINITY_HAT, ItemId.DARK_INFINITY_TOP, ItemId.DARK_INFINITY_BOTTOMS,
    ItemId.LIGHT_INFINITY_HAT, ItemId.LIGHT_INFINITY_TOP, ItemId.LIGHT_INFINITY_BOTTOMS,
    ItemId.AMULET_OF_FURY_OR, ItemId.AMULET_OF_TORTURE_OR, ItemId.OCCULT_NECKLACE_OR,
    ItemId.DRAGON_FULL_HELM_G, ItemId.ARMADYL_GODSWORD_OR, ItemId.BANDOS_GODSWORD_OR,
    ItemId.SARADOMIN_GODSWORD_OR, ItemId.ZAMORAK_GODSWORD_OR, ItemId.DRAGON_BOOTS_G,
    ItemId.DRAGON_PLATEBODY_G, ItemId.DRAGON_KITESHIELD_G, ItemId.NECKLACE_OF_ANGUISH_OR,
    ItemId.ODIUM_WARD_12807, ItemId.MALEDICTION_WARD_12806, ItemId.RUNE_DEFENDER_T,
    ItemId.TZHAAR_KET_OM_T, ItemId.BERSERKER_NECKLACE_OR, ItemId.RUNE_SCIMITAR_23330,
    ItemId.RUNE_SCIMITAR_23332, ItemId.RUNE_SCIMITAR_23334, ItemId.TORMENTED_BRACELET_OR,
    ItemId.DRAGON_PICKAXE_OR })
class DragonChainbodyGItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (item.getInfoDef().getExchangeIds() == null) {
      return;
    }
    if (player.getInventory().getRemainingSlots() < item.getInfoDef().getExchangeIds().length - 1) {
      player.getInventory().notEnoughSpace();
      return;
    }
    item.remove();
    for (var exchangeId : item.getInfoDef().getExchangeIds()) {
      player.getInventory().addItem(exchangeId);
    }
  }
}
