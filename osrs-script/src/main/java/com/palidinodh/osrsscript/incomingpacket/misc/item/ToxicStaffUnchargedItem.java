package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.TOXIC_STAFF_UNCHARGED)
class ToxicStaffUnchargedItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (player.getInventory().getRemainingSlots() < 1) {
      player.getInventory().notEnoughSpace();
      return;
    }
    item.replace(new Item(ItemId.STAFF_OF_THE_DEAD));
    player.getInventory().addItem(ItemId.MAGIC_FANG, 1);
  }
}
