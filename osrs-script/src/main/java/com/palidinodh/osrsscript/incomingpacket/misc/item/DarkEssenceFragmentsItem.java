package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.DARK_ESSENCE_FRAGMENTS)
class DarkEssenceFragmentsItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (player.getCharges().getDarkEssenceFragments() <= 0) {
      player.getInventory().deleteItem(ItemId.DARK_ESSENCE_FRAGMENTS);
    } else {
      player.getGameEncoder().sendMessage("You currently have "
          + player.getCharges().getDarkEssenceFragments() + " charges stored.");
      return;
    }
  }
}
