package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.ELEMENTAL_RUNE_PACK)
class ElementalRunePackItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.remove();
    player.getInventory().addOrDropItem(ItemId.AIR_RUNE, 50);
    player.getInventory().addOrDropItem(ItemId.WATER_RUNE, 50);
    player.getInventory().addOrDropItem(ItemId.EARTH_RUNE, 50);
    player.getInventory().addOrDropItem(ItemId.FIRE_RUNE, 50);
  }
}
