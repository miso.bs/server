package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.CLUE_BOX)
class ClueBoxItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.remove();
    var randomItems = new RandomItem[] { new RandomItem(2677, 1).weight(8) /* Clue scroll (easy) */,
        new RandomItem(2801, 1).weight(6) /* Clue scroll (medium) */,
        new RandomItem(2722, 1).weight(4) /* Clue scroll (hard) */,
        new RandomItem(12073, 1).weight(2) /* Clue scroll (elite) */,
        new RandomItem(19835, 1).weight(1) /* Clue scroll (master) */
    };
    player.getInventory().addOrDropItem(RandomItem.getItem(randomItems));
    if (PRandom.randomE(4) == 0) {
      player.getInventory().addOrDropItem(RandomItem.getItem(randomItems));
    }
  }
}
