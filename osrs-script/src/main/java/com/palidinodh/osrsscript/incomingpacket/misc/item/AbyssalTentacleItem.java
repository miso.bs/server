package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.ABYSSAL_TENTACLE)
class AbyssalTentacleItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "dissolve":
        player.openDialogue(new OptionsDialogue("Are you sure you want to dissolve this item?",
            new DialogueOption("Yes, dissolve this item and lose the abyssal whip.", (c, s) -> {
              item.replace(new Item(ItemId.KRAKEN_TENTACLE));
            }), new DialogueOption("Cancel.")));
        break;
    }
  }
}
