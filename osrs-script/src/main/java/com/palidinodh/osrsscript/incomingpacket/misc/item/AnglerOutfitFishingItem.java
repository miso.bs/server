package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.ANGLER_OUTFIT_FISHING_32293)
class AnglerOutfitFishingItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (player.getInventory().getRemainingSlots() < 4 - 1) {
      player.getInventory().notEnoughSpace();
      return;
    }
    item.remove();
    player.getInventory().addItem(ItemId.ANGLER_HAT);
    player.getInventory().addItem(ItemId.ANGLER_TOP);
    player.getInventory().addItem(ItemId.ANGLER_WADERS);
    player.getInventory().addItem(ItemId.ANGLER_BOOTS);
  }
}
