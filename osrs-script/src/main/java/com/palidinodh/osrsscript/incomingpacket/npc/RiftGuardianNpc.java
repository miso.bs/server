package com.palidinodh.osrsscript.incomingpacket.npc;

import java.util.List;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PCollection;

@ReferenceId({ NpcId.RIFT_GUARDIAN, NpcId.RIFT_GUARDIAN_7338, NpcId.RIFT_GUARDIAN_7339,
    NpcId.RIFT_GUARDIAN_7340, NpcId.RIFT_GUARDIAN_7341, NpcId.RIFT_GUARDIAN_7342,
    NpcId.RIFT_GUARDIAN_7343, NpcId.RIFT_GUARDIAN_7344, NpcId.RIFT_GUARDIAN_7345,
    NpcId.RIFT_GUARDIAN_7346, NpcId.RIFT_GUARDIAN_7347, NpcId.RIFT_GUARDIAN_7348,
    NpcId.RIFT_GUARDIAN_7349, NpcId.RIFT_GUARDIAN_7350, NpcId.RIFT_GUARDIAN_8024 })
class RiftGuardianNpc implements NpcHandler {

  public static final List<Integer> recolorItem = PCollection.toList(ItemId.AIR_RUNE,
      ItemId.MIND_RUNE, ItemId.WATER_RUNE, ItemId.EARTH_RUNE, ItemId.FIRE_RUNE, ItemId.BODY_RUNE,
      ItemId.COSMIC_RUNE, ItemId.CHAOS_RUNE, ItemId.ASTRAL_RUNE, ItemId.NATURE_RUNE,
      ItemId.LAW_RUNE, ItemId.DEATH_RUNE, ItemId.BLOOD_RUNE, ItemId.SOUL_RUNE, ItemId.WRATH_RUNE);

  @Override
  public void npcOption(Player player, int option, Npc npc) {
    player.getGameEncoder().sendMessage("Use a rune on me to change my color.");
  }

  @Override
  public void itemOnNpc(Player player, int slot, int itemId, Npc npc) {
    if (npc != player.getFamiliar().getFamiliar()) {
      return;
    }
    if (!recolorItem.contains(itemId)) {
      return;
    }
    if (recolorItem.contains(itemId)) {
      switch (itemId) {
        case ItemId.AIR_RUNE:
          player.getFamiliar().transform(NpcId.RIFT_GUARDIAN_7338);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.MIND_RUNE:
          player.getFamiliar().transform(NpcId.RIFT_GUARDIAN_7339);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.WATER_RUNE:
          player.getFamiliar().transform(NpcId.RIFT_GUARDIAN_7340);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.EARTH_RUNE:
          player.getFamiliar().transform(NpcId.RIFT_GUARDIAN_7341);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.FIRE_RUNE:
          player.getFamiliar().transform(NpcId.RIFT_GUARDIAN);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.BODY_RUNE:
          player.getFamiliar().transform(NpcId.RIFT_GUARDIAN_7342);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.COSMIC_RUNE:
          player.getFamiliar().transform(NpcId.RIFT_GUARDIAN_7343);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.CHAOS_RUNE:
          player.getFamiliar().transform(NpcId.RIFT_GUARDIAN_7344);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.ASTRAL_RUNE:
          player.getFamiliar().transform(NpcId.RIFT_GUARDIAN_7349);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.NATURE_RUNE:
          player.getFamiliar().transform(NpcId.RIFT_GUARDIAN_7345);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.LAW_RUNE:
          player.getFamiliar().transform(NpcId.RIFT_GUARDIAN_7346);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.DEATH_RUNE:
          player.getFamiliar().transform(NpcId.RIFT_GUARDIAN_7347);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.BLOOD_RUNE:
          player.getFamiliar().transform(NpcId.RIFT_GUARDIAN_7350);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.SOUL_RUNE:
          player.getFamiliar().transform(NpcId.RIFT_GUARDIAN_7348);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.WRATH_RUNE:
          player.getFamiliar().transform(NpcId.RIFT_GUARDIAN_8024);
          player.getInventory().deleteItem(itemId, 1);
          break;
      }
    }
  }
}
