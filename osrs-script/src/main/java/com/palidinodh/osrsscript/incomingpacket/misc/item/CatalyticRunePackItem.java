package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.CATALYTIC_RUNE_PACK)
class CatalyticRunePackItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.remove();
    player.getInventory().addOrDropItem(ItemId.CHAOS_RUNE, 50);
    player.getInventory().addOrDropItem(ItemId.DEATH_RUNE, 50);
    player.getInventory().addOrDropItem(ItemId.BLOOD_RUNE, 50);
    player.getInventory().addOrDropItem(ItemId.WRATH_RUNE, 50);
    player.getInventory().addOrDropItem(ItemId.NATURE_RUNE, 50);
  }
}
