package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Magic;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.KHARYRLL_TELEPORT)
class KharyrllTeleportItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (!player.getController().canTeleport(true)) {
      return;
    }
    item.remove(1);
    var height = 0;
    if ((player.inEdgeville() || player.getArea().inWilderness())
        && player.getClientHeight() == 0) {
      height = player.getHeight();
    }
    player.getMovement().animatedTeleport(new Tile(3510, 3480, height),
        Magic.TABLET_ANIMATION_START, Magic.TABLET_ANIMATION_END, -1, null, Magic.TABLET_GRAPHIC,
        null, 0, 2);
    player.getController().stopWithTeleport();
    player.getCombat().clearHitEvents();
    player.log(PlayerLogType.TELEPORT, "Kharyrll using a tablet");
  }
}
