package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Magic;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.TAVERLEY_TELEPORT)
class TaverleyTeleportItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (!player.getController().canTeleport(true)) {
      return;
    }
    item.remove(1);
    player.getMovement().animatedTeleport(new Tile(2894, 3465), Magic.TABLET_ANIMATION_START,
        Magic.TABLET_ANIMATION_END, -1, null, Magic.TABLET_GRAPHIC, null, 0, 2);
    player.getController().stopWithTeleport();
    player.getCombat().clearHitEvents();
    player.log(PlayerLogType.TELEPORT, "Taverley using a tablet");
  }
}
