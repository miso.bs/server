package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Magic;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ ItemId.ARDOUGNE_CLOAK_2, ItemId.ARDOUGNE_CLOAK_3, ItemId.ARDOUGNE_CLOAK_4 })
class ArdougneCloak2Item implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (!player.getController().canTeleport(30, true)) {
      return;
    }
    Tile ardougneCloakTeleport = null;
    switch (option.getText()) {
      case "monastery teleport":
      case "kandarin monastery":
        ardougneCloakTeleport = new Tile(2606, 3223);
        break;
      case "farm teleport":
      case "ardougne farm":
        ardougneCloakTeleport = new Tile(2673, 3374);
        break;
    }
    if (ardougneCloakTeleport == null) {
      return;
    }
    player.getMovement().animatedTeleport(ardougneCloakTeleport, Magic.NORMAL_MAGIC_ANIMATION_START,
        Magic.NORMAL_MAGIC_ANIMATION_END, Magic.NORMAL_MAGIC_GRAPHIC, null, 2);
    player.getController().stopWithTeleport();
    player.getCombat().clearHitEvents();
  }
}
