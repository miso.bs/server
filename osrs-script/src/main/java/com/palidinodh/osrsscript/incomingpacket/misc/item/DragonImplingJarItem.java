package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.DRAGON_IMPLING_JAR)
class DragonImplingJarItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.remove();
    var randomItems = new RandomItem[] { new RandomItem(11232, 100, 350) /* Dragon dart tip */,
        new RandomItem(11237, 100, 500) /* Dragon arrowtips */,
        new RandomItem(9193, 10, 49) /* Dragon bolt tips */,
        new RandomItem(19582, 25, 35) /* Dragon javelin heads */,
        new RandomItem(11212, 100, 500).weight(4) /* Dragon arrow */,
        new RandomItem(9244, 3, 40).weight(4) /* Dragon bolts (e) */,
        new RandomItem(11230, 100, 350).weight(4) /* Dragon dart */,
        new RandomItem(4093, 1, 1).weight(2) /* Mystic robe bottom */,
        new RandomItem(1713, 2, 3).weight(2) /* Amulet of glory(4) (noted) */,
        new RandomItem(1703, 2, 3).weight(2) /* Dragonstone amulet (noted) */,
        new RandomItem(1305, 1, 1).weight(2) /* Dragon longsword */,
        new RandomItem(5699, 3, 3).weight(2) /* Dragon dagger(p++) (noted) */,
        new RandomItem(535, 100, 300).weight(2) /* Babydragon bones (noted) */,
        new RandomItem(5316, 1, 1).weight(2) /* Magic seed */,
        new RandomItem(537, 50, 100).weight(2) /* Dragon bones (noted) */,
        new RandomItem(1616, 3, 6).weight(2) /* Dragonstone (noted) */,
        new RandomItem(5300, 5, 5).weight(2) /* Snapdragon seed */,
        new RandomItem(7219, 15, 15).weight(2) /* Summer pie (noted) */,
        new RandomItem(12073, 1, 1).weight(2) /* Clue scroll (elite) */
    };
    player.getInventory().addOrDropItem(RandomItem.getItem(randomItems));
  }
}
