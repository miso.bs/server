package com.palidinodh.osrsscript.incomingpacket;

import java.util.HashMap;
import java.util.Map;
import com.palidinodh.io.Readers;
import com.palidinodh.io.Stream;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.InStreamKey;
import com.palidinodh.osrscore.io.incomingpacket.IncomingPacketDecoder;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.AchievementDiary;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.util.RequestManager;
import com.palidinodh.rs.adaptive.GrandExchangeUser;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PLogger;
import lombok.var;

class NpcOptionDecoder extends IncomingPacketDecoder {
  static final Map<Integer, NpcHandler> HANDLERS = new HashMap<>();

  @Override
  public boolean execute(Player player, Stream stream) {
    var option = getInt(InStreamKey.PACKET_OPTION);
    var npcIndex = getInt(InStreamKey.TARGET_INDEX);
    var ctrlRun = getInt(InStreamKey.CTRL_RUN);
    player.clearIdleTime();
    var npc = player.getWorld().getNpcByIndex(npcIndex);
    if (npc == null) {
      return false;
    }
    var message = "[NpcOption(" + option + ")] index=" + npcIndex + "; ctrlRun=" + ctrlRun + "; id="
        + npc.getId() + "/" + NpcId.valueOf(npc.getId());
    player.log(PlayerLogType.DECODER_DEBUG, message);
    if (Settings.getInstance().isLocal()) {
      PLogger.println(message);
    }
    if (player.getOptions().isDebug()) {
      player.getGameEncoder().sendMessage(message);
    }
    RequestManager.addUserPacketLog(player, message);
    if (player.isLocked()) {
      return false;
    }
    if (player.getMovement().isViewing()) {
      return false;
    }
    player.clearAllActions(false, true);
    player.setFaceEntity(npc);
    if (npc.getSpawn().getMoveRange() == null && !npc.getDef().getName().equals("Fishing spot")) {
      player.getMovement().setFollowFront(npc);
    } else {
      player.getMovement().setFollowing(npc);
    }
    if (option == 1 && npc.getCombat().getMaxHitpoints() > 0) {
      player.setAttacking(true);
      player.setAttackingEntity(npc);
      player.getCombat().setFollowing(npc);
      if (player.getMagic().getAutoSpellId() != 0 && player.getCombat().getHitDelay() <= 0) {
        player.getCombat().setHitDelay(2);
      }
      return false;
    }
    return true;
  }

  @Override
  public boolean complete(Player player) {
    var option = getInt(InStreamKey.PACKET_OPTION);
    var npcIndex = getInt(InStreamKey.TARGET_INDEX);
    var npc = player.getWorld().getNpcByIndex(npcIndex);
    if (npc == null || !npc.isVisible()) {
      return true;
    }
    if (player.isLocked()) {
      return false;
    }
    var handler = player.getArea().getNpcHandler(npc.getId());
    if (handler == null) {
      handler = HANDLERS.get(npc.getId());
    }
    var type = handler != null ? handler.canReach(player, npc) : null;
    if (type == NpcHandler.ReachType.FALSE) {
      return false;
    }
    if (type == null || type == NpcHandler.ReachType.DEFAULT) {
      if (!npc.getMovement().isRouting() && npc.getSizeX() == 1 && player.getX() != npc.getX()
          && player.getY() != npc.getY()) {
        return false;
      }
      var range = 1;
      if (npc.getDef().hasOption("bank")) {
        range = 2;
      }
      if (player.getMovement().isRouting() && npc.getMovement().isRouting()) {
        range++;
      }
      if (!player.withinDistance(npc, range)) {
        return false;
      }
    }
    player.setFaceEntity(null);
    player.getMovement().setFollowing(null);
    if (player.getX() == npc.getX() || player.getY() == npc.getY()) {
      player.getMovement().clear();
    }
    player.setFaceTile(npc);
    AchievementDiary.npcOptionUpdate(player, option, npc);
    if (SkillContainer.npcOptionHooks(player, option, npc)) {
      return true;
    }
    if (npc.getDef().isOption(option, "pick-up") && npc == player.getFamiliar().getFamiliar()) {
      player.getFamiliar().removeFamiliar();
      return true;
    }
    if (handler != null) {
      handler.npcOption(player, option, npc);
      return true;
    }
    if (basicAction(player, option, npc)) {
      return true;
    }
    player.getGameEncoder().sendMessage("Nothing interesting happens.");
    return true;
  }

  private static boolean basicAction(Player player, int option, Npc npc) {
    switch (npc.getDef().getLowerCaseName()) {
      case "banker":
      case "ghost banker":
      case "gnome banker":
        if (option == 0) {
          player.openDialogue("bank", 1);
        } else if (option == 2) {
          player.getBank().open();
        }
        return true;
      case "grand exchange clerk":
        if (option == 0) {
          player.openDialogue("grandexchange", 0);
        } else if (option == 2) {
          player.getGrandExchange().open();
        } else if (option == 3) {
          player.getGrandExchange().openHistory(GrandExchangeUser.HISTORY);
        } else if (option == 4) {
          player.getGrandExchange().exchangeItemSets();
        }
        return true;
    }
    return false;
  }

  public static void load(boolean force) {
    if (!force && !HANDLERS.isEmpty()) {
      return;
    }
    HANDLERS.clear();
    try {
      var classes = Readers.getScriptClasses(NpcHandler.class, "incomingpacket.npc");
      for (var clazz : classes) {
        var handler = Readers.newInstance(clazz);
        var ids = NpcHandler.getIds(handler);
        if (ids == null) {
          continue;
        }
        for (var id : ids) {
          if (HANDLERS.containsKey(id)) {
            throw new RuntimeException(clazz.getName() + " - " + id + ": npc id already used.");
          }
          HANDLERS.put(id, handler);
        }
      }
    } catch (Exception e) {
      PLogger.error(e);
    }
  }

  static {
    load(true);
  }
}
