package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.SPADE)
class SpadeItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (player.getCombat().getBarrows().ahrimMound()) {
      player.getCombat().getBarrows().enterCrypt(NpcId.AHRIM_THE_BLIGHTED_98);
    } else if (player.getCombat().getBarrows().dharokMound()) {
      player.getCombat().getBarrows().enterCrypt(NpcId.DHAROK_THE_WRETCHED_115);
    } else if (player.getCombat().getBarrows().guthanMound()) {
      player.getCombat().getBarrows().enterCrypt(NpcId.GUTHAN_THE_INFESTED_115);
    } else if (player.getCombat().getBarrows().karilMound()) {
      player.getCombat().getBarrows().enterCrypt(NpcId.KARIL_THE_TAINTED_98);
    } else if (player.getCombat().getBarrows().toragMound()) {
      player.getCombat().getBarrows().enterCrypt(NpcId.TORAG_THE_CORRUPTED_115);
    } else if (player.getCombat().getBarrows().veracMound()) {
      player.getCombat().getBarrows().enterCrypt(NpcId.VERAC_THE_DEFILED_115);
    }
  }
}
