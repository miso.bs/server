package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Magic;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.CRAFTING_CAPE, ItemId.CRAFTING_CAPE_T })
class CraftingCapeItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "teleport":
        if (!player.getController().canTeleport(20, true)) {
          break;
        }
        var craftingGuildTeleport = new Tile(2935, 3283);
        player.getMovement().animatedTeleport(craftingGuildTeleport,
            Magic.NORMAL_MAGIC_ANIMATION_START, Magic.NORMAL_MAGIC_ANIMATION_END,
            Magic.NORMAL_MAGIC_GRAPHIC, null, 2);
        player.getController().stopWithTeleport();
        player.getCombat().clearHitEvents();
        break;
    }
  }
}
