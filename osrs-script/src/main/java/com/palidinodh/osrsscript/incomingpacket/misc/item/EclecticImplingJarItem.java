package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.ECLECTIC_IMPLING_JAR)
class EclecticImplingJarItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.remove();
    var randomItems = new RandomItem[] { new RandomItem(1273, 1) /* Mithril pickaxe */,
        new RandomItem(1199, 1) /* Adamant kiteshield */,
        new RandomItem(2493, 1) /* Blue d'hide chaps */,
        new RandomItem(10083, 1) /* Red spiky vambs */, new RandomItem(1213, 1) /* Rune dagger */,
        new RandomItem(1391, 1) /* Battlestaff */, new RandomItem(5970, 1) /* Curry leaf */,
        new RandomItem(231, 1) /* Snape grass */, new RandomItem(556, 30, 57) /* Air rune */,
        new RandomItem(8779, 4) /* Oak plank (noted) */,
        new RandomItem(4529, 1) /* Candle lantern (empty) */, new RandomItem(444, 1) /* Gold ore */,
        new RandomItem(2358, 5) /* Gold bar (noted) */, new RandomItem(237, 1) /* Unicorn horn */,
        new RandomItem(450, 5) /* Adamantite ore (noted) */,
        new RandomItem(5760, 2) /* Slayer's respite (noted) */,
        new RandomItem(7208, 1) /* Wild pie */, new RandomItem(5321, 3) /* Watermelon seed */,
        new RandomItem(2801, 1, 1).weight(4) /* Clue scroll (medium) */,
        new RandomItem(1601, 1) /* Diamond */
    };
    player.getInventory().addOrDropItem(RandomItem.getItem(randomItems));
  }
}
