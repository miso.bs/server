package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.CLUE_SCROLL_EASY, ItemId.CLUE_BOTTLE_EASY, ItemId.CLUE_GEODE_EASY,
    ItemId.CLUE_NEST_EASY })
class ClueScrollEasyItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var ttLoot = new int[] { ItemId.HOLY_BLESSING, ItemId.UNHOLY_BLESSING, ItemId.PEACEFUL_BLESSING,
        ItemId.HONOURABLE_BLESSING, ItemId.WAR_BLESSING, ItemId.ANCIENT_BLESSING,
        ItemId.WILLOW_COMP_BOW, ItemId.YEW_COMP_BOW, ItemId.MAGIC_COMP_BOW, ItemId.BEAR_FEET,
        ItemId.MOLE_SLIPPERS, ItemId.FROG_SLIPPERS, ItemId.DEMON_FEET, ItemId.SANDWICH_LADY_HAT,
        ItemId.SANDWICH_LADY_TOP, ItemId.SANDWICH_LADY_BOTTOM,
        ItemId.RUNE_SCIMITAR_ORNAMENT_KIT_GUTHIX, ItemId.RUNE_SCIMITAR_ORNAMENT_KIT_SARADOMIN,
        ItemId.RUNE_SCIMITAR_ORNAMENT_KIT_ZAMORAK, ItemId.MONKS_ROBE_TOP_T, ItemId.MONKS_ROBE_T,
        ItemId.AMULET_OF_DEFENCE_T, ItemId.JESTER_CAPE, ItemId.SHOULDER_PARROT,
        ItemId.PURPLE_SWEETS };
    var ttEasy = new int[] { ItemId.BRONZE_FULL_HELM_T, ItemId.BRONZE_PLATEBODY_T,
        ItemId.BRONZE_PLATELEGS_T, ItemId.BRONZE_PLATESKIRT_T, ItemId.BRONZE_KITESHIELD_T,
        ItemId.BRONZE_FULL_HELM_G, ItemId.BRONZE_PLATEBODY_G, ItemId.BRONZE_PLATELEGS_G,
        ItemId.BRONZE_PLATESKIRT_G, ItemId.BRONZE_KITESHIELD_G, ItemId.IRON_FULL_HELM_T,
        ItemId.IRON_PLATEBODY_T, ItemId.IRON_PLATELEGS_T, ItemId.IRON_PLATESKIRT_T,
        ItemId.IRON_KITESHIELD_T, ItemId.IRON_FULL_HELM_G, ItemId.IRON_PLATEBODY_G,
        ItemId.IRON_PLATELEGS_G, ItemId.IRON_PLATESKIRT_G, ItemId.IRON_KITESHIELD_G,
        ItemId.STEEL_FULL_HELM_T, ItemId.STEEL_PLATEBODY_T, ItemId.STEEL_PLATELEGS_T,
        ItemId.STEEL_PLATESKIRT_T, ItemId.STEEL_KITESHIELD_T, ItemId.STEEL_FULL_HELM_G,
        ItemId.STEEL_PLATEBODY_G, ItemId.STEEL_PLATELEGS_G, ItemId.STEEL_PLATESKIRT_G,
        ItemId.STEEL_KITESHIELD_G, ItemId.BLACK_FULL_HELM_T, ItemId.BLACK_PLATEBODY_T,
        ItemId.BLACK_PLATELEGS_T, ItemId.BLACK_PLATESKIRT_T, ItemId.BLACK_KITESHIELD_T,
        ItemId.BLACK_FULL_HELM_G, ItemId.BLACK_PLATEBODY_G, ItemId.BLACK_PLATELEGS_G,
        ItemId.BLACK_PLATESKIRT_G, ItemId.BLACK_KITESHIELD_G, ItemId.BLACK_BERET, ItemId.BLUE_BERET,
        ItemId.WHITE_BERET, ItemId.RED_BERET, ItemId.HIGHWAYMAN_MASK, ItemId.BEANIE,
        ItemId.BLUE_WIZARD_HAT_T, ItemId.BLUE_WIZARD_ROBE_T, ItemId.BLUE_SKIRT_T,
        ItemId.BLUE_WIZARD_HAT_G, ItemId.BLUE_WIZARD_ROBE_G, ItemId.BLUE_SKIRT_G,
        ItemId.BLACK_WIZARD_HAT_T, ItemId.BLACK_WIZARD_ROBE_T, ItemId.BLACK_SKIRT_T,
        ItemId.BLACK_WIZARD_HAT_G, ItemId.BLACK_WIZARD_ROBE_G, ItemId.BLACK_SKIRT_G,
        ItemId.STUDDED_BODY_T, ItemId.STUDDED_CHAPS_T, ItemId.STUDDED_BODY_G,
        ItemId.STUDDED_CHAPS_G, ItemId.BLACK_HELM_H1, ItemId.BLACK_HELM_H2, ItemId.BLACK_HELM_H3,
        ItemId.BLACK_HELM_H4, ItemId.BLACK_HELM_H5, ItemId.BLACK_PLATEBODY_H1,
        ItemId.BLACK_PLATEBODY_H2, ItemId.BLACK_PLATEBODY_H3, ItemId.BLACK_PLATEBODY_H4,
        ItemId.BLACK_PLATEBODY_H5, ItemId.BLACK_SHIELD_H1, ItemId.BLACK_SHIELD_H2,
        ItemId.BLACK_SHIELD_H3, ItemId.BLACK_SHIELD_H4, ItemId.BLACK_SHIELD_H5,
        ItemId.BLUE_ELEGANT_SHIRT, ItemId.BLUE_ELEGANT_LEGS, ItemId.BLUE_ELEGANT_BLOUSE,
        ItemId.BLUE_ELEGANT_SKIRT, ItemId.GREEN_ELEGANT_SHIRT, ItemId.GREEN_ELEGANT_LEGS,
        ItemId.GREEN_ELEGANT_BLOUSE, ItemId.GREEN_ELEGANT_SKIRT, ItemId.RED_ELEGANT_SHIRT,
        ItemId.RED_ELEGANT_LEGS, ItemId.RED_ELEGANT_BLOUSE, ItemId.RED_ELEGANT_SKIRT,
        ItemId.BOBS_RED_SHIRT, ItemId.BOBS_BLUE_SHIRT, ItemId.BOBS_GREEN_SHIRT,
        ItemId.BOBS_BLACK_SHIRT, ItemId.BOBS_PURPLE_SHIRT, ItemId.STAFF_OF_BOB_THE_CAT,
        ItemId.A_POWDERED_WIG, ItemId.FLARED_TROUSERS, ItemId.PANTALOONS, ItemId.SLEEPING_CAP,
        ItemId.AMULET_OF_MAGIC_T, ItemId.AMULET_OF_POWER_T, ItemId.RAIN_BOW, ItemId.HAM_JOINT,
        ItemId.BLACK_CANE, ItemId.BLACK_PICKAXE, ItemId.GUTHIX_ROBE_TOP, ItemId.GUTHIX_ROBE_LEGS,
        ItemId.SARADOMIN_ROBE_TOP, ItemId.SARADOMIN_ROBE_LEGS, ItemId.ZAMORAK_ROBE_TOP,
        ItemId.ZAMORAK_ROBE_LEGS, ItemId.ANCIENT_ROBE_TOP, ItemId.ANCIENT_ROBE_LEGS,
        ItemId.BANDOS_ROBE_LEGS, ItemId.ARMADYL_ROBE_TOP, ItemId.ARMADYL_ROBE_LEGS, ItemId.IMP_MASK,
        ItemId.GOBLIN_MASK, ItemId.TEAM_CAPE_I, ItemId.TEAM_CAPE_X, ItemId.TEAM_CAPE_ZERO,
        ItemId.CAPE_OF_SKULLS, ItemId.WOODEN_SHIELD_G, ItemId.GOLDEN_CHEFS_HAT, ItemId.GOLDEN_APRON,
        ItemId.MONKS_ROBE_TOP_G, ItemId.MONKS_ROBE_G, ItemId.LARGE_SPADE, ItemId.LEATHER_BODY_G,
        ItemId.LEATHER_CHAPS_G };
    item.remove();
    var easyItemId = ttEasy[PRandom.randomE(ttEasy.length)];
    player.getInventory().addItem(easyItemId, 1);
    if (PRandom.randomE(5) == 0) {
      var extraItemId = ttLoot[PRandom.randomE(ttLoot.length)];
      if (extraItemId == ItemId.PURPLE_SWEETS) {
        player.getInventory().addOrDropItem(extraItemId, 8 + PRandom.randomI(24));
      } else {
        player.getInventory().addOrDropItem(extraItemId, 1);
      }
    }
    player.getSkills().increaseClueScrollCount(Skills.CLUE_SCROLL_EASY);
  }
}
