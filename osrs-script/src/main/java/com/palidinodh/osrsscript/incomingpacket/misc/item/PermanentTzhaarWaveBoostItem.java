package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.PERMANENT_TZHAAR_WAVE_BOOST_32300)
class PermanentTzhaarWaveBoostItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.remove();
    player.getCombat().getTzHaar().setPermanentWaveBoost(true);
    player.getGameEncoder().sendMessage("The effects of the scroll have been activated.");
  }
}
