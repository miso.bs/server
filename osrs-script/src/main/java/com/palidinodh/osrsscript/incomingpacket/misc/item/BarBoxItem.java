package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrsscript.player.plugin.slayer.SlayerPlugin;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.BAR_BOX_32302)
class BarBoxItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.remove();
    var randomItems = new RandomItem[] { new RandomItem(2364, 1) /* Runite bar */,
        new RandomItem(2362, 1) /* Adamantite bar */, new RandomItem(2360, 1) /* Mithril bar */, };
    for (var i = 0; i < 30; i++) {
      var barItem = RandomItem.getItem(randomItems);
      player.getInventory().addOrDropItem(barItem.getNotedId(), barItem.getAmount());
    }
    var plugin = player.getPlugin(SlayerPlugin.class);
    plugin.incrimentBarBoxes();
    player.getGameEncoder().sendMessage("You have opened " + plugin.getBarBoxes() + " bar boxes!");
  }
}
