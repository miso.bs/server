package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.THAMMARONS_SCEPTRE)
class ThammaronsSceptreItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "uncharge":
        if (player.getInventory().getRemainingSlots() < 1) {
          player.getInventory().notEnoughSpace();
          return;
        }
        player.getInventory().addItem(21820, item.getCharges());
        item.replace(new Item(ItemId.THAMMARONS_SCEPTRE_U));
        break;
    }
  }
}
