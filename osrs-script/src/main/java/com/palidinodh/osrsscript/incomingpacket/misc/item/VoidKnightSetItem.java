package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.VOID_KNIGHT_SET_32289)
class VoidKnightSetItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (player.getInventory().getRemainingSlots() < 9 - 1) {
      player.getInventory().notEnoughSpace();
      return;
    }
    item.remove();
    player.getInventory().addItem(ItemId.VOID_KNIGHT_TOP);
    player.getInventory().addItem(ItemId.VOID_KNIGHT_ROBE);
    player.getInventory().addItem(ItemId.VOID_KNIGHT_MACE);
    player.getInventory().addItem(ItemId.VOID_KNIGHT_GLOVES);
    player.getInventory().addItem(ItemId.VOID_MAGE_HELM);
    player.getInventory().addItem(ItemId.VOID_RANGER_HELM);
    player.getInventory().addItem(ItemId.VOID_MELEE_HELM);
    player.getInventory().addItem(ItemId.ELITE_VOID_TOP);
    player.getInventory().addItem(ItemId.ELITE_VOID_ROBE);
  }
}
