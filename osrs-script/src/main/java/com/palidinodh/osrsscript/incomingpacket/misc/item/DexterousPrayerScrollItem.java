package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.DEXTEROUS_PRAYER_SCROLL)
class DexterousPrayerScrollItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (player.getPrayer().getRigourUnlocked()) {
      player.getGameEncoder().sendMessage("You have already unlocked the prayer Rigour.");
      return;
    }
    player.getPrayer().setRigourUnlocked(true);
    item.remove();
  }
}
