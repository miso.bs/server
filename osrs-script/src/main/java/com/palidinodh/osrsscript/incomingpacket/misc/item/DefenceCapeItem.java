package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ ItemId.DEFENCE_CAPE, ItemId.DEFENCE_CAPE_T })
class DefenceCapeItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    player.getEquipment().setDefenceCapeEffect(!player.getEquipment().getDefenceCapeEffect());
    player.getGameEncoder()
        .sendMessage("Ring of life: " + player.getEquipment().getDefenceCapeEffect());
  }
}
