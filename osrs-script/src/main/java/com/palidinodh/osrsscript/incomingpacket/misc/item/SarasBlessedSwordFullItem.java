package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ ItemId.SARAS_BLESSED_SWORD_FULL, ItemId.SARADOMINS_BLESSED_SWORD })
class SarasBlessedSwordFullItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "revert":
        item.replace(new Item(ItemId.SARADOMINS_TEAR));
        if (item.getId() == ItemId.SARAS_BLESSED_SWORD_FULL) {
          player.getInventory().addOrDropItem(ItemId.SARADOMIN_SWORD, 1);
        }
        break;
    }
  }
}
