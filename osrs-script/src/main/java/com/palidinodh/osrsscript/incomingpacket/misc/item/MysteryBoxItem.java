package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.MysteryBox;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ ItemId.MYSTERY_BOX, ItemId.SUPER_MYSTERY_BOX_32286, ItemId.PET_MYSTERY_BOX_32311,
    ItemId._3RD_AGE_BOX_32340 })
class MysteryBoxItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "open":
        MysteryBox.open(player, item.getId());
        break;
      case "quick open":
        MysteryBox.quickOpen(player, item.getId());
        break;
    }
  }
}
