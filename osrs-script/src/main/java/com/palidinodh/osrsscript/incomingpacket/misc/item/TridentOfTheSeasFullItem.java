package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.TRIDENT_OF_THE_SEAS_FULL)
class TridentOfTheSeasFullItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "uncharge":
        item.replace(new Item(ItemId.UNCHARGED_TRIDENT));
        player.getInventory().addOrDropItem(ItemId.DEATH_RUNE, 2500);
        player.getInventory().addOrDropItem(ItemId.CHAOS_RUNE, 2500);
        player.getInventory().addOrDropItem(ItemId.FIRE_RUNE, 12500);
        break;
    }
  }
}
