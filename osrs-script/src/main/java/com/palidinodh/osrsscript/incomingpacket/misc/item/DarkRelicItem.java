package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.DARK_RELIC)
class DarkRelicItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var raidsSkills = new int[] { Skills.ATTACK, Skills.DEFENCE, Skills.STRENGTH, Skills.HITPOINTS,
        Skills.RANGED, Skills.PRAYER, Skills.MAGIC, Skills.MINING, Skills.WOODCUTTING,
        Skills.HERBLORE, Skills.FARMING, Skills.HUNTER, Skills.COOKING, Skills.FISHING,
        Skills.THIEVING, Skills.FIREMAKING, Skills.AGILITY };
    player.getWidgetManager().sendChooseAdvanceSkill(item.getId(), 0, 0.5, 0);
    player.getWidgetManager().setChooseAdvanceSkillLevelMultiplier(50);
    for (var raidsSkill : raidsSkills) {
      player.getWidgetManager().setChooseAdvanceSkillLevelMultiplier(raidsSkill, 150);
    }
  }
}
