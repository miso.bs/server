package com.palidinodh.osrsscript.incomingpacket.npc;

import java.util.List;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PCollection;

@ReferenceId({ NpcId.ROCK_GOLEM_7439, NpcId.ROCK_GOLEM_7440, NpcId.ROCK_GOLEM_7441,
    NpcId.ROCK_GOLEM_7442, NpcId.ROCK_GOLEM_7443, NpcId.ROCK_GOLEM_7444, NpcId.ROCK_GOLEM_7445,
    NpcId.ROCK_GOLEM_7446, NpcId.ROCK_GOLEM_7447, NpcId.ROCK_GOLEM_7448, NpcId.ROCK_GOLEM_7449,
    NpcId.ROCK_GOLEM_7450, NpcId.ROCK_GOLEM_7736, NpcId.ROCK_GOLEM_7737, NpcId.ROCK_GOLEM_7738,
    NpcId.ROCK_GOLEM })
class RockGolemNpc implements NpcHandler {

  public static final List<Integer> recolorItem = PCollection.toList(ItemId.RUNITE_ORE,
      ItemId.DAEYALT_ORE, ItemId.ROCK_1480, ItemId.COPPER_ORE, ItemId.TIN_ORE, ItemId.IRON_ORE,
      ItemId.SILVER_ORE, ItemId.GOLD_ORE, ItemId.MITHRIL_ORE, ItemId.ADAMANTITE_ORE,
      ItemId.BLURITE_ORE, ItemId.COAL, ItemId.GRANITE_500G, ItemId.GRANITE_2KG, ItemId.GRANITE_5KG,
      ItemId.AMETHYST, ItemId.LOVAKITE_ORE, ItemId.ELEMENTAL_ORE);

  @Override
  public void npcOption(Player player, int option, Npc npc) {
    player.getGameEncoder().sendMessage("Use a ore on me to change my color.");
  }

  @Override
  public void itemOnNpc(Player player, int slot, int itemId, Npc npc) {
    if (npc != player.getFamiliar().getFamiliar()) {
      return;
    }
    if (!recolorItem.contains(itemId)) {
      return;
    }
    if (recolorItem.contains(itemId)) {
      switch (itemId) {
        case ItemId.ROCK_1480:
          player.getFamiliar().transform(NpcId.ROCK_GOLEM_7439);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.TIN_ORE:
          player.getFamiliar().transform(NpcId.ROCK_GOLEM_7440);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.COPPER_ORE:
          player.getFamiliar().transform(NpcId.ROCK_GOLEM_7441);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.IRON_ORE:
          player.getFamiliar().transform(NpcId.ROCK_GOLEM_7442);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.BLURITE_ORE:
          player.getFamiliar().transform(NpcId.ROCK_GOLEM_7443);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.SILVER_ORE:
          player.getFamiliar().transform(NpcId.ROCK_GOLEM_7444);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.COAL:
          player.getFamiliar().transform(NpcId.ROCK_GOLEM_7445);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.GOLD_ORE:
          player.getFamiliar().transform(NpcId.ROCK_GOLEM_7446);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.MITHRIL_ORE:
          player.getFamiliar().transform(NpcId.ROCK_GOLEM_7447);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.GRANITE_500G:
          player.getFamiliar().transform(NpcId.ROCK_GOLEM_7448);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.GRANITE_2KG:
          player.getFamiliar().transform(NpcId.ROCK_GOLEM_7448);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.GRANITE_5KG:
          player.getFamiliar().transform(NpcId.ROCK_GOLEM_7448);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.ADAMANTITE_ORE:
          player.getFamiliar().transform(NpcId.ROCK_GOLEM_7449);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.RUNITE_ORE:
          player.getFamiliar().transform(NpcId.ROCK_GOLEM_7450);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.AMETHYST:
          player.getFamiliar().transform(NpcId.ROCK_GOLEM);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.LOVAKITE_ORE:
          player.getFamiliar().transform(NpcId.ROCK_GOLEM_7736);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.ELEMENTAL_ORE:
          player.getFamiliar().transform(NpcId.ROCK_GOLEM_7737);
          player.getInventory().deleteItem(itemId, 1);
          break;
        case ItemId.DAEYALT_ORE:
          player.getFamiliar().transform(NpcId.ROCK_GOLEM_7738);
          player.getInventory().deleteItem(itemId, 1);
          break;
      }
    }
  }
}
