package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrsscript.player.plugin.magic.SpellTeleport;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ ItemId.MAX_CAPE_13280, ItemId.MAX_CAPE })
class MaxCapeItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (!player.getEquipment().meetsMaxCapeRequirements(true)) {
      return;
    }
    if (!player.getController().canTeleport(30, true)) {
      return;
    }
    switch (option.getText()) {
      case "teleports":
        player.openDialogue(new MaxCapeDialogue(player));
        break;
      case "features":
        player.getGameEncoder()
            .sendMessage("There are currently no features. Feel free to suggest some!");
        break;
      case "warriors' guild":
        SpellTeleport.normalTeleport(player, new Tile(2845, 3544));
        break;
      case "crafting guild":
        SpellTeleport.normalTeleport(player, new Tile(2936, 3282));
        break;
      case "tele to poh":
        SpellTeleport.normalTeleport(player, player.getWidgetManager().getHomeTile());
        break;
    }
  }

  public static class MaxCapeDialogue extends OptionsDialogue {
    public MaxCapeDialogue(Player player) {
      action((c, s) -> {
        Tile maxCapeTele = null;
        if (s == 0) {
          maxCapeTele = new Tile(3093, 3495);
        } else if (s == 1) {
          maxCapeTele = new Tile(1233, 3565);
        } else if (s == 2) {
          maxCapeTele = new Tile(1666, 10050);
        }
        if (!player.getController().canTeleport(true)) {
          return;
        }
        if (maxCapeTele == null) {
          return;
        }
        SpellTeleport.normalTeleport(player, maxCapeTele);
      });
      addOption("Edgeville");
      addOption("Chambers of Xeric");
      addOption("Catacombs of Kourend");
    }
  }
}
