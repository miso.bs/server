package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.CLUE_SCROLL_MASTER)
class ClueScrollMasterItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var ttLoot = new int[] { ItemId.HOLY_BLESSING, ItemId.UNHOLY_BLESSING, ItemId.PEACEFUL_BLESSING,
        ItemId.HONOURABLE_BLESSING, ItemId.WAR_BLESSING, ItemId.ANCIENT_BLESSING,
        ItemId.WILLOW_COMP_BOW, ItemId.YEW_COMP_BOW, ItemId.MAGIC_COMP_BOW, ItemId.BEAR_FEET,
        ItemId.MOLE_SLIPPERS, ItemId.FROG_SLIPPERS, ItemId.DEMON_FEET, ItemId.SANDWICH_LADY_HAT,
        ItemId.SANDWICH_LADY_TOP, ItemId.SANDWICH_LADY_BOTTOM,
        ItemId.RUNE_SCIMITAR_ORNAMENT_KIT_GUTHIX, ItemId.RUNE_SCIMITAR_ORNAMENT_KIT_SARADOMIN,
        ItemId.RUNE_SCIMITAR_ORNAMENT_KIT_ZAMORAK, ItemId.MONKS_ROBE_TOP_T, ItemId.MONKS_ROBE_T,
        ItemId.AMULET_OF_DEFENCE_T, ItemId.JESTER_CAPE, ItemId.SHOULDER_PARROT,
        ItemId.PURPLE_SWEETS };
    var ttMaster = new int[] { ItemId.DRAGON_PLATEBODY_ORNAMENT_KIT,
        ItemId.DRAGON_KITESHIELD_ORNAMENT_KIT, ItemId.DRAGON_DEFENDER_ORNAMENT_KIT,
        ItemId.ANGUISH_ORNAMENT_KIT, ItemId.TORTURE_ORNAMENT_KIT, ItemId.OCCULT_ORNAMENT_KIT,
        ItemId.ARMADYL_GODSWORD_ORNAMENT_KIT, ItemId.BANDOS_GODSWORD_ORNAMENT_KIT,
        ItemId.SARADOMIN_GODSWORD_ORNAMENT_KIT, ItemId.ZAMORAK_GODSWORD_ORNAMENT_KIT,
        ItemId.TORMENTED_ORNAMENT_KIT, ItemId.LESSER_DEMON_MASK, ItemId.GREATER_DEMON_MASK,
        ItemId.BLACK_DEMON_MASK, ItemId.JUNGLE_DEMON_MASK, ItemId.OLD_DEMON_MASK,
        ItemId.ARCEUUS_HOOD, ItemId.HOSIDIUS_HOOD, ItemId.LOVAKENGJ_HOOD, ItemId.PISCARILIUS_HOOD,
        ItemId.SHAYZIEN_HOOD, ItemId.SAMURAI_KASA, ItemId.SAMURAI_SHIRT, ItemId.SAMURAI_GLOVES,
        ItemId.SAMURAI_GREAVES, ItemId.SAMURAI_BOOTS, ItemId.MUMMYS_HEAD, ItemId.MUMMYS_BODY,
        ItemId.MUMMYS_HANDS, ItemId.MUMMYS_LEGS, ItemId.MUMMYS_FEET, ItemId.ANKOU_MASK,
        ItemId.ANKOU_TOP, ItemId.ANKOU_GLOVES, ItemId.ANKOUS_LEGGINGS, ItemId.ANKOU_SOCKS,
        ItemId.HOOD_OF_DARKNESS, ItemId.ROBE_TOP_OF_DARKNESS, ItemId.GLOVES_OF_DARKNESS,
        ItemId.ROBE_BOTTOM_OF_DARKNESS, ItemId.BOOTS_OF_DARKNESS, ItemId.RING_OF_COINS,
        ItemId.LEFT_EYE_PATCH, ItemId.OBSIDIAN_CAPE_R, ItemId.FANCY_TIARA,
        ItemId.HALF_MOON_SPECTACLES, ItemId.ALE_OF_THE_GODS, ItemId.BUCKET_HELM_G, ItemId.BOWL_WIG,
        ItemId.SCROLL_SACK };
    var gildedMaster =
        new int[] { ItemId.GILDED_FULL_HELM, ItemId.GILDED_PLATEBODY, ItemId.GILDED_PLATELEGS,
            ItemId.GILDED_PLATESKIRT, ItemId.GILDED_KITESHIELD, ItemId.GILDED_MED_HELM,
            ItemId.GILDED_CHAINBODY, ItemId.GILDED_SQ_SHIELD, ItemId.GILDED_2H_SWORD,
            ItemId.GILDED_SPEAR, ItemId.GILDED_HASTA, ItemId.GILDED_BOOTS, ItemId.GILDED_SCIMITAR,
            ItemId.GILDED_DHIDE_VAMBS, ItemId.GILDED_DHIDE_BODY, ItemId.GILDED_DHIDE_CHAPS,
            ItemId.GILDED_COIF, ItemId.GILDED_AXE, ItemId.GILDED_PICKAXE, ItemId.GILDED_SPADE };
    var thirdageMaster = new int[] { ItemId._3RD_AGE_FULL_HELMET, ItemId._3RD_AGE_PLATEBODY,
        ItemId._3RD_AGE_PLATELEGS, ItemId._3RD_AGE_KITESHIELD, ItemId._3RD_AGE_RANGE_COIF,
        ItemId._3RD_AGE_RANGE_TOP, ItemId._3RD_AGE_RANGE_LEGS, ItemId._3RD_AGE_VAMBRACES,
        ItemId._3RD_AGE_MAGE_HAT, ItemId._3RD_AGE_ROBE_TOP, ItemId._3RD_AGE_ROBE,
        ItemId._3RD_AGE_AMULET, ItemId._3RD_AGE_CLOAK, ItemId._3RD_AGE_WAND, ItemId._3RD_AGE_BOW,
        ItemId._3RD_AGE_LONGSWORD, ItemId._3RD_AGE_AXE, ItemId._3RD_AGE_PICKAXE,
        ItemId._3RD_AGE_DRUIDIC_ROBE_TOP, ItemId._3RD_AGE_DRUIDIC_ROBE_BOTTOMS,
        ItemId._3RD_AGE_DRUIDIC_CLOAK, ItemId._3RD_AGE_DRUIDIC_STAFF, ItemId.RING_OF_3RD_AGE,
        ItemId._3RD_AGE_PLATESKIRT };
    item.remove();
    if (PRandom.inRange(0.1)) {
      var thirdAgeId = thirdageMaster[PRandom.randomE(thirdageMaster.length)];
      player.getInventory().addItem(thirdAgeId, 1);
      player.getWorld().sendItemDropNews(player, thirdAgeId, "a master clue scroll");
    } else if (PRandom.inRange(0.5)) {
      var gildedId = gildedMaster[PRandom.randomE(gildedMaster.length)];
      player.getInventory().addItem(gildedId, 1);
      player.getWorld().sendItemDropNews(player, gildedId, "a master clue scroll");
    } else {
      var masterItemId = ttMaster[PRandom.randomE(ttMaster.length)];
      player.getInventory().addItem(masterItemId, 1);
    }
    if (PRandom.randomE(5) == 0) {
      var extraItemId = ttLoot[PRandom.randomE(ttLoot.length)];
      if (extraItemId == ItemId.PURPLE_SWEETS) {
        player.getInventory().addOrDropItem(extraItemId, 8 + PRandom.randomI(24));
      } else {
        player.getInventory().addOrDropItem(extraItemId, 1);
      }
    }
    player.getFamiliar().rollPet(ItemId.BLOODHOUND, 0.1);
    player.getSkills().increaseClueScrollCount(Skills.CLUE_SCROLL_MASTER);
  }
}
