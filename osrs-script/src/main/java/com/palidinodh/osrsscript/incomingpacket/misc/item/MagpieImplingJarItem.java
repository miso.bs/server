package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.MAGPIE_IMPLING_JAR)
class MagpieImplingJarItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.remove();
    var randomItems = new RandomItem[] { new RandomItem(1701, 3) /* Diamond amulet (noted) */,
        new RandomItem(1732, 3) /* Amulet of power (noted) */,
        new RandomItem(2569, 3) /* Ring of forging (noted) */,
        new RandomItem(3391, 1) /* Splitbark gauntlets */,
        new RandomItem(4097, 1) /* Mystic boots */, new RandomItem(4095, 1) /* Mystic gloves */,
        new RandomItem(1347, 1) /* Rune warhammer */,
        new RandomItem(2571, 4) /* Ring of life (noted) */,
        new RandomItem(1185, 1) /* Rune sq shield */, new RandomItem(1215, 1) /* Dragon dagger */,
        new RandomItem(5541, 1) /* Nature tiara */,
        new RandomItem(1748, 6) /* Black dragonhide (noted) */,
        new RandomItem(2722, 1, 1).weight(4) /* Clue scroll (hard) */,
        new RandomItem(2364, 2) /* Runite bar (noted) */,
        new RandomItem(1602, 4) /* Diamond (noted) */, new RandomItem(5287, 1) /* Pineapple seed */,
        new RandomItem(987, 1) /* Loop half of key */,
        new RandomItem(985, 1) /* Tooth half of key */, new RandomItem(993, 1) /* Sinister key */,
        new RandomItem(5300, 1) /* Snapdragon seed */
    };
    player.getInventory().addOrDropItem(RandomItem.getItem(randomItems));
  }
}
