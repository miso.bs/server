package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.setting.Settings;

@ReferenceId(ItemId.HYDRA_LEATHER)
class HydraLeatherItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (Settings.getInstance().isSpawn()) {
      item.replace(new Item(ItemId.FEROCIOUS_GLOVES));
    } else {
      player.getGameEncoder().sendMessage(
          "This leather looks pretty tough to work with... Maybe the dragonkin had a way.");
    }
  }
}
