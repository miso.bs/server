package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrsscript.player.plugin.slayer.SlayerPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.SUPPLY_CRATE)
class SupplyCrateItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (player.getInventory().getRemainingSlots() < 3) {
      player.getInventory().notEnoughSpace();
      return;
    }
    var randomItems = new RandomItem[] { new RandomItem(1522, 13, 148),
        new RandomItem(1520, 13, 20), new RandomItem(1518, 10, 16), new RandomItem(6334, 14, 59),
        new RandomItem(8836, 10, 48), new RandomItem(1516, 10, 49), new RandomItem(1514, 10, 23),
        new RandomItem(1624, 1, 5), new RandomItem(1622, 1, 5), new RandomItem(1618, 1, 5),
        new RandomItem(1620, 1, 5), new RandomItem(454, 3, 12), new RandomItem(441, 3, 15),
        new RandomItem(443, 3, 12), new RandomItem(445, 3, 70), new RandomItem(448, 2, 7),
        new RandomItem(450, 2, 15), new RandomItem(452, 1, 2), new RandomItem(200, 3, 6),
        new RandomItem(202, 3, 6), new RandomItem(206, 7), new RandomItem(208, 1, 3),
        new RandomItem(212, 2, 3), new RandomItem(214, 1, 4), new RandomItem(216, 2, 3),
        new RandomItem(220, 1, 3), new RandomItem(5321, 1, 7), new RandomItem(5293, 1, 3),
        new RandomItem(5294, 1, 3), new RandomItem(5295, 1, 3), new RandomItem(5296, 1, 3),
        new RandomItem(5298, 1, 3), new RandomItem(5300, 1, 4), new RandomItem(5303, 1, 3),
        new RandomItem(5312, 1), new RandomItem(5313, 1, 2), new RandomItem(5284, 1, 3),
        new RandomItem(21486, 1, 3), new RandomItem(5314, 1, 3), new RandomItem(21488, 1, 3),
        new RandomItem(5304, 1, 3), new RandomItem(5315, 1, 3), new RandomItem(5316, 1, 3),
        new RandomItem(5317, 1), new RandomItem(322, 5, 12), new RandomItem(336, 5, 12),
        new RandomItem(332, 5, 12), new RandomItem(378, 5, 11), new RandomItem(360, 5, 12),
        new RandomItem(372, 5, 21), new RandomItem(384, 5, 21),
        new RandomItem(ItemId.COINS, 2030, 9048), new RandomItem(13422, 3, 24),
        new RandomItem(3212, 4, 7), new RandomItem(7937, 29, 391), new RandomItem(13574, 3, 19) };
    item.remove();
    var supplyCount = 2 + PRandom.randomI(2);
    for (var i = 0; i < supplyCount; i++) {
      if (PRandom.inRange(player.getCombat().getDropRate(0.01, ItemId.DRAGON_AXE))) {
        player.getInventory().addItem(ItemId.DRAGON_AXE, 1);
      } else if (PRandom.inRange(player.getCombat().getDropRate(0.02, ItemId.PHOENIX))) {
        player.getInventory().addItem(ItemId.PHOENIX, 1);
        player.getWorld().sendItemDropNews(player, ItemId.PHOENIX, "a supply crate");
      } else if (PRandom.inRange(player.getCombat().getDropRate(0.1, ItemId.TOME_OF_FIRE_EMPTY))) {
        player.getInventory().addItem(ItemId.TOME_OF_FIRE_EMPTY, 1);
      } else if (PRandom.inRange(player.getCombat().getDropRate(0.66, ItemId.BRUMA_TORCH))) {
        player.getInventory().addItem(ItemId.BRUMA_TORCH, 1);
      } else if (PRandom.inRange(player.getCombat().getDropRate(2.2, ItemId.BURNT_PAGE))) {
        player.getInventory().addItem(ItemId.BURNT_PAGE, 5 + PRandom.randomE(26));
      } else {
        player.getInventory().addItem(RandomItem.getItem(randomItems));
      }
    }
    var plugin = player.getPlugin(SlayerPlugin.class);
    plugin.incrimentSupplyBoxes();
    player.getGameEncoder()
        .sendMessage("You have opened " + plugin.getSupplyBoxes() + " Supply boxes!");
  }
}
