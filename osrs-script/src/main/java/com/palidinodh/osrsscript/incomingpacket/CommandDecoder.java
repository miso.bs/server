package com.palidinodh.osrsscript.incomingpacket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import com.palidinodh.io.Readers;
import com.palidinodh.io.Stream;
import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.io.incomingpacket.InStreamKey;
import com.palidinodh.osrscore.io.incomingpacket.IncomingPacketDecoder;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.Scroll;
import com.palidinodh.osrscore.util.RequestManager;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.rs.setting.UserRank;
import com.palidinodh.util.PLogger;
import lombok.var;

class CommandDecoder extends IncomingPacketDecoder {
  private static final Map<String, CommandHandler> COMMANDS = new HashMap<>();

  @Override
  public boolean execute(Player player, Stream stream) {
    var name = getString(InStreamKey.STRING_INPUT);
    player.clearIdleTime();
    RequestManager.addUserPacketLog(player, "[Command] name=" + name);
    if (name.equals("commands")) {
      var examples = new ArrayList<String>();
      for (var entry : COMMANDS.entrySet()) {
        if (!canUse(entry.getValue(), player)) {
          continue;
        }
        examples.add(getExample(entry.getKey(), entry.getValue()));
      }
      Scroll.open(player, "Commands", examples);
      return true;
    }
    var message = "";
    if (name.contains(" ")) {
      var indexOfSpace = name.indexOf(" ");
      message = name.substring(indexOfSpace + 1);
      name = name.substring(0, indexOfSpace);
    }
    name = name.toLowerCase();
    var command = COMMANDS.get(name);
    if (command == null) {
      player.getGameEncoder().sendMessage("Command not found.");
      return false;
    }
    if (!canUse(command, player)) {
      player.getGameEncoder().sendMessage("You don't have permission to use this command.");
      return false;
    }
    try {
      command.execute(player, name, message);
      player.log(PlayerLogType.COMMAND, "::" + name + " " + message);
    } catch (Exception e) {
      player.getGameEncoder().sendMessage(getExample(name, command));
      if (Settings.getInstance().isLocal()) {
        e.printStackTrace();
      }
    }
    return true;
  }

  public static boolean canUse(CommandHandler command, Player player) {
    if (!command.canUse(player)) {
      return false;
    }
    if (Settings.getInstance().isLocal()) {
      return true;
    }
    if (command instanceof CommandHandler.Teleport) {
      if (!Main.isOwnerIp(player.getIP()) && !player.getController().canTeleport(true)) {
        return false;
      }
    }
    if (command instanceof CommandHandler.OverseerRank) {
      if (!player.isUsergroup(UserRank.OVERSEER) && !player.isUsergroup(UserRank.FORUM_MODERATOR)
          && !player.isUsergroup(UserRank.MODERATOR)
          && !player.isUsergroup(UserRank.SENIOR_MODERATOR)
          && !player.isUsergroup(UserRank.ADVERTISEMENT_MANAGER)
          && !player.isUsergroup(UserRank.COMMUNITY_MANAGER)
          && !player.isUsergroup(UserRank.ADMINISTRATOR)) {
        return false;
      }
    }
    if (command instanceof CommandHandler.ModeratorRank) {
      if (!player.isUsergroup(UserRank.MODERATOR) && !player.isUsergroup(UserRank.SENIOR_MODERATOR)
          && !player.isUsergroup(UserRank.ADVERTISEMENT_MANAGER)
          && !player.isUsergroup(UserRank.COMMUNITY_MANAGER)
          && !player.isUsergroup(UserRank.ADMINISTRATOR)) {
        return false;
      }
    }
    if (command instanceof CommandHandler.SeniorModeratorRank) {
      if (!player.isUsergroup(UserRank.SENIOR_MODERATOR)
          && !player.isUsergroup(UserRank.ADVERTISEMENT_MANAGER)
          && !player.isUsergroup(UserRank.COMMUNITY_MANAGER)
          && !player.isUsergroup(UserRank.ADMINISTRATOR)) {
        return false;
      }
    }
    if (command instanceof CommandHandler.AdministratorRank) {
      if (!player.isUsergroup(UserRank.ADVERTISEMENT_MANAGER)
          && !player.isUsergroup(UserRank.COMMUNITY_MANAGER)
          && !player.isUsergroup(UserRank.ADMINISTRATOR)) {
        return false;
      }
      if (!Main.adminPrivledges(player)) {
        return false;
      }
    }
    if (command instanceof CommandHandler.OwnerRank) {
      if (!Main.isOwnerIp(player.getIP())) {
        return false;
      }
    }
    if (command instanceof CommandHandler.BetaWorld) {
      if (!Settings.getInstance().isBeta()
          && (!player.isUsergroup(UserRank.ADMINISTRATOR) || !Main.adminPrivledges(player))) {
        return false;
      }
    }
    return true;
  }

  public String getExample(String name, CommandHandler command) {
    return "::" + name.toLowerCase() + " " + command.getExample(name);
  }

  public static void load(boolean force) {
    if (!force && !COMMANDS.isEmpty()) {
      return;
    }
    try {
      COMMANDS.clear();
      var classes = Readers.getRecursiveScriptClasses(CommandHandler.class, "player.command");
      for (var clazz : classes) {
        var classInstance = Readers.newInstance(clazz);
        var names = CommandHandler.getNames(classInstance);
        for (var name : names) {
          if (COMMANDS.containsKey(name)) {
            throw new RuntimeException(
                clazz.getName() + " - " + name + ": command name already used.");
          }
          COMMANDS.put(name.toLowerCase(), classInstance);
        }
      }
    } catch (Exception e) {
      PLogger.error(e);
    }
  }

  static {
    load(true);
  }
}
