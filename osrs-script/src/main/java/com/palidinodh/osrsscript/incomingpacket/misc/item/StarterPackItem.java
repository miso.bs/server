package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.STARTER_PACK_32288)
class StarterPackItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (!player.hasVoted() && player.getRights() == Player.RIGHTS_NONE) {
      player.getGameEncoder().sendMessage("To open this, you first need to vote.");
      player.getGameEncoder().sendMessage("Make sure to relog after voting!");
      return;
    }
    item.remove();
    if (player.isGameModeNormal()) {
      // player.getInventory().addOrDropItem(ItemId.COMBAT_LAMP_LEVEL_99_32337, 2);
      player.getInventory().addOrDropItem(ItemId.DRAGON_SCIMITAR);
      player.getInventory().addOrDropItem(ItemId.RUNE_FULL_HELM);
      player.getInventory().addOrDropItem(ItemId.RUNE_PLATEBODY);
      player.getInventory().addOrDropItem(ItemId.RUNE_PLATELEGS);
      player.getInventory().addOrDropItem(ItemId.RUNE_KITESHIELD);
      player.getInventory().addOrDropItem(ItemId.RUNE_BOOTS);
      player.getInventory().addOrDropItem(ItemId.RUNE_CROSSBOW);
      player.getInventory().addOrDropItem(ItemId.DIAMOND_BOLTS_E, 100);
      player.getInventory().addOrDropItem(ItemId.BLACK_DHIDE_BODY);
      player.getInventory().addOrDropItem(ItemId.BLACK_DHIDE_CHAPS);
      player.getInventory().addOrDropItem(ItemId.BLACK_DHIDE_VAMB);
      player.getInventory().addOrDropItem(ItemId.BLACK_DHIDE_SHIELD);
      player.getInventory().addOrDropItem(ItemId.MYSTIC_AIR_STAFF);
      player.getInventory().addOrDropItem(ItemId.DEATH_RUNE, 100);
      player.getInventory().addOrDropItem(ItemId.BLOOD_RUNE, 100);
      player.getInventory().addOrDropItem(ItemId.MYSTIC_HAT);
      player.getInventory().addOrDropItem(ItemId.MYSTIC_ROBE_TOP);
      player.getInventory().addOrDropItem(ItemId.MYSTIC_ROBE_BOTTOM);
      player.getInventory().addOrDropItem(ItemId.MYSTIC_GLOVES);
      player.getInventory().addOrDropItem(ItemId.MYSTIC_BOOTS);
    }
    if (player.isGameModeNormal() || player.isGameModeHard()) {
      player.getInventory().addOrDropItem(ItemId.COINS, 400000);
    }
    player.getInventory().addOrDropItem(ItemId.MONKFISH_NOTED, 50);
    player.getInventory().addOrDropItem(ItemId.SUPER_ATTACK_4_NOTED, 10);
    player.getInventory().addOrDropItem(ItemId.SUPER_STRENGTH_4_NOTED, 10);
    player.getInventory().addOrDropItem(ItemId.SUPER_DEFENCE_4_NOTED, 10);
    player.getInventory().addOrDropItem(ItemId.PRAYER_POTION_4_NOTED, 20);
  }
}
