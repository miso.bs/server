package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Herblore;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.HERB_SACK)
class HerbSackItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "fill": {
        for (var i = player.getInventory().size() - 1; i >= 0; i--) {
          var addingId = player.getInventory().getId(i);
          if (!Herblore.isHerb(addingId)) {
            continue;
          }
          var addingAmount = player.getInventory().getAmount(i);
          addingAmount =
              player.getWidgetManager().getHerbSack().canAddAmount(addingId, addingAmount);
          player.getWidgetManager().getHerbSack().addItem(addingId, addingAmount);
          player.getInventory().deleteItem(addingId, addingAmount, i);
        }
        break;
      }
      case "empty": {
        for (var i = player.getWidgetManager().getHerbSack().size() - 1; i >= 0; i--) {
          var addingId = player.getWidgetManager().getHerbSack().getId(i);
          var addingAmount = player.getWidgetManager().getHerbSack().getAmount(i);
          addingAmount = player.getInventory().canAddAmount(addingId, addingAmount);
          player.getInventory().addItem(addingId, addingAmount);
          player.getWidgetManager().getHerbSack().deleteItem(addingId, addingAmount);
        }
        break;
      }
      case "check":
        player.getWidgetManager().getHerbSack().displayItemList();
        break;
    }
  }
}
