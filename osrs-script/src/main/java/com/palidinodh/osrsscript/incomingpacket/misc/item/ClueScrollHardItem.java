package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.CLUE_SCROLL_HARD, ItemId.CLUE_BOTTLE_HARD, ItemId.CLUE_GEODE_HARD,
    ItemId.CLUE_NEST_HARD })
class ClueScrollHardItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var ttLoot = new int[] { ItemId.HOLY_BLESSING, ItemId.UNHOLY_BLESSING, ItemId.PEACEFUL_BLESSING,
        ItemId.HONOURABLE_BLESSING, ItemId.WAR_BLESSING, ItemId.ANCIENT_BLESSING,
        ItemId.WILLOW_COMP_BOW, ItemId.YEW_COMP_BOW, ItemId.MAGIC_COMP_BOW, ItemId.BEAR_FEET,
        ItemId.MOLE_SLIPPERS, ItemId.FROG_SLIPPERS, ItemId.DEMON_FEET, ItemId.SANDWICH_LADY_HAT,
        ItemId.SANDWICH_LADY_TOP, ItemId.SANDWICH_LADY_BOTTOM,
        ItemId.RUNE_SCIMITAR_ORNAMENT_KIT_GUTHIX, ItemId.RUNE_SCIMITAR_ORNAMENT_KIT_SARADOMIN,
        ItemId.RUNE_SCIMITAR_ORNAMENT_KIT_ZAMORAK, ItemId.MONKS_ROBE_TOP_T, ItemId.MONKS_ROBE_T,
        ItemId.AMULET_OF_DEFENCE_T, ItemId.JESTER_CAPE, ItemId.SHOULDER_PARROT,
        ItemId.PURPLE_SWEETS };
    var ttHard = new int[] { ItemId.RUNE_FULL_HELM_T, ItemId.RUNE_PLATEBODY_T,
        ItemId.RUNE_PLATELEGS_T, ItemId.RUNE_PLATESKIRT_T, ItemId.RUNE_KITESHIELD_T,
        ItemId.RUNE_FULL_HELM_G, ItemId.RUNE_PLATEBODY_G, ItemId.RUNE_PLATELEGS_G,
        ItemId.RUNE_PLATESKIRT_G, ItemId.RUNE_KITESHIELD_G, ItemId.GUTHIX_FULL_HELM,
        ItemId.GUTHIX_PLATEBODY, ItemId.GUTHIX_PLATELEGS, ItemId.GUTHIX_PLATESKIRT,
        ItemId.GUTHIX_KITESHIELD, ItemId.SARADOMIN_FULL_HELM, ItemId.SARADOMIN_PLATEBODY,
        ItemId.SARADOMIN_PLATELEGS, ItemId.SARADOMIN_PLATESKIRT, ItemId.SARADOMIN_KITESHIELD,
        ItemId.ZAMORAK_FULL_HELM, ItemId.ZAMORAK_PLATEBODY, ItemId.ZAMORAK_PLATELEGS,
        ItemId.ZAMORAK_PLATESKIRT, ItemId.ZAMORAK_KITESHIELD, ItemId.ANCIENT_FULL_HELM,
        ItemId.ANCIENT_PLATEBODY, ItemId.ANCIENT_PLATELEGS, ItemId.ANCIENT_PLATESKIRT,
        ItemId.ANCIENT_KITESHIELD, ItemId.BANDOS_FULL_HELM, ItemId.BANDOS_PLATEBODY,
        ItemId.BANDOS_PLATELEGS, ItemId.BANDOS_PLATESKIRT, ItemId.BANDOS_KITESHIELD,
        ItemId.ARMADYL_FULL_HELM, ItemId.ARMADYL_PLATEBODY, ItemId.ARMADYL_PLATELEGS,
        ItemId.ARMADYL_PLATESKIRT, ItemId.ARMADYL_KITESHIELD, ItemId.RUNE_HELM_H1,
        ItemId.RUNE_HELM_H2, ItemId.RUNE_HELM_H3, ItemId.RUNE_HELM_H4, ItemId.RUNE_HELM_H5,
        ItemId.RUNE_PLATEBODY_H1, ItemId.RUNE_PLATEBODY_H2, ItemId.RUNE_PLATEBODY_H3,
        ItemId.RUNE_PLATEBODY_H4, ItemId.RUNE_PLATEBODY_H5, ItemId.BLUE_DHIDE_BODY_T,
        ItemId.BLUE_DHIDE_CHAPS_T, ItemId.BLUE_DHIDE_BODY_G, ItemId.BLUE_DHIDE_CHAPS_G,
        ItemId.RED_DHIDE_BODY_T, ItemId.RED_DHIDE_CHAPS_T, ItemId.RED_DHIDE_BODY_G,
        ItemId.RED_DHIDE_CHAPS_G, ItemId.ENCHANTED_HAT, ItemId.ENCHANTED_TOP, ItemId.ENCHANTED_ROBE,
        ItemId.ROBIN_HOOD_HAT, ItemId.TAN_CAVALIER, ItemId.DARK_CAVALIER, ItemId.BLACK_CAVALIER,
        ItemId.WHITE_CAVALIER, ItemId.RED_CAVALIER, ItemId.NAVY_CAVALIER, ItemId.PIRATES_HAT,
        ItemId.AMULET_OF_GLORY_T4, ItemId.GUTHIX_COIF, ItemId.GUTHIX_DHIDE, ItemId.GUTHIX_CHAPS,
        ItemId.GUTHIX_BRACERS, ItemId.GUTHIX_DHIDE_BOOTS, ItemId.GUTHIX_DHIDE_SHIELD,
        ItemId.SARADOMIN_COIF, ItemId.SARADOMIN_DHIDE, ItemId.SARADOMIN_CHAPS,
        ItemId.SARADOMIN_BRACERS, ItemId.SARADOMIN_DHIDE_BOOTS, ItemId.SARADOMIN_DHIDE_SHIELD,
        ItemId.ZAMORAK_COIF, ItemId.ZAMORAK_DHIDE, ItemId.ZAMORAK_CHAPS, ItemId.ZAMORAK_BRACERS,
        ItemId.ZAMORAK_DHIDE_BOOTS, ItemId.ZAMORAK_DHIDE_SHIELD, ItemId.ARMADYL_COIF,
        ItemId.ARMADYL_DHIDE, ItemId.ARMADYL_CHAPS, ItemId.ARMADYL_BRACERS,
        ItemId.ARMADYL_DHIDE_BOOTS, ItemId.ARMADYL_DHIDE_SHIELD, ItemId.ANCIENT_COIF,
        ItemId.ANCIENT_DHIDE, ItemId.ANCIENT_CHAPS, ItemId.ANCIENT_BRACERS,
        ItemId.ANCIENT_DHIDE_BOOTS, ItemId.ANCIENT_DHIDE_SHIELD, ItemId.BANDOS_COIF,
        ItemId.BANDOS_DHIDE, ItemId.BANDOS_CHAPS, ItemId.BANDOS_BRACERS, ItemId.BANDOS_DHIDE_BOOTS,
        ItemId.BANDOS_DHIDE_SHIELD, ItemId.GUTHIX_STOLE, ItemId.SARADOMIN_STOLE,
        ItemId.ZAMORAK_STOLE, ItemId.GUTHIX_CROZIER, ItemId.SARADOMIN_CROZIER,
        ItemId.ZAMORAK_CROZIER, ItemId.GREEN_DRAGON_MASK, ItemId.BLUE_DRAGON_MASK,
        ItemId.RED_DRAGON_MASK, ItemId.BLACK_DRAGON_MASK, ItemId.PITH_HELMET,
        ItemId.EXPLORER_BACKPACK, ItemId.RUNE_CANE, ItemId.ZOMBIE_HEAD_19912, ItemId.CYCLOPS_HEAD,
        ItemId.NUNCHAKU, ItemId.DUAL_SAI, ItemId.THIEVING_BAG, ItemId.DRAGON_BOOTS_ORNAMENT_KIT,
        ItemId.RUNE_DEFENDER_ORNAMENT_KIT, ItemId.TZHAAR_KET_OM_ORNAMENT_KIT,
        ItemId.BERSERKER_NECKLACE_ORNAMENT_KIT };
    var gildedHard = new int[] { ItemId.GILDED_FULL_HELM, ItemId.GILDED_PLATEBODY,
        ItemId.GILDED_PLATELEGS, ItemId.GILDED_PLATESKIRT, ItemId.GILDED_KITESHIELD,
        ItemId.GILDED_MED_HELM, ItemId.GILDED_CHAINBODY, ItemId.GILDED_SQ_SHIELD,
        ItemId.GILDED_2H_SWORD, ItemId.GILDED_SPEAR, ItemId.GILDED_HASTA };
    var thirdageHard = new int[] { ItemId._3RD_AGE_FULL_HELMET, ItemId._3RD_AGE_PLATEBODY,
        ItemId._3RD_AGE_PLATELEGS, ItemId._3RD_AGE_KITESHIELD, ItemId._3RD_AGE_RANGE_COIF,
        ItemId._3RD_AGE_RANGE_TOP, ItemId._3RD_AGE_RANGE_LEGS, ItemId._3RD_AGE_VAMBRACES,
        ItemId._3RD_AGE_MAGE_HAT, ItemId._3RD_AGE_ROBE_TOP, ItemId._3RD_AGE_ROBE,
        ItemId._3RD_AGE_AMULET, ItemId._3RD_AGE_PLATESKIRT };
    item.remove();
    if (PRandom.inRange(0.1)) {
      var thirdAgeId = thirdageHard[PRandom.randomE(thirdageHard.length)];
      player.getInventory().addItem(thirdAgeId, 1);
      player.getWorld().sendItemDropNews(player, thirdAgeId, "a hard clue scroll");
    } else if (PRandom.inRange(0.5)) {
      var gildedId = gildedHard[PRandom.randomE(gildedHard.length)];
      player.getInventory().addItem(gildedId, 1);
      player.getWorld().sendItemDropNews(player, gildedId, "a hard clue scroll");
    } else {
      var hardItemId = ttHard[PRandom.randomE(ttHard.length)];
      player.getInventory().addItem(hardItemId, 1);
    }
    if (PRandom.randomE(5) == 0) {
      var extraItemId = ttLoot[PRandom.randomE(ttLoot.length)];
      if (extraItemId == ItemId.PURPLE_SWEETS) {
        player.getInventory().addOrDropItem(extraItemId, 8 + PRandom.randomI(24));
      } else {
        player.getInventory().addOrDropItem(extraItemId, 1);
      }
    }
    player.getSkills().increaseClueScrollCount(Skills.CLUE_SCROLL_HARD);
  }
}
