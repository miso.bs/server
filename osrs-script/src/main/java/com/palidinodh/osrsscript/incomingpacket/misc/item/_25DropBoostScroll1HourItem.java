package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.combat.DropRateBoost;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PTime;

@ReferenceId(ItemId._25_DROP_BOOST_SCROLL_1_HOUR_32355)
class _25DropBoostScroll1HourItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (player.getCombat().getDropRateBoost() != null) {
      player.getGameEncoder().sendMessage("You already have a drop rate boost active.");
      return;
    }
    item.remove(1);
    player.getCombat().setDropRateBoost(new DropRateBoost(1.25, (int) PTime.hourToTick(1)));
    player.getGameEncoder().sendMessage("A drop rate boost of 25% has been added for 1 hour.");
  }
}
