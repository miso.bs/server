package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.CLUE_SCROLL_MEDIUM, ItemId.CLUE_BOTTLE_MEDIUM, ItemId.CLUE_GEODE_MEDIUM,
    ItemId.CLUE_NEST_MEDIUM })
class ClueScrollMediumItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var ttLoot = new int[] { ItemId.HOLY_BLESSING, ItemId.UNHOLY_BLESSING, ItemId.PEACEFUL_BLESSING,
        ItemId.HONOURABLE_BLESSING, ItemId.WAR_BLESSING, ItemId.ANCIENT_BLESSING,
        ItemId.WILLOW_COMP_BOW, ItemId.YEW_COMP_BOW, ItemId.MAGIC_COMP_BOW, ItemId.BEAR_FEET,
        ItemId.MOLE_SLIPPERS, ItemId.FROG_SLIPPERS, ItemId.DEMON_FEET, ItemId.SANDWICH_LADY_HAT,
        ItemId.SANDWICH_LADY_TOP, ItemId.SANDWICH_LADY_BOTTOM,
        ItemId.RUNE_SCIMITAR_ORNAMENT_KIT_GUTHIX, ItemId.RUNE_SCIMITAR_ORNAMENT_KIT_SARADOMIN,
        ItemId.RUNE_SCIMITAR_ORNAMENT_KIT_ZAMORAK, ItemId.MONKS_ROBE_TOP_T, ItemId.MONKS_ROBE_T,
        ItemId.AMULET_OF_DEFENCE_T, ItemId.JESTER_CAPE, ItemId.SHOULDER_PARROT,
        ItemId.PURPLE_SWEETS };
    var ttMedium = new int[] { ItemId.MITHRIL_FULL_HELM_T, ItemId.MITHRIL_PLATEBODY_T,
        ItemId.MITHRIL_PLATELEGS_T, ItemId.MITHRIL_PLATESKIRT_T, ItemId.MITHRIL_KITESHIELD_T,
        ItemId.MITHRIL_FULL_HELM_G, ItemId.MITHRIL_PLATEBODY_G, ItemId.MITHRIL_PLATELEGS_G,
        ItemId.MITHRIL_PLATESKIRT_G, ItemId.MITHRIL_KITESHIELD_G, ItemId.ADAMANT_FULL_HELM_T,
        ItemId.ADAMANT_PLATEBODY_T, ItemId.ADAMANT_PLATELEGS_T, ItemId.ADAMANT_PLATESKIRT_T,
        ItemId.ADAMANT_KITESHIELD_T, ItemId.ADAMANT_FULL_HELM_G, ItemId.ADAMANT_PLATEBODY_G,
        ItemId.ADAMANT_PLATELEGS_G, ItemId.ADAMANT_PLATESKIRT_G, ItemId.ADAMANT_KITESHIELD_G,
        ItemId.CLIMBING_BOOTS_G, ItemId.SPIKED_MANACLES, ItemId.RANGER_BOOTS, ItemId.HOLY_SANDALS,
        ItemId.WIZARD_BOOTS, ItemId.BLACK_HEADBAND, ItemId.RED_HEADBAND, ItemId.BROWN_HEADBAND,
        ItemId.PINK_HEADBAND, ItemId.GREEN_HEADBAND, ItemId.BLUE_HEADBAND, ItemId.WHITE_HEADBAND,
        ItemId.GOLD_HEADBAND, ItemId.RED_BOATER, ItemId.ORANGE_BOATER, ItemId.GREEN_BOATER,
        ItemId.BLUE_BOATER, ItemId.BLACK_BOATER, ItemId.PINK_BOATER, ItemId.PURPLE_BOATER,
        ItemId.WHITE_BOATER, ItemId.GREEN_DHIDE_BODY_T, ItemId.GREEN_DHIDE_CHAPS_T,
        ItemId.GREEN_DHIDE_BODY_G, ItemId.GREEN_DHIDE_CHAPS_G, ItemId.ADAMANT_HELM_H1,
        ItemId.ADAMANT_HELM_H2, ItemId.ADAMANT_HELM_H3, ItemId.ADAMANT_HELM_H4,
        ItemId.ADAMANT_HELM_H5, ItemId.ADAMANT_PLATEBODY_H1, ItemId.ADAMANT_PLATEBODY_H2,
        ItemId.ADAMANT_PLATEBODY_H3, ItemId.ADAMANT_PLATEBODY_H4, ItemId.ADAMANT_PLATEBODY_H5,
        ItemId.ADAMANT_SHIELD_H1, ItemId.ADAMANT_SHIELD_H2, ItemId.ADAMANT_SHIELD_H3,
        ItemId.ADAMANT_SHIELD_H4, ItemId.ADAMANT_SHIELD_H5, ItemId.BLACK_ELEGANT_SHIRT,
        ItemId.BLACK_ELEGANT_LEGS, ItemId.WHITE_ELEGANT_BLOUSE, ItemId.WHITE_ELEGANT_SKIRT,
        ItemId.PURPLE_ELEGANT_SHIRT, ItemId.PURPLE_ELEGANT_LEGS, ItemId.PURPLE_ELEGANT_BLOUSE,
        ItemId.PURPLE_ELEGANT_SKIRT, ItemId.PINK_ELEGANT_SHIRT, ItemId.PINK_ELEGANT_LEGS,
        ItemId.PINK_ELEGANT_BLOUSE, ItemId.PINK_ELEGANT_SKIRT, ItemId.GOLD_ELEGANT_SHIRT,
        ItemId.GOLD_ELEGANT_LEGS, ItemId.GOLD_ELEGANT_BLOUSE, ItemId.GOLD_ELEGANT_SKIRT,
        ItemId.WOLF_MASK, ItemId.WOLF_CLOAK, ItemId.STRENGTH_AMULET_T, ItemId.ADAMANT_CANE,
        ItemId.GUTHIX_MITRE, ItemId.SARADOMIN_MITRE, ItemId.ZAMORAK_MITRE, ItemId.ANCIENT_MITRE,
        ItemId.BANDOS_MITRE, ItemId.ARMADYL_MITRE, ItemId.GUTHIX_CLOAK, ItemId.SARADOMIN_CLOAK,
        ItemId.ZAMORAK_CLOAK, ItemId.ANCIENT_CLOAK, ItemId.BANDOS_CLOAK, ItemId.ARMADYL_CLOAK,
        ItemId.ANCIENT_STOLE, ItemId.ARMADYL_STOLE, ItemId.BANDOS_STOLE, ItemId.ANCIENT_CROZIER,
        ItemId.ARMADYL_CROZIER, ItemId.BANDOS_CROZIER, ItemId.CAT_MASK, ItemId.PENGUIN_MASK,
        ItemId.GNOMISH_FIRELIGHTER, ItemId.CRIER_HAT, ItemId.CRIER_BELL, ItemId.CRIER_COAT,
        ItemId.LEPRECHAUN_HAT, ItemId.BLACK_LEPRECHAUN_HAT, ItemId.BLACK_UNICORN_MASK,
        ItemId.WHITE_UNICORN_MASK, ItemId.ARCEUUS_BANNER, ItemId.HOSIDIUS_BANNER,
        ItemId.LOVAKENGJ_BANNER, ItemId.PISCARILIUS_BANNER, ItemId.SHAYZIEN_BANNER,
        ItemId.CABBAGE_ROUND_SHIELD, ItemId.CLUELESS_SCROLL };
    item.remove();
    var mediumClueSlot = PRandom.randomE(ttMedium.length);
    var mediumItemId = ttMedium[mediumClueSlot];
    player.getInventory().addItem(mediumItemId, 1);
    if (PRandom.randomE(5) == 0) {
      var extraItemId = ttLoot[PRandom.randomE(ttLoot.length)];
      if (extraItemId == ItemId.PURPLE_SWEETS) {
        player.getInventory().addOrDropItem(extraItemId, 8 + PRandom.randomI(24));
      } else {
        player.getInventory().addOrDropItem(extraItemId, 1);
      }
    }
    player.getSkills().increaseClueScrollCount(Skills.CLUE_SCROLL_MEDIUM);
  }
}
