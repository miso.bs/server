package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.GRACEFUL_OUTFIT_AGILITY_32298)
class GracefulOutfitAgilityItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (player.getInventory().getRemainingSlots() < 6 - 1) {
      player.getInventory().notEnoughSpace();
      return;
    }
    item.remove();
    player.getInventory().addItem(ItemId.GRACEFUL_HOOD);
    player.getInventory().addItem(ItemId.GRACEFUL_TOP);
    player.getInventory().addItem(ItemId.GRACEFUL_LEGS);
    player.getInventory().addItem(ItemId.GRACEFUL_GLOVES);
    player.getInventory().addItem(ItemId.GRACEFUL_BOOTS);
    player.getInventory().addItem(ItemId.GRACEFUL_CAPE);
  }
}
