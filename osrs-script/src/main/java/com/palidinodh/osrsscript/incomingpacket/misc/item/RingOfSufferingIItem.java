package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.RING_OF_SUFFERING_I, ItemId.SEERS_RING_I, ItemId.ARCHERS_RING_I,
    ItemId.WARRIOR_RING_I, ItemId.BERSERKER_RING_I, ItemId.TYRANNICAL_RING_I,
    ItemId.TREASONOUS_RING_I, ItemId.RING_OF_THE_GODS_I })
class RingOfSufferingIItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (item.getInfoDef().getExchangeIds() == null) {
      return;
    }
    if (player.getInventory().getRemainingSlots() < item.getInfoDef().getExchangeIds().length - 1) {
      player.getInventory().notEnoughSpace();
      return;
    }
    player.openDialogue(new OptionsDialogue("Are you sure you want to unimbue this item?",
        new DialogueOption("Yes, unimbue this item and lose the vote tickets.", (c, s) -> {
          item.remove();
          for (var exchangeId : item.getInfoDef().getExchangeIds()) {
            player.getInventory().addItem(exchangeId);
          }
        }), new DialogueOption("Cancel.")));
  }
}
