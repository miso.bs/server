package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.CLUE_SCROLL_ELITE, ItemId.CLUE_BOTTLE_ELITE, ItemId.CLUE_GEODE_ELITE,
    ItemId.CLUE_NEST_ELITE })
class ClueScrollEliteItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var ttLoot = new int[] { ItemId.HOLY_BLESSING, ItemId.UNHOLY_BLESSING, ItemId.PEACEFUL_BLESSING,
        ItemId.HONOURABLE_BLESSING, ItemId.WAR_BLESSING, ItemId.ANCIENT_BLESSING,
        ItemId.WILLOW_COMP_BOW, ItemId.YEW_COMP_BOW, ItemId.MAGIC_COMP_BOW, ItemId.BEAR_FEET,
        ItemId.MOLE_SLIPPERS, ItemId.FROG_SLIPPERS, ItemId.DEMON_FEET, ItemId.SANDWICH_LADY_HAT,
        ItemId.SANDWICH_LADY_TOP, ItemId.SANDWICH_LADY_BOTTOM,
        ItemId.RUNE_SCIMITAR_ORNAMENT_KIT_GUTHIX, ItemId.RUNE_SCIMITAR_ORNAMENT_KIT_SARADOMIN,
        ItemId.RUNE_SCIMITAR_ORNAMENT_KIT_ZAMORAK, ItemId.MONKS_ROBE_TOP_T, ItemId.MONKS_ROBE_T,
        ItemId.AMULET_OF_DEFENCE_T, ItemId.JESTER_CAPE, ItemId.SHOULDER_PARROT,
        ItemId.PURPLE_SWEETS };
    var ttElite = new int[] { ItemId.DRAGON_FULL_HELM_ORNAMENT_KIT,
        ItemId.DRAGON_CHAINBODY_ORNAMENT_KIT, ItemId.DRAGON_LEGS_SKIRT_ORNAMENT_KIT,
        ItemId.DRAGON_SQ_SHIELD_ORNAMENT_KIT, ItemId.DRAGON_SCIMITAR_ORNAMENT_KIT,
        ItemId.LIGHT_INFINITY_COLOUR_KIT, ItemId.DARK_INFINITY_COLOUR_KIT, ItemId.FURY_ORNAMENT_KIT,
        ItemId.MUSKETEER_HAT, ItemId.MUSKETEER_TABARD, ItemId.MUSKETEER_PANTS, ItemId.DRAGON_CANE,
        ItemId.BRIEFCASE, ItemId.SAGACIOUS_SPECTACLES, ItemId.TOP_HAT, ItemId.MONOCLE,
        ItemId.BIG_PIRATE_HAT, ItemId.DEERSTALKER, ItemId.BRONZE_DRAGON_MASK,
        ItemId.IRON_DRAGON_MASK, ItemId.STEEL_DRAGON_MASK, ItemId.MITHRIL_DRAGON_MASK,
        ItemId.ADAMANT_DRAGON_MASK, ItemId.RUNE_DRAGON_MASK, ItemId.LAVA_DRAGON_MASK,
        ItemId.BLACK_DHIDE_BODY_T, ItemId.BLACK_DHIDE_CHAPS_T, ItemId.BLACK_DHIDE_BODY_G,
        ItemId.BLACK_DHIDE_CHAPS_G, ItemId.RANGERS_TUNIC, ItemId.AFRO, ItemId.KATANA,
        ItemId.ROYAL_CROWN, ItemId.ROYAL_GOWN_TOP, ItemId.ROYAL_GOWN_BOTTOM, ItemId.ROYAL_SCEPTRE,
        ItemId.ARCEUUS_SCARF, ItemId.HOSIDIUS_SCARF, ItemId.LOVAKENGJ_SCARF,
        ItemId.PISCARILIUS_SCARF, ItemId.SHAYZIEN_SCARF, ItemId.BLACKSMITHS_HELM,
        ItemId.BUCKET_HELM, ItemId.RANGER_GLOVES, ItemId.HOLY_WRAPS, ItemId.RING_OF_NATURE,
        ItemId.DARK_BOW_TIE, ItemId.DARK_TUXEDO_JACKET, ItemId.DARK_TROUSERS,
        ItemId.DARK_TUXEDO_CUFFS, ItemId.DARK_TUXEDO_SHOES, ItemId.LIGHT_BOW_TIE,
        ItemId.LIGHT_TUXEDO_JACKET, ItemId.LIGHT_TROUSERS, ItemId.LIGHT_TUXEDO_CUFFS,
        ItemId.LIGHT_TUXEDO_SHOES, ItemId.RANGERS_TIGHTS, ItemId.URIS_HAT, ItemId.FREMENNIK_KILT,
        ItemId.HEAVY_CASKET, ItemId.GIANT_BOOT };
    var gildedElite = new int[] { ItemId.GILDED_BOOTS, ItemId.GILDED_SCIMITAR,
        ItemId.GILDED_DHIDE_VAMBS, ItemId.GILDED_DHIDE_BODY, ItemId.GILDED_DHIDE_CHAPS,
        ItemId.GILDED_COIF, ItemId.GILDED_AXE, ItemId.GILDED_PICKAXE, ItemId.GILDED_SPADE };
    var thirdageElite = new int[] { ItemId._3RD_AGE_FULL_HELMET, ItemId._3RD_AGE_PLATEBODY,
        ItemId._3RD_AGE_PLATELEGS, ItemId._3RD_AGE_KITESHIELD, ItemId._3RD_AGE_RANGE_COIF,
        ItemId._3RD_AGE_RANGE_TOP, ItemId._3RD_AGE_RANGE_LEGS, ItemId._3RD_AGE_VAMBRACES,
        ItemId._3RD_AGE_MAGE_HAT, ItemId._3RD_AGE_ROBE_TOP, ItemId._3RD_AGE_ROBE,
        ItemId._3RD_AGE_AMULET, ItemId._3RD_AGE_CLOAK, ItemId._3RD_AGE_WAND, ItemId._3RD_AGE_BOW,
        ItemId._3RD_AGE_LONGSWORD, ItemId.RING_OF_3RD_AGE, ItemId._3RD_AGE_PLATESKIRT };
    item.remove();
    if (PRandom.inRange(0.1)) {
      var thirdAgeId = thirdageElite[PRandom.randomE(thirdageElite.length)];
      player.getInventory().addItem(thirdAgeId, 1);
      player.getWorld().sendItemDropNews(player, thirdAgeId, "an elite clue scroll");
    } else if (PRandom.inRange(0.5)) {
      var gildedId = gildedElite[PRandom.randomE(gildedElite.length)];
      player.getInventory().addItem(gildedId, 1);
      player.getWorld().sendItemDropNews(player, gildedId, "an elite clue scroll");
    } else {
      var eliteItemId = ttElite[PRandom.randomE(ttElite.length)];
      player.getInventory().addItem(eliteItemId, 1);
    }
    if (PRandom.randomE(5) == 0) {
      var extraItemId = ttLoot[PRandom.randomE(ttLoot.length)];
      if (extraItemId == ItemId.PURPLE_SWEETS) {
        player.getInventory().addOrDropItem(extraItemId, 8 + PRandom.randomI(24));
      } else {
        player.getInventory().addOrDropItem(extraItemId, 1);
      }
    }
    player.getSkills().increaseClueScrollCount(Skills.CLUE_SCROLL_ELITE);
  }
}
