package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.RANDOM_PVP_WEAPON_32290)
class RandomPvpWeaponItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var items = new int[] { ItemId.VESTAS_LONGSWORD, ItemId.STATIUSS_WARHAMMER, ItemId.VESTAS_SPEAR,
        ItemId.MORRIGANS_JAVELIN, ItemId.MORRIGANS_THROWING_AXE, ItemId.ZURIELS_STAFF };
    item.remove();
    var anItem = new Item(items[PRandom.randomE(items.length)], 1);
    if (anItem.getId() == ItemId.MORRIGANS_JAVELIN
        || anItem.getId() == ItemId.MORRIGANS_THROWING_AXE) {
      anItem.setAmount(100);
    }
    player.getInventory().addOrDropItem(anItem);
    player.log(PlayerLogType.LOOT_BOX, anItem.getLogName() + " from " + item.getLogName());
  }
}
