package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.DWARVEN_ROCK_CAKE_7510)
class DwarvenRockCakeItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    player.putAttribute("guzzle_dwarven_rock_cake", true);
    player.getWidgetManager().removeInteractiveWidgets();
    player.getSkills().eatFood(item.getSlot());
  }
}
