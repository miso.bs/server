package com.palidinodh.osrsscript.incomingpacket.misc.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.ItemCharges;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.RING_OF_RECOIL)
class RingOfRecoilItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "check":
        player.getGameEncoder()
            .sendMessage("You can inflict " + player.getCharges().getRingOfRecoil()
                + " more points of damage before your ring will shatter.");
        break;
      case "break":
        player.getCharges().setRingOfRecoil(ItemCharges.RING_OF_RECOIL);
        item.remove();
        player.getGameEncoder().sendMessage("Your Ring of Recoil has shattered.");
        break;
    }
  }
}
