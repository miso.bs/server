package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.ABYSSAL_WHIP, ItemId.VOLCANIC_ABYSSAL_WHIP, ItemId.FROZEN_ABYSSAL_WHIP,
    ItemId.ABYSSAL_WHIP_20405, ItemId.BLIGHTED_ABYSSAL_WHIP_32361 })
class AbyssalWhipSpecialAttack extends SpecialAttack {
  public AbyssalWhipSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(50);
    entry.setAnimation(1658);
    entry.setTargetGraphic(new Graphic(341, 100));
    entry.setAccuracyModifier(1.25);
    addEntry(entry);
  }
}
