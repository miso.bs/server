package com.palidinodh.osrsscript.player.skill.crafting;

import java.util.List;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.player.AchievementDiary;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.item.ItemDef;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.util.PNumber;
import lombok.var;

public class Crafting extends SkillContainer {
  private static final int LEATHER_COST = 100;
  private static final int HARD_LEATHER_COST = 300;
  private static final int SNAKESKIN_LEATHER_COST = 1500;
  private static final int GREEN_DRAGON_LEATHER_COST = 2000;
  private static final int BLUE_DRAGON_LEATHER_COST = 2000;
  private static final int RED_DRAGON_LEATHER_COST = 2000;
  private static final int BLACK_DRAGON_LEATHER_COST = 2000;

  @Override
  public int getSkillId() {
    return Skills.CRAFTING;
  }

  public List<SkillEntry> getEntries() {
    return CraftingEntries.getEntries();
  }

  @Override
  public boolean widgetHook(Player player, int option, int widgetId, int childId, int slot,
      int itemId) {
    if (widgetId == WidgetId.TANNING) {
      var amount = 1;
      if (childId >= 124 && childId <= 130) {
        amount = -1;
      } else if (childId >= 132 && childId <= 138) {
        amount = -2;
      } else if (childId >= 140 && childId <= 146) {
        amount = 5;
      }
      int makeItemId;
      if (childId == 124 || childId == 132 || childId == 140 || childId == 148) {
        makeItemId = ItemId.LEATHER;
      } else if (childId == 125 || childId == 133 || childId == 141 || childId == 149) {
        makeItemId = ItemId.HARD_LEATHER;
      } else if (childId == 126 || childId == 134 || childId == 142 || childId == 150) {
        makeItemId = ItemId.GREEN_DRAGON_LEATHER;
      } else if (childId == 127 || childId == 135 || childId == 143 || childId == 151) {
        makeItemId = ItemId.BLUE_DRAGON_LEATHER;
      } else if (childId == 128 || childId == 136 || childId == 144 || childId == 152) {
        makeItemId = ItemId.RED_DRAGON_LEATHER;
      } else if (childId == 129 || childId == 137 || childId == 145 || childId == 153) {
        makeItemId = ItemId.BLACK_DRAGON_LEATHER;
      } else if (childId == 130 || childId == 138 || childId == 146 || childId == 154) {
        makeItemId = ItemId.SNAKESKIN;
      } else {
        makeItemId = -1;
      }
      if (makeItemId == -1) {
        return true;
      }
      if (amount == -2) {
        player.getGameEncoder().sendEnterAmount(value -> {
          tanHide(player, makeItemId, value);
        });
      } else {
        tanHide(player, makeItemId, amount);
      }
      return true;
    } else if (widgetId == WidgetId.MAKE_JEWELRY) {
      if (itemId == ItemId.GOLD_BRACELET_11068) {
        itemId = ItemId.GOLD_BRACELET;
      } else if (itemId == ItemId.SAPPHIRE_BRACELET_11071) {
        itemId = ItemId.SAPPHIRE_BRACELET;
      } else if (itemId == ItemId.EMERALD_BRACELET_11078) {
        itemId = ItemId.EMERALD_BRACELET;
      } else if (itemId == ItemId.RUBY_BRACELET_11087) {
        itemId = ItemId.RUBY_BRACELET;
      } else if (itemId == ItemId.DIAMOND_BRACELET_11094) {
        itemId = ItemId.DIAMOND_BRACELET;
      } else if (itemId == ItemId.DRAGON_BRACELET) {
        itemId = ItemId.DRAGONSTONE_BRACELET;
      } else if (itemId == ItemId.ONYX_BRACELET_11132) {
        itemId = ItemId.ONYX_BRACELET;
      } else if (itemId == ItemId.ZENYTE_BRACELET_19492) {
        itemId = ItemId.ZENYTE_BRACELET;
      }
      var entry = findEntryFromCreate(itemId);
      if (entry == null) {
        return true;
      }
      if (option == 0) {
        startEvent(player, entry, 1);
      } else if (option == 1) {
        startEvent(player, entry, 5);
      } else if (option == 2) {
        startEvent(player, entry, 10);
      } else if (option == 3) {
        player.getGameEncoder().sendEnterAmount(value -> {
          startEvent(player, entry, value);
        });
      } else if (option == 4) {
        startEvent(player, entry, 28);
      }
      return true;
    }
    return false;

  }

  @Override
  public boolean widgetOnMapObjectHook(Player player, int widgetId, int childId, int slot,
      int itemId, MapObject mapObject) {
    if (widgetId == WidgetId.INVENTORY && mapObject.getDef().hasOption("smelt")) {
      if (itemId == ItemId.RING_MOULD || itemId == ItemId.NECKLACE_MOULD
          || itemId == ItemId.BRACELET_MOULD || itemId == ItemId.AMULET_MOULD
          || itemId == ItemId.GOLD_BAR || itemId == ItemId.SAPPHIRE || itemId == ItemId.EMERALD
          || itemId == ItemId.RUBY || itemId == ItemId.DIAMOND || itemId == ItemId.DRAGONSTONE
          || itemId == ItemId.ONYX || itemId == ItemId.ZENYTE) {
        player.getWidgetManager().sendInteractiveOverlay(WidgetId.MAKE_JEWELRY);
        return true;
      }
    }
    return false;
  }

  public void openTanning(Player player) {
    player.getWidgetManager().sendInteractiveOverlay(WidgetId.TANNING);
    player.getGameEncoder().sendWidgetItemModel(WidgetId.TANNING, 100, ItemId.LEATHER, 200);
    player.getGameEncoder().sendWidgetItemModel(WidgetId.TANNING, 101, ItemId.HARD_LEATHER, 200);
    player.getGameEncoder().sendWidgetItemModel(WidgetId.TANNING, 102, ItemId.GREEN_DRAGON_LEATHER,
        200);
    player.getGameEncoder().sendWidgetItemModel(WidgetId.TANNING, 103, ItemId.BLUE_DRAGON_LEATHER,
        200);
    player.getGameEncoder().sendWidgetItemModel(WidgetId.TANNING, 104, ItemId.RED_DRAGON_LEATHER,
        200);
    player.getGameEncoder().sendWidgetItemModel(WidgetId.TANNING, 105, ItemId.BLACK_DRAGON_LEATHER,
        200);
    player.getGameEncoder().sendWidgetItemModel(WidgetId.TANNING, 106, ItemId.SNAKESKIN, 200);
    player.getGameEncoder().sendWidgetText(WidgetId.TANNING, 108, "Leather");
    player.getGameEncoder().sendWidgetText(WidgetId.TANNING, 116,
        PNumber.formatNumber(LEATHER_COST) + " Coins");
    player.getGameEncoder().sendWidgetText(WidgetId.TANNING, 109, "Hard Leather");
    player.getGameEncoder().sendWidgetText(WidgetId.TANNING, 117,
        PNumber.formatNumber(HARD_LEATHER_COST) + " Coins");
    player.getGameEncoder().sendWidgetText(WidgetId.TANNING, 110, "Green Leather");
    player.getGameEncoder().sendWidgetText(WidgetId.TANNING, 118,
        PNumber.formatNumber(GREEN_DRAGON_LEATHER_COST) + " Coins");
    player.getGameEncoder().sendWidgetText(WidgetId.TANNING, 111, "Blue Leather");
    player.getGameEncoder().sendWidgetText(WidgetId.TANNING, 119,
        PNumber.formatNumber(BLUE_DRAGON_LEATHER_COST) + " Coins");
    player.getGameEncoder().sendWidgetText(WidgetId.TANNING, 112, "Red Leather");
    player.getGameEncoder().sendWidgetText(WidgetId.TANNING, 120,
        PNumber.formatNumber(RED_DRAGON_LEATHER_COST) + " Coins");
    player.getGameEncoder().sendWidgetText(WidgetId.TANNING, 113, "Black Leather");
    player.getGameEncoder().sendWidgetText(WidgetId.TANNING, 121,
        PNumber.formatNumber(BLACK_DRAGON_LEATHER_COST) + " Coins");
    player.getGameEncoder().sendWidgetText(WidgetId.TANNING, 114, "Snakeskin");
    player.getGameEncoder().sendWidgetText(WidgetId.TANNING, 122,
        PNumber.formatNumber(SNAKESKIN_LEATHER_COST) + " Coins");
  }

  private void tanHide(Player player, int craftItemId, int amount) {
    var baseItemId = -1;
    var cost = -1;
    if (craftItemId == ItemId.LEATHER) {
      baseItemId = ItemId.COWHIDE;
      cost = LEATHER_COST;
    } else if (craftItemId == ItemId.HARD_LEATHER) {
      baseItemId = ItemId.COWHIDE;
      cost = HARD_LEATHER_COST;
    } else if (craftItemId == ItemId.GREEN_DRAGON_LEATHER) {
      baseItemId = ItemId.GREEN_DRAGONHIDE;
      cost = GREEN_DRAGON_LEATHER_COST;
    } else if (craftItemId == ItemId.BLUE_DRAGON_LEATHER) {
      baseItemId = ItemId.BLUE_DRAGONHIDE;
      cost = BLUE_DRAGON_LEATHER_COST;
    } else if (craftItemId == ItemId.RED_DRAGON_LEATHER) {
      baseItemId = ItemId.RED_DRAGONHIDE;
      cost = RED_DRAGON_LEATHER_COST;
    } else if (craftItemId == ItemId.BLACK_DRAGON_LEATHER) {
      baseItemId = ItemId.BLACK_DRAGONHIDE;
      cost = BLACK_DRAGON_LEATHER_COST;
    } else if (craftItemId == ItemId.SNAKESKIN) {
      baseItemId = ItemId.SNAKE_HIDE;
      cost = SNAKESKIN_LEATHER_COST;
    }
    if (baseItemId == -1 || cost == -1) {
      return;
    }
    if (player.getInventory().getCount(baseItemId) == 0) {
      baseItemId = ItemDef.getNotedId(baseItemId);
      craftItemId = ItemDef.getNotedId(craftItemId);
    }
    if (amount == -1) {
      amount = player.getInventory().getCount(baseItemId);
    }
    if (amount > player.getInventory().getCount(baseItemId)) {
      amount = player.getInventory().getCount(baseItemId);
    }
    if (baseItemId == -1 || craftItemId == -1 || amount == 0) {
      player.getGameEncoder().sendMessage("You don't have raw hide to do this.");
      return;
    }
    if (player.getInventory().getCount(ItemId.COINS) < amount * cost) {
      player.getGameEncoder().sendMessage("You don't have enough coins to do this.");
      return;
    }
    player.getInventory().deleteItem(ItemId.COINS, amount * cost);
    player.getInventory().deleteItem(baseItemId, amount);
    player.getInventory().addItem(craftItemId, amount);
    AchievementDiary.makeItemUpdate(player, getSkillId(), new RandomItem(craftItemId, amount), null,
        null);
  }
}
