package com.palidinodh.osrsscript.player.skill.crafting;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.ObjectId;
import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import lombok.Getter;
import lombok.var;

class CraftingEntries {
  @Getter
  private static List<SkillEntry> entries = load();

  private static List<SkillEntry> load() {
    var entries = new ArrayList<SkillEntry>();

    var entry = SkillEntry.builder();
    entry.level(41).experience(70).animation(7386).tool(new Item(ItemId.HAMMER))
        .consume(new RandomItem(ItemId.OAK_SHIELD)).consume(new RandomItem(ItemId.HARD_LEATHER, 2))
        .consume(new RandomItem(ItemId.BRONZE_NAILS, 15))
        .create(new RandomItem(ItemId.HARD_LEATHER_SHIELD));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(56).experience(100).animation(7387).tool(new Item(ItemId.HAMMER))
        .consume(new RandomItem(ItemId.WILLOW_SHIELD)).consume(new RandomItem(ItemId.SNAKESKIN, 2))
        .consume(new RandomItem(ItemId.IRON_NAILS, 15))
        .create(new RandomItem(ItemId.SNAKESKIN_SHIELD));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(62).experience(124).animation(7831).tool(new Item(ItemId.HAMMER))
        .consume(new RandomItem(ItemId.MAPLE_SHIELD))
        .consume(new RandomItem(ItemId.GREEN_DRAGON_LEATHER, 2))
        .consume(new RandomItem(ItemId.STEEL_NAILS, 15))
        .create(new RandomItem(ItemId.GREEN_DHIDE_SHIELD));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(69).experience(140).animation(7832).tool(new Item(ItemId.HAMMER))
        .consume(new RandomItem(ItemId.YEW_SHIELD))
        .consume(new RandomItem(ItemId.BLUE_DRAGON_LEATHER, 2))
        .consume(new RandomItem(ItemId.MITHRIL_NAILS, 15))
        .create(new RandomItem(ItemId.BLUE_DHIDE_SHIELD));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(76).experience(156).animation(7833).tool(new Item(ItemId.HAMMER))
        .consume(new RandomItem(ItemId.MAGIC_SHIELD))
        .consume(new RandomItem(ItemId.RED_DRAGON_LEATHER, 2))
        .consume(new RandomItem(ItemId.ADAMANTITE_NAILS, 15))
        .create(new RandomItem(ItemId.RED_DHIDE_SHIELD));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(83).experience(172).animation(7834).tool(new Item(ItemId.HAMMER))
        .consume(new RandomItem(ItemId.REDWOOD_SHIELD))
        .consume(new RandomItem(ItemId.BLACK_DRAGON_LEATHER, 2))
        .consume(new RandomItem(ItemId.RUNE_NAILS, 15))
        .create(new RandomItem(ItemId.BLACK_DHIDE_SHIELD));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(1).experience(14).animation(1249).tool(new Item(ItemId.NEEDLE))
        .consume(new RandomItem(ItemId.LEATHER)).consume(new RandomItem(ItemId.THREAD))
        .create(new RandomItem(ItemId.LEATHER_GLOVES));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(7).experience(16).animation(1249).tool(new Item(ItemId.NEEDLE))
        .consume(new RandomItem(ItemId.LEATHER)).consume(new RandomItem(ItemId.THREAD))
        .create(new RandomItem(ItemId.LEATHER_BOOTS));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(9).experience(19).animation(1249).tool(new Item(ItemId.NEEDLE))
        .consume(new RandomItem(ItemId.LEATHER)).consume(new RandomItem(ItemId.THREAD))
        .create(new RandomItem(ItemId.LEATHER_COWL));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(11).experience(22).animation(1249).tool(new Item(ItemId.NEEDLE))
        .consume(new RandomItem(ItemId.LEATHER)).consume(new RandomItem(ItemId.THREAD))
        .create(new RandomItem(ItemId.LEATHER_VAMBRACES));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(14).experience(25).animation(1249).tool(new Item(ItemId.NEEDLE))
        .consume(new RandomItem(ItemId.LEATHER)).consume(new RandomItem(ItemId.THREAD))
        .create(new RandomItem(ItemId.LEATHER_BODY));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(18).experience(27).animation(1249).tool(new Item(ItemId.NEEDLE))
        .consume(new RandomItem(ItemId.LEATHER)).consume(new RandomItem(ItemId.THREAD))
        .create(new RandomItem(ItemId.LEATHER_CHAPS));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(38).experience(37).animation(1249).tool(new Item(ItemId.NEEDLE))
        .consume(new RandomItem(ItemId.LEATHER)).consume(new RandomItem(ItemId.THREAD))
        .create(new RandomItem(ItemId.COIF));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(28).experience(35).animation(1249).tool(new Item(ItemId.NEEDLE))
        .consume(new RandomItem(ItemId.HARD_LEATHER)).consume(new RandomItem(ItemId.THREAD))
        .create(new RandomItem(ItemId.HARDLEATHER_BODY));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(57).experience(62).animation(1249).tool(new Item(ItemId.NEEDLE))
        .consume(new RandomItem(ItemId.GREEN_DRAGON_LEATHER)).consume(new RandomItem(ItemId.THREAD))
        .create(new RandomItem(ItemId.GREEN_DHIDE_VAMB));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(60).experience(124).animation(1249).tool(new Item(ItemId.NEEDLE))
        .consume(new RandomItem(ItemId.GREEN_DRAGON_LEATHER, 2))
        .consume(new RandomItem(ItemId.THREAD)).create(new RandomItem(ItemId.GREEN_DHIDE_CHAPS));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(63).experience(186).animation(1249).tool(new Item(ItemId.NEEDLE))
        .consume(new RandomItem(ItemId.GREEN_DRAGON_LEATHER, 3))
        .consume(new RandomItem(ItemId.THREAD)).create(new RandomItem(ItemId.GREEN_DHIDE_BODY));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(66).experience(70).animation(1249).tool(new Item(ItemId.NEEDLE))
        .consume(new RandomItem(ItemId.BLUE_DRAGON_LEATHER)).consume(new RandomItem(ItemId.THREAD))
        .create(new RandomItem(ItemId.BLUE_DHIDE_VAMB));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(68).experience(140).animation(1249).tool(new Item(ItemId.NEEDLE))
        .consume(new RandomItem(ItemId.BLUE_DRAGON_LEATHER, 2))
        .consume(new RandomItem(ItemId.THREAD)).create(new RandomItem(ItemId.BLUE_DHIDE_CHAPS));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(71).experience(210).animation(1249).tool(new Item(ItemId.NEEDLE))
        .consume(new RandomItem(ItemId.BLUE_DRAGON_LEATHER, 3))
        .consume(new RandomItem(ItemId.THREAD)).create(new RandomItem(ItemId.BLUE_DHIDE_BODY));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(73).experience(78).animation(1249).tool(new Item(ItemId.NEEDLE))
        .consume(new RandomItem(ItemId.RED_DRAGON_LEATHER)).consume(new RandomItem(ItemId.THREAD))
        .create(new RandomItem(ItemId.RED_DHIDE_VAMB));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(75).experience(156).animation(1249).tool(new Item(ItemId.NEEDLE))
        .consume(new RandomItem(ItemId.RED_DRAGON_LEATHER, 2))
        .consume(new RandomItem(ItemId.THREAD)).create(new RandomItem(ItemId.RED_DHIDE_CHAPS));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(77).experience(234).animation(1249).tool(new Item(ItemId.NEEDLE))
        .consume(new RandomItem(ItemId.RED_DRAGON_LEATHER, 3))
        .consume(new RandomItem(ItemId.THREAD)).create(new RandomItem(ItemId.RED_DHIDE_BODY));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(79).experience(86).animation(1249).tool(new Item(ItemId.NEEDLE))
        .consume(new RandomItem(ItemId.BLACK_DRAGON_LEATHER)).consume(new RandomItem(ItemId.THREAD))
        .create(new RandomItem(ItemId.BLACK_DHIDE_VAMB));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(82).experience(172).animation(1249).tool(new Item(ItemId.NEEDLE))
        .consume(new RandomItem(ItemId.BLACK_DRAGON_LEATHER, 2))
        .consume(new RandomItem(ItemId.THREAD)).create(new RandomItem(ItemId.BLACK_DHIDE_CHAPS));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(84).experience(258).animation(1249).tool(new Item(ItemId.NEEDLE))
        .consume(new RandomItem(ItemId.BLACK_DRAGON_LEATHER, 3))
        .consume(new RandomItem(ItemId.THREAD)).create(new RandomItem(ItemId.BLACK_DHIDE_BODY));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(45).experience(30).animation(1249).tool(new Item(ItemId.NEEDLE))
        .consume(new RandomItem(ItemId.SNAKESKIN, 6)).consume(new RandomItem(ItemId.THREAD))
        .create(new RandomItem(ItemId.SNAKESKIN_BOOTS));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(47).experience(35).animation(1249).tool(new Item(ItemId.NEEDLE))
        .consume(new RandomItem(ItemId.SNAKESKIN, 8)).consume(new RandomItem(ItemId.THREAD))
        .create(new RandomItem(ItemId.SNAKESKIN_VAMBRACES));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(48).experience(45).animation(1249).tool(new Item(ItemId.NEEDLE))
        .consume(new RandomItem(ItemId.SNAKESKIN, 5)).consume(new RandomItem(ItemId.THREAD))
        .create(new RandomItem(ItemId.SNAKESKIN_BANDANA));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(51).experience(50).animation(1249).tool(new Item(ItemId.NEEDLE))
        .consume(new RandomItem(ItemId.SNAKESKIN, 12)).consume(new RandomItem(ItemId.THREAD))
        .create(new RandomItem(ItemId.SNAKESKIN_CHAPS));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(53).experience(55).animation(1249).tool(new Item(ItemId.NEEDLE))
        .consume(new RandomItem(ItemId.SNAKESKIN, 15)).consume(new RandomItem(ItemId.THREAD))
        .create(new RandomItem(ItemId.SNAKESKIN_BODY));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(1).experience(15).animation(886).tool(new Item(ItemId.CHISEL))
        .consume(new RandomItem(ItemId.UNCUT_OPAL)).create(new RandomItem(ItemId.OPAL));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(13).experience(20).animation(889).tool(new Item(ItemId.CHISEL))
        .consume(new RandomItem(ItemId.UNCUT_JADE)).create(new RandomItem(ItemId.JADE));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(16).experience(25).animation(887).tool(new Item(ItemId.CHISEL))
        .consume(new RandomItem(ItemId.UNCUT_RED_TOPAZ)).create(new RandomItem(ItemId.RED_TOPAZ));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(20).experience(50).animation(888).tool(new Item(ItemId.CHISEL))
        .consume(new RandomItem(ItemId.UNCUT_SAPPHIRE)).create(new RandomItem(ItemId.SAPPHIRE));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(27).experience(68).animation(889).tool(new Item(ItemId.CHISEL))
        .consume(new RandomItem(ItemId.UNCUT_EMERALD)).create(new RandomItem(ItemId.EMERALD));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(34).experience(85).animation(887).tool(new Item(ItemId.CHISEL))
        .consume(new RandomItem(ItemId.UNCUT_RUBY)).create(new RandomItem(ItemId.RUBY));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(43).experience(108).animation(886).tool(new Item(ItemId.CHISEL))
        .consume(new RandomItem(ItemId.UNCUT_DIAMOND)).create(new RandomItem(ItemId.DIAMOND));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(55).experience(138).animation(885).tool(new Item(ItemId.CHISEL))
        .consume(new RandomItem(ItemId.UNCUT_DRAGONSTONE))
        .create(new RandomItem(ItemId.DRAGONSTONE));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(67).experience(168).animation(2717).tool(new Item(ItemId.CHISEL))
        .consume(new RandomItem(ItemId.UNCUT_ONYX)).create(new RandomItem(ItemId.ONYX));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(89).experience(200).animation(2717).tool(new Item(ItemId.CHISEL))
        .consume(new RandomItem(ItemId.UNCUT_ZENYTE)).create(new RandomItem(ItemId.ZENYTE));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(83).experience(60).animation(887).tool(new Item(ItemId.CHISEL))
        .consume(new RandomItem(ItemId.AMETHYST))
        .create(new RandomItem(ItemId.AMETHYST_BOLT_TIPS, 15));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(85).experience(60).animation(887).tool(new Item(ItemId.CHISEL))
        .consume(new RandomItem(ItemId.AMETHYST))
        .create(new RandomItem(ItemId.AMETHYST_ARROWTIPS, 15));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(87).experience(60).animation(887).tool(new Item(ItemId.CHISEL))
        .consume(new RandomItem(ItemId.AMETHYST))
        .create(new RandomItem(ItemId.AMETHYST_JAVELIN_HEADS, 5));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(5).experience(15).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.RING_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .create(new RandomItem(ItemId.GOLD_RING));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(6).experience(20).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.NECKLACE_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .create(new RandomItem(ItemId.GOLD_NECKLACE));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(7).experience(25).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.BRACELET_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .create(new RandomItem(ItemId.GOLD_BRACELET));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(8).experience(30).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.AMULET_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .create(new RandomItem(ItemId.GOLD_AMULET_U));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(20).experience(40).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.RING_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.SAPPHIRE)).create(new RandomItem(ItemId.SAPPHIRE_RING));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(22).experience(55).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.NECKLACE_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.SAPPHIRE)).create(new RandomItem(ItemId.SAPPHIRE_NECKLACE));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(23).experience(60).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.BRACELET_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.SAPPHIRE)).create(new RandomItem(ItemId.SAPPHIRE_BRACELET));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(24).experience(65).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.AMULET_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.SAPPHIRE)).create(new RandomItem(ItemId.SAPPHIRE_AMULET_U));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(27).experience(55).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.RING_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.EMERALD)).create(new RandomItem(ItemId.EMERALD_RING));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(29).experience(60).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.NECKLACE_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.EMERALD)).create(new RandomItem(ItemId.EMERALD_NECKLACE));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(30).experience(65).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.BRACELET_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.EMERALD)).create(new RandomItem(ItemId.EMERALD_BRACELET));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(31).experience(70).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.AMULET_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.EMERALD)).create(new RandomItem(ItemId.EMERALD_AMULET_U));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(34).experience(70).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.RING_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.RUBY)).create(new RandomItem(ItemId.RUBY_RING));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(40).experience(75).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.NECKLACE_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.RUBY)).create(new RandomItem(ItemId.RUBY_NECKLACE));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(42).experience(80).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.BRACELET_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.RUBY)).create(new RandomItem(ItemId.RUBY_BRACELET));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(50).experience(85).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.AMULET_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.RUBY)).create(new RandomItem(ItemId.RUBY_AMULET_U));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(43).experience(85).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.RING_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.DIAMOND)).create(new RandomItem(ItemId.DIAMOND_RING));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(56).experience(90).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.NECKLACE_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.DIAMOND)).create(new RandomItem(ItemId.DIAMOND_NECKLACE));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(58).experience(95).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.BRACELET_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.DIAMOND)).create(new RandomItem(ItemId.DIAMOND_BRACELET));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(70).experience(100).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.AMULET_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.DIAMOND)).create(new RandomItem(ItemId.DIAMOND_AMULET_U));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(55).experience(100).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.RING_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.DRAGONSTONE))
        .create(new RandomItem(ItemId.DRAGONSTONE_RING));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(72).experience(105).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.NECKLACE_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.DRAGONSTONE)).create(new RandomItem(ItemId.DRAGON_NECKLACE));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(74).experience(110).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.BRACELET_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.DRAGONSTONE))
        .create(new RandomItem(ItemId.DRAGONSTONE_BRACELET));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(80).experience(150).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.AMULET_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.DRAGONSTONE))
        .create(new RandomItem(ItemId.DRAGONSTONE_AMULET_U));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(67).experience(115).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.RING_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.ONYX)).create(new RandomItem(ItemId.ONYX_RING));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(82).experience(120).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.NECKLACE_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.ONYX)).create(new RandomItem(ItemId.ONYX_NECKLACE));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(84).experience(125).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.BRACELET_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.ONYX)).create(new RandomItem(ItemId.ONYX_BRACELET));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(90).experience(165).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.AMULET_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.ONYX)).create(new RandomItem(ItemId.ONYX_AMULET_U));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(89).experience(150).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.RING_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.ZENYTE)).create(new RandomItem(ItemId.ZENYTE_RING));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(92).experience(165).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.NECKLACE_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.ZENYTE)).create(new RandomItem(ItemId.ZENYTE_NECKLACE));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(95).experience(180).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.BRACELET_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.ZENYTE)).create(new RandomItem(ItemId.ZENYTE_BRACELET));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(98).experience(200).animation(899).widgetId(WidgetId.MAKE_JEWELRY)
        .tool(new Item(ItemId.AMULET_MOULD)).consume(new RandomItem(ItemId.GOLD_BAR))
        .consume(new RandomItem(ItemId.ZENYTE)).create(new RandomItem(ItemId.ZENYTE_AMULET_U));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(8).experience(4).consume(new RandomItem(ItemId.BALL_OF_WOOL))
        .consume(new RandomItem(ItemId.GOLD_AMULET_U)).create(new RandomItem(ItemId.GOLD_AMULET));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(24).experience(4).consume(new RandomItem(ItemId.BALL_OF_WOOL))
        .consume(new RandomItem(ItemId.SAPPHIRE_AMULET_U))
        .create(new RandomItem(ItemId.SAPPHIRE_AMULET));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(31).experience(4).consume(new RandomItem(ItemId.BALL_OF_WOOL))
        .consume(new RandomItem(ItemId.EMERALD_AMULET_U))
        .create(new RandomItem(ItemId.EMERALD_AMULET));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(50).experience(4).consume(new RandomItem(ItemId.BALL_OF_WOOL))
        .consume(new RandomItem(ItemId.RUBY_AMULET_U)).create(new RandomItem(ItemId.RUBY_AMULET));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(70).experience(4).consume(new RandomItem(ItemId.BALL_OF_WOOL))
        .consume(new RandomItem(ItemId.DIAMOND_AMULET_U))
        .create(new RandomItem(ItemId.DIAMOND_AMULET));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(80).experience(4).consume(new RandomItem(ItemId.BALL_OF_WOOL))
        .consume(new RandomItem(ItemId.DRAGONSTONE_AMULET_U))
        .create(new RandomItem(ItemId.DRAGONSTONE_AMULET));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(90).experience(4).consume(new RandomItem(ItemId.BALL_OF_WOOL))
        .consume(new RandomItem(ItemId.ONYX_AMULET_U)).create(new RandomItem(ItemId.ONYX_AMULET));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(98).experience(4).consume(new RandomItem(ItemId.BALL_OF_WOOL))
        .consume(new RandomItem(ItemId.ZENYTE_AMULET_U))
        .create(new RandomItem(ItemId.ZENYTE_AMULET));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(35).experience(4).consume(new RandomItem(ItemId.BALL_OF_WOOL))
        .consume(new RandomItem(ItemId.SALVE_SHARD)).create(new RandomItem(ItemId.SALVE_AMULET));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(54).experience(100).consume(new RandomItem(ItemId.BATTLESTAFF))
        .consume(new RandomItem(ItemId.WATER_ORB)).create(new RandomItem(ItemId.WATER_BATTLESTAFF));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(58).experience(113).consume(new RandomItem(ItemId.BATTLESTAFF))
        .consume(new RandomItem(ItemId.EARTH_ORB)).create(new RandomItem(ItemId.EARTH_BATTLESTAFF));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(62).experience(125).consume(new RandomItem(ItemId.BATTLESTAFF))
        .consume(new RandomItem(ItemId.FIRE_ORB)).create(new RandomItem(ItemId.FIRE_BATTLESTAFF));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(66).experience(138).consume(new RandomItem(ItemId.BATTLESTAFF))
        .consume(new RandomItem(ItemId.AIR_ORB)).create(new RandomItem(ItemId.AIR_BATTLESTAFF));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(1).experience(17).animation(884).tool(new Item(ItemId.GLASSBLOWING_PIPE))
        .consume(new RandomItem(ItemId.MOLTEN_GLASS)).create(new RandomItem(ItemId.BEER_GLASS));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(4).experience(19).animation(884).tool(new Item(ItemId.GLASSBLOWING_PIPE))
        .consume(new RandomItem(ItemId.MOLTEN_GLASS))
        .create(new RandomItem(ItemId.EMPTY_CANDLE_LANTERN));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(12).experience(25).animation(884).tool(new Item(ItemId.GLASSBLOWING_PIPE))
        .consume(new RandomItem(ItemId.MOLTEN_GLASS)).create(new RandomItem(ItemId.EMPTY_OIL_LAMP));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(33).experience(35).animation(884).tool(new Item(ItemId.GLASSBLOWING_PIPE))
        .consume(new RandomItem(ItemId.MOLTEN_GLASS)).create(new RandomItem(ItemId.VIAL));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(42).experience(42).animation(884).tool(new Item(ItemId.GLASSBLOWING_PIPE))
        .consume(new RandomItem(ItemId.MOLTEN_GLASS)).create(new RandomItem(ItemId.EMPTY_FISHBOWL));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(46).experience(52).animation(884).tool(new Item(ItemId.GLASSBLOWING_PIPE))
        .consume(new RandomItem(ItemId.MOLTEN_GLASS)).create(new RandomItem(ItemId.UNPOWERED_ORB));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(49).experience(55).animation(884).tool(new Item(ItemId.GLASSBLOWING_PIPE))
        .consume(new RandomItem(ItemId.MOLTEN_GLASS)).create(new RandomItem(ItemId.LANTERN_LENS));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(87).experience(70).animation(884).tool(new Item(ItemId.GLASSBLOWING_PIPE))
        .consume(new RandomItem(ItemId.MOLTEN_GLASS))
        .create(new RandomItem(ItemId.EMPTY_LIGHT_ORB));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(1).consume(new RandomItem(ItemId.BULLSEYE_LANTERN_UNF))
        .consume(new RandomItem(ItemId.LANTERN_LENS))
        .create(new RandomItem(ItemId.BULLSEYE_LANTERN_EMPTY));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(1).itemOnMapObject(ObjectId.FRACTIONALIZING_STILL)
        .consume(new RandomItem(ItemId.BULLSEYE_LANTERN_EMPTY))
        .consume(new RandomItem(ItemId.SWAMP_TAR)).create(new RandomItem(ItemId.BULLSEYE_LANTERN));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(52).experience(120).tool(new Item(ItemId.CHISEL))
        .consume(new RandomItem(ItemId.SERPENTINE_VISAGE))
        .create(new RandomItem(ItemId.SERPENTINE_HELM_UNCHARGED));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(59).tool(new Item(ItemId.CHISEL)).consume(new RandomItem(ItemId.MAGIC_FANG))
        .consume(new RandomItem(ItemId.STAFF_OF_THE_DEAD))
        .create(new RandomItem(ItemId.TOXIC_STAFF_UNCHARGED));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(59).tool(new Item(ItemId.CHISEL)).consume(new RandomItem(ItemId.MAGIC_FANG))
        .consume(new RandomItem(ItemId.UNCHARGED_TRIDENT))
        .create(new RandomItem(ItemId.UNCHARGED_TOXIC_TRIDENT));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(59).tool(new Item(ItemId.CHISEL)).consume(new RandomItem(ItemId.MAGIC_FANG))
        .consume(new RandomItem(ItemId.UNCHARGED_TRIDENT_E))
        .create(new RandomItem(ItemId.UNCHARGED_TOXIC_TRIDENT_E));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(1).consume(new RandomItem(ItemId.ONYX)).consume(new RandomItem(ItemId.ZENYTE_SHARD))
        .create(new RandomItem(ItemId.UNCUT_ZENYTE));
    entries.add(entry.build());

    return entries;
  }
}
