package com.palidinodh.osrsscript.player.command.owner;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.util.PLogger;
import lombok.var;

@ReferenceName({ "npc", "savenpc" })
class NpcCommand implements CommandHandler, CommandHandler.OwnerRank {
  @Override
  public String getExample(String name) {
    return "id_or_name (distance) (direction)";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.split(message);
    var id = messages[0].matches("[0-9]+") ? Integer.parseInt(messages[0])
        : NpcId.valueOf(messages[0].replace(" ", "_").toUpperCase());
    if (id == -1) {
      player.getGameEncoder().sendMessage("Couldn't find npc.");
      return;
    }
    var distance = 0;
    if (messages.length >= 2) {
      distance = Integer.parseInt(messages[1]);
    }
    var direction = Tile.Direction.SOUTH;
    if (messages.length >= 3) {
      direction = Tile.Direction.valueOf(messages[2].replace(" ", "_").toUpperCase());
    }
    if (direction == null) {
      player.getGameEncoder().sendMessage("Couldn't find direction.");
      return;
    }
    var npc = player.getController().addNpc(new NpcSpawn(id, player, direction, distance));
    player.getGameEncoder().sendMessage("Spawned " + npc.getDef().getName());
    if (name.equals("savenpc")) {
      // TODO: make this actually good
      // Example: make it auto save into the correct spawn class
      var npcIdInfo = "NpcId." + NpcId.valueOf(id);
      var tileInfo = "new Tile(" + player.getX() + ", " + player.getY()
          + (player.getHeight() > 0 ? ", " + player.getHeight() : "") + ")";
      var directionInfo = "Tile.Direction." + direction;
      var line = "    spawns.add(new NpcSpawn(" + npcIdInfo + ", " + tileInfo + "));";
      if (distance > 0 && direction != Tile.Direction.SOUTH) {
        line = "    spawns.add(new NpcSpawn(" + npcIdInfo + ", " + tileInfo + ", " + directionInfo
            + ", " + distance + "));";
      } else if (distance > 0) {
        line =
            "    spawns.add(new NpcSpawn(" + npcIdInfo + ", " + tileInfo + ", " + distance + "));";
      } else if (direction != Tile.Direction.SOUTH) {
        line =
            "    spawns.add(new NpcSpawn(" + npcIdInfo + ", " + tileInfo + ", " + direction + "));";
      }
      PLogger.println(line);
    }
  }
}
