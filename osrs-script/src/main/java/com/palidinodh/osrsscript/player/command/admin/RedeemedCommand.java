package com.palidinodh.osrsscript.player.command.admin;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrsscript.player.plugin.bond.BondPlugin;
import com.palidinodh.rs.reference.ReferenceName;
import lombok.var;

@ReferenceName("setredeemed")
class RedeemedCommand implements CommandHandler, CommandHandler.AdministratorRank {
  @Override
  public String getExample(String name) {
    return "\"username\" amount";
  }

  @Override
  public void execute(Player player, String user, String message) {
    var messages = CommandHandler.split(message);
    var username = messages[0].replace("_", " ");
    var amount = Integer.parseInt(messages[1]);
    var player2 = player.getWorld().getPlayerByUsername(username);
    if (player2 == null) {
      player.getGameEncoder().sendMessage("Unable to find player.");
      return;
    }
    var plugin = player2.getPlugin(BondPlugin.class);
    if (amount == 0) {
      plugin.setTotalRedeemed(0);
      player.getGameEncoder().sendMessage("Reset redeemed bonds for " + player2.getUsername());
      player2.getGameEncoder().sendMessage(player.getUsername() + " reset your redeemed bonds.");
    } else {
      plugin.setTotalRedeemed(plugin.getTotalRedeemed() + amount);
      player.getGameEncoder()
          .sendMessage("Added " + amount + " redeemed bonds to " + player2.getUsername());
      player2.getGameEncoder().sendMessage(
          player.getUsername() + " added " + amount + " redeemed bonds to your account.");
    }
  }
}
