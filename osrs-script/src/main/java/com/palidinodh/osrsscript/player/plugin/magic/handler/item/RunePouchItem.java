package com.palidinodh.osrsscript.player.plugin.magic.handler.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.cache.definition.util.DefinitionOption;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.RUNE_POUCH)
class RunePouchItem implements ItemHandler {
  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "open":
        player.getController().openRunePouch();
        break;
      case "empty":
        player.getController().removeRunePouchRunes();
        break;
    }
  }

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    var slot = onItem.getId() == ItemId.RUNE_POUCH ? useItem.getSlot() : onItem.getSlot();
    return player.getController().addRunePouchRune(slot, player.getInventory().getAmount(slot));
  }
}
