package com.palidinodh.osrsscript.player.command.betaworld;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.cache.definition.osrs.NpcDefinition;
import com.palidinodh.rs.reference.ReferenceName;
import lombok.var;

@ReferenceName("pnpc")
class PnpcCommand implements CommandHandler, CommandHandler.BetaWorld {
  @Override
  public String getExample(String name) {
    return "id";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var id = message.matches("[0-9]+") ? Integer.parseInt(message)
        : NpcId.valueOf(message.replace(" ", "_").toUpperCase());
    player.getAppearance().setNpcId(id);
    if (id != -1 && NpcDefinition.getDefinition(id) != null) {
      var animations = NpcDefinition.getDefinition(id).getStanceAnimations();
      player.getGameEncoder().sendMessage("Anim: " + animations[0]);
    }
  }
}
