package com.palidinodh.osrsscript.player.mysterybox;

import java.util.List;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.MysteryBox;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.MYSTERY_BOX)
class RegularBox extends MysteryBox {
  private static List<RandomItem> weightless =
      RandomItem.weightless(RandomItem.combine(ItemTables.VERY_RARE, ItemTables.RARE,
          ItemTables.UNCOMMON, ItemTables.COMMON,
          RandomItem.buildList(new RandomItem(ItemId.CLUE_SCROLL_MASTER),
              new RandomItem(ItemId.CLUE_SCROLL_ELITE), new RandomItem(ItemId.CLUE_SCROLL_HARD),
              new RandomItem(ItemId.CLUE_SCROLL_MEDIUM))));

  @Override
  public Item getRandomItem(Player player) {
    if (PRandom.randomE(128) == 0) {
      return RandomItem.getItem(ItemTables.VERY_RARE);
    } else if (PRandom.randomE(32) == 0) {
      var type = PRandom.randomE(4);
      if (type == 0) {
        return new Item(ItemId.CLUE_SCROLL_MASTER);
      } else if (type == 1) {
        return new Item(ItemId.CLUE_SCROLL_ELITE);
      } else if (type == 2) {
        return new Item(ItemId.CLUE_SCROLL_HARD);
      } else if (type == 3) {
        return new Item(ItemId.CLUE_SCROLL_MEDIUM);
      }
      return new Item(ItemId.CLUE_SCROLL_EASY);
    } else if (PRandom.randomE(8) == 0) {
      return RandomItem.getItem(ItemTables.RARE);
    } else if (PRandom.randomE(4) == 0) {
      return RandomItem.getItem(ItemTables.UNCOMMON);
    }
    return RandomItem.getItem(ItemTables.COMMON);
  }

  @Override
  public List<RandomItem> getAllItems(Player player) {
    return weightless;
  }
}
