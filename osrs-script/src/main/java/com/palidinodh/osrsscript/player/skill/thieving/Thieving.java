package com.palidinodh.osrsscript.player.skill.thieving;

import java.util.List;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrsscript.map.area.asgarnia.donator.DonatorArea;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.setting.UserRank;
import com.palidinodh.util.PEvent;
import lombok.var;

public class Thieving extends SkillContainer {
  @Override
  public int getSkillId() {
    return Skills.THIEVING;
  }

  @Override
  public List<SkillEntry> getEntries() {
    return ThievingEntries.getEntries();
  }

  @Override
  public int getDefaultMakeAmount() {
    return 1;
  }

  @Override
  public void actionSuccess(Player player, PEvent event, Npc npc, MapObject mapObject,
      SkillEntry entry) {
    if (npc != null) {
      player.getGameEncoder().sendMessage("You pick the " + npc.getName() + "'s pocket.");
    } else if (mapObject != null) {
      player.getGameEncoder().sendMessage("You steal from the " + mapObject.getName() + ".");
      setTemporaryMapObject(player, mapObject, entry);
    }
  }

  @Override
  public Item createHook(Player player, Item item, Npc npc, MapObject mapObject, SkillEntry entry) {
    var farmer = npc != null
        && (npc.getId() == NpcId.MASTER_FARMER || npc.getId() == NpcId.MASTER_FARMER_3258
            || npc.getId() == NpcId.MARTIN_THE_MASTER_GARDENER);
    if (player.getEquipment().wearingRogueOutfit() && (PRandom.randomE(10) == 0 || farmer)) {
      item = new Item(item.getId(), item.getAmount() * 2);
    }
    return item;
  }

  @Override
  public int experienceHook(Player player, int experience, Npc npc, MapObject mapObject,
      SkillEntry entry) {
    if (player.getEquipment().wearingRogueOutfit()) {
      experience *= 1.1;
    }
    if (player.getArea().isArea(DonatorArea.class) && player.isUsergroup(UserRank.OPAL_MEMBER)) {
      experience *= 1.1;
    }
    return experience;
  }

  @Override
  public boolean failedActionHook(Player player, PEvent event, Npc npc, MapObject mapObject,
      SkillEntry entry) {
    var chance =
        Math.min(128.0 - entry.getLevel() + player.getSkills().getLevel(getSkillId()) * 1.4, 240.0)
            / 255.0 * 100.0;
    chance *= 1 + getMembershipBoost(player);
    if (player.getEquipment().wearingRogueOutfit()) {
      chance += 20;
    }
    if (player.inArdougne() && player.hasItem(ItemId.ARDOUGNE_CLOAK_2)
        || player.hasItem(ItemId.ARDOUGNE_CLOAK_3) || player.hasItem(ItemId.ARDOUGNE_CLOAK_4)
        || player.hasItem(ItemId.ARDOUGNE_MAX_CAPE)) {
      chance += 10;
    }
    if (player.getArea().isArea(DonatorArea.class) && player.isUsergroup(UserRank.OPAL_MEMBER)) {
      chance += 10;
    }
    if (player.getEquipment().wearingAccomplishmentCape(getSkillId())) {
      chance += 10;
    }
    if (player.hasVoted()) {
      chance += 5;
    }
    chance = Math.min(chance, 100);
    if (npc != null && PRandom.randomE(100) > chance) {
      npc.setForceMessage("What do you think you're doing?");
      npc.setAnimation(npc.getCombatDef().getAttackAnimation());
      npc.setFaceTile(player);
      player.setAnimation(player.getCombat().getBlockAnimation());
      player.setGraphic(80, 100);
      player.getGameEncoder().sendMessage("You've been stunned!");
      player.getController().setMagicBind(8);
      player.getSkills().setSkillDelay(8);
      return true;
    }
    return false;
  }

  @Override
  public String deathReasonHook(Player player, PEvent event, Npc npc, MapObject mapObject,
      SkillEntry entry) {
    if (npc != null) {
      return "pickpocketing a " + npc.getName();
    }
    return "thieving";
  }
}
