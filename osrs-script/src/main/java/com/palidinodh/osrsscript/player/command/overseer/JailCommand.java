package com.palidinodh.osrsscript.player.command.overseer;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.reference.ReferenceName;
import lombok.var;

@ReferenceName({ "jail", "unjail" })
class JailCommand implements CommandHandler, CommandHandler.OverseerRank {
  @Override
  public String getExample(String name) {
    return "username";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var targetPlayer = player.getWorld().getPlayerByUsername(message);
    if (targetPlayer == null) {
      player.getGameEncoder().sendMessage("Unable to find user " + message + ".");
      return;
    }
    if (name.equals("jail")) {
      if (!targetPlayer.getController().canTeleport(false) && !player.isHigherStaffUsergroup()) {
        player.getGameEncoder().sendMessage("The player you are trying to move can't teleport.");
        player.log(PlayerLogType.STAFF, "failed to jail " + targetPlayer.getLogName());
        return;
      }
      targetPlayer.getMovement().teleport(2094, 4466);
      targetPlayer.getGameEncoder().sendMessage("You have been jailed by " + player.getUsername());
      player.getGameEncoder().sendMessage(message + " has been jailed.");
      player.getWorld().sendStaffMessage(
          player.getUsername() + " has jailed " + targetPlayer.getUsername() + ".");
      player.log(PlayerLogType.STAFF, "jailed " + targetPlayer.getLogName());
    } else if (name.equals("unjail")) {
      if (targetPlayer.getRegionId() != 8261) {
        player.getGameEncoder().sendMessage("The player you are trying to move isn't jailed.");
        return;
      }
      targetPlayer.getMovement().teleport(3093, 3495);
      targetPlayer.getGameEncoder().sendMessage("You have been unjailed.");
      player.getGameEncoder().sendMessage(message + " has been unjailed.");
      player.getWorld().sendStaffMessage(
          player.getUsername() + " has unjailed " + targetPlayer.getUsername() + ".");
      player.log(PlayerLogType.STAFF, "unjailed " + targetPlayer.getLogName() + " from "
          + targetPlayer.getX() + ", " + targetPlayer.getY() + ", " + targetPlayer.getHeight());
    }
  }
}
