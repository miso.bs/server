package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.DRAGON_THROWNAXE, ItemId.DRAGON_THROWNAXE_21207 })
class DragonThrownaxeSpecialAttack extends SpecialAttack {
  public DragonThrownaxeSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(25);
    entry.setAnimation(7521);
    entry.setCastGraphic(new Graphic(1317, 96));
    entry.setProjectileId(1318);
    entry.setAccuracyModifier(1.25);
    entry.setInstant(true);
    addEntry(entry);
  }
}
