package com.palidinodh.osrsscript.player.command.all;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.world.BadWords;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.UserRank;
import com.palidinodh.util.PTime;
import lombok.var;

@ReferenceName("yell")
class YellCommand implements CommandHandler {
  @Override
  public String getExample(String name) {
    return "- message";
  }

  @Override
  public boolean canUse(Player player) {
    return player.isUsergroup(UserRank.OPAL_MEMBER) || player.isUsergroup(UserRank.OVERSEER)
        || player.isStaffUsergroup();
  }

  @Override
  public void execute(Player player, String name, String message) {
    var yellDelay = 0L;
    if (player.getMessaging().getYellDelay() > 0) {
      player.getGameEncoder()
          .sendMessage("You need to wait " + PTime.tickToSec(player.getMessaging().getYellDelay())
              + " seconds before you can yell again.");
      return;
    }
    if (player.inJail()) {
      player.getGameEncoder().sendMessage("You can not yell while in jail..");
      return;
    }
    if (player.getMessaging().isMuted()) {
      player.getGameEncoder().sendMessage("You can not yell while muted.");
      return;
    }
    if (BadWords.containsBadWord(message)) {
      player.getGameEncoder().sendMessage("Please don't use that word.");
      return;
    }
    if (!player.isStaffUsergroup()) {
      message = message.replaceAll("<(.*?)>", "");
    }
    if (player.isUsergroup(UserRank.OVERSEER) || player.isUsergroup(UserRank.SENIOR_MODERATOR)
        || player.isUsergroup(UserRank.MODERATOR) || player.getRights() == Player.RIGHTS_ADMIN
        || player.isUsergroup(UserRank.COMMUNITY_MANAGER)) {
      yellDelay = PTime.secToTick(5);
    } else if (player.isUsergroup(UserRank.EMERALD_MEMBER)) {
      yellDelay = PTime.secToTick(5);
    } else if (player.isUsergroup(UserRank.SAPPHIRE_MEMBER)) {
      yellDelay = PTime.secToTick(15);
    } else if (player.isUsergroup(UserRank.TOPAZ_MEMBER)) {
      yellDelay = PTime.secToTick(30);
    } else if (player.isUsergroup(UserRank.JADE_MEMBER)) {
      yellDelay = PTime.secToTick(45);
    } else if (player.isUsergroup(UserRank.OPAL_MEMBER)) {
      yellDelay = PTime.secToTick(60);
    }
    player.getWorld().sendMessage(player, "[<col=ff0000>Yell</col>] "
        + player.getMessaging().getIconImage() + player.getUsername() + ": " + message);
    player.getMessaging().setYellDelay((int) yellDelay);
    player.log(PlayerLogType.YELL, message);
  }
}
