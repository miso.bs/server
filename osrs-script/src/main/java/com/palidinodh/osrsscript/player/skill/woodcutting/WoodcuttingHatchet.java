package com.palidinodh.osrsscript.player.skill.woodcutting;

import lombok.Getter;

@Getter
class WoodcuttingHatchet {
  private int itemId;
  private int level;
  private int animation;

  public WoodcuttingHatchet(int itemId, int level, int animation) {
    this.itemId = itemId;
    this.level = level;
    this.animation = animation;
  }
}
