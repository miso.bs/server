package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.DRAGON_MACE)
class DragonMaceSpecialAttack extends SpecialAttack {
  public DragonMaceSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(25);
    entry.setAnimation(1060);
    entry.setCastGraphic(new Graphic(251, 100));
    entry.setAccuracyModifier(1.25);
    entry.setDamageModifier(1.5);
    addEntry(entry);
  }
}
