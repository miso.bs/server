package com.palidinodh.osrsscript.player.command.seniormod;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import lombok.var;

@ReferenceName("resetloadouts")
class ResetLoadoutsCommand implements CommandHandler, CommandHandler.SeniorModeratorRank {
  @Override
  public String getExample(String name) {
    return "username";
  }

  @Override
  public void execute(Player player, String user, String message) {
    var player2 = player.getWorld().getPlayerByUsername(message);
    if (player2 == null) {
      player.getGameEncoder().sendMessage("Unable to find user " + message + ".");
      return;
    }
    if (player2.getLoadout().getEntries() != null) {
      player2.getLoadout().getEntries().clear();
    }
    player2.getLoadout().setQuickIndex(-1);
    player2.getGameEncoder()
        .sendMessage("Your loadouts have been reset by " + player.getUsername());
    player.getGameEncoder().sendMessage("Success");
  }
}
