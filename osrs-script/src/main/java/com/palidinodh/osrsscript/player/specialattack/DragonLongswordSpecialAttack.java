package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.DRAGON_LONGSWORD)
class DragonLongswordSpecialAttack extends SpecialAttack {
  public DragonLongswordSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(25);
    entry.setAnimation(1058);
    entry.setCastGraphic(new Graphic(248, 100));
    entry.setDamageModifier(1.15);
    addEntry(entry);
  }
}
