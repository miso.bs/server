package com.palidinodh.osrsscript.player.command.all;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("clearloadouts")
class ClearLoadoutsCommand implements CommandHandler {
  @Override
  public void execute(Player player, String user, String message) {
    player.getLoadout().clear();
  }
}
