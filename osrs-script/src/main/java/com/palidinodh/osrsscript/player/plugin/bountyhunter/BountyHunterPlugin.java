package com.palidinodh.osrsscript.player.plugin.bountyhunter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import com.google.inject.Inject;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.VarbitId;
import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.combat.RecentCombatant;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.map.route.Route;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrsscript.player.plugin.magic.SpellTeleport;
import com.palidinodh.osrsscript.world.event.wildernesshotspot.WildernessHotspotEvent;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PTime;
import lombok.Getter;
import lombok.Setter;
import lombok.var;

public class BountyHunterPlugin implements PlayerPlugin {
  private static final boolean ENABLED = true;
  private static final int MAX_COMBAT_DIFFERENCE = 10;
  private static final int UPDATE_DELAY = 25;
  private static final int TARGET_ACTION_DELAY = 50;
  public static List<RandomItem> TARGET_DROP_TABLE = RandomItem.buildList(
      new RandomItem(-1).weight(8192), new RandomItem(ItemId.ANTIQUE_EMBLEM_TIER_1).weight(4096),
      new RandomItem(ItemId.ANTIQUE_EMBLEM_TIER_2).weight(2048),
      new RandomItem(ItemId.ANTIQUE_EMBLEM_TIER_3).weight(1024),
      new RandomItem(ItemId.ANTIQUE_EMBLEM_TIER_4).weight(512),
      new RandomItem(ItemId.ANTIQUE_EMBLEM_TIER_5).weight(256),
      new RandomItem(ItemId.ANTIQUE_EMBLEM_TIER_6).weight(128),
      new RandomItem(ItemId.ANTIQUE_EMBLEM_TIER_7).weight(64),
      new RandomItem(ItemId.ANTIQUE_EMBLEM_TIER_8).weight(32),
      new RandomItem(ItemId.ANTIQUE_EMBLEM_TIER_9).weight(16),
      new RandomItem(ItemId.ANTIQUE_EMBLEM_TIER_10).weight(8),
      new RandomItem(ItemId.VESTAS_LONGSWORD).weight(1),
      new RandomItem(ItemId.STATIUSS_WARHAMMER).weight(1),
      new RandomItem(ItemId.VESTAS_SPEAR).weight(1),
      new RandomItem(ItemId.MORRIGANS_JAVELIN, 50).weight(1),
      new RandomItem(ItemId.MORRIGANS_THROWING_AXE, 50).weight(1),
      new RandomItem(ItemId.ZURIELS_STAFF).weight(1));

  @Inject
  private transient Player player;
  @Getter
  private transient Player target;
  private transient int targetUpdateDelay;
  private transient RiskTier targetRisk;
  private transient MysteriousEmblem targetEmblem;
  private transient String targetLevelRange;
  private transient boolean inWilderness;
  private transient int leftTime;
  private transient int combatIdleTime = -1;
  private transient int searchDelay = UPDATE_DELAY;
  private transient int teleportCooldown;
  private int targetKills;
  private List<RecentCombatant> recentKills = new ArrayList<>();
  @Getter
  private int penalty;
  private long firstAbandonTime;
  private int totalAbandoned;
  @Getter
  private boolean minimised;
  private boolean targetIndicator;
  @Getter
  private boolean teleportUnlocked;
  @Getter
  @Setter
  private boolean teleportWarning;

  @Override
  public void loadLegacy(Map<String, Object> map) {
    if (map.containsKey("bountyHunter.show")) {
      setMinimised(!(boolean) map.get("bountyHunter.show"));
    }
    if (map.containsKey("bountyHunter.teleportUnlocked")) {
      setTeleportUnlocked((boolean) map.get("bountyHunter.teleportUnlocked"));
    }
    if (map.containsKey("bountyHunter.targetKills")) {
      targetKills = (int) map.get("bountyHunter.targetKills");
    }
    if (map.containsKey("bountyHunter.penaltyDelay")) {
      penalty = (int) map.get("bountyHunter.penaltyDelay");
    }
    if (map.containsKey("bountyHunter.targetIndicator")) {
      targetIndicator = (boolean) map.get("bountyHunter.targetIndicator");
    }
    if (map.containsKey("bountyHunter.teleportWarning")) {
      teleportWarning = (boolean) map.get("bountyHunter.teleportWarning");
    }
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("bounty_hunter_target")) {
      return target;
    }
    if (name.equals("bounty_hunter_target_kills")) {
      return targetKills;
    }
    if (name.equals("bounty_hunter_emblem_coin_value")) {
      return MysteriousEmblem.getCoinValue((int) args[0]);
    }
    if (name.equals("bounty_hunter_emblem_blood_money_value")) {
      return MysteriousEmblem.getBloodMoneyValue((int) args[0]);
    }
    return null;
  }

  @Override
  public void login() {
    setTeleportUnlocked(teleportUnlocked);
    player.getGameEncoder().setVarbit(VarbitId.BOUNTY_HUNTER_ROGUE_HUNTER_COUNTS, 1);
  }

  @Override
  public void logout() {
    abandonTarget();
  }

  @Override
  public void tick() {
    if (searchDelay > 0) {
      searchDelay--;
    }
    if (penalty > 0) {
      penalty--;
    }
    if (teleportCooldown > 0) {
      teleportCooldown--;
    }
    if (!ENABLED) {
      return;
    }
    if (!player.getWorld().isPrimary()) {
      return;
    }
    if (player.getArea().inPvpWorld()) {
      return;
    }
    checkWildernessState();
    if (inWilderness) {
      findTarget();
    }
    checkTargetState();
    if (targetUpdateDelay > 0) {
      targetUpdateDelay--;
      if (targetUpdateDelay == 0) {
        sendTarget();
      }
    }
  }

  @Override
  public void playerKilled(Player player2) {
    if (!inWilderness || !player2.getArea().inWilderness()) {
      return;
    }
    if (target != player2) {
      return;
    }
    player.getGameEncoder()
        .sendMessage("<col=ff0000>You've killed your target: " + player2.getUsername() + "!</col>");
    player.getGameEncoder().sendMessage("Next target in: <col=ff0000>1 minute.</col>");
    isRecentTargetKill(player2, true);
    player2.getPlugin(BountyHunterPlugin.class).isRecentTargetKill(player, true);
    cancelTarget();
    targetKills++;
  }

  public void checkWildernessState() {
    var previouslyInWilderness = inWilderness;
    inWilderness = player.getArea().inWilderness();
    if (previouslyInWilderness != inWilderness) {
      player.getGameEncoder().sendHideWidget(WidgetId.WILDERNESS, 55, !inWilderness);
      if (target == null) {
        penalty = Math.max(penalty, TARGET_ACTION_DELAY);
      }
    }
    if (previouslyInWilderness != inWilderness && inWilderness) {
      sendTarget();
      if (player.getArea().inPvpWorld() || player.getHeight() != player.getClientHeight()) {
        player.getGameEncoder().sendHideWidget(WidgetId.WILDERNESS, 6, true);
      }
      setMinimised(minimised);
    }
  }

  public void checkTargetState() {
    if (target == null) {
      return;
    }
    if (!target.isVisible()) {
      cancelTarget();
      player.getGameEncoder().sendMessage(
          "Your target is no longer available, so you shall be assigned a new target.");
      return;
    }
    if (target.getCombat().isDead() && target.getCombat().getRespawnDelay() == 1) {
      if (player != target.getCombat().getPlayerFromHitCount(false)) {
        cancelTarget();
        player.getGameEncoder()
            .sendMessage("Your target has died. You'll get a new one presently.");
        return;
      }
    }
    if (player.getSkills().getCombatLevel()
        - target.getSkills().getCombatLevel() > MAX_COMBAT_DIFFERENCE + 1) {
      cancelTarget();
      return;
    }
    if (player.isAttacking() && player.getAttackingEntity() == target
        || target.isAttacking() && target.getAttackingEntity() == player) {
      combatIdleTime = 0;
    }
    if (combatIdleTime != -1) {
      combatIdleTime++;
    }
    if (!inWilderness) {
      if (leftTime == 0) {
        player.getGameEncoder().sendMessage(
            "<col=ff0000>You have two minutes to return to the Wilderness before you lose your target.");
      } else if (leftTime == 100) {
        player.getGameEncoder().sendMessage(
            "<col=ff0000>You have one minute to return to the Wilderness before you lose your target.");
      }
      leftTime++;
    }
    if (leftTime >= 200) {
      abandonTarget();
    } else if (combatIdleTime >= 25) {
      var myTarget = target;
      abandonTarget();
      if (!inWilderness && !myTarget.getArea().inWilderness()) {
        myTarget.getPlugin(BountyHunterPlugin.class).abandonTarget();
      }
    }
  }

  public void cancelTarget() {
    var myTarget = target;
    reset();
    if (myTarget != null && myTarget.isVisible()) {
      myTarget.getPlugin(BountyHunterPlugin.class).reset();
    }
  }

  public void reset() {
    target = null;
    targetUpdateDelay = 0;
    targetRisk = null;
    targetEmblem = null;
    targetLevelRange = null;
    leftTime = 0;
    combatIdleTime = -1;
    penalty = Math.max(penalty, 100);
    sendTarget();
  }

  public void abandonTarget() {
    if (target == null || !target.isVisible()) {
      return;
    }
    var myTarget = target;
    cancelTarget();
    player.getGameEncoder().sendMessage("<col=ff0000>You have abandoned your target.");
    if (myTarget.getCombat().getPKSkullDelay() == 0) {
      return;
    }
    if (hasEmblem() && !myTarget.getPlugin(BountyHunterPlugin.class).hasEmblem()) {
      return;
    }
    if (myTarget.getController().inMultiCombat()) {
      return;
    }
    var timeSinceFirstAbandon = PTime.betweenMilliToMin(firstAbandonTime);
    if (timeSinceFirstAbandon < 30) {
      if (++totalAbandoned == 5) {
        penalty = (!myTarget.getArea().inWilderness() || Math.abs(
            player.getArea().getWildernessLevel() - myTarget.getArea().getWildernessLevel()) > 5)
                ? 100
                : 1000;
      }
    } else {
      firstAbandonTime = PTime.currentTimeMillis();
      totalAbandoned = 1;
    }
  }

  public void findTarget() {
    if (target != null) {
      return;
    }
    if (!inWilderness || player.getArea().inPvpWorld()
        || player.getHeight() != player.getClientHeight() || player.getController().isInstanced()) {
      return;
    }
    if (!hasEmblem()) {
      return;
    }
    if (searchDelay > 0 || penalty > 0) {
      return;
    }
    searchDelay = UPDATE_DELAY;
    var players = player.getWorld().getWildernessPlayers();
    Collections.shuffle(players);
    var hotspot = player.getWorld().getWorldEvent(WildernessHotspotEvent.class).inside(player);
    Player target = null;
    Player hotspotTarget = null;
    for (var player2 : players) {
      if (player == player2) {
        continue;
      }
      var targetPlugin = player2.getPlugin(BountyHunterPlugin.class);
      if (player2.getArea().inPvpWorld() || player2.getHeight() != player2.getClientHeight()
          || player2.getController().isInstanced()) {
        continue;
      }
      if (targetPlugin.getTarget() != null) {
        continue;
      }
      if (isRecentTargetKill(player2, false) || targetPlugin.isRecentTargetKill(player, false)) {
        continue;
      }
      if (Math.abs(player.getSkills().getCombatLevel()
          - player2.getSkills().getCombatLevel()) > MAX_COMBAT_DIFFERENCE) {
        continue;
      }
      var targetHotspot =
          player.getWorld().getWorldEvent(WildernessHotspotEvent.class).inside(player2);
      if (targetPlugin.getPenalty() > 0) {
        if (hotspot && targetHotspot && targetPlugin.getPenalty() < TARGET_ACTION_DELAY) {
          return;
        }
        continue;
      }
      if (target == null && (hotspot || !targetHotspot)) {
        target = player2;
      }
      if (hotspot && targetHotspot) {
        hotspotTarget = player2;
        break;
      }
    }
    if (hotspotTarget != null) {
      target = hotspotTarget;
    }
    combatIdleTime = -1;
    if (target != null && target.isVisible()) {
      target.getPlugin(BountyHunterPlugin.class).assignTarget(player);
      assignTarget(target);
    }
  }

  public void assignTarget(Player target) {
    this.target = target;
    if (target == null || !target.isVisible()) {
      return;
    }
    penalty = TARGET_ACTION_DELAY;
    sendTarget();
    player.getGameEncoder()
        .sendMessage("<col=ff0000>You've been assigned a target: " + target.getUsername());
  }

  public void sendTarget() {
    targetUpdateDelay = UPDATE_DELAY;
    targetRisk = null;
    targetEmblem = null;
    var hasTarget = target != null && target.isVisible();
    if (hasTarget) {
      targetRisk = RiskTier.getByValue(target.getCombat().getRiskedValue());
      for (var emblem : MysteriousEmblem.values()) {
        if (emblem.isArchaic()) {
          continue;
        }
        if (target.getInventory().hasItem(emblem.getItemId())) {
          targetEmblem = emblem;
        }
      }
      var min = Math.max(1, target.getArea().getWildernessLevel() - 1 - PRandom.randomI(2));
      var max = target.getArea().getWildernessLevel() + 1 + PRandom.randomI(2);
      if (target.getArea().getWildernessLevel() == 0) {
        min = max = 0;
      }
      var distance = player.getDistance(target);
      var color = "<col=1462ab>";
      if (distance < 20) {
        color = "<col=009900>";
      } else if (distance < 40) {
        color = "<col=93732a>";
      } else if (distance < 60) {
        color = "<col=620000>";
      } else if (distance < 80) {
        color = "<col=ad0400>";
      }
      targetLevelRange = min == 0 && max == 0 ? "Safe" : color + "Lvl " + min + "-" + max;
    }
    var targetName = "None";
    var targetInfo = "Level: -----";
    if (hasTarget) {
      if (targetIndicator) {
        player.getGameEncoder().sendHintIconPlayer(target.getIndex());
      }
      targetName =
          (target.getCombat().getPKSkullDelay() > 0 ? "<img=9>" : "") + target.getUsername();
      targetInfo = targetLevelRange + ", Cmb " + target.getSkills().getCombatLevel();
    } else {
      if (targetIndicator) {
        player.getGameEncoder().sendHintIconReset();
      }
      if (penalty > 100) {
        var mins = Math.max(1, PTime.tickToMin(penalty));
        targetInfo = mins + (mins > 1 ? " mins" : " min");
      }
    }
    player.getGameEncoder().sendWidgetText(WidgetId.WILDERNESS, 54, targetName);
    player.getGameEncoder().sendWidgetText(WidgetId.WILDERNESS, 55, targetInfo);
    player.getGameEncoder().setVarbit(VarbitId.BOUNTY_HUNTER_WEALTH,
        targetRisk != null ? targetRisk.getVarbitValue() : 0);
    player.getGameEncoder().setVarbit(VarbitId.BOUNTY_HUNTER_EMBLEM,
        targetEmblem != null ? targetEmblem.getVarbitValue() : 0);
  }

  public boolean isRecentTargetKill(Player opponent, boolean shouldUpdate) {
    if (player.getIP().equals(opponent.getIP())) {
      return true;
    }
    for (var rc : recentKills) {
      if (opponent.getId() != rc.userId && !opponent.getIP().equals(rc.ip)) {
        continue;
      }
      var minutes = PTime.betweenMilliToMin(rc.time);
      if (minutes < 10) {
        return true;
      }
      break;
    }
    if (shouldUpdate) {
      while (recentKills.size() >= 4) {
        recentKills.remove(0);
      }
      recentKills.add(new RecentCombatant(opponent));
    }
    return false;
  }

  public boolean upgradeEmblem() {
    var values = MysteriousEmblem.values();
    for (var i = values.length - 1; i >= 0; i--) {
      var emblem = values[i];
      if (emblem.isArchaic()) {
        continue;
      }
      var slot = player.getInventory().getSlotById(emblem.getItemId());
      if (slot == -1) {
        continue;
      }
      var nextId = MysteriousEmblem.getNextId(emblem.getItemId());
      if (nextId == -1 || emblem.getItemId() == nextId) {
        continue;
      }
      player.getInventory().deleteItem(emblem.getItemId(), 1, slot);
      player.getInventory().addItem(nextId, 1, slot);
      return true;
    }
    return false;
  }

  public boolean hasEmblem() {
    return getEmblemId() != -1;
  }

  public int getEmblemId() {
    var values = MysteriousEmblem.values();
    for (var i = values.length - 1; i >= 0; i--) {
      var emblem = values[i];
      if (emblem.isArchaic()) {
        continue;
      }
      if (!player.getInventory().hasItem(emblem.getItemId())) {
        continue;
      }
      return emblem.getItemId();
    }
    return -1;
  }

  public void selectTeleportToTarget() {
    if (player.getInventory().hasItem(ItemId.BLOODY_KEY)
        || player.getInventory().hasItem(ItemId.BLOODIER_KEY)) {
      player.getGameEncoder().sendMessage("You can't teleport while carrying a bloody key.");
      return;
    }
    if (target == null || !target.isVisible()) {
      player.getGameEncoder().sendMessage("You don't have a target.");
      return;
    }
    if (!target.getArea().inWilderness()) {
      player.getGameEncoder().sendMessage("Your target isn't in the Wilderness.");
      return;
    }
    if (!teleportUnlocked) {
      player.getGameEncoder().sendMessage("You need to unlock this spell to use it.");
      return;
    }
    if (teleportWarning) {
      var inMulti = target.getController().inMultiCombat();
      player.openDialogue(new OptionsDialogue(
          target.getUsername() + ": " + (inMulti ? "Multi-way" : "Single-way")
              + ", Wilderness level " + target.getArea().getWildernessLevel(),
          new DialogueOption(
              "Teleport <col=ff0000>near</col> level " + target.getArea().getWildernessLevel()
                  + " <col=ff0000>" + (inMulti ? "multi-way" : "single-way") + "</col> Wilderness.",
              (c2, s2) -> teleportToTarget()),
          new DialogueOption("Cancel.")));
    } else {
      teleportToTarget();
    }
  }

  public void teleportToTarget() {
    if (player.isLocked()) {
      return;
    }
    if (player.getMovement().getTeleportBlock() > 0) {
      player.getGameEncoder()
          .sendMessage("A teleport block has been cast on you. It should wear off in "
              + player.getMovement().getTeleportBlockRemaining() + ".");
      return;
    }
    if (player.getInventory().hasItem(ItemId.BLOODY_KEY)
        || player.getInventory().hasItem(ItemId.BLOODIER_KEY)) {
      player.getGameEncoder().sendMessage("You can't teleport while carrying a bloody key.");
      return;
    }
    if (target == null || !target.isVisible()) {
      player.getGameEncoder().sendMessage("You don't have a target.");
      return;
    }
    if (!target.getArea().inWilderness()) {
      player.getGameEncoder().sendMessage("Your target isn't in the Wilderness.");
      return;
    }
    if (player.getHeight() != target.getHeight() || player.getArea().inPvpWorld()
        || target.getArea().inPvpWorld()) {
      player.getGameEncoder().sendMessage("There was a problem locating your target.");
      return;
    }
    if (!teleportUnlocked) {
      player.getGameEncoder().sendMessage("You need to unlock this spell first.");
      return;
    }
    if (teleportCooldown > 0) {
      player.getGameEncoder().sendMessage("This spell is currently on cooldown.");
      return;
    }
    var runes = new Item[] { new Item(ItemId.LAW_RUNE), new Item(ItemId.DEATH_RUNE),
        new Item(ItemId.CHAOS_RUNE) };
    for (var rune : runes) {
      if (!player.getMagic().hasRune(rune)) {
        player.getMagic().notEnoughRunes();
        return;
      }
    }
    var specialRules =
        player.getArea().getWildernessLevel() > 20 || target.getArea().getWildernessLevel() > 20;
    var withinLevelDifference = Math
        .abs(player.getArea().getWildernessLevel() - target.getArea().getWildernessLevel()) <= 10;
    if (player.getInCombatDelay() > 0) {
      player.getGameEncoder().sendMessage("You can't use this right now.");
      return;
    }
    if (specialRules && !withinLevelDifference) {
      player.getGameEncoder()
          .sendMessage("You need to move closer to your target to use this spell.");
      return;
    }
    if ((!inWilderness || !specialRules) && !player.getController().canTeleport(true)) {
      return;
    }
    for (var rune : runes) {
      player.getMagic().deleteRune(rune);
    }
    var tile = new Tile(target);
    for (var i = 8; i > 0; i--) {
      if (Route.canMove(target, target.getX() - i, target.getY())) {
        tile.setTile(target.getX() - i, target.getY(), target.getHeight());
        break;
      }
      if (Route.canMove(target, target.getX() + i, target.getY())) {
        tile.setTile(target.getX() + i, target.getY(), target.getHeight());
        break;
      }
      if (Route.canMove(target, target.getX(), target.getY() - i)) {
        tile.setTile(target.getX(), target.getY() - i, target.getHeight());
        break;
      }
      if (Route.canMove(target, target.getX(), target.getY() + i)) {
        tile.setTile(target.getX(), target.getY() + i, target.getHeight());
        break;
      }
    }
    SpellTeleport.normalTeleport(player, tile);
    player.getController().stopWithTeleport();
    player.getCombat().clearHitEvents();
    teleportCooldown = 50;
  }

  public void setTeleportUnlocked(boolean teleportUnlocked) {
    this.teleportUnlocked = teleportUnlocked;
    player.getGameEncoder().setVarbit(VarbitId.MAGIC_BOUNTY_TELEPORT_UNLOCKED,
        teleportUnlocked ? 1 : 0);
  }

  public void setMinimised(boolean minimised) {
    this.minimised = minimised;
    player.getGameEncoder().setVarbit(VarbitId.BOUNTY_HUNTER_MINIMISED, minimised ? 1 : 0);
  }
}
