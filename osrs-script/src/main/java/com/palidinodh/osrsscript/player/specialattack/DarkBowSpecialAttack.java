package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.DARK_BOW, ItemId.DARK_BOW_12765, ItemId.DARK_BOW_12766, ItemId.DARK_BOW_12767,
    ItemId.DARK_BOW_12768, ItemId.DARK_BOW_20408 })
class DarkBowSpecialAttack extends SpecialAttack {
  public DarkBowSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(55);
    entry.setAnimation(426);
    entry.setTargetGraphic(new Graphic(1100, 96));
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    var player = hooks.getPlayer();
    var ammoId = player.getEquipment().getAmmoId();
    var specialProjectileId = -1;
    if (ammoId == ItemId.DRAGON_ARROW || ammoId == ItemId.DRAGON_FIRE_ARROWS
        || ammoId == ItemId.DRAGON_FIRE_ARROWS_11222 || ammoId == ItemId.DRAGON_ARROW_P
        || ammoId == ItemId.DRAGON_ARROW_P_PLUS || ammoId == ItemId.DRAGON_ARROW_P_PLUS_PLUS
        || ammoId == ItemId.DRAGON_ARROW_20389) {
      specialProjectileId = 1099;
    } else {
      specialProjectileId = 1101;
    }
    var projectile = hooks.getProjectile();
    if (hooks.getApplyAttackLoopCount() == 0) {
      player.getController()
          .sendMapProjectile(projectile.rebuilder().id(specialProjectileId).build());
    } else if (hooks.getApplyAttackLoopCount() == 1) {
      var projectileSpeed = new Graphic.ProjectileSpeed(projectile.getProjectileSpeed());
      projectileSpeed.setClientSpeed(projectileSpeed.getClientSpeed() + 14);
      player.getController().sendMapProjectile(
          projectile.rebuilder().id(specialProjectileId).speed(projectileSpeed).curve(25).build());
    }
  }
}
