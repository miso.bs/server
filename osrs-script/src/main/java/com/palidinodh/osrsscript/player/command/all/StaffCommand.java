package com.palidinodh.osrsscript.player.command.all;

import java.util.ArrayList;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.Scroll;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.UserRank;
import com.palidinodh.util.PString;
import lombok.var;

@ReferenceName("staff")
class StaffCommand implements CommandHandler {
  @Override
  public String getExample(String name) {
    return "- Show current staff online";
  }

  @Override
  public void execute(Player player, String user, String message) {
    var lines = new ArrayList<String>();
    for (var staff : player.getWorld().getStaffPlayers()) {
      var rank = "";
      if (staff.isUsergroup(UserRank.ADMINISTRATOR)) {
        rank = "Administrator";
      } else if (staff.isUsergroup(UserRank.COMMUNITY_MANAGER)) {
        rank = "Community Manager";
      } else if (staff.isUsergroup(UserRank.SENIOR_MODERATOR)) {
        rank = "Head Moderator";
      } else if (staff.isUsergroup(UserRank.MODERATOR)) {
        rank = "Moderator";
      } else if (staff.isUsergroup(UserRank.OVERSEER)) {
        rank = "Overseer";
      }
      lines.add(staff.getMessaging().getIconImage() + staff.getUsername() + " - " + rank);
    }
    Scroll.open(player, "Staff Members Online", PString.toStringArray(lines));
  }
}
