package com.palidinodh.osrsscript.player.command.admin;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.adaptive.RsGameMode;
import com.palidinodh.rs.reference.ReferenceName;
import lombok.var;

@ReferenceName("tohcim")
class ToHcimCommand implements CommandHandler, CommandHandler.AdministratorRank {
  @Override
  public String getExample(String name) {
    return "username";
  }

  @Override
  public void execute(Player player, String user, String message) {
    var player2 = player.getWorld().getPlayerByUsername(message);
    if (player2 == null) {
      player.getGameEncoder().sendMessage("Unable to find user " + message + ".");
      return;
    }
    player2.setGameMode(RsGameMode.HARDCORE_IRONMAN.ordinal());
    player2.getGameEncoder()
        .sendMessage("Your gamemode has been set to Hardcore mode by " + player.getUsername());
    player.getGameEncoder().sendMessage("Success");
  }
}
