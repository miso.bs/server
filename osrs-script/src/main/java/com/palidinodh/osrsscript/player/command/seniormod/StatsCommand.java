package com.palidinodh.osrsscript.player.command.seniormod;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.world.event.stats.StatsEvent;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("stats")
class StatsCommand implements CommandHandler, CommandHandler.SeniorModeratorRank {
  @Override
  public void execute(Player player, String name, String message) {
    player.getWorld().getWorldEvent(StatsEvent.class).open(player);
  }
}
