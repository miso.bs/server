package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.DRAGON_CLAWS, ItemId.DRAGON_CLAWS_20784, ItemId.DRAGON_CLAWS_BEGINNER_32327 })
class DragonClawsSpecialAttack extends SpecialAttack {
  public DragonClawsSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(50);
    entry.setAnimation(7514);
    entry.setCastGraphic(new Graphic(1171));
    addEntry(entry);
  }
}
