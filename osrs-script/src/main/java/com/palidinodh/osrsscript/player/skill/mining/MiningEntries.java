package com.palidinodh.osrsscript.player.skill.mining;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.ObjectId;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.entity.player.skill.SkillModel;
import com.palidinodh.osrscore.model.entity.player.skill.SkillPet;
import com.palidinodh.osrscore.model.entity.player.skill.SkillTemporaryMapObject;
import com.palidinodh.osrscore.model.item.RandomItem;
import lombok.Getter;
import lombok.var;

class MiningEntries {
  @Getter
  private static List<SkillEntry> entries = load();

  private static List<SkillEntry> load() {
    var entries = new ArrayList<SkillEntry>();

    var entry = SkillEntry.builder();
    entry.level(1).experience(17).mapObject(new SkillModel(ObjectId.ROCKS_7453, 0))
        .mapObject(new SkillModel(ObjectId.ROCKS_7484, 0)).create(new RandomItem(ItemId.COPPER_ORE))
        .pet(new SkillPet(ItemId.ROCK_GOLEM, 741600)).clueChance(741600)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_7469, 1, 4))
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_7468, 1, 4))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 200);
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(1).experience(17).mapObject(new SkillModel(ObjectId.ROCKS_7485, 0))
        .mapObject(new SkillModel(ObjectId.ROCKS_7486, 0)).create(new RandomItem(ItemId.TIN_ORE))
        .pet(new SkillPet(ItemId.ROCK_GOLEM, 741600)).clueChance(741600)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_7468, 1, 4))
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_7469, 1, 4))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 200);
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(15).experience(35).mapObject(new SkillModel(ObjectId.ROCKS_7455, 0))
        .mapObject(new SkillModel(ObjectId.ROCKS_7488, 0)).create(new RandomItem(ItemId.IRON_ORE))
        .pet(new SkillPet(ItemId.ROCK_GOLEM, 741600)).clueChance(741600)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_7469, 3, 4))
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_7468, 3, 4))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 100);
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(30).experience(50).mapObject(new SkillModel(ObjectId.ROCKS_7456, 0))
        .mapObject(new SkillModel(ObjectId.ROCKS_7489, 0)).create(new RandomItem(ItemId.COAL))
        .pet(new SkillPet(ItemId.ROCK_GOLEM, 290640)).clueChance(290640)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_7469, 7, 4))
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_7468, 7, 4))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 60);
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(40).experience(65).mapObject(new SkillModel(ObjectId.ROCKS_7458, 0))
        .mapObject(new SkillModel(ObjectId.ROCKS_7491, 0)).create(new RandomItem(ItemId.GOLD_ORE))
        .pet(new SkillPet(ItemId.ROCK_GOLEM, 290640)).clueChance(290640)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_7469, 10, 4))
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_7468, 10, 4))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 50);
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(40).experience(65).mapObject(new SkillModel(ObjectId.ROCKS_7463, 0))
        .mapObject(new SkillModel(ObjectId.ROCKS_7464, 0)).create(new RandomItem(ItemId.UNCUT_OPAL))
        .pet(new SkillPet(ItemId.ROCK_GOLEM, 211886)).clueChance(211886)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_7469, 10, 4))
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_7468, 10, 4))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 50);
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(55).experience(80).mapObject(new SkillModel(ObjectId.ROCKS_7459, 0))
        .mapObject(new SkillModel(ObjectId.ROCKS_7492, 0))
        .create(new RandomItem(ItemId.MITHRIL_ORE)).pet(new SkillPet(ItemId.ROCK_GOLEM, 148320))
        .clueChance(148320)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_7469, 13, 4))
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_7468, 13, 4))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 40);
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(70).experience(95).mapObject(new SkillModel(ObjectId.ROCKS_7460, 0))
        .mapObject(new SkillModel(ObjectId.ROCKS_7493, 0))
        .create(new RandomItem(ItemId.ADAMANTITE_ORE)).pet(new SkillPet(ItemId.ROCK_GOLEM, 59328))
        .clueChance(59328)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_7469, 17, 4))
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_7468, 17, 4))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 30);
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(85).experience(125).mapObject(new SkillModel(ObjectId.ROCKS_7461, 0))
        .mapObject(new SkillModel(ObjectId.ROCKS_7494, 0)).create(new RandomItem(ItemId.RUNITE_ORE))
        .pet(new SkillPet(ItemId.ROCK_GOLEM, 42377)).clueChance(42377)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_7469, 21, 4))
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_7468, 21, 4))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 20);
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(92).experience(240).mapObject(new SkillModel(ObjectId.CRYSTALS, 0))
        .mapObject(new SkillModel(ObjectId.CRYSTALS_30372, 0))
        .create(new RandomItem(ItemId.AMETHYST)).pet(new SkillPet(ItemId.ROCK_GOLEM, 46350))
        .clueChance(46350)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.EMPTY_WALL, 23, 4))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 20);
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(22).experience(10).mapObject(new SkillModel(ObjectId.ASH_PILE, 0))
        .create(new RandomItem(ItemId.VOLCANIC_ASH)).pet(new SkillPet(ItemId.ROCK_GOLEM, 741600))
        .clueChance(741600)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.EMPTY_ASH_PILE, 5, 4))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 80);
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(10).experience(17).mapObject(new SkillModel(ObjectId.ROCKS_7462, 0))
        .mapObject(new SkillModel(ObjectId.ROCKS_7495, 0))
        .create(new RandomItem(ItemId.BLURITE_ORE))
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_7469, 2, 4))
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.ROCKS_7468, 2, 4))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 150);
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(30).experience(5).mapObject(new SkillModel(ObjectId.RUNE_ESSENCE, 0))
        .create(new RandomItem(ItemId.PURE_ESSENCE));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(38).experience(12).mapObject(new SkillModel(ObjectId.DENSE_RUNESTONE_10796, 0))
        .mapObject(new SkillModel(ObjectId.DENSE_RUNESTONE_8981, 0))
        .create(new RandomItem(ItemId.DENSE_ESSENCE_BLOCK))
        .variable(Mining.UNIDENTIFIED_MINERAL_VARIABLE, 60);
    entries.add(entry.build());

    return entries;
  }
}
