package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.SARADOMIN_SWORD, ItemId.SARAS_BLESSED_SWORD_FULL,
    ItemId.SARADOMINS_BLESSED_SWORD })
class SaradominSwordSpecialAttack extends SpecialAttack {
  public SaradominSwordSpecialAttack() {
    var entry = new Entry(this);
    entry.addItemId(ItemId.SARADOMIN_SWORD);
    entry.setDrain(100);
    entry.setAnimation(1132);
    entry.setCastGraphic(new Graphic(1213));
    entry.setTargetGraphic(new Graphic(1196));
    entry.setDamageModifier(1.1);
    entry.setDoubleHit(true);
    addEntry(entry);

    entry = new Entry(this);
    entry.addItemId(ItemId.SARAS_BLESSED_SWORD_FULL);
    entry.addItemId(ItemId.SARADOMINS_BLESSED_SWORD);
    entry.setDrain(65);
    entry.setAnimation(1133);
    entry.setCastGraphic(new Graphic(1213));
    entry.setTargetGraphic(new Graphic(1196));
    entry.setDamageModifier(1.25);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    var entry = hooks.getEntry();
    if (entry.containsItemId(ItemId.SARADOMIN_SWORD) && hooks.getApplyAttackLoopCount() == 1
        && hooks.getLastHitEvent().getDamage() > 0) {
      hooks.setDamage(PRandom.randomI(16));
    } else if (entry.containsItemId(ItemId.SARAS_BLESSED_SWORD_FULL,
        ItemId.SARADOMINS_BLESSED_SWORD)) {
      hooks.getPlayer().getController().sendMapGraphic(hooks.getOpponent(), new Graphic(1221));
    }
  }
}
