package com.palidinodh.osrsscript.player.plugin.magic;

import java.util.List;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.widget.SpellbookChild;
import com.palidinodh.osrscore.io.cache.widget.ViewportIcon;
import com.palidinodh.osrscore.model.entity.player.AchievementDiary;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.Smithing;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.util.PCollection;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.var;

@AllArgsConstructor
@Getter
public enum SpellOnItem {
  LVL_1_ENCHANT(SpellbookChild.LVL_1_ENCHANT, 719, new Graphic(114, 92), 7, 18,
      PCollection.toImmutableList(new Item(ItemId.WATER_RUNE), new Item(ItemId.COSMIC_RUNE)),
      PCollection.toImmutableList(new LinkedItem(ItemId.SAPPHIRE_RING, ItemId.RING_OF_RECOIL),
          new LinkedItem(ItemId.SAPPHIRE_NECKLACE, ItemId.GAMES_NECKLACE_8),
          new LinkedItem(ItemId.SAPPHIRE_AMULET, ItemId.AMULET_OF_MAGIC),
          new LinkedItem(ItemId.SAPPHIRE_BRACELET, ItemId.BRACELET_OF_CLAY)),
      null),
  LVL_2_ENCHANT(SpellbookChild.LVL_2_ENCHANT, 719, new Graphic(114, 92), 27, 37,
      PCollection.toImmutableList(new Item(ItemId.AIR_RUNE, 3), new Item(ItemId.COSMIC_RUNE)),
      PCollection.toImmutableList(new LinkedItem(ItemId.EMERALD_RING, ItemId.RING_OF_DUELING_8),
          new LinkedItem(ItemId.EMERALD_NECKLACE, ItemId.BINDING_NECKLACE),
          new LinkedItem(ItemId.EMERALD_AMULET, ItemId.AMULET_OF_DEFENCE),
          new LinkedItem(ItemId.EMERALD_BRACELET, ItemId.CASTLE_WARS_BRACELET_3)),
      null),
  LVL_3_ENCHANT(SpellbookChild.LVL_3_ENCHANT, 720, new Graphic(115, 92), 49, 59,
      PCollection.toImmutableList(new Item(ItemId.FIRE_RUNE, 5), new Item(ItemId.COSMIC_RUNE)),
      PCollection.toImmutableList(new LinkedItem(ItemId.RUBY_RING, ItemId.RING_OF_FORGING),
          new LinkedItem(ItemId.RUBY_NECKLACE, ItemId.DIGSITE_PENDANT_5),
          new LinkedItem(ItemId.RUBY_AMULET, ItemId.AMULET_OF_STRENGTH),
          new LinkedItem(ItemId.RUBY_BRACELET, ItemId.INOCULATION_BRACELET)),
      null),
  LVL_4_ENCHANT(SpellbookChild.LVL_4_ENCHANT, 720, new Graphic(115, 92), 57, 67,
      PCollection.toImmutableList(new Item(ItemId.EARTH_RUNE, 10), new Item(ItemId.COSMIC_RUNE)),
      PCollection.toImmutableList(new LinkedItem(ItemId.DIAMOND_RING, ItemId.RING_OF_LIFE),
          new LinkedItem(ItemId.DIAMOND_NECKLACE, ItemId.PHOENIX_NECKLACE),
          new LinkedItem(ItemId.DIAMOND_AMULET, ItemId.AMULET_OF_POWER),
          new LinkedItem(ItemId.DIAMOND_BRACELET, ItemId.ABYSSAL_BRACELET_5)),
      null),
  LVL_5_ENCHANT(SpellbookChild.LVL_5_ENCHANT, 721, new Graphic(116, 92), 68, 78,
      PCollection.toImmutableList(new Item(ItemId.WATER_RUNE, 15), new Item(ItemId.EARTH_RUNE, 15),
          new Item(ItemId.COSMIC_RUNE)),
      PCollection.toImmutableList(new LinkedItem(ItemId.DRAGON_NECKLACE, ItemId.SKILLS_NECKLACE_4),
          new LinkedItem(ItemId.DRAGONSTONE_AMULET, ItemId.AMULET_OF_GLORY_4),
          new LinkedItem(ItemId.DRAGONSTONE_BRACELET, ItemId.COMBAT_BRACELET_4)),
      null),
  LVL_6_ENCHANT(SpellbookChild.LVL_6_ENCHANT, 721, new Graphic(452, 92), 87, 97,
      PCollection.toImmutableList(new Item(ItemId.EARTH_RUNE, 20), new Item(ItemId.FIRE_RUNE, 20),
          new Item(ItemId.COSMIC_RUNE)),
      PCollection.toImmutableList(new LinkedItem(ItemId.ONYX_RING, ItemId.RING_OF_STONE),
          new LinkedItem(ItemId.ONYX_NECKLACE, ItemId.BERSERKER_NECKLACE),
          new LinkedItem(ItemId.ONYX_AMULET, ItemId.AMULET_OF_FURY),
          new LinkedItem(ItemId.ONYX_BRACELET, ItemId.REGEN_BRACELET)),
      null),
  LVL_7_ENCHANT(SpellbookChild.LVL_7_ENCHANT, 725, new Graphic(452, 92), 93, 110,
      PCollection.toImmutableList(new Item(ItemId.BLOOD_RUNE, 20), new Item(ItemId.SOUL_RUNE, 20),
          new Item(ItemId.COSMIC_RUNE)),
      PCollection.toImmutableList(new LinkedItem(ItemId.ZENYTE_RING, ItemId.RING_OF_SUFFERING),
          new LinkedItem(ItemId.ZENYTE_NECKLACE, ItemId.NECKLACE_OF_ANGUISH),
          new LinkedItem(ItemId.ZENYTE_AMULET, ItemId.AMULET_OF_TORTURE),
          new LinkedItem(ItemId.ZENYTE_BRACELET, ItemId.TORMENTED_BRACELET)),
      null),
  SUPERHEAT_ITEM(SpellbookChild.SUPERHEAT_ITEM, 721, new Graphic(148, 92), 43, 53,
      PCollection.toImmutableList(new Item(ItemId.FIRE_RUNE, 4), new Item(ItemId.NATURE_RUNE)),
      null, (player, spell, itemSlot, itemId) -> {
        var makeId = -1;
        if (itemId == ItemId.COPPER_ORE || itemId == ItemId.TIN_ORE) {
          makeId = ItemId.BRONZE_BAR;
        } else if (itemId == ItemId.BLURITE_ORE) {
          makeId = ItemId.BLURITE_BAR;
        } else if (itemId == ItemId.IRON_ORE && player.getInventory().getCount(ItemId.COAL) >= 2) {
          makeId = ItemId.STEEL_BAR;
        } else if (itemId == ItemId.IRON_ORE) {
          makeId = ItemId.IRON_BAR;
        } else if (itemId == ItemId.SILVER_ORE) {
          makeId = ItemId.SILVER_BAR;
        } else if (itemId == ItemId.GOLD_ORE) {
          makeId = ItemId.GOLD_BAR;
        } else if (itemId == ItemId.MITHRIL_ORE) {
          makeId = ItemId.MITHRIL_BAR;
        } else if (itemId == ItemId.ADAMANTITE_ORE) {
          makeId = ItemId.ADAMANTITE_BAR;
        } else if (itemId == ItemId.RUNITE_ORE) {
          makeId = ItemId.RUNITE_BAR;
        } else {
          player.getGameEncoder().sendMessage("You can't use this spell on this item.");
          return false;
        }
        return Smithing.make1(player, makeId);
      }),
  HIGH_LEVEL_ALCHEMY(SpellbookChild.HIGH_LEVEL_ALCHEMY, 713, new Graphic(113, 92), 55, 65,
      PCollection.toImmutableList(new Item(ItemId.FIRE_RUNE, 5), new Item(ItemId.NATURE_RUNE)),
      null, (player, spell, itemSlot, itemId) -> {
        var item = player.getInventory().getItem(itemSlot);
        if (item == null || item.getInfoDef().getUntradable() || item.getInfoDef().isFree()) {
          player.getGameEncoder().sendMessage("You can't alch this item.");
          return false;
        }
        if (itemId == ItemId.COINS) {
          player.getGameEncoder().sendMessage("You can't alch this item.");
          return false;
        }
        if (itemId == ItemId.OLD_SCHOOL_BOND || itemId == ItemId.OLD_SCHOOL_BOND_UNTRADEABLE
            || itemId == ItemId._14_DAYS_PREMIUM_MEMBERSHIP_32303 || itemId == ItemId.BOND_32318) {
          player.getGameEncoder().sendMessage("You can't alch this item.");
          return false;
        }
        if (item.getDef().getName().startsWith("Blighted")) {
          player.getGameEncoder().sendMessage("You can't alch this item.");
          return false;
        }
        var value = item.getInfoDef().getHighAlch();
        if (player.getInventory().getEmptySlot() == -1 && item.getInfoDef().isStackOrNote()
            && !player.getInventory().canAddItem(ItemId.COINS, value)) {
          player.getInventory().notEnoughSpace();
          return false;
        }
        player.getInventory().deleteItem(itemId, 1, itemSlot);
        player.getInventory().addItem(ItemId.COINS, value, itemSlot);
        return true;
      });

  private SpellbookChild widgetChild;
  private int animation;
  private Graphic graphic;
  private int level;
  private int experience;
  private List<Item> runes;
  private List<LinkedItem> linkedItems;
  private Action action;

  public void onItem(Player player, int itemSlot, int itemId) {
    if (player.getHeight() != player.getClientHeight()) {
      player.getGameEncoder().sendMessage("You can't do this here.");
      return;
    }
    if (player.getSkills().getLevel(Skills.MAGIC) < level) {
      player.getGameEncoder()
          .sendMessage("You need a Magic level of " + level + " to cast this spell.");
      return;
    }
    if (runes != null && !runes.isEmpty()) {
      for (var rune : runes) {
        if (player.getMagic().hasRune(rune)) {
          continue;
        }
        player.getGameEncoder().sendMessage("You don't have enough runes to cast this spell.");
        return;
      }
    }
    LinkedItem linkedItem = null;
    if (linkedItems != null && !linkedItems.isEmpty()) {
      for (var aLinkedItem : linkedItems) {
        if (aLinkedItem.getFromItemId() != itemId) {
          continue;
        }
        linkedItem = aLinkedItem;
        break;
      }
      if (linkedItem == null) {
        player.getGameEncoder().sendMessage("You can't use this spell on this item.");
        return;
      }
    }
    if (action != null && !action.execute(player, this, itemSlot, itemId)) {
      return;
    }
    if (runes != null && !runes.isEmpty()) {
      for (var rune : runes) {
        player.getMagic().deleteRune(rune);
      }
    }
    if (linkedItem != null) {
      player.getInventory().deleteItem(linkedItem.getFromItemId(), 1, itemSlot);
      player.getInventory().addItem(linkedItem.getToItemId(), 1, itemSlot);
    }
    if (experience > 0) {
      player.getSkills().addXp(Skills.MAGIC, experience);
    }
    player.setAnimation(animation);
    player.setGraphic(graphic);
    player.getGameEncoder().sendViewingIcon(ViewportIcon.MAGIC);
    AchievementDiary.castSpellUpdate(player, widgetChild, null, null, null);
  }

  public static SpellOnItem get(SpellbookChild widgetChild) {
    for (var spell : values()) {
      if (spell.getWidgetChild() != widgetChild) {
        continue;
      }
      return spell;
    }
    return null;
  }

  @AllArgsConstructor
  @Getter
  private static class LinkedItem {
    private int fromItemId;
    private int toItemId;
  }

  private interface Action {
    boolean execute(Player player, SpellOnItem spell, int itemSlot, int itemId);
  }
}
