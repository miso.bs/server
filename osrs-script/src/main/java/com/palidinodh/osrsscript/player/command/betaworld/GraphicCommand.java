package com.palidinodh.osrsscript.player.command.betaworld;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.util.PEvent;
import lombok.var;

@ReferenceName({ "gfx", "gfxloop" })
class GraphicCommand implements CommandHandler, CommandHandler.BetaWorld {
  @Override
  public String getExample(String name) {
    switch (name) {
      case "gfx":
        return "id (height)";
      case "gfxloop":
        return "start_id";
    }
    return "";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.splitInt(message);
    var height = messages.length > 1 ? messages[1] : 0;
    switch (name) {
      case "gfx":
        player.setGraphic(messages[0], height);
        break;
      case "gfxloop": {
        var fromId = messages[0];
        var event = new PEvent(1) {
          int id = fromId;
          boolean reset = false;

          @Override
          public void execute() {
            if (reset) {
              reset = false;
              player.setAnimation(-1);
              setTick(0);
            } else {
              reset = true;
              player.getGameEncoder().sendMessage("Graphic: " + id);
              player.setGraphic(id++, height);
              setTick(1);
            }
          }
        };
        player.setAction(event);
        break;
      }
    }
  }
}
