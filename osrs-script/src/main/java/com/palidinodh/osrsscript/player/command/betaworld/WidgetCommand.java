package com.palidinodh.osrsscript.player.command.betaworld;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName({ "widget", "w" })
class WidgetCommand implements CommandHandler, CommandHandler.BetaWorld {
  @Override
  public String getExample(String name) {
    return "id";
  }

  @Override
  public void execute(Player player, String name, String message) {
    player.getWidgetManager().sendInteractiveOverlay(Integer.parseInt(message));
  }
}
