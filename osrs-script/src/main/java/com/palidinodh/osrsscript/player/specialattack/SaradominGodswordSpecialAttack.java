package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.SARADOMIN_GODSWORD, ItemId.SARADOMIN_GODSWORD_OR })
class SaradominGodswordSpecialAttack extends SpecialAttack {
  public SaradominGodswordSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(50);
    entry.setAnimation(7640);
    entry.setCastGraphic(new Graphic(1209));
    entry.setAccuracyModifier(2);
    entry.setDamageModifier(1.1);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    var damage = hooks.getDamage();
    if (damage == 0) {
      return;
    }
    var player = hooks.getPlayer();
    var heal = damage / 2;
    if (heal < 10) {
      heal = 10;
    }
    player.getCombat().changeHitpoints(heal);
    var prayer = damage / 4;
    if (prayer < 5) {
      prayer = 5;
    }
    player.getPrayer().changePoints(prayer);
  }
}
