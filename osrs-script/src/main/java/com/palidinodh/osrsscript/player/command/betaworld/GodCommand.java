package com.palidinodh.osrsscript.player.command.betaworld;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import lombok.var;

@ReferenceName("god")
class GodCommand implements CommandHandler, CommandHandler.BetaWorld {
  @Override
  public void execute(Player player, String name, String message) {
    for (var i = 0; i <= 6; i++) {
      player.getSkills().setLevel(i, 4096);
      player.getGameEncoder().sendSkillLevel(i);
    }
    player.getMovement().setEnergy(Integer.MAX_VALUE);
    player.getCombat().setHitpoints(Integer.MAX_VALUE);
    player.getPrayer().setPoints(Integer.MAX_VALUE);
    player.getGameEncoder().sendMessage("You feel like a god..");
  }
}
