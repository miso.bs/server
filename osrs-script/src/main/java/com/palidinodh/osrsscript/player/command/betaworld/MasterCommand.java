package com.palidinodh.osrsscript.player.command.betaworld;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.rs.reference.ReferenceName;
import lombok.var;

@ReferenceName("master")
class MasterCommand implements CommandHandler, CommandHandler.BetaWorld {
  @Override
  public void execute(Player player, String name, String message) {
    for (var id = 0; id < Skills.SKILL_COUNT; id++) {
      player.getSkills().setLevel(id, 99);
      player.getSkills().setXP(id, Skills.XP_TABLE[99]);
      player.getGameEncoder().sendSkillLevel(id);
    }
    player.getSkills().setCombatLevel();
    player.rejuvenate();
  }
}
