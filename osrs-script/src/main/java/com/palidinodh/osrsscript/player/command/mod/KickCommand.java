package com.palidinodh.osrsscript.player.command.mod;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.reference.ReferenceName;
import lombok.var;

@ReferenceName("kick")
class KickCommand implements CommandHandler, CommandHandler.ModeratorRank {
  @Override
  public String getExample(String name) {
    return "username";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var targetPlayer = player.getWorld().getPlayerByUsername(message);
    if (targetPlayer == null) {
      player.getGameEncoder().sendMessage("Unable to find user " + message + ".");
      return;
    }
    if (targetPlayer.getArea().inWilderness()) {
      player.getGameEncoder().sendMessage("This player is in the wilderness.");
      player.log(PlayerLogType.STAFF, "failed to kick " + targetPlayer.getLogName());
      return;
    }
    if (targetPlayer.getArea().inPvpWorld()) {
      player.getGameEncoder().sendMessage("This player is in the PvP world.");
      player.log(PlayerLogType.STAFF, "failed to kick " + targetPlayer.getLogName());
      return;
    }
    targetPlayer.getGameEncoder().sendLogout();
    targetPlayer.setVisible(false);
    targetPlayer.getGameEncoder().sendMessage(player.getUsername() + " has kicked you.");
    player.getGameEncoder().sendMessage(message + " has been kicked.");
    player.getWorld()
        .sendStaffMessage(player.getUsername() + " has kicked " + targetPlayer.getUsername() + ".");
    player.log(PlayerLogType.STAFF, "kicked " + targetPlayer.getLogName() + " from  "
        + targetPlayer.getX() + ", " + targetPlayer.getY() + ", " + targetPlayer.getHeight());
  }
}
