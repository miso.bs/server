package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.LIGHT_BALLISTA, ItemId.HEAVY_BALLISTA, ItemId.HEAVY_BALLISTA_BEGINNER_32328 })
class BallistaSpecialAttack extends SpecialAttack {
  public BallistaSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(65);
    entry.setAnimation(7222);
    entry.setAccuracyModifier(1.25);
    entry.setDamageModifier(1.25);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    hooks.getProjectile().getProjectileSpeed().setClientDelay(61);
  }
}
