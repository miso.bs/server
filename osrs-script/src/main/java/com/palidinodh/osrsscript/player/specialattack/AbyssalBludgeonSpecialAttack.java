package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.ABYSSAL_BLUDGEON)
class AbyssalBludgeonSpecialAttack extends SpecialAttack {
  public AbyssalBludgeonSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(50);
    entry.setAnimation(3299);
    entry.setTargetGraphic(new Graphic(1284));
    addEntry(entry);
  }
}
