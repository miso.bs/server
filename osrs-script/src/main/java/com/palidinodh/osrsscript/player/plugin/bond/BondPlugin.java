package com.palidinodh.osrsscript.player.plugin.bond;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.google.inject.Inject;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.util.PNumber;
import lombok.Getter;
import lombok.Setter;
import lombok.var;

@Getter
public class BondPlugin implements PlayerPlugin {
  @Inject
  private transient Player player;

  @Setter
  private long pouch;
  @Setter
  private long totalPurchased;
  @Setter
  private long totalRedeemed;
  @Setter
  private boolean hideRankIcon;
  private List<DonatorEffectType> disabledEffects;

  @Override
  public void loadLegacy(Map<String, Object> map) {
    if (map.containsKey("player.bondPouch")) {
      if (map.get("player.bondPouch") instanceof Long) {
        pouch = (long) map.get("player.bondPouch");
      } else if (map.get("player.bondPouch") instanceof Integer) {
        pouch = (int) map.get("player.bondPouch");
      }
    }
    if (map.containsKey("bonds.totalPurchased")) {
      totalPurchased = (long) map.get("bonds.totalPurchased");
    }
    if (map.containsKey("bonds.totalRedeemed")) {
      totalRedeemed = (long) map.get("bonds.totalRedeemed");
    }
    if (map.containsKey("bonds.hideRankIcon")) {
      hideRankIcon = (boolean) map.get("bonds.hideRankIcon");
    }
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("bond_plugin_send_pouch")) {
      sendPouch();
    }
    if (name.equals("bond_plugin_is_donator_effect_enabled")) {
      return isDonatorEffectEnabled(DonatorEffectType.valueOf(((String) args[0]).toUpperCase()))
          ? Boolean.TRUE
          : Boolean.FALSE;
    }
    if (name.equals("bond_plugin_can_donator_pickup_item")) {
      return canDonatorPickupItem((int) args[0]) ? Boolean.TRUE : Boolean.FALSE;
    }
    if (name.equals("bond_plugin_pickup_type_is_pickup_item_id")) {
      return canDonatorNoteItem((int) args[0]) ? Boolean.TRUE : Boolean.FALSE;
    }
    if (name.equals("bond_plugin_hide_rank_icon")) {
      return hideRankIcon ? Boolean.TRUE : Boolean.FALSE;
    }
    if (name.equals("bond_plugin_add_bonds")) {
      var count = (int) args[0];
      pouch += count;
      totalPurchased += count;
    }
    return null;
  }

  @Override
  public void login() {
    sendPouchCounts();
  }

  @Override
  public int getCurrency(String identifier) {
    if (identifier.equals("bonds")) {
      return (int) Math.min(pouch, Item.MAX_AMOUNT);
    }
    return 0;
  }

  @Override
  public void changeCurrency(String identifier, int amount) {
    if (identifier.equals("bonds")) {
      pouch += amount;
      if (amount < 0) {
        totalRedeemed += -amount;
        sendPouchCounts();
      }
    }
  }

  public void sendPouch() {
    player.getWidgetManager().sendInteractiveOverlay(WidgetId.BOND_POUCH_704);
    sendPouchCounts();
  }

  public void sendPouchCounts() {
    var inventoryCount = player.getInventory().getCount(ItemId.BOND_32318);
    var totalBonds = totalPurchased + totalRedeemed;
    var rankName = "None";
    for (var rank : DonatorRankType.values()) {
      if (totalBonds < rank.getBonds()) {
        break;
      }
      player.addUsergroup(rank.getRank());
      rankName = rank.getFormattedName();
    }
    if (player.getWidgetManager().readInteractiveOverlay() == WidgetId.BOND_POUCH_704) {
      player.getGameEncoder().sendWidgetText(WidgetId.BOND_POUCH_704, 36,
          PNumber.formatNumber(inventoryCount));
      player.getGameEncoder().sendWidgetText(WidgetId.BOND_POUCH_704, 37,
          PNumber.formatNumber(pouch));
      player.getGameEncoder().sendWidgetText(WidgetId.BOND_POUCH_704, 43,
          PNumber.formatNumber(totalPurchased));
      player.getGameEncoder().sendWidgetText(WidgetId.BOND_POUCH_704, 47,
          PNumber.formatNumber(totalRedeemed));
      player.getGameEncoder().sendWidgetText(WidgetId.BOND_POUCH_704, 52,
          PNumber.formatNumber(totalBonds));
      player.getGameEncoder().sendWidgetText(WidgetId.BOND_POUCH_704, 56, "Rank: " + rankName);
      player.getGameEncoder().sendWidgetText(WidgetId.BOND_POUCH_704, 77,
          (hideRankIcon ? "Show" : "Hide") + " Chat Icon");
    }
  }

  public boolean isDonatorEffectEnabled(DonatorEffectType type) {
    if (!player.isUsergroup(type.getMinimumRank())) {
      return false;
    }
    return disabledEffects == null || !disabledEffects.contains(type);
  }

  public boolean canDonatorPickupItem(int itemId) {
    for (var type : DonatorEffectType.values()) {
      if (!isDonatorEffectEnabled(type)) {
        continue;
      }
      if (type.getPickupItemIds() == null) {
        continue;
      }
      if (!type.isPickupItemId(itemId)) {
        continue;
      }
      return true;
    }
    return false;
  }

  public boolean canDonatorNoteItem(int itemId) {
    for (var type : DonatorEffectType.values()) {
      if (!isDonatorEffectEnabled(type)) {
        continue;
      }
      if (type.getPickupType() == null) {
        continue;
      }
      if (!type.getPickupType().isPickupItemId(itemId)) {
        continue;
      }
      return true;
    }
    return false;
  }

  public void toggleDonatorEffect(DonatorEffectType type) {
    if (!player.isUsergroup(type.getMinimumRank())) {
      return;
    }
    if (disabledEffects == null) {
      disabledEffects = new ArrayList<>();
    }
    if (disabledEffects.contains(type)) {
      disabledEffects.remove(type);
    } else {
      disabledEffects.add(type);
    }
  }
}
