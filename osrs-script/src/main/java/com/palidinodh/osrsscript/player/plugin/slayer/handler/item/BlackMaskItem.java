package com.palidinodh.osrsscript.player.plugin.slayer.handler.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerUnlock;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrsscript.player.plugin.slayer.SlayerPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.BLACK_MASK, ItemId.BLACK_MASK_1, ItemId.BLACK_MASK_2, ItemId.BLACK_MASK_3,
    ItemId.BLACK_MASK_4, ItemId.BLACK_MASK_5, ItemId.BLACK_MASK_6, ItemId.BLACK_MASK_7,
    ItemId.BLACK_MASK_8, ItemId.BLACK_MASK_9, ItemId.BLACK_MASK_10, ItemId.BLACK_MASK_I,
    ItemId.BLACK_MASK_1_I, ItemId.BLACK_MASK_2_I, ItemId.BLACK_MASK_3_I, ItemId.BLACK_MASK_4_I,
    ItemId.BLACK_MASK_5_I, ItemId.BLACK_MASK_6_I, ItemId.BLACK_MASK_7_I, ItemId.BLACK_MASK_8_I,
    ItemId.BLACK_MASK_9_I, ItemId.BLACK_MASK_10_I })
class BlackMaskItem implements ItemHandler {
  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    var onSlot = onItem.getSlot();
    var plugin = player.getPlugin(SlayerPlugin.class);
    var blackMaskId = ItemHandler.getMatch(useItem, onItem, ItemId.BLACK_MASK, ItemId.BLACK_MASK_1,
        ItemId.BLACK_MASK_2, ItemId.BLACK_MASK_3, ItemId.BLACK_MASK_4, ItemId.BLACK_MASK_5,
        ItemId.BLACK_MASK_6, ItemId.BLACK_MASK_7, ItemId.BLACK_MASK_8, ItemId.BLACK_MASK_9,
        ItemId.BLACK_MASK_10);
    if (blackMaskId != -1) {
      if (!plugin.isUnlocked(SlayerUnlock.MALEVOLENT_MASQUERADE)) {
        player.getGameEncoder().sendMessage("You need to unlock this feature first.");
        return true;
      }
      if (!player.getInventory().hasItem(ItemId.EARMUFFS)
          || !player.getInventory().hasItem(ItemId.FACEMASK)
          || !player.getInventory().hasItem(ItemId.NOSE_PEG)
          || !player.getInventory().hasItem(ItemId.SPINY_HELMET)
          || !player.getInventory().hasItem(ItemId.ENCHANTED_GEM)) {
        player.getGameEncoder()
            .sendMessage("You don't have all the required items to put these together.");
        return true;
      }
      player.getInventory().deleteItem(blackMaskId);
      player.getInventory().deleteItem(ItemId.EARMUFFS);
      player.getInventory().deleteItem(ItemId.FACEMASK);
      player.getInventory().deleteItem(ItemId.NOSE_PEG);
      player.getInventory().deleteItem(ItemId.SPINY_HELMET);
      player.getInventory().deleteItem(ItemId.ENCHANTED_GEM);
      player.getInventory().addItem(ItemId.SLAYER_HELMET, 1, onSlot);
      return true;
    }
    var blackMaskIId = ItemHandler.getMatch(useItem, onItem, ItemId.BLACK_MASK_I,
        ItemId.BLACK_MASK_1_I, ItemId.BLACK_MASK_2_I, ItemId.BLACK_MASK_3_I, ItemId.BLACK_MASK_4_I,
        ItemId.BLACK_MASK_5_I, ItemId.BLACK_MASK_6_I, ItemId.BLACK_MASK_7_I, ItemId.BLACK_MASK_8_I,
        ItemId.BLACK_MASK_9_I, ItemId.BLACK_MASK_10_I);
    if (blackMaskIId != -1) {
      if (!plugin.isUnlocked(SlayerUnlock.MALEVOLENT_MASQUERADE)) {
        player.getGameEncoder().sendMessage("You need to unlock this feature first.");
        return true;
      }
      if (!player.getInventory().hasItem(ItemId.EARMUFFS)
          || !player.getInventory().hasItem(ItemId.FACEMASK)
          || !player.getInventory().hasItem(ItemId.NOSE_PEG)
          || !player.getInventory().hasItem(ItemId.SPINY_HELMET)
          || !player.getInventory().hasItem(ItemId.ENCHANTED_GEM)) {
        player.getGameEncoder()
            .sendMessage("You don't have all the required items to put these together.");
        return true;
      }
      player.getInventory().deleteItem(blackMaskIId);
      player.getInventory().deleteItem(ItemId.EARMUFFS);
      player.getInventory().deleteItem(ItemId.FACEMASK);
      player.getInventory().deleteItem(ItemId.NOSE_PEG);
      player.getInventory().deleteItem(ItemId.SPINY_HELMET);
      player.getInventory().deleteItem(ItemId.ENCHANTED_GEM);
      player.getInventory().addItem(ItemId.SLAYER_HELMET_I, 1, onSlot);
      return true;
    }
    return false;
  }
}
