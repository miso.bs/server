package com.palidinodh.osrsscript.player.command.betaworld;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("debug")
class DebugCommand implements CommandHandler, CommandHandler.BetaWorld {
  @Override
  public void execute(Player player, String name, String message) {
    player.getOptions().setDebug(!player.getOptions().isDebug());
    player.getGameEncoder().sendMessage("Debug: " + player.getOptions().isDebug());
  }
}
