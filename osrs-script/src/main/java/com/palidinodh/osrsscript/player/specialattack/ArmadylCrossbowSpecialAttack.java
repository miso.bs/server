package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.ARMADYL_CROSSBOW)
class ArmadylCrossbowSpecialAttack extends SpecialAttack {
  public ArmadylCrossbowSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(40);
    entry.setAnimation(4230);
    entry.setProjectileId(301);
    entry.setAccuracyModifier(2);
    addEntry(entry);
  }
}
