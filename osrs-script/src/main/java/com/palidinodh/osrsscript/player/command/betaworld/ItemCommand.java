package com.palidinodh.osrsscript.player.command.betaworld;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import lombok.var;

@ReferenceName({ "item", "i" })
class ItemCommand implements CommandHandler, CommandHandler.BetaWorld {
  @Override
  public String getExample(String name) {
    return "id_or_name (quantity)";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.split(message);
    var id = messages[0].matches("[0-9]+") ? Integer.parseInt(messages[0])
        : ItemId.valueOf(messages[0].replace(" ", "_").toUpperCase());
    if (id == -1) {
      player.getGameEncoder().sendMessage("Couldn't find item.");
      return;
    }
    var amount = messages.length == 2 ? Integer.parseInt(messages[1]) : 1;
    player.getInventory().addItem(id, amount);
  }
}
