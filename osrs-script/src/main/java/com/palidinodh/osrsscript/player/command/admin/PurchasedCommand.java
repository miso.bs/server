package com.palidinodh.osrsscript.player.command.admin;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrsscript.player.plugin.bond.BondPlugin;
import com.palidinodh.rs.reference.ReferenceName;
import lombok.var;

@ReferenceName("setpurchased")
class PurchasedCommand implements CommandHandler, CommandHandler.AdministratorRank {
  @Override
  public String getExample(String name) {
    return "\"username\" amount";
  }

  @Override
  public void execute(Player player, String user, String message) {
    var messages = CommandHandler.split(message);
    var username = messages[0].replace("_", " ");
    var amount = Integer.parseInt(messages[1]);
    var player2 = player.getWorld().getPlayerByUsername(username);
    if (player2 == null) {
      player.getGameEncoder().sendMessage("Unable to find player.");
      return;
    }
    var plugin = player2.getPlugin(BondPlugin.class);
    if (amount == 0) {
      plugin.setTotalPurchased(0);
      player.getGameEncoder().sendMessage("Reset purchased bonds for " + player2.getUsername());
      player2.getGameEncoder().sendMessage(player.getUsername() + " reset your purchased bonds.");
    } else {
      plugin.setTotalPurchased(plugin.getTotalPurchased() + amount);
      player.getGameEncoder()
          .sendMessage("Added " + amount + " purchased bonds to " + player2.getUsername());
      player2.getGameEncoder().sendMessage(
          player.getUsername() + " added " + amount + " purchased bonds to your account.");
    }
  }
}
