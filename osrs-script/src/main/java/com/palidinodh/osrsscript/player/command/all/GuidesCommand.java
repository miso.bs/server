package com.palidinodh.osrsscript.player.command.all;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.Settings;

@ReferenceName({ "guide", "guides" })
class GuidesCommand implements CommandHandler {
  @Override
  public String getExample(String name) {
    return "- Opens up guides on the forums";
  }

  @Override
  public void execute(Player player, String user, String message) {
    player.getGameEncoder().sendOpenUrl(Settings.getInstance().getGuidesUrl());
  }
}
