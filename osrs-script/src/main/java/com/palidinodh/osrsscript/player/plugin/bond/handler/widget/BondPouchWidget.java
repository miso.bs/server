package com.palidinodh.osrsscript.player.plugin.bond.handler.widget;

import java.util.ArrayList;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.LargeOptionsDialogue;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.entity.player.guide.Guide;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrsscript.player.plugin.bond.BondPlugin;
import com.palidinodh.osrsscript.player.plugin.bond.DonatorEffectType;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.rs.setting.UserRank;
import lombok.var;

@ReferenceId(WidgetId.BOND_POUCH_704)
class BondWidget implements WidgetHandler {
  @Override
  public void widgetOption(Player player, int option, int widgetId, int childId, int slot,
      int itemId) {
    var plugin = player.getPlugin(BondPlugin.class);
    switch (childId) {
      case 28:
        var inventoryCount = player.getInventory().getCount(ItemId.BOND_32318);
        if (inventoryCount == 0) {
          player.getGameEncoder().sendMessage("You have no bonds in your inventory.");
          break;
        }
        if (option == 0) {
          player.getInventory().deleteItem(ItemId.BOND_32318, 1);
          plugin.setPouch(plugin.getPouch() + 1);
        } else if (option == 1) {
          player.getGameEncoder().sendEnterAmount(value -> {
            var inventoryCount2 = player.getInventory().getCount(ItemId.BOND_32318);
            value = Math.min(value, inventoryCount2);
            player.getInventory().deleteItem(ItemId.BOND_32318, value);
            plugin.setPouch(plugin.getPouch() + value);
            plugin.sendPouchCounts();
          });
        } else if (option == 2) {
          player.getInventory().deleteItem(ItemId.BOND_32318, inventoryCount);
          plugin.setPouch(plugin.getPouch() + inventoryCount);
        }
        break;
      case 29:
        if (plugin.getPouch() == 0) {
          player.getGameEncoder().sendMessage("You have no bonds in your pouch.");
          break;
        }
        if (option == 0) {
          var maxWithdraw = (int) Math.min(plugin.getPouch(), Item.MAX_AMOUNT);
          maxWithdraw = Math.min(1, maxWithdraw);
          maxWithdraw = player.getInventory().canAddAmount(ItemId.BOND_32318, maxWithdraw);
          if (maxWithdraw == 0) {
            player.getInventory().notEnoughSpace();
            break;
          }
          plugin.setPouch(plugin.getPouch() - maxWithdraw);
          player.getInventory().addItem(ItemId.BOND_32318, maxWithdraw);
        } else if (option == 1) {
          player.getGameEncoder().sendEnterAmount(value -> {
            var maxWithdraw = (int) Math.min(plugin.getPouch(), Item.MAX_AMOUNT);
            value = Math.min(value, maxWithdraw);
            maxWithdraw = player.getInventory().canAddAmount(ItemId.BOND_32318, maxWithdraw);
            if (maxWithdraw == 0) {
              player.getInventory().notEnoughSpace();
              return;
            }
            plugin.setPouch(plugin.getPouch() - value);
            player.getInventory().addItem(ItemId.BOND_32318, value);
            plugin.sendPouchCounts();
          });
        } else if (option == 2) {
          var maxWithdraw = (int) Math.min(plugin.getPouch(), Item.MAX_AMOUNT);
          maxWithdraw = player.getInventory().canAddAmount(ItemId.BOND_32318, maxWithdraw);
          if (maxWithdraw == 0) {
            player.getInventory().notEnoughSpace();
            break;
          }
          plugin.setPouch(plugin.getPouch() - maxWithdraw);
          player.getInventory().addItem(ItemId.BOND_32318, maxWithdraw);
        }
        break;
      case 69:
        player.getGameEncoder().sendOpenUrl(Settings.getInstance().getStoreUrl());
        break;
      case 71:
        player.openShop("bond");
        break;
      case 73:
        Guide.openEntry(player, "main", "bonds");
        break;
      case 75:
        donatorDialogue(player);
        break;
      case 77:
        plugin.setHideRankIcon(!plugin.isHideRankIcon());
        player.getGameEncoder().sendMessage("Hide rank: " + plugin.isHideRankIcon());
        break;
    }
    plugin.sendPouchCounts();
  }

  public void donatorDialogue(Player player) {
    player.openDialogue(new OptionsDialogue(new DialogueOption("Skin Colors", (c, s) -> {
      skinColorDialogue(player);
    }), new DialogueOption("Effects", (c, s) -> {
      effectsDialogue(player);
    })));
  }

  public void skinColorDialogue(Player player) {
    player.openDialogue(new LargeOptionsDialogue(new DialogueOption("Default", (c, s) -> {
      player.getAppearance().setColor(4, 1);
    }), new DialogueOption("Opal", (c, s) -> {
      if (!player.isUsergroup(UserRank.OPAL_MEMBER)) {
        player.getGameEncoder().sendMessage("Only opal members or above can use this.");
        return;
      }
      player.getAppearance().setColor(4, 16);
    }), new DialogueOption("Jade", (c, s) -> {
      if (!player.isUsergroup(UserRank.JADE_MEMBER)) {
        player.getGameEncoder().sendMessage("Only jade members and above can use this.");
        return;
      }
      player.getAppearance().setColor(4, 17);
    }), new DialogueOption("Topaz", (c, s) -> {
      if (!player.isUsergroup(UserRank.TOPAZ_MEMBER)) {
        player.getGameEncoder().sendMessage("Only topaz members and above can use this.");
        return;
      }
      player.getAppearance().setColor(4, 18);
    }), new DialogueOption("Sapphire", (c, s) -> {
      if (!player.isUsergroup(UserRank.SAPPHIRE_MEMBER)) {
        player.getGameEncoder().sendMessage("Only sapphire members and above can use this.");
        return;
      }
      player.getAppearance().setColor(4, 19);
    }), new DialogueOption("Emerald", (c, s) -> {
      if (!player.isUsergroup(UserRank.EMERALD_MEMBER)) {
        player.getGameEncoder().sendMessage("Only emerald members can use this.");
        return;
      }
      player.getAppearance().setColor(4, 20);
    }), new DialogueOption("Ruby", (c, s) -> {
      if (!player.isUsergroup(UserRank.RUBY_MEMBER)) {
        player.getGameEncoder().sendMessage("Only ruby members can use this.");
        return;
      }
      player.getAppearance().setColor(4, 21);
    }), new DialogueOption("Diamond", (c, s) -> {
      if (!player.isUsergroup(UserRank.DIAMOND_MEMBER)) {
        player.getGameEncoder().sendMessage("Only diamond members can use this.");
        return;
      }
      player.getAppearance().setColor(4, 22);
    }), new DialogueOption("Dragonstone", (c, s) -> {
      if (!player.isUsergroup(UserRank.DRAGONSTONE_MEMBER)) {
        player.getGameEncoder().sendMessage("Only dragonstone members can use this.");
        return;
      }
      player.getAppearance().setColor(4, 23);
    }), new DialogueOption("Onyx", (c, s) -> {
      if (!player.isUsergroup(UserRank.ONYX_MEMBER)) {
        player.getGameEncoder().sendMessage("Only onyx members can use this.");
        return;
      }
      player.getAppearance().setColor(4, 24);
    })));
  }

  public void effectsDialogue(Player player) {
    var plugin = player.getPlugin(BondPlugin.class);
    var effects = DonatorEffectType.values();
    var effectOptions = new ArrayList<String>();
    for (var effect : effects) {
      effectOptions.add(effect.getFormattedName() + ": " + plugin.isDonatorEffectEnabled(effect));
    }
    player.openDialogue(
        new LargeOptionsDialogue(DialogueOption.toOptions(effectOptions)).action((c, s) -> {
          if (s >= effects.length) {
            return;
          }
          plugin.toggleDonatorEffect(effects[s]);
          effectsDialogue(player);
        }));
  }
}
