package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.DARKLIGHT, ItemId.ARCLIGHT })
class DarklightSpecialAttack extends SpecialAttack {
  public DarklightSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(50);
    entry.setAnimation(2890);
    entry.setTargetGraphic(new Graphic(483));
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    if (hooks.getDamage() == 0) {
      return;
    }
    var opponent = hooks.getOpponent();
    var changeAmount = opponent.getCombat().getDefenceLevel() * 0.05;
    if (opponent.isNpc() && opponent.asNpc().getCombatDef().isTypeDemon()) {
      changeAmount = opponent.getCombat().getDefenceLevel() * 0.1;
    }
    opponent.getCombat().changeDefenceLevel((int) -changeAmount);
  }
}
