package com.palidinodh.osrsscript.player.plugin.bond;

import com.palidinodh.rs.setting.UserRank;
import com.palidinodh.util.PString;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum DonatorRankType {
  OPAL(UserRank.OPAL_MEMBER, 200), // $10-$20
  JADE(UserRank.JADE_MEMBER, 500), // $25-$50
  TOPAZ(UserRank.TOPAZ_MEMBER, 1_000), // $50-$100
  SAPPHIRE(UserRank.SAPPHIRE_MEMBER, 2_500), // $125-$250
  EMERALD(UserRank.EMERALD_MEMBER, 5_000), // $250-$500
  RUBY(UserRank.RUBY_MEMBER, 10_000), // $500-$1,000
  DIAMOND(UserRank.DIAMOND_MEMBER, 20_000), // $1,000-$2,000
  DRAGONSTONE(UserRank.DRAGONSTONE_MEMBER, 30_000), // $1,500-$3,000
  ONYX(UserRank.ONYX_MEMBER, 40_000); // $2,000-$4,000

  private UserRank rank;
  private int bonds;

  public String getFormattedName() {
    return PString.formatName(name().toLowerCase().replace('_', ' '));
  }
}
