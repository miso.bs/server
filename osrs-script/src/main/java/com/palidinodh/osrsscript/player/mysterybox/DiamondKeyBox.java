package com.palidinodh.osrsscript.player.mysterybox;

import java.util.List;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.MysteryBox;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.DIAMOND_KEY_32309)
class DiamondKeyBox extends MysteryBox {
  private static List<RandomItem> items = RandomItem.combine(ItemTables.RARE, ItemTables.UNCOMMON);
  private static List<RandomItem> weightless = RandomItem.weightless(items);

  @Override
  public Item getRandomItem(Player player) {
    return RandomItem.getItem(items);
  }

  @Override
  public List<RandomItem> getAllItems(Player player) {
    return weightless;
  }
}
