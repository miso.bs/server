package com.palidinodh.osrsscript.player.plugin.clanwars.handler.widget;

import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrsscript.player.plugin.clanwars.ClanWarsPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(WidgetId.TOURNAMENT_VIEWER)
class TournamentViewerWidget implements WidgetHandler {
  @Override
  public boolean isLockedUsable() {
    return true;
  }

  @Override
  public void widgetOption(Player player, int option, int widgetId, int childId, int slot,
      int itemId) {
    if (player.getMovement().isTeleportStateStarted()
        || player.getMovement().isTeleportStateFinished() || player.getMovement().isViewing()) {
      return;
    }
    switch (childId) {
      case 8:
        player.getPlugin(ClanWarsPlugin.class).teleportViewing(-1);
        break;
    }
  }
}
