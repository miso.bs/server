package com.palidinodh.osrsscript.player.plugin.bountyhunter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.var;

@AllArgsConstructor
@Getter
enum RiskTier {
  FIRST(100_000, 1), //
  SECOND(500_000, 2),
  THIRD(1_100_000, 3),
  FOURTH(2_500_000, 4),
  FIFTH(5_000_000, 5);

  private int value;
  private int varbitValue;

  public static RiskTier getByValue(long value) {
    var values = values();
    for (var i = values.length - 1; i >= 0; i--) {
      if (value >= values[i].getValue()) {
        return values[i];
      }
    }
    return values[0];
  }
}
