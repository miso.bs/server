package com.palidinodh.osrsscript.player.mysterybox;

import java.util.List;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.MysteryBox;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.GOLD_KEY_32308)
class GoldKeyBox extends MysteryBox {
  private static List<RandomItem> items =
      RandomItem.combine(ItemTables.UNCOMMON, ItemTables.COMMON);
  private static List<RandomItem> weightless = RandomItem.weightless(items);

  @Override
  public Item getRandomItem(Player player) {
    return RandomItem.getItem(items);
  }

  @Override
  public List<RandomItem> getAllItems(Player player) {
    return weightless;
  }
}
