package com.palidinodh.osrsscript.player.plugin.lootingbag.handler.widget;

import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrsscript.player.plugin.lootingbag.LootingBagPlugin;
import com.palidinodh.osrsscript.player.plugin.lootingbag.LootingBagStoreType;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(WidgetId.LOOTING_BAG_DEPOSIT)
class LootingBagDepositWidget implements WidgetHandler {
  @Override
  public void widgetOption(Player player, int option, int widgetId, int childId, int slot,
      int itemId) {
    var plugin = player.getPlugin(LootingBagPlugin.class);
    switch (childId) {
      case 5:
        if (option == 0) {
          plugin.storeItemFromInventory(slot, LootingBagStoreType.STORE_1);
        } else if (option == 1) {
          plugin.storeItemFromInventory(slot, LootingBagStoreType.STORE_5);
        } else if (option == 2) {
          plugin.storeItemFromInventory(slot, LootingBagStoreType.STORE_ALL);
        } else if (option == 3) {
          plugin.storeItemFromInventory(slot, LootingBagStoreType.STORE_X);
        }
    }
  }
}
