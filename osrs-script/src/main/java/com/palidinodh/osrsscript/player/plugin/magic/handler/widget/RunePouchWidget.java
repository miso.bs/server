package com.palidinodh.osrsscript.player.plugin.magic.handler.widget;

import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(WidgetId.RUNE_POUCH)
class RunePouchWidget implements WidgetHandler {
  @Override
  public void widgetOption(Player player, int option, int widgetId, int childId, int slot,
      int itemId) {
    switch (childId) {
      case 4:
        if (option == 0) {
          player.getController().removeRunePouchRune(slot, 1);
        } else if (option == 1) {
          player.getController().removeRunePouchRune(slot, 5);
        } else if (option == 2) {
          player.getController().removeRunePouchRune(slot, Item.MAX_AMOUNT);
        } else if (option == 3) {
          player.getGameEncoder()
              .sendEnterAmount(value -> player.getController().removeRunePouchRune(slot, value));
        }
        break;
      case 8:
        if (option == 0) {
          player.getController().addRunePouchRune(slot, 1);
        } else if (option == 1) {
          player.getController().addRunePouchRune(slot, 5);
        } else if (option == 2) {
          player.getController().addRunePouchRune(slot, Item.MAX_AMOUNT);
        } else if (option == 3) {
          player.getGameEncoder()
              .sendEnterAmount(value -> player.getController().addRunePouchRune(slot, value));
        }
        break;
    }
  }
}
