package com.palidinodh.osrsscript.player.plugin.slayer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.google.inject.Inject;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.cache.id.VarbitId;
import com.palidinodh.osrscore.io.cache.id.VarpId;
import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.AchievementDiary;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.entity.player.slayer.AssignedSlayerTask;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerMaster;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerTask;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerTaskIdentifier;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerUnlock;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrscore.world.CompetitiveHiscoresCategoryType;
import com.palidinodh.osrsscript.player.plugin.bountyhunter.BountyHunterPlugin;
import com.palidinodh.osrsscript.player.plugin.bountyhunter.MysteriousEmblem;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.cache.definition.osrs.NpcDefinition;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.rs.setting.UserRank;
import com.palidinodh.util.PCollection;
import com.palidinodh.util.PNumber;
import com.palidinodh.util.PTime;
import lombok.Getter;
import lombok.Setter;
import lombok.var;

public class SlayerPlugin implements PlayerPlugin {
  public static final String[] MASTER_NAMES =
      { "Turael", "Mazchna", "Vannaka", "Chaeldar", "Nieve", "Duradel" };
  public static final String WILDERNESS_MASTER = "Krystilia";
  public static final String BOSS_MASTER = "Boss Master";
  private static final int[] BLOCKED_TASK_VARBITS =
      { VarbitId.SLAYER_BLOCKED_TASK_1, VarbitId.SLAYER_BLOCKED_TASK_2,
          VarbitId.SLAYER_BLOCKED_TASK_3, VarbitId.SLAYER_BLOCKED_TASK_4,
          VarbitId.SLAYER_BLOCKED_TASK_5, VarbitId.SLAYER_BLOCKED_TASK_6 };

  @Inject
  private transient Player player;

  @Getter
  private AssignedSlayerTask task = new AssignedSlayerTask();
  private AssignedSlayerTask wildernessTask = new AssignedSlayerTask();
  @Getter
  private AssignedSlayerTask bossTask = new AssignedSlayerTask();
  private int consecutiveTasks;
  private int consecutiveWildernessTasks;
  private int totalTasks;
  private int totalBossTasks;
  private int wildernessTaskEmblemId = -1;
  private List<SlayerTaskIdentifier> blockedTasks;
  private List<SlayerUnlock> unlocks;
  @Getter
  private int crystalKeys;
  @Getter
  private int brimstoneKeys;
  @Getter
  private int larransKeys;
  @Getter
  private int supplyBoxes;
  @Getter
  private int gemBags;
  @Getter
  private int herbBoxes;
  @Getter
  private int barBoxes;
  @Getter
  @Setter
  private int points;
  private int bossPoints;

  @Override
  @SuppressWarnings("rawtypes")
  public void loadLegacy(Map<String, Object> map) {
    if (!map.containsKey("slayer.wildernessOnly") && map.containsKey("slayer.taskAmount")) {
      task = new AssignedSlayerTask((String) map.get("slayer.taskMaster"),
          SlayerTaskIdentifier.get((int) map.get("slayer.taskConfig")),
          (String) map.get("slayer.taskName"), (int) map.get("slayer.taskAmount"), PTime.getDate());
    }
    if (map.containsKey("slayer.wildernessTaskAmount")) {
      wildernessTask = new AssignedSlayerTask((String) map.get("slayer.wildernessTaskMaster"),
          SlayerTaskIdentifier.get((int) map.get("slayer.wildernessTaskConfig")),
          (String) map.get("slayer.wildernessTaskName"),
          (int) map.get("slayer.wildernessTaskAmount"),
          (String) map.get("slayer.wildernessTaskDate"));
    }
    if (map.containsKey("slayer.bossTaskAmount")) {
      bossTask = new AssignedSlayerTask(BOSS_MASTER, null, (String) map.get("slayer.bossTaskNamr"),
          (int) map.get("slayer.bossTaskAmount"), (String) map.get("slayer.bossTaskDate"));
    }
    if (map.containsKey("slayer.consecutiveTasks")) {
      consecutiveTasks = (int) map.get("slayer.consecutiveTasks");
    }
    if (map.containsKey("slayer.consecutiveWildernessTasks")) {
      consecutiveWildernessTasks = (int) map.get("slayer.consecutiveWildernessTasks");
    }
    if (map.containsKey("slayer.totalTasks")) {
      totalTasks = (int) map.get("slayer.totalTasks");
    }
    if (map.containsKey("slayer.points")) {
      points = (int) map.get("slayer.points");
    }
    if (map.containsKey("slayer.blockedTasks")) {
      List<Integer> oldBlockedTasks =
          PCollection.castList((List) map.get("slayer.blockedTasks"), Integer.class);
      blockedTasks = new ArrayList<>();
      for (int blockedTask : oldBlockedTasks) {
        SlayerTaskIdentifier identifier = SlayerTaskIdentifier.get(blockedTask);
        if (identifier == null || blockedTasks.contains(identifier)) {
          continue;
        }
        blockedTasks.add(identifier);
      }
    }
    if (map.containsKey("slayer.bossPoints")) {
      bossPoints = (int) map.get("slayer.bossPoints");
    }
    if (map.containsKey("slayer.totalBossTasks")) {
      totalBossTasks = (int) map.get("slayer.totalBossTasks");
    }
    if (map.containsKey("slayer.unlocks")) {
      List<Integer> oldUnlocks =
          PCollection.castList((List) map.get("slayer.unlocks"), Integer.class);
      unlocks = new ArrayList<>();
      for (int unlock : oldUnlocks) {
        SlayerUnlock slayerUnlock = SlayerUnlock.get(unlock);
        if (slayerUnlock == null || unlocks.contains(slayerUnlock)) {
          continue;
        }
        unlocks.add(slayerUnlock);
      }
    }
    if (map.containsKey("slayer.brimstoneKeys")) {
      brimstoneKeys = (int) map.get("slayer.brimstoneKeys");
    }
  }

  @Override
  public void login() {
    sendVarps();
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("slayer_is_any_task")) {
      var npcId = args[0] instanceof Npc ? ((Npc) args[0]).getId() : (int) args[0];
      return isAnyTask(npcId, NpcDefinition.getName(npcId));
    } else if (name.equals("slayer_is_task")) {
      var npc = (Npc) args[0];
      return isTask(task, npc.getId(), npc.getName());
    } else if (name.equals("slayer_is_wilderness_task")) {
      var npc = (Npc) args[0];
      return isTask(wildernessTask, npc.getId(), npc.getName());
    } else if (name.equals("slayer_is_boss_task")) {
      var npc = (Npc) args[0];
      return isTask(bossTask, npc.getId(), npc.getName());
    } else if (name.equals("slayer_is_unlocked")) {
      return isUnlocked((SlayerUnlock) args[0]);
    } else if (name.equals("slayer_get_task")) {
      var plural = task.getQuantity() > 1
          ? (task.getName().endsWith("ss") ? "es" : (task.getName().endsWith("s") ? "'" : "s"))
          : "";
      return !task.isComplete() ? task.getQuantity() + " " + task.getName() + plural : "none";
    } else if (name.equals("slayer_get_wilderness_task")) {
      var plural =
          wildernessTask.getQuantity() > 1
              ? (wildernessTask.getName().endsWith("ss") ? "es"
                  : (wildernessTask.getName().endsWith("s") ? "'" : "s"))
              : "";
      return !wildernessTask.isComplete()
          ? wildernessTask.getQuantity() + " " + wildernessTask.getName() + plural
          : "none";
    } else if (name.equals("slayer_get_boss_task")) {
      var plural =
          bossTask.getQuantity() > 1
              ? (bossTask.getName().endsWith("ss") ? "es"
                  : (bossTask.getName().endsWith("s") ? "'" : "s"))
              : "";
      return !bossTask.isComplete() ? bossTask.getQuantity() + " " + bossTask.getName() + plural
          : "none";
    } else if (name.equals("slayer_send_information")) {
      player.getGameEncoder().sendMessage(
          "Slayer Task Streaks: " + consecutiveTasks + " / " + consecutiveWildernessTasks);
      player.getGameEncoder().sendMessage("Slayer Points: " + PNumber.formatNumber(points)
          + "; Boss Slayer Points: " + PNumber.formatNumber(bossPoints));
      player.getGameEncoder().sendMessage("Total Slayer Tasks: " + PNumber.formatNumber(totalTasks)
          + "; Total Boss Slayer Tasks: " + PNumber.formatNumber(totalBossTasks));
      player.getGameEncoder().sendMessage("Brimstone keys: " + brimstoneKeys);
      player.getGameEncoder().sendMessage("Larran's keys: " + larransKeys);
      sendTask();
    } else if (name.equals("slayer_reset_task")) {
      task.cancel();
    } else if (name.equals("slayer_reset_wilderness_task")) {
      wildernessTask.cancel();
    }
    return null;
  }

  @Override
  public int getCurrency(String identifier) {
    if (identifier.equals("slayer points")) {
      return points;
    }
    if (identifier.equals("boss slayer points")) {
      return bossPoints;
    }
    return 0;
  }

  @Override
  public void changeCurrency(String identifier, int amount) {
    if (identifier.equals("slayer points")) {
      points += amount;
    } else if (identifier.equals("boss slayer points")) {
      bossPoints += amount;
    }
  }

  @Override
  public void npcKilled(Npc npc) {
    taskKillCheck(task, npc);
    taskKillCheck(wildernessTask, npc);
    taskKillCheck(bossTask, npc);
  }

  public void getAssignment() {
    if (!task.isComplete()) {
      player.getGameEncoder().sendMessage("You already have a task.");
      return;
    }
    for (var i = MASTER_NAMES.length - 1; i >= 0; i--) {
      var master = SlayerMaster.get(MASTER_NAMES[i]);
      if (master == null) {
        continue;
      }
      if (player.getSkills().getCombatLevel() >= master.getCombatLevel()
          && player.getController().getLevelForXP(Skills.SLAYER) >= master.getSlayerLevel()
          && master.getTasks() != null) {
        getAssignment(MASTER_NAMES[i]);
        return;
      }
    }
    player.getGameEncoder().sendMessage("Couldn't find a suitable Slayer master.");
  }

  public void getAssignment(String name) {
    var master = SlayerMaster.get(name);
    if (master == null) {
      return;
    }
    var isWilderness = name.equals(WILDERNESS_MASTER);
    var isBoss = name.equals(BOSS_MASTER);
    var assignedTask = task;
    var assignedSlayerTask = task.getSlayerTask();
    if (isWilderness) {
      assignedTask = wildernessTask;
    } else if (isBoss) {
      assignedTask = bossTask;
    }
    if (!assignedTask.isComplete() && (!isBoss || PTime.getDate().equals(assignedTask.getDate()))) {
      player.getGameEncoder().sendMessage("You already have a task.");
      return;
    }
    if (player.getSkills().getCombatLevel() < master.getCombatLevel()) {
      player.getGameEncoder().sendMessage(
          "You need a combat level of " + master.getCombatLevel() + " to use this Slayer master.");
      return;
    }
    if (player.getController().getLevelForXP(Skills.SLAYER) < master.getSlayerLevel()) {
      player.getGameEncoder().sendMessage(
          "You need a Slayer level of " + master.getSlayerLevel() + " to get one of these tasks.");
      return;
    }
    if (isBoss && !Settings.getInstance().isLocal() && !isUnlocked(SlayerUnlock.LIKE_A_BOSS)) {
      player.getGameEncoder().sendMessage("You need to unlock this feature first.");
      return;
    }
    SlayerTask selectedTask = null;
    if (assignedSlayerTask != null && player.getEquipment().wearingAccomplishmentCape(Skills.SLAYER)
        && PRandom.randomE(10) == 0) {
      selectedTask = assignedSlayerTask;
    }
    for (var i = 0; i < 128 && selectedTask == null; i++) {
      var aTask = PRandom.listRandom(master.getTasks());
      if (player.getController().getLevelForXP(Skills.SLAYER) < aTask.getSlayerLevel()) {
        continue;
      }
      if (!isWilderness && isBlockedTask(aTask.getIdentifier())) {
        continue;
      }
      if (aTask.getIdentifier() == SlayerTaskIdentifier.RED_DRAGON
          && !isUnlocked(SlayerUnlock.SEEING_RED)) {
        continue;
      }
      if (aTask.getIdentifier() == SlayerTaskIdentifier.MITHRIL_DRAGON
          && !isUnlocked(SlayerUnlock.I_HOPE_YOU_MITH_ME)) {
        continue;
      }
      if (aTask.getIdentifier() == SlayerTaskIdentifier.AVIANSIE
          && !isUnlocked(SlayerUnlock.WATCH_THE_BIRDIE)) {
        continue;
      }
      if (aTask.getIdentifier() == SlayerTaskIdentifier.TZHAAR
          && !isUnlocked(SlayerUnlock.HOT_STUFF)) {
        continue;
      }
      if (aTask.getIdentifier() == SlayerTaskIdentifier.LIZARDMAN
          && !isUnlocked(SlayerUnlock.REPTILE_GOT_RIPPED)) {
        continue;
      }
      if (aTask.getIdentifier() == SlayerTaskIdentifier.FOSSIL_ISLAND_WYVERN
          && isUnlocked(SlayerUnlock.STOP_THE_WYVERN)) {
        continue;
      }
      if (aTask.getName().equals("Corporeal Beast") && !isUnlocked(SlayerUnlock.CORPOREAL_BEAST)) {
        continue;
      }
      if (aTask.getName().equals("Raids Boss") && !isUnlocked(SlayerUnlock.RAIDS)) {
        continue;
      }
      if (aTask.getName().equals("Grotesque Guardians")
          && !isUnlocked(SlayerUnlock.GROTESQUE_GUARDIANS)) {
        continue;
      }
      if (isBoss && aTask.isWilderness() && isUnlocked(SlayerUnlock.WILDERNESS_BOSS)) {
        continue;
      }
      if (assignedSlayerTask != null
          && (assignedSlayerTask.getIdentifier() != null || aTask.getIdentifier() != null)
          && assignedSlayerTask.getIdentifier() == aTask.getIdentifier()) {
        continue;
      }
      if (assignedSlayerTask != null
          && (assignedSlayerTask.getName() != null || aTask.getName() != null)
          && assignedSlayerTask.getName().equals(aTask.getName())) {
        continue;
      }
      if (isBoss && assignedSlayerTask != null && assignedSlayerTask.isWilderness()
          && aTask.isWilderness()) {
        continue;
      }
      selectedTask = aTask;
      break;
    }
    if (selectedTask == null) {
      player.getGameEncoder().sendMessage("Failed to assign a task!");
      return;
    }
    int quantity = selectedTask.getRandomQuantity();
    if (selectedTask.getIdentifier() == SlayerTaskIdentifier.DARK_BEAST
        && isUnlocked(SlayerUnlock.NEED_MORE_DARKNESS)
        || selectedTask.getIdentifier() == SlayerTaskIdentifier.ANKOU
            && isUnlocked(SlayerUnlock.ANKOU_VERY_MUCH)
        || selectedTask.getIdentifier() == SlayerTaskIdentifier.BLACK_DRAGON
            && isUnlocked(SlayerUnlock.FIRE_AND_DARKNESS)
        || (selectedTask.getIdentifier() == SlayerTaskIdentifier.BRONZE_DRAGON
            || selectedTask.getIdentifier() == SlayerTaskIdentifier.IRON_DRAGON
            || selectedTask.getIdentifier() == SlayerTaskIdentifier.STEEL_DRAGON)
            && isUnlocked(SlayerUnlock.PEDAL_TO_THE_METALS)
        || selectedTask.getIdentifier() == SlayerTaskIdentifier.ABYSSAL_DEMON
            && isUnlocked(SlayerUnlock.AUGMENT_MY_ABBIES)
        || selectedTask.getIdentifier() == SlayerTaskIdentifier.BLACK_DEMON
            && isUnlocked(SlayerUnlock.ITS_DARK_IN_HERE)
        || selectedTask.getIdentifier() == SlayerTaskIdentifier.GREATER_DEMON
            && isUnlocked(SlayerUnlock.GREATER_CHALLENGE)
        || selectedTask.getIdentifier() == SlayerTaskIdentifier.BLOODVELD
            && isUnlocked(SlayerUnlock.BLEED_ME_DRY)
        || selectedTask.getIdentifier() == SlayerTaskIdentifier.ABERRANT_SPECTRE
            && isUnlocked(SlayerUnlock.SMELL_YA_LATER)
        || selectedTask.getIdentifier() == SlayerTaskIdentifier.AVIANSIE
            && isUnlocked(SlayerUnlock.BIRDS_OF_A_FEATHER)
        || selectedTask.getIdentifier() == SlayerTaskIdentifier.MITHRIL_DRAGON
            && isUnlocked(SlayerUnlock.I_REALLY_MITH_YOU)
        || selectedTask.getIdentifier() == SlayerTaskIdentifier.CAVE_HORROR
            && isUnlocked(SlayerUnlock.HORRORIFIC)
        || selectedTask.getIdentifier() == SlayerTaskIdentifier.DUST_DEVIL
            && isUnlocked(SlayerUnlock.TO_DUST_YOU_SHALL_RETURN)
        || selectedTask.getIdentifier() == SlayerTaskIdentifier.SKELETAL_WYVERN
            && isUnlocked(SlayerUnlock.WYVERNOTHER_ONE)
        || selectedTask.getIdentifier() == SlayerTaskIdentifier.FOSSIL_ISLAND_WYVERN
            && isUnlocked(SlayerUnlock.WYVER_NOTHER_TWO)
        || selectedTask.getIdentifier() == SlayerTaskIdentifier.GARGOYLE
            && isUnlocked(SlayerUnlock.GET_SMASHED)
        || selectedTask.getIdentifier() == SlayerTaskIdentifier.NECHRYAEL
            && isUnlocked(SlayerUnlock.NECHS_PLEASE)
        || selectedTask.getIdentifier() == SlayerTaskIdentifier.CAVE_KRAKEN
            && isUnlocked(SlayerUnlock.KRACK_ON)
        || selectedTask.getIdentifier() == SlayerTaskIdentifier.ADAMANT_DRAGON
            && isUnlocked(SlayerUnlock.ADAMIND_SOME_MORE)
        || selectedTask.getIdentifier() == SlayerTaskIdentifier.RUNE_DRAGON
            && isUnlocked(SlayerUnlock.RUUUUUNE)) {
      quantity *= 1.5;
    }
    if (!isWilderness && !isBoss && !name.equalsIgnoreCase(assignedTask.getMaster())) {
      consecutiveTasks = 0;
    }
    assignedTask = new AssignedSlayerTask(master.getName(), selectedTask.getIdentifier(),
        selectedTask.getName(), quantity, PTime.getDate());
    if (isWilderness) {
      wildernessTask = assignedTask;
    } else if (isBoss) {
      bossTask = assignedTask;
    } else {
      task = assignedTask;
    }
    var plural =
        assignedTask.getQuantity() > 1
            ? (assignedTask.getName().endsWith("ss") ? "es"
                : (assignedTask.getName().endsWith("s") ? "'" : "s"))
            : "";
    if (isWilderness) {
      player.getGameEncoder().sendMessage("Your new wilderness task is to kill " + quantity + " "
          + selectedTask.getName() + plural + ".");
      player.getGameEncoder().sendMessage(
          "Carrying a mysterious emblem during your task will reward you with blood money for every kill. Carrying the same emblem for your entire task will also upgrade it upon completion.");
      wildernessTaskEmblemId = -1;
    } else {
      player.getGameEncoder().sendMessage(
          "Your new task is to kill " + quantity + " " + selectedTask.getName() + plural + ".");
    }
    if (selectedTask.getLocation() != null) {
      player.getGameEncoder()
          .sendMessage("They are located at " + selectedTask.getLocation() + ".");
    }
    AchievementDiary.slayerAssignmentUpdate(player, master, selectedTask, quantity);
  }

  public void sendTask() {
    if (task.isComplete() && wildernessTask.isComplete()) {
      player.getGameEncoder().sendMessage("You need something new to hunt.");
      return;
    }
    if (!task.isComplete()) {
      player.getGameEncoder().sendMessage("You're assigned to kill " + task.getName() + "s; only "
          + task.getQuantity() + " more to go.");
      if (task.getSlayerTask().getLocation() != null) {
        player.getGameEncoder()
            .sendMessage("They are located at " + task.getSlayerTask().getLocation() + ".");
      }
    }
    if (!wildernessTask.isComplete()) {
      player.getGameEncoder().sendMessage("You're assigned to kill " + wildernessTask.getName()
          + "s in the wilderness; only " + wildernessTask.getQuantity() + " more to go.");
      if (wildernessTask.getSlayerTask().getLocation() != null) {
        player.getGameEncoder().sendMessage(
            "They are located at " + wildernessTask.getSlayerTask().getLocation() + ".");
      }
    }
  }

  public void cancelWildernessTask() {
    if (wildernessTask.isComplete()) {
      player.getGameEncoder().sendMessage("You need something new to hunt.");
      return;
    }
    var cost = 30;
    if (player.isUsergroup(UserRank.ZENYTE_MEMBER)) {
      cost -= 27;
    } else if (player.isUsergroup(UserRank.ONYX_MEMBER)) {
      cost -= 25;
    } else if (player.isUsergroup(UserRank.DRAGONSTONE_MEMBER)) {
      cost -= 22;
    } else if (player.isUsergroup(UserRank.DIAMOND_MEMBER)) {
      cost -= 20;
    } else if (player.isUsergroup(UserRank.RUBY_MEMBER)) {
      cost -= 17;
    } else if (player.isUsergroup(UserRank.EMERALD_MEMBER)) {
      cost -= 15;
    } else if (player.isUsergroup(UserRank.SAPPHIRE_MEMBER)) {
      cost -= 12;
    } else if (player.isUsergroup(UserRank.TOPAZ_MEMBER)) {
      cost -= 10;
    } else if (player.isUsergroup(UserRank.JADE_MEMBER)) {
      cost -= 7;
    } else if (player.isUsergroup(UserRank.OPAL_MEMBER)) {
      cost -= 5;
    }
    cost = Math.max(0, cost);
    if (points < cost) {
      player.getGameEncoder().sendMessage("You need " + cost + " points to cancel a task.");
      return;
    }
    player.getGameEncoder().sendMessage("Your wilderness task has been cancelled.");
    wildernessTask.cancel();
    points -= cost;
  }

  public void openRewards() {
    player.getWidgetManager().sendInteractiveOverlay(WidgetId.SLAYER_REWARDS);
    player.getGameEncoder().sendWidgetSettings(WidgetId.SLAYER_REWARDS, 8, 0, 100, 2);
    player.getGameEncoder().sendWidgetSettings(WidgetId.SLAYER_REWARDS, 12, 0, 10, 2);
    player.getGameEncoder().sendWidgetSettings(WidgetId.SLAYER_REWARDS, 23, 0, 10, 1086);
    sendRewardsVarbits();
  }

  public boolean isUnlocked(SlayerUnlock unlock) {
    return unlocks != null && unlocks.contains(unlock);
  }

  public void unlock(SlayerUnlock unlock) {
    if (isUnlocked(unlock)) {
      lock(unlock);
      return;
    }
    var cost = unlock.getPrice();
    if (points < cost) {
      player.getGameEncoder().sendMessage("You need " + cost + " points to unlock this.");
      return;
    }
    if (unlocks == null) {
      unlocks = new ArrayList<>();
    }
    if (!unlocks.contains(unlock)) {
      unlocks.add(unlock);
    }
    points -= cost;
    sendRewardsVarbits();
  }

  public void lock(SlayerUnlock unlock) {
    if (unlocks == null || !unlocks.contains(unlock)) {
      return;
    }
    unlocks.remove(unlock);
    sendRewardsVarbits();
  }

  public void incrimentBrimstoneKeys() {
    brimstoneKeys++;
  }

  public void incrimentCrystalKeys() {
    crystalKeys++;
  }

  public void incrimentSupplyBoxes() {
    supplyBoxes++;
  }

  public void incrimentLarransKeys() {
    larransKeys++;
  }

  public void incrimentGemBags() {
    gemBags++;
  }

  public void incrimentBarBoxes() {
    barBoxes++;
  }

  public void incrimentHerbBoxes() {
    herbBoxes++;
  }

  public void cancelTask() {
    if (task.isComplete()) {
      player.getGameEncoder().sendMessage("You need something new to hunt.");
      return;
    }
    var cost = 30;
    if (player.isUsergroup(UserRank.ZENYTE_MEMBER)) {
      cost -= 27;
    } else if (player.isUsergroup(UserRank.ONYX_MEMBER)) {
      cost -= 25;
    } else if (player.isUsergroup(UserRank.DRAGONSTONE_MEMBER)) {
      cost -= 22;
    } else if (player.isUsergroup(UserRank.DIAMOND_MEMBER)) {
      cost -= 20;
    } else if (player.isUsergroup(UserRank.RUBY_MEMBER)) {
      cost -= 17;
    } else if (player.isUsergroup(UserRank.EMERALD_MEMBER)) {
      cost -= 15;
    } else if (player.isUsergroup(UserRank.SAPPHIRE_MEMBER)) {
      cost -= 12;
    } else if (player.isUsergroup(UserRank.TOPAZ_MEMBER)) {
      cost -= 10;
    } else if (player.isUsergroup(UserRank.JADE_MEMBER)) {
      cost -= 7;
    } else if (player.isUsergroup(UserRank.OPAL_MEMBER)) {
      cost -= 5;
    }
    cost = Math.max(0, cost);
    if (points < cost) {
      player.getGameEncoder().sendMessage("You need " + cost + " points to cancel a task.");
      return;
    }
    player.getGameEncoder().sendMessage("Your task has been cancelled.");
    task.cancel();
    points -= cost;
    sendRewardsVarbits();
  }

  public void blockTask() {
    if (task.isComplete()) {
      player.getGameEncoder().sendMessage("You need something new to hunt.");
      return;
    }
    if (task.getIdentifier() == null) {
      player.getGameEncoder().sendMessage("This task can't be blocked.");
      return;
    }
    if (blockedTasks != null && blockedTasks.contains(task.getIdentifier())) {
      player.getGameEncoder().sendMessage("This task is already blocked.");
      return;
    }
    if (points < 100) {
      player.getGameEncoder().sendMessage("You need 100 points to block a task.");
      return;
    }
    if (blockedTasks != null && blockedTasks.size() >= 6) {
      player.getGameEncoder().sendMessage("You can't block any more tasks.");
      return;
    }
    if (blockedTasks == null) {
      blockedTasks = new ArrayList<>();
    }
    blockedTasks.add(task.getIdentifier());
    points -= 100;
    task.cancel();
    sendRewardsVarbits();
  }

  public void unblockTask(int option) {
    if (blockedTasks == null || option >= blockedTasks.size()) {
      return;
    }
    blockedTasks.remove(option);
    if (blockedTasks.size() == 0) {
      blockedTasks = null;
    }
    sendRewardsVarbits();
  }

  public void buy(Item item, int cost) {
    if (points < cost) {
      player.getGameEncoder()
          .sendMessage("You need " + PNumber.formatNumber(cost) + " points to buy this.");
      return;
    }
    if (!player.getInventory().canAddItem(item)) {
      player.getInventory().notEnoughSpace();
      return;
    }
    player.getInventory().addItem(item);
    points -= cost;
    sendRewardsVarbits();
  }

  public void openRewardsDialogue() {
    player.openDialogue(new OptionsDialogue(new DialogueOption("Slayer rewards", (c, s) -> {
      openRewards();
    }), new DialogueOption("Boss Slayer rewards", (c, s) -> {
      player.openShop("boss_slayer");
    })));
  }

  public void openMasterMenuDialogue() {
    player.openDialogue(new OptionsDialogue(new DialogueOption("Get task", (c, s) -> {
      openChooseMasterDialogue();
    }), new DialogueOption("Current task", (c, s) -> {
      sendTask();
    }), new DialogueOption("Get boss task", (c, s) -> {
      getAssignment(BOSS_MASTER);
    }), new DialogueOption("Cancel boss task", (c, s) -> {
      if (player.getInventory().getCount(ItemId.COINS) < 500000) {
        player.getGameEncoder().sendMessage("You need 500K coins to do this.");
        return;
      }
      if (player.getInventory().getCount(ItemId.VOTE_TICKET) < 2) {
        player.getGameEncoder().sendMessage("You need 2 vote tickets to do this.");
        return;
      }
      if (bossTask.isComplete()) {
        player.getGameEncoder().sendMessage("You don't have a boss task to cancel.");
        return;
      }
      bossTask.cancel();
      player.getInventory().deleteItem(ItemId.COINS, 500000);
      player.getInventory().deleteItem(ItemId.VOTE_TICKET, 2);
      player.getGameEncoder().sendMessage("Your boss task has been cancelled.");
    })));
  }

  public void openChooseMasterDialogue() {
    player.openDialogue(new OptionsDialogue(new DialogueOption("Mazchna - level 20", (c, s) -> {
      getAssignment("Mazchna");
    }), new DialogueOption("Chaeldar - level 70", (c, s) -> {
      getAssignment("Chaeldar");
    }), new DialogueOption("Nieve - level 85", (c, s) -> {
      getAssignment("Nieve");
    }), new DialogueOption("Duradel - level 100", (c, s) -> {
      getAssignment("Duradel");
    }), new DialogueOption("Krystilia - wilderness", (c, s) -> {
      player.getGameEncoder().sendMessage("Please speak to krystilia for a wilderness task.");
    })));
  }

  private void sendVarps() {
    player.getGameEncoder().setVarp(VarpId.SLAYER_QUANTITY, task.getQuantity());
    player.getGameEncoder().setVarp(VarpId.SLAYER_TASK_IDENTIFIER,
        !task.isComplete() ? task.getIdentifier() != null ? task.getIdentifier().ordinal() : 156
            : 0);
    player.getGameEncoder().setVarbit(VarbitId.SLAYER_GROTESQUE_GUARDIANS_DOOR,
        isUnlocked(SlayerUnlock.GROTESQUE_GUARDIANS) ? 1 : 0);
  }

  private void sendRewardsVarbits() {
    sendVarps();
    player.getGameEncoder().setVarbit(VarbitId.SLAYER_POINTS, points);
    if (unlocks != null) {
      for (SlayerUnlock unlock : unlocks) {
        if (unlock.getVarbit() == -1) {
          continue;
        }
        player.getGameEncoder().setVarbit(unlock.getVarbit(), 1);
      }
    }
    if (blockedTasks != null) {
      for (int i = 0; i < blockedTasks.size(); i++) {
        player.getGameEncoder().setVarbit(BLOCKED_TASK_VARBITS[i], blockedTasks.get(i).ordinal());
      }
    }
  }

  private boolean isBlockedTask(SlayerTaskIdentifier identifier) {
    return blockedTasks != null && blockedTasks.contains(identifier);
  }

  private void taskKillCheck(AssignedSlayerTask assignedTask, Npc npc) {
    if (assignedTask == wildernessTask && player.getSkills().getCombatLevel() < 100) {
      return;
    }
    if (!countsTowardTaskQuantity(assignedTask, npc.getId())) {
      return;
    }
    if (assignedTask.isComplete() || !isTask(assignedTask, npc.getId(), npc.getName())) {
      return;
    }
    var experience = npc.getCombat().getMaxHitpoints();
    if (npc.getCombatDef().getSlayer().getExperience() > 0) {
      experience = npc.getCombatDef().getSlayer().getExperience();
    }
    player.getSkills().addXp(Skills.SLAYER, experience);
    if (npc.getCombatDef().getSlayer().getSuperiorId() != -1
        && isUnlocked(SlayerUnlock.BIGGER_BADDER)
        && (PRandom.randomE(100) == 0 || Settings.getInstance().isBeta())) {
      player.getGameEncoder().sendMessage("<col=ff0000>A superior foe has appeared...");
      var superiorNpc = player.getController()
          .addNpc(new NpcSpawn(npc.getCombatDef().getSlayer().getSuperiorId(), npc));
      superiorNpc.getCombat().setTarget(player);
    }
    // var hasRoWICharge = player.getCharges().hasRoWICharge(0);
    int brimstoneKeyChance;
    if (npc.getDef().getCombatLevel() >= 100) {
      brimstoneKeyChance = (int) (-0.2 * Math.min(npc.getDef().getCombatLevel(), 350) + 120);
    } else {
      brimstoneKeyChance = (int) (0.2 * Math.pow(npc.getDef().getCombatLevel() - 100, 2) + 100);
    }
    var brimstoneKeyPercent = 100.0 / brimstoneKeyChance;
    if (brimstoneKeyChance > 0 && assignedTask == task && PRandom.inRange(brimstoneKeyPercent)) {
      Tile tile = npc;
      if (npc.getCombatDef().getDrop().isUnderKiller()) {
        tile = player;
      }
      if ((player.canDonatorPickupItem(ItemId.BRIMSTONE_KEY)/* || hasRoWICharge */)
          && player.getInventory().canAddItem(ItemId.BRIMSTONE_KEY)) {
        player.getInventory().addItem(ItemId.BRIMSTONE_KEY);
      } else {
        player.getController().addMapItem(new Item(ItemId.BRIMSTONE_KEY), tile, player);
      }
    }
    int larransKeyChance;
    if (npc.getDef().getCombatLevel() >= 81) {
      larransKeyChance = (int) (-0.18 * Math.min(npc.getDef().getCombatLevel(), 350) + 113);
    } else {
      larransKeyChance = (int) (0.3 * Math.pow(80 - npc.getDef().getCombatLevel(), 2) + 100);
    }
    if (npc.getCombatDef().getSlayer().getLevel() > 1) {
      larransKeyChance *= 1.25;
    }
    var larransKeyPercent = 100.0 / larransKeyChance;
    if (larransKeyChance > 0 && assignedTask == wildernessTask
        && PRandom.inRange(larransKeyPercent)) {
      Tile tile = npc;
      if (npc.getCombatDef().getDrop().isUnderKiller()) {
        tile = player;
      }
      if ((player.canDonatorPickupItem(ItemId.LARRANS_KEY)/* || hasRoWICharge */)
          && player.getInventory().canAddItem(ItemId.LARRANS_KEY)) {
        player.getInventory().addItem(ItemId.LARRANS_KEY);
      } else {
        player.getController().addMapItem(new Item(ItemId.LARRANS_KEY), tile, player);
      }
    }
    if (assignedTask == wildernessTask) {
      if (wildernessTaskEmblemId == -1 && assignedTask.getQuantity() == assignedTask.getTotal()) {
        wildernessTaskEmblemId = player.getPlugin(BountyHunterPlugin.class).getEmblemId();
      }
      if (wildernessTaskEmblemId > -1 && !player.getInventory().hasItem(wildernessTaskEmblemId)) {
        wildernessTaskEmblemId = -1;
        player.getGameEncoder()
            .sendMessage("Your task is no longer eligible to upgrade an emblem.");
      }
    }
    assignedTask.decreaseQuantity();
    if (!assignedTask.isComplete()) {
      return;
    }
    var tasksCompleted = 0;
    if (assignedTask == task) {
      totalTasks++;
      tasksCompleted = ++consecutiveTasks;
    } else if (assignedTask == wildernessTask) {
      totalTasks++;
      tasksCompleted = ++consecutiveWildernessTasks;
    } else if (assignedTask == bossTask) {
      tasksCompleted = ++totalBossTasks;
    }
    var slayerMaster = SlayerMaster.get(assignedTask.getMaster());
    var slayerTask = assignedTask.getSlayerTask();
    var rewardPoints = slayerMaster.getPoints();
    if (player.isUsergroup(UserRank.ZENYTE_MEMBER)) {
      rewardPoints *= 1.275;
    } else if (player.isUsergroup(UserRank.ONYX_MEMBER)) {
      rewardPoints *= 1.25;
    } else if (player.isUsergroup(UserRank.DRAGONSTONE_MEMBER)) {
      rewardPoints *= 1.225;
    } else if (player.isUsergroup(UserRank.DIAMOND_MEMBER)) {
      rewardPoints *= 1.2;
    } else if (player.isUsergroup(UserRank.RUBY_MEMBER)) {
      rewardPoints *= 1.175;
    } else if (player.isUsergroup(UserRank.EMERALD_MEMBER)) {
      rewardPoints *= 1.15;
    } else if (player.isUsergroup(UserRank.SAPPHIRE_MEMBER)) {
      rewardPoints *= 1.125;
    } else if (player.isUsergroup(UserRank.TOPAZ_MEMBER)) {
      rewardPoints *= 1.1;
    } else if (player.isUsergroup(UserRank.JADE_MEMBER)) {
      rewardPoints *= 1.075;
    } else if (player.isUsergroup(UserRank.OPAL_MEMBER)) {
      rewardPoints *= 1.05;
    }
    if (slayerTask.isWilderness() && !Settings.getInstance().isSpawn()) {
      if (assignedTask == wildernessTask) {
        player.getInventory().addOrDropItem(ItemId.BLOOD_MONEY, 10_000);
      } else if (assignedTask == bossTask) {
        player.getInventory().addOrDropItem(ItemId.BLOOD_MONEY, 20_000);
      }
    }
    if (assignedTask == wildernessTask) {
      if (wildernessTaskEmblemId > -1 && player.getInventory().hasItem(wildernessTaskEmblemId)) {
        var nextEmblemId = MysteriousEmblem.getNextId(wildernessTaskEmblemId);
        if (nextEmblemId != -1) {
          player.getInventory().deleteItem(wildernessTaskEmblemId);
          player.getInventory().addItem(nextEmblemId);
        }
      }
      if (PRandom.randomE(10) == 0) {
        Tile tile = npc;
        if (npc.getCombatDef().getDrop().isUnderKiller()) {
          tile = player;
        }
        player.getController().addMapItem(new Item(ItemId.BLOODY_KEY), tile, player);
        player.getWorld()
            .sendMessage("<col=ff0000>A bloody key has been dropped near " + assignedTask.getName()
                + "s at Level " + Area.getWildernessLevel(tile) + " for " + player.getUsername()
                + "!");
      }
    }
    if (tasksCompleted % 1000 == 0) {
      rewardPoints *= 50;
    } else if (tasksCompleted % 250 == 0) {
      rewardPoints *= 35;
    } else if (tasksCompleted % 100 == 0) {
      rewardPoints *= 25;
    } else if (tasksCompleted % 50 == 0) {
      rewardPoints *= 15;
    } else if (tasksCompleted % 25 == 0) {
      rewardPoints *= 10;
    } else if (tasksCompleted % 10 == 0) {
      rewardPoints *= 5;
    } else if (tasksCompleted % 5 == 0) {
      rewardPoints *= 2.5;
    }
    if (assignedTask == bossTask) {
      bossPoints += rewardPoints;
    } else {
      points += rewardPoints;
    }
    if (assignedTask == bossTask) {
      player.getGameEncoder().sendMessage("<col=8F4808>You've completed " + tasksCompleted
          + " boss tasks and received " + rewardPoints + " points; return to a Slayer master.");
    } else if (rewardPoints > 0) {
      player.getGameEncoder().sendMessage("<col=8F4808>You've completed " + tasksCompleted
          + " tasks in a row and received " + rewardPoints + " points; return to a Slayer master.");
    } else {
      player.getGameEncoder().sendMessage(
          "<col=8F4808>You've completed " + tasksCompleted + " tasks; return to a Slayer master.");
    }
    AchievementDiary.slayerAssignmentCompleteUpdate(player, slayerMaster, slayerTask);
    if (assignedTask == task) {
      player.getSkills().competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.SLAYER_TASKS, 1);
    } else if (assignedTask == bossTask) {
      player.getSkills()
          .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.BOSS_SLAYER_TASKS, 1);
    } else if (assignedTask == wildernessTask) {
      player.getSkills()
          .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.WILD_SLAYER_TASKS, 1);
    }
  }

  private boolean countsTowardTaskQuantity(AssignedSlayerTask assignedTask, int id) {
    if (id == NpcId.RESPIRATORY_SYSTEM) {
      return false;
    }
    if ((id == NpcId.DAWN_228 || id == NpcId.DAWN_228_7885)
        && !isUnlocked(SlayerUnlock.DOUBLE_TROUBLE)) {
      return false;
    }
    if (assignedTask == bossTask) {
      if (id == NpcId.WINGMAN_SKREE_143 || id == NpcId.FLOCKLEADER_GEERIN_149
          || id == NpcId.FLIGHT_KILISA_159) {
        return false;
      } else if (id == NpcId.STARLIGHT_149 || id == NpcId.GROWLER_139 || id == NpcId.BREE_146) {
        return false;
      } else if (id == NpcId.SERGEANT_STRONGSTACK_141 || id == NpcId.SERGEANT_STEELWILL_142
          || id == NpcId.SERGEANT_GRIMSPIKE_142) {
        return false;
      } else if (id == NpcId.TSTANON_KARLAK_145 || id == NpcId.ZAKLN_GRITCH_142
          || id == NpcId.BALFRUG_KREEYATH_151) {
        return false;
      } else if (id == NpcId.GREAT_OLM_RIGHT_CLAW || id == NpcId.GREAT_OLM_LEFT_CLAW
          || id == NpcId.GREAT_OLM_RIGHT_CLAW_549 || id == NpcId.GREAT_OLM_LEFT_CLAW_750) {
        return false;
      }
    }
    return true;
  }

  private boolean isAnyTask(int id, String name) {
    return isTask(task, id, name) || isTask(wildernessTask, id, name) || isTask(bossTask, id, name);
  }

  private boolean isTask(AssignedSlayerTask assignedTask, int id, String name) {
    if (assignedTask.isComplete()) {
      return false;
    }
    if (assignedTask == wildernessTask && !player.getArea().inWilderness()) {
      return false;
    }
    return assignedTask.containsId(id)
        || name != null && name.matches(".*(?i)(" + assignedTask.getName() + ")\\b.*");
  }
}
