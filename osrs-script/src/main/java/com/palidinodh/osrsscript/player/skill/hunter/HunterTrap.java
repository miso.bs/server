package com.palidinodh.osrsscript.player.skill.hunter;

import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.map.TempMapObject;
import lombok.var;

class HunterTrap extends TempMapObject {
  private MapObject[] trap;
  private Item[] items;

  public HunterTrap(Player player, MapObject[] trap, Item[] items) {
    super(Hunter.TRAP_EXPIRIY, player.getController(), trap);
    setAttachment(player.getId());
    this.trap = trap;
    this.items = items;
  }

  @Override
  public void executeScript() {
    var player = Main.getWorld().getPlayerById((int) getAttachment());
    if (player == null) {
      return;
    }
    player.getGameEncoder().sendMessage("Your trap has broken.");
    for (var item : items) {
      getController().addMapItem(item, trap[0], player);
    }
  }
}
