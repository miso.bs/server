package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.DRAGON_HALBERD)
class DragonHalberdSpecialAttack extends SpecialAttack {
  public DragonHalberdSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(30);
    entry.setAnimation(1203);
    entry.setTargetGraphic(new Graphic(1172, 100));
    entry.setDamageModifier(1.1);
    addEntry(entry);
  }

  @Override
  public void attackTickHook(AttackTickHooks hooks) {
    var opponent = hooks.getOpponent();
    if (opponent.getSizeX() > 1 || opponent.isNpc() && opponent.getId() == NpcId.COMBAT_DUMMY) {
      hooks.setDoubleHit(true);
    }
  }
}
