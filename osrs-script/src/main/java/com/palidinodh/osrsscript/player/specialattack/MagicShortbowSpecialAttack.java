package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.MAGIC_SHORTBOW, ItemId.MAGIC_SHORTBOW_20558, ItemId.MAGIC_SHORTBOW_I })
class MagicShortbowSpecialAttack extends SpecialAttack {
  public MagicShortbowSpecialAttack() {
    var entry = new Entry(this);
    entry.addItemId(ItemId.MAGIC_SHORTBOW);
    entry.addItemId(ItemId.MAGIC_SHORTBOW_20558);
    entry.setDrain(55);
    entry.setAnimation(1074);
    entry.setCastGraphic(new Graphic(256, 92, 30));
    entry.setProjectileId(249);
    entry.setDoubleHit(true);
    addEntry(entry);

    entry = new Entry(this);
    entry.addItemId(ItemId.MAGIC_SHORTBOW_I);
    entry.setDrain(50);
    entry.setAnimation(1074);
    entry.setCastGraphic(new Graphic(256, 92, 30));
    entry.setProjectileId(249);
    entry.setDoubleHit(true);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    if (hooks.getApplyAttackLoopCount() > 0) {
      return;
    }
    var player = hooks.getPlayer();
    var distance = hooks.getPlayer().getDistance(hooks.getOpponent());
    var projectile = hooks.getProjectile();
    var projectileSpeed = projectile.getProjectileSpeed();
    projectileSpeed.setClientDelay(50);
    projectileSpeed.setClientSpeed(distance * 3);
    player.getController().sendMapProjectile(projectile);
    projectileSpeed.setClientDelay(20);
    projectileSpeed.setClientSpeed(projectileSpeed.getClientSpeed() + 10);
    player.getController().sendMapProjectile(projectile);
    hooks.setProjectile(null);
  }
}
