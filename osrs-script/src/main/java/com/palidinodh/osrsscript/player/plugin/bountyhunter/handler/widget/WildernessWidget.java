package com.palidinodh.osrsscript.player.plugin.bountyhunter.handler.widget;

import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.MessageDialogue;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrsscript.player.plugin.bountyhunter.BountyHunterPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(WidgetId.WILDERNESS)
class WildernessWidget implements WidgetHandler {
  @Override
  public void widgetOption(Player player, int option, int widgetId, int childId, int slot,
      int itemId) {
    var plugin = player.getPlugin(BountyHunterPlugin.class);
    switch (childId) {
      case 57:
        skipWarningDialogue(player);
        break;
      case 58:
        plugin.setMinimised(!plugin.isMinimised());
        break;
    }
  }

  public void skipWarningDialogue(Player player) {
    player.openDialogue(new MessageDialogue(
        "<col=ff0000>WARNING</col>: Skipping too many targets in a short period of time can cause you to incur a target restriction penalty. You should not use this too frequently.",
        (c, s) -> {
          player.openDialogue(new OptionsDialogue("Do you want to skip your target?",
              new DialogueOption("Yes.", (c2, s2) -> {
                if (player.getInCombatDelay() > 0) {
                  player.getGameEncoder().sendMessage("You can't do this right now.");
                  return;
                }
                var plugin = player.getPlugin(BountyHunterPlugin.class);
                if (plugin.getPenalty() > 0) {
                  player.getGameEncoder().sendMessage("You can't do this right now.");
                  return;
                }
                plugin.abandonTarget();
              }), new DialogueOption("No.")));
        }));
  }
}
