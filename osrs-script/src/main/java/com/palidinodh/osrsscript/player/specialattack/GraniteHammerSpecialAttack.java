package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.GRANITE_HAMMER)
class GraniteHammerSpecialAttack extends SpecialAttack {
  public GraniteHammerSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(60);
    entry.setAnimation(1378);
    entry.setCastGraphic(new Graphic(1450));
    entry.setAccuracyModifier(1.5);
    addEntry(entry);
  }
}
