package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.TOXIC_BLOWPIPE)
class ToxicBlowpipeSpecialAttack extends SpecialAttack {
  public ToxicBlowpipeSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(50);
    entry.setAnimation(5061);
    entry.setProjectileId(1043);
    entry.setDamageModifier(1.5);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    hooks.getPlayer().getCombat().changeHitpoints(hooks.getDamage() / 2);
  }
}
