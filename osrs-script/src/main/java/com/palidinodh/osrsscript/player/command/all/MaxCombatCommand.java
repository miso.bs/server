package com.palidinodh.osrsscript.player.command.all;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.rs.reference.ReferenceName;
import lombok.var;

@ReferenceName("max")
class MaxCombatCommand implements CommandHandler {
  @Override
  public boolean canUse(Player player) {
    return player.isGameModeNormal();
  }

  @Override
  public void execute(Player player, String name, String message) {
    if (!player.getArea().inLoadoutZone()) {
      player.getGameEncoder().sendMessage("You can't use this here.");
      return;
    }
    var skills = new int[] { Skills.ATTACK, Skills.DEFENCE, Skills.STRENGTH, Skills.RANGED,
        Skills.MAGIC, Skills.PRAYER, Skills.HITPOINTS };
    for (var id : skills) {
      player.getSkills().setLevel(id, 99);
      player.getSkills().setXP(id, Skills.XP_TABLE[99]);
      player.getGameEncoder().sendSkillLevel(id);
    }
    player.getSkills().setCombatLevel();
    player.getCombat().setMaxHitpoints(99);
    player.rejuvenate();
  }
}
