package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.MORRIGANS_JAVELIN)
class MorrigansJavelinSpecialAttack extends SpecialAttack {
  public MorrigansJavelinSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(50);
    entry.setAnimation(806);
    entry.setCastGraphic(new Graphic(1621, 96));
    entry.setProjectileId(1622);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    var projectileSpeed = hooks.getProjectile().getProjectileSpeed();
    var distance = hooks.getPlayer().getDistance(hooks.getOpponent());
    projectileSpeed.setEventDelay(1);
    if (distance >= 6) {
      projectileSpeed.setEventDelay(2);
    }
    projectileSpeed.setClientSpeed(11 + distance * 5);
    projectileSpeed.setClientDelay(21);
  }
}
