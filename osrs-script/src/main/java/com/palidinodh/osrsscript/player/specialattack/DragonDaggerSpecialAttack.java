package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.DRAGON_DAGGER, ItemId.DRAGON_DAGGER_P, ItemId.DRAGON_DAGGER_P_PLUS,
    ItemId.DRAGON_DAGGER_P_PLUS_PLUS, ItemId.DRAGON_DAGGER_20407,
    ItemId.BLIGHTED_DRAGON_DAGGER_P_PLUS_PLUS_32368 })
class DragonDaggerSpecialAttack extends SpecialAttack {
  public DragonDaggerSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(25);
    entry.setAnimation(1062);
    entry.setCastGraphic(new Graphic(252, 100));
    entry.setAccuracyModifier(1.25);
    entry.setDamageModifier(1.15);
    entry.setDoubleHit(true);
    addEntry(entry);
  }
}
