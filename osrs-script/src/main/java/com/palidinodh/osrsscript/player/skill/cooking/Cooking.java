package com.palidinodh.osrsscript.player.skill.cooking;

import java.util.List;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.ObjectId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.entity.player.skill.SkillModel;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.map.ObjectDef;
import com.palidinodh.osrsscript.map.area.asgarnia.donator.DonatorArea;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.setting.UserRank;
import com.palidinodh.util.PEvent;
import lombok.var;

public class Cooking extends SkillContainer {
  @Override
  public int getSkillId() {
    return Skills.COOKING;
  }

  @Override
  public List<SkillEntry> getEntries() {
    return CookingEntries.getEntries();
  }

  @Override
  public void actionSuccess(Player player, PEvent event, Npc npc, MapObject mapObject,
      SkillEntry entry) {
    if (entry.getConsume() != null && entry.getConsume().getId() == ItemId.SACRED_EEL) {
      var scaleCount = 3 + PRandom.randomI(6);
      player.getInventory().addOrDropItem(ItemId.ZULRAHS_SCALES, scaleCount * 2);
      player.getSkills().addXp(getSkillId(), scaleCount * 3);
    } else if (entry.getConsume() != null && entry.getConsume().getId() == ItemId.INFERNAL_EEL) {
      if (PRandom.randomE(16) == 0) {
        player.getInventory().addOrDropItem(ItemId.ONYX_BOLT_TIPS, 2);
      } else if (PRandom.randomE(12) == 0) {
        player.getInventory().addOrDropItem(ItemId.LAVA_SCALE_SHARD, 2 + PRandom.randomI(8));
      } else {
        player.getInventory().addOrDropItem(ItemId.TOKKUL, 20 + PRandom.randomI(20));
      }
    } else if (entry.getConsume() != null
        && entry.getConsume().getId() == ItemId.LEAPING_STURGEON) {
      if (PRandom.randomE(2) == 0) {
        player.getInventory().addOrDropItem(ItemId.FISH_OFFCUTS);
      } else {
        player.getInventory().addOrDropItem(ItemId.CAVIAR);
      }
      player.getSkills().addXp(getSkillId(), 15);
    }
  }

  @Override
  public Item createHook(Player player, Item item, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (mapObject != null && isFire(mapObject.getName())) {
      if (item.getName().endsWith("s")) {
        player.getGameEncoder()
            .sendFilteredMessage("You successfully cook some " + item.getName() + ".");
      } else if (item.getAmount() > 1) {
        player.getGameEncoder()
            .sendFilteredMessage("You successfully cook some " + item.getName() + "s.");
      } else {
        player.getGameEncoder()
            .sendFilteredMessage("You successfully cook a " + item.getName() + ".");
      }
    }
    return item;
  }

  @Override
  public Item failedCreateHook(Player player, Item item, Npc npc, MapObject mapObject,
      SkillEntry entry) {
    if (mapObject != null && isFire(mapObject.getName())) {
      var itemName = item.getName().replace("Burnt ", "");
      if (itemName.endsWith("s")) {
        player.getGameEncoder().sendFilteredMessage("You burn some " + itemName + ".");
      } else if (item.getAmount() > 1) {
        player.getGameEncoder().sendFilteredMessage("You burn some " + itemName + "s.");
      } else {
        player.getGameEncoder().sendFilteredMessage("You burn a " + itemName + ".");
      }
    }
    return item;
  }

  @Override
  public int experienceHook(Player player, int experience, Npc npc, MapObject mapObject,
      SkillEntry entry) {
    if (player.getEquipment().getHandId() == ItemId.COOKING_GAUNTLETS) {
      experience *= 1.1;
    }
    if (player.getArea().isArea(DonatorArea.class) && player.isUsergroup(UserRank.OPAL_MEMBER)) {
      experience *= 1.1;
    }
    return experience;
  }

  @Override
  public boolean failedActionHook(Player player, PEvent event, Npc npc, MapObject mapObject,
      SkillEntry entry) {
    if (entry.getFailedCreate() == null) {
      return false;
    }
    var failFactor = entry.getFailFactor();
    if (player.getEquipment().getHandId() == ItemId.COOKING_GAUNTLETS
        && entry.getCreate() != null) {
      if (entry.getCreate().getId() == ItemId.RAW_TUNA) {
        failFactor--;
      } else if (entry.getCreate().getId() == ItemId.RAW_LOBSTER) {
        failFactor -= 10;
      } else if (entry.getCreate().getId() == ItemId.RAW_SWORDFISH) {
        failFactor -= 5;
      } else if (entry.getCreate().getId() == ItemId.RAW_MONKFISH) {
        failFactor -= 2;
      } else if (entry.getCreate().getId() == ItemId.RAW_SHARK) {
        failFactor -= 11;
      } else if (entry.getCreate().getId() == ItemId.RAW_ANGLERFISH) {
        failFactor -= 10;
      }
    }
    var level = player.getSkills().getLevel(getSkillId());
    if (failFactor > 0 && level >= failFactor
        || player.getEquipment().wearingAccomplishmentCape(getSkillId())) {
      return false;
    }
    var power = level + 8;
    var failure = entry.getLevel() + 2;
    if (player.inWildernessResourceArea()) {
      failure /= 2;
    }
    var chance = 0;
    if (power > failure) {
      chance = (int) ((1 - (failure + 2) / ((power + 1) * 2.0)) * 100);
    } else {
      chance = (int) ((power / ((failure + 1) * 2.0)) * 100);
    }
    chance *= 1 + getMembershipBoost(player);
    if (player.getEquipment().getHandId() == ItemId.COOKING_GAUNTLETS) {
      chance += 10;
    }
    if (player.getArea().isArea(DonatorArea.class) && player.isUsergroup(UserRank.OPAL_MEMBER)) {
      chance += 10;
    }
    if (player.hasVoted()) {
      chance += 5;
    }
    chance = Math.min(chance, 100);
    return PRandom.randomE(100) > chance;
  }

  @Override
  public boolean findEntriesMapObjectMatchHook(Player player, SkillModel mapObjectModel,
      int widgetOnMapObjectId, SkillEntry entry) {
    var fireName = "";
    if (widgetOnMapObjectId > 0 && entry.getItemOnMapObjects().contains(ObjectId.COOKING_RANGE)) {
      fireName = ObjectDef.getObjectDef(widgetOnMapObjectId).getName().toLowerCase();
    }
    if (mapObjectModel != null && entry.getItemOnMapObjects().contains(ObjectId.COOKING_RANGE)) {
      if (entry.getConsume() != null && player.getInventory().hasItem(entry.getConsume().getId())) {
        fireName = ObjectDef.getObjectDef(mapObjectModel.getId()).getName().toLowerCase();
      }
    }
    if (isFire(fireName)) {
      return true;
    }
    return false;
  }

  private boolean isFire(String name) {
    return name.equals("fire") || name.endsWith("range") || name.endsWith("stove")
        || name.endsWith("oven");
  }
}
