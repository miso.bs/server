package com.palidinodh.osrsscript.player.command.betaworld;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.util.PEvent;
import lombok.var;

@ReferenceName({ "anim", "animloop" })
class AnimationCommand implements CommandHandler, CommandHandler.BetaWorld {
  @Override
  public String getExample(String name) {
    switch (name) {
      case "anim":
        return "id";
      case "animloop":
        return "start_id";
    }
    return "";
  }

  @Override
  public void execute(Player player, String name, String message) {
    switch (name) {
      case "anim":
        player.setAnimation(Integer.parseInt(message));
        break;
      case "animloop": {
        var fromId = Integer.parseInt(message);
        var event = new PEvent(1) {
          int id = fromId;
          boolean reset = false;

          @Override
          public void execute() {
            if (reset) {
              reset = false;
              player.setAnimation(-1);
              setTick(0);
            } else {
              reset = true;
              player.getGameEncoder().sendMessage("Anim: " + id);
              player.setAnimation(id++);
              setTick(1);
            }
          }
        };
        player.setAction(event);
        break;
      }
    }
  }
}
