package com.palidinodh.osrsscript.player.plugin.achievementdiary;

import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import lombok.Getter;
import lombok.Setter;

public class AchievementDiaryPlugin implements PlayerPlugin {
  @Getter
  @Setter
  private String faladorShield1And2;
  @Getter
  @Setter
  private String faladorShield3And4;
}
