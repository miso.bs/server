package com.palidinodh.osrsscript.player.command.owner;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("shop")
class ShopCommand implements CommandHandler, CommandHandler.OwnerRank {
  @Override
  public String getExample(String name) {
    return "name";
  }

  @Override
  public void execute(Player player, String name, String message) {
    player.openShop(message);
  }
}
