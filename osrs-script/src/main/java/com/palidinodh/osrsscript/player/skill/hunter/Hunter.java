package com.palidinodh.osrsscript.player.skill.hunter;

import java.util.List;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.ObjectId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.map.TempMapObject;
import com.palidinodh.osrsscript.player.plugin.hunter.HunterPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PEvent;
import lombok.var;

public class Hunter extends SkillContainer {
  public static final int TRAP_EXPIRIY = 100;
  private static Item MAGIC_BUTTERFLY_NET = new Item(ItemId.MAGIC_BUTTERFLY_NET);

  @Override
  public int getSkillId() {
    return Skills.HUNTER;
  }

  @Override
  public List<SkillEntry> getEntries() {
    return HunterEntries.getEntries();
  }

  @Override
  public int getDefaultMakeAmount() {
    return 1;
  }

  @Override
  public void actionSuccess(Player player, PEvent event, Npc npc, MapObject mapObject,
      SkillEntry entry) {
    if (npc != null) {
      npc.getCombat().timedDeath(2);
    }
  }

  @Override
  public Item createHook(Player player, Item item, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (entry.getConsume() != null && entry.getConsume().getId() == ItemId.IMPLING_JAR) {
      player.getGameEncoder()
          .sendFilteredMessage("You manage to catch the impling and squeeze it into a jar.");
    } else if (entry.getConsume() != null && entry.getConsume().getId() == ItemId.BUTTERFLY_JAR) {
      player.getGameEncoder()
          .sendFilteredMessage("You manage to catch the butterfly and squeeze it into a jar.");
    }
    return item;
  }

  @Override
  public int experienceHook(Player player, int experience, Npc npc, MapObject mapObject,
      SkillEntry entry) {
    if (player.getEquipment().wearingLarupiaOutfit()) {
      experience *= 1.1;
    }
    return experience;
  }

  @Override
  public Item toolHook(Player player, Item tool, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (tool.getId() == ItemId.BUTTERFLY_NET
        && player.getEquipment().getWeaponId() == ItemId.MAGIC_BUTTERFLY_NET) {
      return MAGIC_BUTTERFLY_NET;
    }
    return tool;
  }

  @Override
  public boolean skipActionHook(Player player, PEvent event, Npc npc, MapObject mapObject,
      SkillEntry entry) {
    var power = player.getSkills().getLevel(getSkillId()) + 8;
    var failure = entry.getLevel() + 2;
    if (player.inWildernessResourceArea()) {
      failure /= 2;
    }
    var chance = 0;
    if (power > failure) {
      chance = (int) ((1 - (failure + 2) / ((power + 1) * 2.0)) * 100);
    } else {
      chance = (int) ((power / ((failure + 1) * 2.0)) * 100);
    }
    chance *= 1 + getMembershipBoost(player);
    if (npc != null && player.getEquipment().getWeaponId() == ItemId.MAGIC_BUTTERFLY_NET) {
      chance += 10;
    }
    if (player.getEquipment().wearingLarupiaOutfit()) {
      chance += 10;
    }
    if (player.hasVoted()) {
      chance += 5;
    }
    chance = Math.min(chance, 100);
    return PRandom.randomE(100) > chance;
  }

  @Override
  public boolean mapObjectOptionHook(Player player, int option, MapObject mapObject) {
    switch (mapObject.getId()) {
      case ObjectId.YOUNG_TREE_8732:
      case ObjectId.YOUNG_TREE_8990:
      case ObjectId.YOUNG_TREE_8999:
      case ObjectId.YOUNG_TREE_9000:
      case ObjectId.YOUNG_TREE_9341:
        layTrap(player, -1, mapObject);
        return true;
    }
    switch (mapObject.getName().toLowerCase()) {
      case "bird snare":
      case "net trap":
      case "shaking box":
      case "box trap":
      case "young tree":
        pickupTrap(player, mapObject);
        return true;
    }
    return false;
  }

  public boolean layTrap(Player player, int itemId, MapObject fromMapObject) {
    var maxTraps = 1;
    var nextLevel = 20;
    if (player.getSkills().getLevel(Skills.HUNTER) >= 80) {
      maxTraps = 5;
    } else if (player.getSkills().getLevel(Skills.HUNTER) >= 60) {
      maxTraps = 4;
      nextLevel = 80;
    } else if (player.getSkills().getLevel(Skills.HUNTER) >= 40) {
      maxTraps = 3;
      nextLevel = 60;
    } else if (player.getSkills().getLevel(Skills.HUNTER) >= 20) {
      maxTraps = 2;
      nextLevel = 40;
    }
    if (player.getArea().inWilderness()) {
      maxTraps++;
    }
    final Item[] items;
    final MapObject[] trap;
    if (itemId == ItemId.BIRD_SNARE) {
      items = new Item[] { new Item(itemId) };
      trap = new MapObject[] { new MapObject(ObjectId.BIRD_SNARE_9345, 10, 0, player) };
    } else if (fromMapObject != null && (fromMapObject.getId() == ObjectId.YOUNG_TREE_8732
        || fromMapObject.getId() == ObjectId.YOUNG_TREE_8990
        || fromMapObject.getId() == ObjectId.YOUNG_TREE_8999
        || fromMapObject.getId() == ObjectId.YOUNG_TREE_9000
        || fromMapObject.getId() == ObjectId.YOUNG_TREE_9341)) {
      if (player.getX() != fromMapObject.getX() && player.getY() != fromMapObject.getY()) {
        return false;
      }
      items = new Item[] { new Item(ItemId.SMALL_FISHING_NET), new Item(ItemId.ROPE) };
      var direction = 0;
      if (player.getX() == fromMapObject.getX() && player.getY() < fromMapObject.getY()) {
        direction = 2;
      } else if (player.getX() > fromMapObject.getX() && player.getY() == fromMapObject.getY()) {
        direction = 1;
      } else if (player.getX() < fromMapObject.getX() && player.getY() == fromMapObject.getY()) {
        direction = 3;
      }
      trap = new MapObject[] { new MapObject(ObjectId.NET_TRAP_9343, 10, direction, player),
          new MapObject(ObjectId.YOUNG_TREE_8989, 10, direction, fromMapObject)
              .setOriginal(fromMapObject) };
    } else if (itemId == ItemId.BOX_TRAP) {
      items = new Item[] { new Item(itemId) };
      trap = new MapObject[] { new MapObject(ObjectId.BOX_TRAP_9380, 10, 0, player) };
    } else {
      return false;
    }
    var plugin = player.getPlugin(HunterPlugin.class);
    plugin.removeLostTraps();
    for (var item : items) {
      if (player.getInventory().getCount(item.getId()) < item.getAmount()) {
        player.getGameEncoder()
            .sendMessage("You need " + item.getAmount() + " " + item.getName() + " to do this.");
        return false;
      }
    }
    if (player.getController().hasSolidMapObject(trap[0])) {
      player.getGameEncoder().sendMessage("You can't set a trap here.");
      return false;
    }
    if (plugin.getTraps().size() >= maxTraps) {
      if (nextLevel > 0) {
        player.getGameEncoder()
            .sendMessage("You can't set any more traps until level " + nextLevel + ".");
      } else {
        player.getGameEncoder().sendMessage("You can't set any more traps.");
      }
      return false;
    }
    for (var i = 1; i < trap.length; i++) {
      var existing = player.getController().getSolidMapObject(trap[i]);
      if (existing != null && existing.getAttachment() instanceof TempMapObject) {
        player.getGameEncoder().sendMessage("You can't set a trap here.");
        return false;
      }
    }
    var tempTrap = new HunterTrap(player, trap, items);
    if (!tempTrap.isRunning()) {
      return false;
    }
    player.getWorld().addEvent(tempTrap);
    plugin.getTraps().add(tempTrap);
    for (var item : items) {
      player.getInventory().deleteItem(item.getId(), item.getAmount());
    }
    player.lock();
    player.setAnimation(5208);
    player.getWorld().addEvent(new PEvent(2) {
      @Override
      public void execute() {
        if (player.getController().canMove(player.getX() - 1, player.getY())) {
          player.getMovement().quickRoute(player.getX() - 1, player.getY());
        } else if (player.getController().canMove(player.getX() + 1, player.getY())) {
          player.getMovement().quickRoute(player.getX() + 1, player.getY());
        } else if (player.getController().canMove(player.getX(), player.getY() - 1)) {
          player.getMovement().quickRoute(player.getX(), player.getY() - 1);
        } else if (player.getController().canMove(player.getX(), player.getY() + 1)) {
          player.getMovement().quickRoute(player.getX(), player.getY() + 1);
        }
        player.unlock();
        stop();
      }
    });
    return true;
  }

  public boolean pickupTrap(Player player, MapObject trap) {
    if (!(trap.getAttachment() instanceof TempMapObject)) {
      return false;
    }
    var tempTrap = (TempMapObject) trap.getAttachment();
    if (!(tempTrap.getAttachment() instanceof Integer)) {
      return false;
    }
    var userId = (Integer) tempTrap.getAttachment();
    if (player.getId() != userId) {
      player.getGameEncoder().sendMessage("This trap isn't yours.");
      return false;
    }
    switch (trap.getName().toLowerCase()) {
      case "bird snare": {
        player.getInventory().addOrDropItem(ItemId.BIRD_SNARE);
        break;
      }
      case "net trap": {
        player.getInventory().addOrDropItem(ItemId.SMALL_FISHING_NET);
        player.getInventory().addOrDropItem(ItemId.ROPE);
        break;
      }
      case "shaking box":
      case "box trap": {
        player.getInventory().addOrDropItem(ItemId.BOX_TRAP);
        break;
      }
      default:
        return false;
    }
    tempTrap.setDisableScript(true);
    tempTrap.execute();
    var capturedTrap = getCapturedTrap(trap.getId());
    if (capturedTrap == null) {
      return true;
    }
    var xp = capturedTrap.getExperience();
    if (player.getEquipment().wearingLarupiaOutfit()) {
      xp *= 1.1;
    }
    player.getSkills().addXp(Skills.HUNTER, xp);
    if (capturedTrap.getItems() != null) {
      for (var item : capturedTrap.getItems()) {
        player.getInventory().addOrDropItem(item.getId(), item.getRandomAmount());
      }
    }
    if (trap.getId() == ObjectId.SHAKING_BOX_9382 || trap.getId() == ObjectId.SHAKING_BOX_9383
        || trap.getId() == ObjectId.SHAKING_BOX) {
      player.getFamiliar().rollSkillPet(Skills.HUNTER, 131395, ItemId.BABY_CHINCHOMPA_13324);
    }
    return true;
  }

  public static CapturedHunterTrap getCapturedTrap(int objectId) {
    for (var i = 0; i < HunterEntries.getCapturedTrapEntries().size(); i++) {
      var trap = HunterEntries.getCapturedTrapEntries().get(i);
      if (trap.getId() != objectId) {
        continue;
      }
      return trap;
    }
    return null;
  }

  public static boolean canCaptureTrap(Player player, int objectId) {
    return player.getSkills().getLevel(Skills.HUNTER) >= getCapturedTrapLevelRequirement(objectId);
  }

  public static int getCapturedTrapLevelRequirement(int objectId) {
    var trap = getCapturedTrap(objectId);
    return trap != null ? trap.getLevel() : 1;
  }

  public static boolean success(Player player, Npc npc, int level) {
    var entry = SkillEntry.builder().level(level).build();
    return !SkillContainer.getContainer(Hunter.class).skipActionHook(player, null, npc, null,
        entry);
  }
}
