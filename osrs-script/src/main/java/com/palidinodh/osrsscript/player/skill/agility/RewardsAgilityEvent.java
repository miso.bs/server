package com.palidinodh.osrsscript.player.skill.agility;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.random.PRandom;
import lombok.Builder;
import lombok.var;

@Builder
public class RewardsAgilityEvent extends AgilityEvent {
  private int petChance;
  private int marksOfGraceChance;

  public boolean complete(Player player) {
    if (marksOfGraceChance > 0 && PRandom.randomE(marksOfGraceChance) == 0) {
      var amount = 4;
      if (player.isPremiumMember()) {
        amount = 6;
      }
      player.getInventory().addOrDropItem(ItemId.MARK_OF_GRACE, amount);
    }
    if (PRandom.randomE(2) == 0) {
      var rewardType = PRandom.randomE(3);
      if (rewardType == 0) {
        player.getInventory().addOrDropItem(ItemId.ENERGY_POTION_4_NOTED, 1);
      } else if (rewardType == 1) {
        player.getInventory().addOrDropItem(ItemId.SUPER_ENERGY_4_NOTED, 1);
      } else if (rewardType == 2) {
        player.getInventory().addOrDropItem(ItemId.AMYLASE_CRYSTAL, 4);
      }
    }
    if (petChance > 0) {
      player.getFamiliar().rollSkillPet(Skills.AGILITY, petChance, ItemId.GIANT_SQUIRREL);
    }
    return true;
  }
}
