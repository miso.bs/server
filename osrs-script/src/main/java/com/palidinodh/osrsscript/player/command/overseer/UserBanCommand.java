package com.palidinodh.osrsscript.player.command.overseer;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.ban.BannedByUser;
import com.palidinodh.rs.ban.BannedUser;
import com.palidinodh.rs.ban.Bans;
import com.palidinodh.rs.ban.UserBan;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.reference.ReferenceName;
import lombok.var;

@ReferenceName("userban")
class UserBanCommand implements CommandHandler, CommandHandler.OverseerRank {
  @Override
  public void execute(Player player, String user, String message) {
    player.getGameEncoder().sendEnterString("Username", ue -> {
      var player2 = player.getWorld().getPlayerByUsername(ue);
      if (player2 == null) {
        player.getGameEncoder().sendMessage("Unable to find user " + ue + ".");
        return;
      }
      player.getGameEncoder().sendEnterAmount("Number of Hours (Max: 12)", he -> {
        if (he <= 0 || he > 12) {
          player.getGameEncoder()
              .sendMessage("Bans longer than 12 hours need to be applied on the website.");
          return;
        }
        player.getGameEncoder().sendEnterString("Reason", re -> {
          Bans.addBan(new UserBan(
              new BannedUser(player2.getId(), player2.getUsername().toLowerCase(),
                  player2.getSession().getRemoteAddress(), player2.getSession().getMacAddress(),
                  player2.getSession().getUuid()),
              new BannedByUser(player.getId(), player.getUsername()), he, re));
          player2.unlock();
          player2.getGameEncoder().sendLogout();
          player2.setVisible(false);
          var duration = he > 0 ? "for " + he + " hours" : "permanently";
          player.getGameEncoder().sendMessage(ue + " has been user banned " + duration + ".");
          player.log(PlayerLogType.STAFF, "user banned " + player2.getLogName() + " " + duration);
        });
      });
    });
  }
}
