package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.ELDRITCH_NIGHTMARE_STAFF)
class EldritchNightmareStaffSpecialAttack extends SpecialAttack {
  public EldritchNightmareStaffSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(75);
    entry.setAnimation(8532);
    entry.setCastGraphic(new Graphic(1762, 0, 50));
    entry.setTargetGraphic(new Graphic(1761));
    entry.setMagic(true);
    entry.setMagicDamage(34);
    entry.setMagicLevelScale(75);
    entry.setMagicLevelScaleDivider(2.4);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    hooks.getPlayer().getPrayer().changePoints(hooks.getDamage() / 2, 21);
  }
}
