package com.palidinodh.osrsscript.player.achievementdiary;

import static com.palidinodh.osrscore.model.entity.player.AchievementDiaryTask.Difficulty.EASY;
import static com.palidinodh.osrscore.model.entity.player.AchievementDiaryTask.Difficulty.ELITE;
import static com.palidinodh.osrscore.model.entity.player.AchievementDiaryTask.Difficulty.HARD;
import static com.palidinodh.osrscore.model.entity.player.AchievementDiaryTask.Difficulty.MEDIUM;
import java.util.Arrays;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.cache.widget.SpellbookChild;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.AchievementDiary;
import com.palidinodh.osrscore.model.entity.player.AchievementDiaryTask;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.Shop;
import com.palidinodh.osrscore.model.item.ShopItem;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;

@SuppressWarnings("unused")
class FremennikDiary extends AchievementDiary {
  public FremennikDiary() {
    super(AchievementDiary.Name.FREMENNIK);
  }

  @Override
  public AchievementDiaryTask[] getTasks() {
    return Arrays.stream(FremennikTask.values()).map(FremennikTask::getTask)
        .toArray(AchievementDiaryTask[]::new);
  }

  @Override
  public void animation(Player player, int id, int delay) {
    if (true) {
      return;
    }
    if (id == 4959 || id == 4961 || id == 4981 || id == 4971 || id == 4973 || id == 4979
        || id == 4939 || id == 4955 || id == 4957 || id == 4937 || id == 4951 || id == 4975
        || id == 4949 || id == 4943 || id == 4941 || id == 4969 || id == 4977 || id == 4965
        || id == 4967 || id == 4963 || id == 4947 || id == 5158 || id == 4953 || id == 7121) {
      if (player.getHeight() == 3 && player.within(2954, 3328, 2998, 3353)) {
        // addCompletedTask(player, FaladorTask.CAPE_EMOTE);
      }
    }
  }

  @Override
  public void teleported(Player player, Tile fromTile) {
    if (true) {
      return;
    }
    if (player.getRegionId() == 11321 && fromTile.getRegionId() == 11421) {
      // addCompletedTask(player, FremennikTask.TROLL_STRONGHOLD);
    }
  }

  @Override
  public void equipItem(Player player, Item item, int slot) {
    if (true) {
      return;
    }
    if (item.getId() == ItemId.DWARVEN_HELMET && player.within(2962, 9699, 3061, 9852)) {
      // addCompletedTask(player, FaladorTask.DWARVEN_HELMET);
    }
  }

  @Override
  public void buyShopItem(Player player, Shop shop, ShopItem shopItem, Item item, int price) {
    if (true) {
      return;
    }
    if (item.getId() == ItemId.WHITE_2H_SWORD && item.getAmount() > 0
        && shopItem.getMonsterKillCount() > 0) {
      // addCompletedTask(player, FaladorTask.WHITE_2H);
    }
  }

  @Override
  public void makeItem(Player player, int skillId, Item item, Npc npc, MapObject mapObject) {
    if (true) {
      return;
    }
    if (item.getId() == ItemId.BUCKET_OF_WATER && player.getRegionId() == 10553) {
      addCompletedTask(player, FremennikTask.FILL_A_BUCKET);
    } else if (item.getId() == ItemId.TIARA) {
      addCompletedTask(player, FremennikTask.CRAFT_TIARA);
    } else if (item.getId() == ItemId.BLURITE_LIMBS) {
      // addCompletedTask(player, FaladorTask.BLURITE_LIMBS);
    } else if (item.getId() == ItemId.BULLSEYE_LANTERN_4550
        && player.within(2925, 3205, 2941, 3215)) {
      // addCompletedTask(player, FaladorTask.BULLSEYE_LANTERN);
    } else if (npc != null && npc.getId() == NpcId.CERULEAN_TWITCH
        && (player.getRegionId() == 10811 || player.getRegionId() == 10810)) {
      addCompletedTask(player, FremennikTask.CERULEAN_TWITCH);
      // TODO: Check if this works
    } else if (item.getId() == ItemId.GOLD_ORE && player.getRegionId() == 11571) {
      // addCompletedTask(player, FaladorTask.MINE_GOLD);
    } else if (item.getId() == ItemId.MIND_RUNE && item.getAmount() >= 140) {
      // addCompletedTask(player, FaladorTask.CRAFT_MINDS);
    } else if (item.getId() == ItemId.ASTRAL_RUNE && item.getAmount() >= 56) {
      addCompletedTask(player, FremennikTask.ASTRAL_RUNES);
    } else if (item.getId() == ItemId.MAGIC_ROOTS && item.getAmount() >= 3) {
      // addCompletedTask(player, FaladorTask.MAGIC_ROOTS);
    } else if (item.getId() == ItemId.SARADOMIN_BREW_3 && player.within(3009, 3355, 3019, 3358)) {
      // addCompletedTask(player, FaladorTask.SARADOMIN_BREW);
    }
  }

  @Override
  public void makeFire(Player player, Item item) {
    if (true) {
      return;
    }
    if (item.getId() == ItemId.OAK_LOGS && player.getRegionId() == 10552) {
      addCompletedTask(player, FremennikTask.CHOP_OAK_LOGS);
    }
  }

  @Override
  public void castSpell(Player player, SpellbookChild spellbookChild, Item item, Entity entity,
      MapObject mapObject) {
    if (true) {
      return;
    }
    if (spellbookChild == SpellbookChild.TROLLHEIM_TELEPORT) {
      addCompletedTask(player, FremennikTask.TELEPORT_TO_TROLLHEIM);
    } else if (spellbookChild == SpellbookChild.WATERBIRTH_TELEPORT) {
      addCompletedTask(player, FremennikTask.TELEPORT_TO_WATERBIRTH);
    }
  }

  @Override
  public void agilityObstacle(Player player, MapObject mapObject) {
    if (true) {
      return;
    }
    if (mapObject.getId() == 11404 && player.getRegionId() == 10553) {
      addCompletedTask(player, FremennikTask.RELLEKKA_AGILITY);
    }
  }

  @Override
  public void npcKilled(Player player, Npc npc) {
    if (true) {
      return;
    }
    if (npc.getId() == NpcId.ROCK_CRAB_13) {
      addCompletedTask(player, FremennikTask.KILL_ROCK_CRAB);
    } else if (npc.getId() == NpcId.DAGANNOTH_SUPREME_303) {
      addCompletedTask(player, FremennikTask.DAGANNOTH_SUPREME);
    } else if (npc.getId() == NpcId.DAGANNOTH_PRIME_303) {
      addCompletedTask(player, FremennikTask.DAGANNOTH_PRIME);
    } else if (npc.getId() == NpcId.DAGANNOTH_REX_303) {
      addCompletedTask(player, FremennikTask.DAGANNOTH_REX);
    }
  }

  @Override
  public void openShop(Player player, String referenceName) {
    if (true) {
      return;
    }
    if (referenceName != null && referenceName.equals("skilling")
        && player.getRegionId() == 12083) {
      // addCompletedTask(player, FaladorTask.SARAHS_SHOP);
    }
  }

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    if (true) {
      return;
    }
    if (mapObject.getId() == 172 && mapObject.getX() == 2914 && mapObject.getY() == 3452
        && player.getInventory().hasItem(ItemId.CRYSTAL_KEY)) {
      // addCompletedTask(player, FaladorTask.CRYSTAL_CHEST);
    } else if (mapObject.getId() == 410
        && player.getEquipment().getHeadId() == ItemId.INITIATE_SALLET
        && player.getEquipment().getChestId() == ItemId.INITIATE_HAUBERK
        && player.getEquipment().getLegId() == ItemId.INITIATE_CUISSE) {
      // addCompletedTask(player, FaladorTask.GUTHIX_ALTAR);
    } else if (mapObject.getId() == 409
        && player.getEquipment().getHeadId() == ItemId.PROSELYTE_SALLET
        && player.getEquipment().getChestId() == ItemId.PROSELYTE_HAUBERK
        && player.getEquipment().getLegId() == ItemId.PROSELYTE_CUISSE) {
      // addCompletedTask(player, FaladorTask.SARIM_ALTAR);
    } else if (mapObject.getId() == 24318) {
      if (player.getController().getLevelForXP(Skills.ATTACK) == 99
          || player.getController().getLevelForXP(Skills.STRENGTH) == 99
          || player.getController().getLevelForXP(Skills.ATTACK)
              + player.getController().getLevelForXP(Skills.STRENGTH) >= 130) {
        // addCompletedTask(player, FaladorTask.WARRIORS_GUILD);
      }
    }
  }

  @Override
  public void npcOption(Player player, int option, Npc npc) {
    if (true) {
      return;
    }
    if (npc.getId() == NpcId.SECURITY_GUARD) {
      // addCompletedTask(player, FaladorTask.SECURITY_BOOK);
    }
  }

  public void addCompletedTask(Player player, FremennikTask task) {
    if (true) {
      return;
    }
    super.addCompletedTask(player, task.getTask());
  }
}


enum FremennikTask {
  CERULEAN_TWITCH(new AchievementDiaryTask("Catch a cerulean twitch.", EASY)),
  KILL_ROCK_CRAB(new AchievementDiaryTask("Kill a Rock Crab.", EASY)),
  CRAFT_TIARA(new AchievementDiaryTask("Craft a tiara from scratch in Rellekka.", EASY)),
  STONEMASON_SHOP(new AchievementDiaryTask("Browse the Stonemason's shop.", EASY)),
  COLLECT_SNAPE_GRASS(new AchievementDiaryTask("Collect a snape grass on Waterbirth.", EASY)),
  STEAL_KELDAGRIM(
      new AchievementDiaryTask("Steal from the Keldagrim crafting or baker's stall.", EASY)),
  FILL_A_BUCKET(new AchievementDiaryTask("Fill a bucket with water at the Rellekka well.", EASY)),
  CHOP_OAK_LOGS(
      new AchievementDiaryTask("Chop and burn some oak logs in the Fremennik Province.", EASY)),

  BRINE_RAT(new AchievementDiaryTask("Slay a brine rat.", MEDIUM)),
  SNOWY_HUNTER_AREA(new AchievementDiaryTask("Travel to the Snowy Hunter Area via Eagle.", MEDIUM)),
  RELLEKKA_COAL(new AchievementDiaryTask("Mine some coal in Rellekka.", MEDIUM)),
  STEAL_RELLEKKA(new AchievementDiaryTask("Steal from the Rellekka fish stalls.", MEDIUM)),
  MISCELLANIA_FAIRY(new AchievementDiaryTask("Travel to Miscellania by fairy ring.", MEDIUM)),
  SNOWY_KNIGHT(new AchievementDiaryTask("Catch a Snowy knight.", MEDIUM)),
  PET_ROCK(new AchievementDiaryTask("Pick up your pet rock from your POH menagerie.", MEDIUM)),
  WATERBIRTH_LIGHTHOUSE(
      new AchievementDiaryTask("Visit the Lighthouse from Waterbirth Island.", MEDIUM)),
  ARZINIAN_GOLD(new AchievementDiaryTask("Mine some gold at the Arzinian Mine.", MEDIUM)),

  TELEPORT_TO_TROLLHEIM(new AchievementDiaryTask("Teleport to Trollheim.", HARD)),
  SABRE_TOOTHED_KYATT(new AchievementDiaryTask("Catch a Sabre-toothed kyatt.", HARD)),
  SUPER_DEFENCE_POTION(
      new AchievementDiaryTask("Mix a Super defence potion in the Fremennik Province.", HARD)),
  STEAL_KELDAGRIM_GEM(new AchievementDiaryTask("Steal from the Keldagrim Gem Stall.", HARD)),
  CRAFT_FREMENNIK_SHIELD(new AchievementDiaryTask("Craft a Fremennik shield on Neitiznot.", HARD)),
  MINE_ADAMANT(new AchievementDiaryTask("Mine 5 adamantite ores on Jatizso.", HARD)),
  TELEPORT_TO_WATERBIRTH(new AchievementDiaryTask("Teleport to Waterbirth Island.", HARD)),

  DAGANNOTH_SUPREME(new AchievementDiaryTask("Kill the Dagannoth Supreme.", ELITE)),
  DAGANNOTH_REX(new AchievementDiaryTask("Kill the Dagannoth Rex.", ELITE)),
  DAGANNOTH_PRIME(new AchievementDiaryTask("Kill the Dagannoth Prime.", ELITE)),
  ASTRAL_RUNES(new AchievementDiaryTask("Craft 56 astral runes at once.", ELITE)),
  DRAGONSTONE_AMULET(
      new AchievementDiaryTask("Create a dragonstone amulet in the Neitiznot furnace.", ELITE)),
  RELLEKKA_AGILITY(
      new AchievementDiaryTask("Complete a lap of the Rellekka Agility Course.", ELITE)),
  GODWARS_GENERALS(new AchievementDiaryTask("Kill each of the God Wars Dungeon generals.", ELITE)),
  SPIRITUAL_MAGE(
      new AchievementDiaryTask("Slay a Spiritual mage within the God Wars Dungeon.", ELITE));

  private AchievementDiaryTask task;

  private FremennikTask(AchievementDiaryTask task) {
    this.task = task;
  }

  public AchievementDiaryTask getTask() {
    return task;
  }
}
