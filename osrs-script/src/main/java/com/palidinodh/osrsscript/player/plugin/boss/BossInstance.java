package com.palidinodh.osrsscript.player.plugin.boss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrsscript.map.area.trollcountry.godwars.GodWarsDungeonArea;
import com.palidinodh.rs.cache.definition.osrs.NpcDefinition;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PEvent;
import lombok.Getter;
import lombok.var;

class BossInstance {
  private static final Map<Integer, BossInstance> INSTANCES;

  private enum TeleportType {
    DEFAULT, LADDER_UP, LADDER_DOWN
  }

  private Tile playerTile;
  private TeleportType teleportType = TeleportType.DEFAULT;
  private boolean taskOnly;
  private CanStartEvent canStartEvent;
  private StartEvent startEvent;
  @Getter
  private List<NpcSpawn> spawns = new ArrayList<>();

  public static void start(Player player, int npcId, boolean isPrivate) {
    var instance = INSTANCES.get(npcId);
    if (instance == null) {
      return;
    }
    var requiresCost = npcId != NpcId.KRAKEN_291 && npcId != NpcId.VORKATH_732
        && npcId != NpcId.ALCHEMICAL_HYDRA_426 && npcId != NpcId.DUSK_248;
    if (instance.taskOnly && !Settings.getInstance().isLocal() && !Settings.getInstance().isBeta()
        && !Settings.getInstance().isSpawn()) {
      if (!player.getSkills().isAnySlayerTask(npcId)) {
        player.getGameEncoder().sendMessage("You need an appropriate Slayer task to do this.");
        return;
      }
    }
    if (requiresCost && !player.getInventory()
        .hasItem(ItemId.BOSS_INSTANCE_SCROLL_32313)/*
                                                    * && !player.getCharges().hasRoWICharge(1)
                                                    */) {
      player.getGameEncoder().sendMessage("You need an instance creation item to do this.");
      return;
    }
    if (!isPrivate && !player.getMessaging().canClanChatEvent()) {
      player.getGameEncoder()
          .sendMessage("Your Clan Chat privledges aren't high enough to do that.");
      return;
    }
    var clanChatUsername = player.getMessaging().getClanChatUsername();
    var playerInstance =
        player.getWorld().getPlayerBossInstance(clanChatUsername, player.getController());
    if (!isPrivate && playerInstance != null
        && playerInstance.isController(BossInstanceController.class)) {
      player.getGameEncoder().sendMessage("There is already a boss instance for this Clan Chat.");
      return;
    }
    if (instance.canStartEvent != null && !instance.canStartEvent.canStart(player)) {
      return;
    }
    if (requiresCost
        && !player.getInventory().deleteItem(ItemId.BOSS_INSTANCE_SCROLL_32313).success()) {
      player.getGameEncoder().sendMessage("You need an instance creation item to do this.");
      return;
    }
    player.setController(new BossInstanceController());
    player.getController().startInstance();
    if (instance.teleportType == TeleportType.DEFAULT) {
      player.getMovement().teleport(instance.playerTile);
    } else if (instance.teleportType == TeleportType.LADDER_UP) {
      player.getMovement().ladderUpTeleport(instance.playerTile);
    } else if (instance.teleportType == TeleportType.LADDER_DOWN) {
      player.getMovement().ladderDownTeleport(instance.playerTile);
    }
    player.getController().cast(BossInstanceController.class).setBossInstance(npcId, instance,
        isPrivate);
    if (instance.startEvent != null) {
      instance.startEvent.start(player);
    }
    if (!isPrivate) {
      player.getWorld().putPlayerBossInstance(clanChatUsername, player.getController());
    }
  }

  public static void join(Player player, int npcId, boolean isClanChat) {
    var instance = INSTANCES.get(npcId);
    if (instance == null) {
      player.getGameEncoder().sendMessage("Unable to locate boss " + NpcDefinition.getName(npcId));
      return;
    }
    if (instance.taskOnly && !Settings.getInstance().isLocal() && !Settings.getInstance().isBeta()
        && !Settings.getInstance().isSpawn()) {
      if (!player.getSkills().isAnySlayerTask(npcId)) {
        player.getGameEncoder().sendMessage("You need an appropriate Slayer task to do this.");
        return;
      }
    }
    var clanChatUsername = player.getMessaging().getClanChatUsername();
    var playerInstance =
        player.getWorld().getPlayerBossInstance(clanChatUsername, player.getController());
    if (isClanChat
        && (playerInstance == null || !playerInstance.isController(BossInstanceController.class))) {
      player.getGameEncoder().sendMessage("Unable to locate a boss instance for this Clan Chat.");
      return;
    }
    if (isClanChat && npcId != playerInstance.cast(BossInstanceController.class).getNpcId()) {
      player.getGameEncoder().sendMessage("The boss instance for this Clan Chat already exists for "
          + NpcDefinition.getName(npcId) + ".");
      return;
    }
    if (!isClanChat && instance.canStartEvent != null && !instance.canStartEvent.canStart(player)) {
      return;
    }
    if (isClanChat) {
      player.setController(new BossInstanceController());
      player.getController().joinInstance(playerInstance);
    }
    if (instance.teleportType == TeleportType.DEFAULT) {
      player.getMovement().teleport(instance.playerTile);
    } else if (instance.teleportType == TeleportType.LADDER_UP) {
      player.getMovement().ladderUpTeleport(instance.playerTile);
    } else if (instance.teleportType == TeleportType.LADDER_DOWN) {
      player.getMovement().ladderDownTeleport(instance.playerTile);
    }
    if (isClanChat) {
      player.getWorld().putPlayerBossInstance(clanChatUsername, player.getController());
    }
    if (!isClanChat && instance.startEvent != null) {
      instance.startEvent.start(player);
    }
  }

  static {
    var instances = new HashMap<Integer, BossInstance>();

    var instance = new BossInstance();
    instance.playerTile = new Tile(2974, 4384, 2);
    instance.spawns = new ArrayList<NpcSpawn>();
    instance.spawns.add(new NpcSpawn(NpcId.CORPOREAL_BEAST_785, new Tile(2986, 4381, 2), 8));
    instances.put(NpcId.CORPOREAL_BEAST_785, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<NpcSpawn>();
    instance.playerTile = new Tile(2900, 4449);
    instance.teleportType = TeleportType.LADDER_UP;
    instance.spawns.add(new NpcSpawn(NpcId.DAGANNOTH_REX_303, new Tile(2920, 4441), 4));
    instance.spawns.add(new NpcSpawn(NpcId.DAGANNOTH_PRIME_303, new Tile(2912, 4455), 4));
    instance.spawns.add(new NpcSpawn(NpcId.DAGANNOTH_SUPREME_303, new Tile(2906, 4441), 4));
    instances.put(NpcId.DAGANNOTH_REX_303, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<NpcSpawn>();
    instance.playerTile = new Tile(2271, 4680);
    instance.spawns.add(new NpcSpawn(NpcId.KING_BLACK_DRAGON_276, new Tile(2269, 4696), 16));
    instances.put(NpcId.KING_BLACK_DRAGON_276, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<NpcSpawn>();
    instance.playerTile = new Tile(1304, 1291);
    instance.taskOnly = true;
    instance.spawns.add(new NpcSpawn(NpcId.CERBERUS_318, new Tile(1302, 1314)));
    instances.put(NpcId.CERBERUS_318, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<NpcSpawn>();
    instance.playerTile = new Tile(2376, 9452);
    instance.teleportType = TeleportType.LADDER_DOWN;
    instance.taskOnly = true;
    instance.spawns.add(new NpcSpawn(NpcId.THERMONUCLEAR_SMOKE_DEVIL_301, new Tile(2363, 9449), 8));
    instance.spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2371, 9452), 8));
    instance.spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2357, 9454), 8));
    instance.spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2356, 9445), 8));
    instance.spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2363, 9443), 8));
    instance.spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2370, 9444), 8));
    instance.spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2366, 9455), 8));
    instances.put(NpcId.THERMONUCLEAR_SMOKE_DEVIL_301, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<NpcSpawn>();
    instance.playerTile = new Tile(1752, 5236);
    instance.teleportType = TeleportType.LADDER_DOWN;
    instance.startEvent = p -> {
      if (!p.getInventory().hasItem(ItemId.FALADOR_SHIELD_3)
          && p.getInventory().hasItem(ItemId.FALADOR_SHIELD_4)) {
        return;
      }
      var event = new PEvent(10) {
        @Override
        public void execute() {
          if (!p.isVisible()) {
            stop();
            return;
          }
          if (!p.inMoleLair()) {
            stop();
            p.getGameEncoder().sendHintIconReset();
            return;
          }
          var mole = p.getController().getNpc(NpcId.GIANT_MOLE_230);
          if (mole == null || !mole.isVisible()) {
            p.getGameEncoder().sendHintIconReset();
          } else if (p.withinVisibilityDistance(mole)) {
            p.getGameEncoder().sendHintIconNpc(mole.getIndex());
          } else {
            p.getGameEncoder().sendHintIconTile(mole);
          }
        }
      };
      p.getWorld().addEvent(event);
    };
    instance.spawns.add(new NpcSpawn(NpcId.GIANT_MOLE_230, new Tile(1759, 5184), 64));
    instances.put(NpcId.GIANT_MOLE_230, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<NpcSpawn>();
    instance.playerTile = new Tile(2839, 5296, 2);
    instance.canStartEvent = p -> {
      if (!p.getArea().isArea(GodWarsDungeonArea.class)) {
        return false;
      }
      var area = p.getArea().cast(GodWarsDungeonArea.class);
      if (area.getArmadylKillcount() < 40 && !p.getInventory().hasItem(ItemId.ECUMENICAL_KEY)) {
        p.getGameEncoder().sendMessage("You need 40 killcount to enter.");
        return false;
      }
      return true;
    };
    instance.startEvent = p -> {
      if (!p.getArea().isArea(GodWarsDungeonArea.class)) {
        return;
      }
      var area = p.getArea().cast(GodWarsDungeonArea.class);
      if (area.getArmadylKillcount() >= 40) {
        area.setArmadylKillcount(area.getArmadylKillcount() - 40);
      } else {
        p.getInventory().deleteItem(ItemId.ECUMENICAL_KEY);
      }
    };
    instance.spawns.add(new NpcSpawn(NpcId.KREEARRA_580, new Tile(2831, 5302, 2), 6));
    instance.spawns.add(new NpcSpawn(NpcId.WINGMAN_SKREE_143, new Tile(2840, 5302, 2), 6));
    instance.spawns.add(new NpcSpawn(NpcId.FLOCKLEADER_GEERIN_149, new Tile(2827, 5299, 2), 6));
    instance.spawns.add(new NpcSpawn(NpcId.FLIGHT_KILISA_159, new Tile(2832, 5297, 2), 6));
    instances.put(NpcId.KREEARRA_580, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<NpcSpawn>();
    instance.playerTile = new Tile(2864, 5354, 2);
    instance.canStartEvent = p -> {
      if (!p.getArea().isArea(GodWarsDungeonArea.class)) {
        return false;
      }
      var area = p.getArea().cast(GodWarsDungeonArea.class);
      if (area.getBandosKillcount() < 40 && !p.getInventory().hasItem(ItemId.ECUMENICAL_KEY)) {
        p.getGameEncoder().sendMessage("You need 40 killcount to enter.");
        return false;
      }
      return true;
    };
    instance.startEvent = p -> {
      if (!p.getArea().isArea(GodWarsDungeonArea.class)) {
        return;
      }
      var area = p.getArea().cast(GodWarsDungeonArea.class);
      if (area.getBandosKillcount() >= 40) {
        area.setBandosKillcount(area.getBandosKillcount() - 40);
      } else {
        p.getInventory().deleteItem(ItemId.ECUMENICAL_KEY);
      }
    };
    instance.spawns.add(new NpcSpawn(NpcId.GENERAL_GRAARDOR_624, new Tile(2872, 5358, 2), 6));
    instance.spawns.add(new NpcSpawn(NpcId.SERGEANT_STRONGSTACK_141, new Tile(2866, 5358, 2), 6));
    instance.spawns.add(new NpcSpawn(NpcId.SERGEANT_STEELWILL_142, new Tile(2873, 5353, 2), 6));
    instance.spawns.add(new NpcSpawn(NpcId.SERGEANT_GRIMSPIKE_142, new Tile(2868, 5362, 2), 6));
    instances.put(NpcId.GENERAL_GRAARDOR_624, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<NpcSpawn>();
    instance.playerTile = new Tile(2925, 5331, 2);
    instance.canStartEvent = p -> {
      if (!p.getArea().isArea(GodWarsDungeonArea.class)) {
        return false;
      }
      var area = p.getArea().cast(GodWarsDungeonArea.class);
      if (area.getZamorakKillcount() < 40 && !p.getInventory().hasItem(ItemId.ECUMENICAL_KEY)) {
        p.getGameEncoder().sendMessage("You need 40 killcount to enter.");
        return false;
      }
      return true;
    };
    instance.startEvent = p -> {
      if (!p.getArea().isArea(GodWarsDungeonArea.class)) {
        return;
      }
      var area = p.getArea().cast(GodWarsDungeonArea.class);
      if (area.getZamorakKillcount() >= 40) {
        area.setZamorakKillcount(area.getZamorakKillcount() - 40);
      } else {
        p.getInventory().deleteItem(ItemId.ECUMENICAL_KEY);
      }
    };
    instance.spawns.add(new NpcSpawn(NpcId.KRIL_TSUTSAROTH_650, new Tile(2925, 5322, 2), 6));
    instance.spawns.add(new NpcSpawn(NpcId.TSTANON_KARLAK_145, new Tile(2932, 5328, 2), 6));
    instance.spawns.add(new NpcSpawn(NpcId.ZAKLN_GRITCH_142, new Tile(2919, 5327, 2), 6));
    instance.spawns.add(new NpcSpawn(NpcId.BALFRUG_KREEYATH_151, new Tile(2921, 5320, 2), 6));
    instances.put(NpcId.KRIL_TSUTSAROTH_650, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<NpcSpawn>();
    instance.playerTile = new Tile(2907, 5265);
    instance.canStartEvent = p -> {
      if (!p.getArea().isArea(GodWarsDungeonArea.class)) {
        return false;
      }
      var area = p.getArea().cast(GodWarsDungeonArea.class);
      if (area.getSaradominKillcount() < 40 && !p.getInventory().hasItem(ItemId.ECUMENICAL_KEY)) {
        p.getGameEncoder().sendMessage("You need 40 killcount to enter.");
        return false;
      }
      return true;
    };
    instance.startEvent = p -> {
      if (!p.getArea().isArea(GodWarsDungeonArea.class)) {
        return;
      }
      var area = p.getArea().cast(GodWarsDungeonArea.class);
      if (area.getSaradominKillcount() >= 40) {
        area.setSaradominKillcount(area.getSaradominKillcount() - 40);
      } else {
        p.getInventory().deleteItem(ItemId.ECUMENICAL_KEY);
      }
    };
    instance.spawns.add(new NpcSpawn(NpcId.COMMANDER_ZILYANA_596, new Tile(2897, 5267), 6));
    instance.spawns.add(new NpcSpawn(NpcId.STARLIGHT_149, new Tile(2903, 5261), 6));
    instance.spawns.add(new NpcSpawn(NpcId.GROWLER_139, new Tile(2896, 5262), 6));
    instance.spawns.add(new NpcSpawn(NpcId.BREE_146, new Tile(2902, 5272), 6));
    instances.put(NpcId.COMMANDER_ZILYANA_596, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<NpcSpawn>();
    instance.playerTile = new Tile(2983, 4820);
    instance.taskOnly = true;
    instance.spawns.add(new NpcSpawn(NpcId.ABYSSAL_SIRE_350, new Tile(2977, 4855)));
    instances.put(NpcId.ABYSSAL_SIRE_350, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<NpcSpawn>();
    instance.playerTile = new Tile(2280, 10022);
    instance.teleportType = TeleportType.LADDER_DOWN;
    instance.taskOnly = true;
    instance.spawns.add(new NpcSpawn(NpcId.WHIRLPOOL, new Tile(2278, 10035)));
    instances.put(NpcId.KRAKEN_291, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<NpcSpawn>();
    instance.playerTile = new Tile(3506, 9494);
    instance.teleportType = TeleportType.LADDER_DOWN;
    instance.spawns.add(new NpcSpawn(NpcId.KALPHITE_QUEEN_333, new Tile(3476, 9492), 8));
    instance.spawns.add(new NpcSpawn(NpcId.KALPHITE_GUARDIAN_141_960, new Tile(3496, 9500), 8));
    instance.spawns.add(new NpcSpawn(NpcId.KALPHITE_GUARDIAN_141_960, new Tile(3494, 9489), 8));
    instance.spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28_956, new Tile(3475, 9488), 8));
    instance.spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28_956, new Tile(3483, 9486), 8));
    instance.spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28_956, new Tile(3488, 9492), 8));
    instance.spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28_956, new Tile(3495, 9496), 8));
    instance.spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28_956, new Tile(3494, 9508), 8));
    instance.spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28_956, new Tile(3485, 9513), 8));
    instance.spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28_956, new Tile(3477, 9506), 8));
    instance.spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28_956, new Tile(3471, 9498), 8));
    instances.put(NpcId.KALPHITE_QUEEN_333, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<NpcSpawn>();
    instance.playerTile = new Tile(2272, 4054);
    instance.spawns.add(new NpcSpawn(NpcId.VORKATH_8059, new Tile(2269, 4062)));
    instances.put(NpcId.VORKATH_732, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<NpcSpawn>();
    instance.playerTile = new Tile(1356, 10258);
    instance.taskOnly = true;
    instance.spawns.add(new NpcSpawn(NpcId.ALCHEMICAL_HYDRA_426, new Tile(1364, 10265), 16));
    instances.put(NpcId.ALCHEMICAL_HYDRA_426, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<NpcSpawn>();
    instance.playerTile = new Tile(1696, 4574);
    instance.taskOnly = true;
    instances.put(NpcId.DUSK_248, instance);

    for (var i : instances.values()) {
      for (var spawn : i.spawns) {
        spawn.respawnable(true);
      }
    }
    INSTANCES = Collections.unmodifiableMap(instances);
  }


  private interface CanStartEvent {
    boolean canStart(Player player);
  }


  private interface StartEvent {
    void start(Player player);
  }
}
