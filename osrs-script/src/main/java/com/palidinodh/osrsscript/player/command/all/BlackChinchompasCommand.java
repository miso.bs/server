package com.palidinodh.osrsscript.player.command.all;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrsscript.player.plugin.magic.SpellTeleport;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("chins")
class BlackChinchompasCommand implements CommandHandler, CommandHandler.Teleport {
  @Override
  public String getExample(String name) {
    return "- Teleports you to wilderness black chins";
  }

  @Override
  public void execute(Player player, String name, String message) {
    player
        .openDialogue(new OptionsDialogue("Are you sure you want to teleport into the wilderness?",
            new DialogueOption("Yes, teleport me into the wilderness!", (c, s) -> {
              if (!player.getController().canTeleport(true)) {
                return;
              }
              if (player.getInCombatDelay() > 0) {
                player.getGameEncoder().sendMessage("You can't do this while in combat.");
                return;
              }
              SpellTeleport.normalTeleport(player, new Tile(3137, 3784));
              player.getController().stopWithTeleport();
              player.getGameEncoder().sendMessage("You teleport to the black chinchompas.");
            }), new DialogueOption("No!")));
  }
}
