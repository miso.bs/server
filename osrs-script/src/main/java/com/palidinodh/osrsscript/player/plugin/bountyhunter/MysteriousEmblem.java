package com.palidinodh.osrsscript.player.plugin.bountyhunter;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.item.ItemDef;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.var;

@AllArgsConstructor
@Getter
public enum MysteriousEmblem {
  ARCHAIC_TIER_1(ItemId.ARCHAIC_EMBLEM_TIER_1, 75_000, 0, 0),
  ARCHAIC_TIER_2(ItemId.ARCHAIC_EMBLEM_TIER_2, 100_000, 0, 0),
  ARCHAIC_TIER_3(ItemId.ARCHAIC_EMBLEM_TIER_3, 200_000, 0, 0),
  ARCHAIC_TIER_4(ItemId.ARCHAIC_EMBLEM_TIER_4, 400_000, 0, 0),
  ARCHAIC_TIER_5(ItemId.ARCHAIC_EMBLEM_TIER_5, 750_000, 0, 0),
  ARCHAIC_TIER_6(ItemId.ARCHAIC_EMBLEM_TIER_6, 1_200_000, 0, 0),
  ARCHAIC_TIER_7(ItemId.ARCHAIC_EMBLEM_TIER_7, 1_750_000, 0, 0),
  ARCHAIC_TIER_8(ItemId.ARCHAIC_EMBLEM_TIER_8, 2_500_000, 0, 0),
  ARCHAIC_TIER_9(ItemId.ARCHAIC_EMBLEM_TIER_9, 3_500_000, 0, 0),
  ARCHAIC_TIER_10(ItemId.ARCHAIC_EMBLEM_TIER_10, 5_000_000, 0, 0),
  ARCHAIC_V2_TIER_1(ItemId.MYSTERIOUS_EMBLEM_TIER_1, 75_000, 0, 0),
  ARCHAIC_V2_TIER_2(ItemId.MYSTERIOUS_EMBLEM_TIER_2, 150_000, 0, 0),
  ARCHAIC_V2_TIER_3(ItemId.MYSTERIOUS_EMBLEM_TIER_3, 300_000, 0, 0),
  ARCHAIC_V2_TIER_4(ItemId.MYSTERIOUS_EMBLEM_TIER_4, 600_000, 0, 0),
  ARCHAIC_V2_TIER_5(ItemId.MYSTERIOUS_EMBLEM_TIER_5, 1_200_000, 0, 0),
  ANTIQUE_TIER_1(ItemId.ANTIQUE_EMBLEM_TIER_1, 50_000, 0, 1),
  ANTIQUE_TIER_2(ItemId.ANTIQUE_EMBLEM_TIER_2, 100_000 * 4, 2_000, 2),
  ANTIQUE_TIER_3(ItemId.ANTIQUE_EMBLEM_TIER_3, 200_000 * 4, 4_000, 3),
  ANTIQUE_TIER_4(ItemId.ANTIQUE_EMBLEM_TIER_4, 400_000 * 4, 8_000, 4),
  ANTIQUE_TIER_5(ItemId.ANTIQUE_EMBLEM_TIER_5, 750_000 * 4, 20_000, 5),
  ANTIQUE_TIER_6(ItemId.ANTIQUE_EMBLEM_TIER_6, 1_200_000 * 4, 24_000, 6),
  ANTIQUE_TIER_7(ItemId.ANTIQUE_EMBLEM_TIER_7, 1_750_000 * 4, 35_000, 7),
  ANTIQUE_TIER_8(ItemId.ANTIQUE_EMBLEM_TIER_8, 2_500_000 * 4, 50_000, 8),
  ANTIQUE_TIER_9(ItemId.ANTIQUE_EMBLEM_TIER_9, 3_500_000 * 4, 70_000, 9),
  ANTIQUE_TIER_10(ItemId.ANTIQUE_EMBLEM_TIER_10, 5_000_000 * 4, 100_000, 10);

  private int itemId;
  private int coinValue;
  private int bloodMoneyValue;
  private int varbitValue;

  public boolean isArchaic() {
    return name().startsWith("ARCHAIC_");
  }

  public static int getCoinValue(int itemId) {
    for (MysteriousEmblem emblem : values()) {
      if (itemId == emblem.itemId || ItemDef.getUnnotedId(itemId) == emblem.itemId) {
        return emblem.coinValue;
      }
    }
    return 0;
  }

  public static int getBloodMoneyValue(int itemId) {
    for (MysteriousEmblem emblem : values()) {
      if (itemId == emblem.itemId || ItemDef.getUnnotedId(itemId) == emblem.itemId) {
        return emblem.bloodMoneyValue;
      }
    }
    return 0;
  }

  public static int getPreviousId(int itemId) {
    var values = values();
    for (int i = 0; i < values.length; i++) {
      MysteriousEmblem emblem = values[i];
      if (itemId != emblem.itemId) {
        continue;
      }
      if (emblem.isArchaic()) {
        break;
      }
      return i - 1 >= 0 ? values[i - 1].itemId : itemId;
    }
    return -1;
  }

  public static int getNextId(int itemId) {
    var values = values();
    for (int i = 0; i < values.length; i++) {
      MysteriousEmblem emblem = values()[i];
      if (itemId != emblem.itemId) {
        continue;
      }
      if (emblem.isArchaic()) {
        break;
      }
      return i + 1 < values.length ? values[i + 1].itemId : itemId;
    }
    return -1;
  }

  public static boolean isEmblem(int itemId) {
    return getNextId(itemId) != -1;
  }
}
