package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.DORGESHUUN_CROSSBOW)
class DorgeshuunCrossbowSpecialAttack extends SpecialAttack {
  public DorgeshuunCrossbowSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(75);
    entry.setAnimation(4230);
    entry.setProjectileId(696);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    hooks.getOpponent().getCombat().changeDefenceLevel(-hooks.getDamage());
  }
}
