package com.palidinodh.osrsscript.player.command.all;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("skull")
class SkullCommand implements CommandHandler {
  @Override
  public String getExample(String name) {
    return "- Gives you a skull";
  }

  @Override
  public void execute(Player player, String name, String message) {
    player.getCombat().setPKSkullDelay(PCombat.SKULL_DELAY);
  }
}
