package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.ABYSSAL_TENTACLE)
class AbyssalTentacleSpecialAttack extends SpecialAttack {
  public AbyssalTentacleSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(50);
    entry.setAnimation(1658);
    entry.setTargetGraphic(new Graphic(341, 100));
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    if (hooks.getDamage() == 0) {
      return;
    }
    var opponent = hooks.getOpponent();
    if (!opponent.getController().canMagicBind()) {
      return;
    }
    opponent.getController().setMagicBind(9, hooks.getPlayer());
  }
}
