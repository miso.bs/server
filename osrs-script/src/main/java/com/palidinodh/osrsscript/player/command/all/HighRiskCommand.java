package com.palidinodh.osrsscript.player.command.all;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrsscript.player.plugin.magic.SpellTeleport;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("highrisk")
class HighRiskCommand implements CommandHandler, CommandHandler.Teleport {
  @Override
  public String getExample(String name) {
    return "- Teleports you to the High risk area (SAFE TELEPORT)";
  }

  @Override
  public void execute(Player player, String name, String message) {
    if (player.getInCombatDelay() > 0) {
      player.getGameEncoder().sendMessage("You can't do this while in combat.");
      return;
    }
    SpellTeleport.normalTeleport(player, new Tile(2655, 5471));
    player.getController().stopWithTeleport();
    player.getGameEncoder().sendMessage("You teleport to the high risk area.");
  }
}
