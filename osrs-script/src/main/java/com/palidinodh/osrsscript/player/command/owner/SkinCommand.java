package com.palidinodh.osrsscript.player.command.owner;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import lombok.var;

@ReferenceName("skin")
class SkinCommand implements CommandHandler, CommandHandler.OwnerRank {
  @Override
  public String getExample(String name) {
    return "id";
  }

  @Override
  public void execute(Player player, String user, String message) {
    var id = Integer.parseInt(message);
    player.getAppearance().setColor(4, id);
  }
}
