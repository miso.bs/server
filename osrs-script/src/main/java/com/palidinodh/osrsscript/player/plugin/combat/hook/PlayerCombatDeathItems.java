package com.palidinodh.osrsscript.player.plugin.combat.hook;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import com.palidinodh.io.JavaCord;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.combat.EloRating;
import com.palidinodh.osrscore.model.entity.player.combat.PlayerCombatDeathItemsHooks;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.ItemList;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.map.MapItem;
import com.palidinodh.osrscore.world.CompetitiveHiscoresCategoryType;
import com.palidinodh.osrscore.world.PKRaffle;
import com.palidinodh.osrsscript.player.plugin.bountyhunter.BountyHunterPlugin;
import com.palidinodh.osrsscript.player.plugin.bountyhunter.MysteriousEmblem;
import com.palidinodh.osrsscript.player.plugin.lootingbag.LootingBagPlugin;
import com.palidinodh.osrsscript.world.event.holidayboss.HolidayBossEvent;
import com.palidinodh.osrsscript.world.event.wildernesshotspot.WildernessHotspotEvent;
import com.palidinodh.osrsscript.world.event.wildernesskey.WildernessKeyEvent;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.adaptive.GrandExchangeItem;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.rs.setting.UserRank;
import com.palidinodh.util.PString;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.var;

class PlayerCombatDeathItems implements PlayerCombatDeathItemsHooks {
  private static int KILL_BLOOD_MONEY = 5_000;
  private static List<RandomItem> WILD_KILL_DROP_TABLE = RandomItem.buildList(
      new RandomItem(-1).weight(32), new RandomItem(ItemId.BRONZE_KEY_32306).weight(16),
      new RandomItem(ItemId.SILVER_KEY_32307).weight(8),
      new RandomItem(ItemId.GOLD_KEY_32308).weight(4),
      new RandomItem(ItemId.DIAMOND_KEY_32309).weight(2));

  @AllArgsConstructor
  @Getter
  private enum Killstreak {
    STREAK_1000(1_000, 2.5), //
    STREAK_250(250, 2),
    STREAK_100(100, 1.75),
    STREAK_50(50, 1.5),
    STREAK_10(10, 1.25);

    private int initerval;
    private double bloodMoneyMultiplier;
  }

  @Override
  public Item[] getItemProtectedOnDeath(Player player, Player killer, Item item) {
    return new Item[] { item };
  }

  @Override
  public Item[] getItemLostOnDeath(Player player, Player killer, Item item) {
    var itemId = item.getId();
    if (player == killer) {
      return new Item[] { item };
    }
    if (itemId == ItemId.LOOTING_BAG || itemId == ItemId.LOOTING_BAG_22586) {
      var lootingBagPlugin = player.getPlugin(LootingBagPlugin.class);
      var lootingBagItems = lootingBagPlugin.getItems().toItemArray();
      lootingBagPlugin.getItems().clear();
      return lootingBagItems;
    }
    if (itemId == ItemId.LUNAR_STAFF || itemId == ItemId.LUNAR_HELM || itemId == ItemId.LUNAR_TORSO
        || itemId == ItemId.LUNAR_LEGS || itemId == ItemId.LUNAR_GLOVES
        || itemId == ItemId.LUNAR_BOOTS || itemId == ItemId.LUNAR_CAPE
        || itemId == ItemId.LUNAR_AMULET || itemId == ItemId.LUNAR_RING
        || itemId == ItemId.AMULET_OF_THE_DAMNED_FULL) {
      return new Item[] { new Item(ItemId.COINS, item.getInfoDef().getValue()) };
    }
    if (itemId == ItemId.RUNE_POUCH) {
      var runePouch = player.getController().getRunePouch();
      var runes = new Item[runePouch.size() + 1];
      runes[0] = item;
      for (var i = 0; i < runePouch.size(); i++) {
        runes[i + 1] = runePouch.getItem(i);
      }
      runePouch.clear();
      player.getController().sendRunePouchVarbits();
      return runes;
    }
    if (itemId == ItemId.ABYSSAL_TENTACLE) {
      return new Item[] { new Item(ItemId.KRAKEN_TENTACLE) };
    }
    if (itemId == ItemId.SARADOMINS_BLESSED_SWORD) {
      return new Item[] { new Item(ItemId.SARADOMIN_SWORD) };
    }
    if (itemId == ItemId.BRACELET_OF_ETHEREUM) {
      return new Item[] { new Item(ItemId.BRACELET_OF_ETHEREUM_UNCHARGED),
          new Item(ItemId.REVENANT_ETHER, item.getCharges()) };
    }
    if (itemId == ItemId.VIGGORAS_CHAINMACE || itemId == ItemId.CRAWS_BOW
        || itemId == ItemId.THAMMARONS_SCEPTRE) {
      var items = new Item[item.getInfoDef().getExchangeIds().length + 1];
      items[0] = new Item(ItemId.REVENANT_ETHER, item.getCharges());
      for (int i = 0; i < item.getInfoDef().getExchangeIds().length; i++) {
        items[i + 1] = new Item(item.getInfoDef().getExchangeIds()[i]);
      }
      return items;
    }
    if (itemId == ItemId.CRYSTAL_BOW || itemId == ItemId.CRYSTAL_BOW_INACTIVE
        || itemId == ItemId.CRYSTAL_SHIELD || itemId == ItemId.CRYSTAL_SHIELD_INACTIVE
        || itemId == ItemId.CRYSTAL_HALBERD || itemId == ItemId.CRYSTAL_HALBERD_INACTIVE) {
      return new Item[] { new Item(ItemId.CRYSTAL_SEED) };
    }
    if (itemId == ItemId.BLADE_OF_SAELDOR) {
      return new Item[] { new Item(ItemId.BLADE_OF_SAELDOR_INACTIVE) };
    }
    if (itemId == ItemId.VESTAS_LONGSWORD_CHARGED_32254) {
      return new Item[] { new Item(ItemId.VESTAS_LONGSWORD) };
    }
    if (itemId == ItemId.VESTAS_SPEAR_CHARGED_32256) {
      return new Item[] { new Item(ItemId.VESTAS_SPEAR) };
    }
    if (itemId == ItemId.STATIUSS_WARHAMMER_CHARGED_32255) {
      return new Item[] { new Item(ItemId.STATIUSS_WARHAMMER) };
    }
    if (itemId == ItemId.ZURIELS_STAFF_CHARGED_32257) {
      return new Item[] { new Item(ItemId.ZURIELS_STAFF) };
    }
    if (itemId == ItemId.ELDRITCH_NIGHTMARE_STAFF) {
      return new Item[] { new Item(ItemId.NIGHTMARE_STAFF), new Item(ItemId.ELDRITCH_ORB) };
    }
    if (itemId == ItemId.VOLATILE_NIGHTMARE_STAFF) {
      return new Item[] { new Item(ItemId.NIGHTMARE_STAFF), new Item(ItemId.VOLATILE_ORB) };
    }
    if (itemId == ItemId.HARMONISED_NIGHTMARE_STAFF) {
      return new Item[] { new Item(ItemId.NIGHTMARE_STAFF), new Item(ItemId.HARMONISED_ORB) };
    }
    if (itemId == ItemId.BLIGHTED_HELM_OF_NEITIZNOT_32359
        || itemId == ItemId.BLIGHTED_AMULET_OF_GLORY_4_32360
        || itemId == ItemId.BLIGHTED_ABYSSAL_WHIP_32361
        || itemId == ItemId.BLIGHTED_RUNE_PLATEBODY_32362
        || itemId == ItemId.BLIGHTED_RUNE_DEFENDER_32363
        || itemId == ItemId.BLIGHTED_RUNE_PLATELEGS_32364
        || itemId == ItemId.BLIGHTED_BARROWS_GLOVES_32365
        || itemId == ItemId.BLIGHTED_RUNE_BOOTS_32366
        || itemId == ItemId.BLIGHTED_DRAGON_BOOTS_32367
        || itemId == ItemId.BLIGHTED_DRAGON_DAGGER_P_PLUS_PLUS_32368
        || itemId == ItemId.BLIGHTED_DRAGON_SCIMITAR_32369
        || itemId == ItemId.BLIGHTED_MYSTIC_ROBE_TOP_32370
        || itemId == ItemId.BLIGHTED_MYSTIC_ROBE_BOTTOM_32371
        || itemId == ItemId.BLIGHTED_ANCIENT_STAFF_32372
        || itemId == ItemId.BLIGHTED_BLACK_DHIDE_BODY_32373
        || itemId == ItemId.BLIGHTED_RUNE_CROSSBOW_32374
        || itemId == ItemId.BLIGHTED_RUNE_KITESHIELD_32375
        || itemId == ItemId.BLIGHTED_BLACK_DHIDE_CHAPS_32376) {
      return null;
    }
    if (item.getInfoDef().getBrokenId() != -1 && item.getInfoDef().getRepairCoinCost() > 0) {
      var coins = item.getInfoDef().getRepairCoinCost();
      return new Item[] { new Item(item.getInfoDef().getBrokenId(), item.getAmount()),
          coins > 0 ? new Item(ItemId.COINS, coins) : null };
    }
    if (item.getInfoDef().getExchangeIds() != null) {
      var items = new Item[item.getInfoDef().getExchangeIds().length];
      for (var i = 0; i < item.getInfoDef().getExchangeIds().length; i++) {
        items[i] = new Item(item.getInfoDef().getExchangeIds()[i]);
      }
      return items;
    }
    return new Item[] { item };
  }

  @Override
  public boolean isUnprotectable(Player player, Player killer, Item item) {
    var itemId = item.getId();
    if (MysteriousEmblem.isEmblem(itemId)) {
      return true;
    }
    if (itemId == ItemId.BRONZE_KEY_32306 || itemId == ItemId.SILVER_KEY_32307
        || itemId == ItemId.GOLD_KEY_32308 || itemId == ItemId.DIAMOND_KEY_32309) {
      return true;
    }
    if (itemId == ItemId.LOOTING_BAG || itemId == ItemId.LOOTING_BAG_22586) {
      return true;
    }
    if (itemId == ItemId.BRACELET_OF_ETHEREUM) {
      return true;
    }
    if (itemId == ItemId.BLOODY_KEY || itemId == ItemId.BLOODIER_KEY) {
      return true;
    }
    if (itemId == ItemId.BLOOD_MONEY) {
      return true;
    }
    return false;
  }

  @Override
  public boolean canTransferUntradable(Player player, Player killer, Item item) {
    var itemId = item.getId();
    if (MysteriousEmblem.isEmblem(itemId)) {
      return true;
    }
    if (itemId == ItemId.BRONZE_KEY_32306 || itemId == ItemId.SILVER_KEY_32307
        || itemId == ItemId.GOLD_KEY_32308 || itemId == ItemId.DIAMOND_KEY_32309) {
      return true;
    }
    if (itemId == ItemId.BLIGHTED_SHARK_32377) {
      return true;
    }
    return false;
  }

  @Override
  public void deathDropItems(Player player) {
    var killer = player.getCombat().getPlayerFromHitCount(false);
    if (killer == null) {
      if (player.getLastHitByEntity() != null && player.getLastHitByEntity().isPlayer()) {
        killer = player.getLastHitByEntity().asPlayer();
      } else {
        killer = player;
      }
      var npc = player.getCombat().getNpcFromHitCount(false);
      if (npc != null) {
        var npcInfo = npc.getDef().getName() + " (Level " + npc.getDef().getCombatLevel() + ")";
        var aOrAn = PString.startsWithVowel(npc.getDef().getName()) ? "an " : "a ";
        if (npc.getCombatDef().getKillCount().isSendMessage()) {
          aOrAn = "";
        }
        player.putAttribute("death_reason", aOrAn + npcInfo);
      }
    } else {
      var playerInfo =
          killer.getUsername() + " (Level " + killer.getSkills().getCombatLevel() + ")";
      player.putAttribute("death_reason", "player " + playerInfo);
    }
    if ((player.getArea().inWilderness() || player.getArea().inPvpWorld()) && player != killer) {
      applyUnsafePkDeath(player, killer);
    }
    var allItems = new ArrayList<Item>();
    for (var item : player.getEquipment()) {
      if (item == null || item.getAmount() < 1) {
        continue;
      }
      allItems.add(item);
    }
    player.getEquipment().clear();
    for (var item : player.getInventory()) {
      if (item == null || item.getAmount() < 1) {
        continue;
      }
      allItems.add(item);
    }
    player.getInventory().clear();
    Collections.sort(allItems);
    var protectedItems = new ArrayList<Item>();
    for (var count = 0; count < player.getController().getProtectedItemCount()
        && !allItems.isEmpty(); count++) {
      for (var index = 0; index < allItems.size(); index++) {
        var baseItem = allItems.get(index);
        if (isUnprotectable(player, killer, baseItem)) {
          continue;
        }
        if (baseItem.getAmount() == 1) {
          allItems.remove(index);
        } else {
          allItems.set(index, new Item(baseItem.getId(), baseItem.getAmount() - 1));
          baseItem = new Item(baseItem.getId(), 1);
        }
        var items = getItemProtectedOnDeath(player, killer, baseItem);
        if (items != null) {
          for (var item : items) {
            protectedItems.add(item);
          }
        }
        break;
      }
    }
    var useItemCollection = (player.getController().isInstanced()
        || player.getCombat().getRiskedValue() > player.getController().getItemReclaimFeeHook())
        && !player.getArea().inWilderness() && !player.getArea().inPvpWorld() && player == killer;
    var itemCollection = player.getCombat().getItemCollection();
    if (useItemCollection) {
      var claimFee = player.getController().getItemReclaimFeeHook();
      if (player.isUsergroup(UserRank.OPAL_MEMBER)) {
        claimFee *= 0.75;
      }
      player.getCombat().setItemCollection(itemCollection = (ItemList) new ItemList(64)
          .setPlayer(player).setCAttachment(claimFee).setAlwaysStack(true));
    }
    var itemLogList = new ArrayList<String>();
    for (var i = 0; i < allItems.size(); i++) {
      var items = getItemLostOnDeath(player, killer, allItems.get(i));
      if (items == null) {
        continue;
      }
      for (var item : items) {
        if (item == null || item.getId() < 0 || item.getAmount() < 1
            || item.getInfoDef().isFree() && !item.getInfoDef().isStackable()
                && !player.getController().isFood(item.getId())
                && !player.getController().isDrink(item.getId())) {
          continue;
        }
        if (item.getId() == ItemId.BLOODY_KEY || item.getId() == ItemId.BLOODIER_KEY) {
          player.getWorld().getWorldEvent(WildernessKeyEvent.class).addMapItem(item.getId(), killer,
              MapItem.NORMAL_TIME, MapItem.NORMAL_TIME - 20);
          continue;
        }
        var unprotectable = isUnprotectable(player, killer, item);
        var transferable = canTransferUntradable(player, killer, item);
        if (unprotectable && (!transferable || player == killer)) {
          continue;
        }
        if (item.getInfoDef().getUntradable() && !transferable || item.getAttachment() != null) {
          protectedItems.add(item);
          continue;
        }
        if (useItemCollection && itemCollection.canAddItem(item)) {
          itemCollection.addItem(item);
        } else if (player == killer) {
          if (!player.getArea().inWilderness() && !player.getArea().inPvpWorld()) {
            player.getController().addMapItem(item, player, MapItem.LONG_TIME,
                MapItem.NORMAL_APPEAR);
          } else {
            player.getController().addMapItem(item, player, killer);
          }
        } else if (killer.isGameModeIronmanRelated() || killer.isGameModeHard()
            && !player.isGameModeHard() && GrandExchangeItem.isBlockedHardModeItem(item.getId())) {
          player.getController().addMapItem(item, player, null);
        } else {
          player.getController().addMapItem(item, player, killer);
        }
        itemLogList.add(item.getLogName());
      }
    }
    if (itemLogList.isEmpty()) {
      itemLogList.add("nothing");
    }
    if (useItemCollection && itemCollection != null && !itemCollection.isEmpty()) {
      player.getGameEncoder()
          .sendMessage("<col=ff0000>Speak to Perdu in Edgeville to reclaim any items lost.");
    }
    for (var item : protectedItems) {
      player.getInventory().addItem(item);
    }
    player.getController().addMapItem(new Item(ItemId.BONES), player, killer);
    if (player != killer) {
      player.log(PlayerLogType.PVP_RISKED_ITEM_DEATH,
          "killed by " + killer.getLogName() + " and lost " + PString.toString(itemLogList, ", "));
      killer.log(PlayerLogType.PVP_RISKED_ITEM_DEATH,
          "killed " + player.getLogName() + " and received " + PString.toString(itemLogList, ", "));
    }
  }

  public void applyUnsafePkDeath(Player player, Player killer) {
    killer.getGameEncoder().sendMessage("You have defeated " + player.getUsername() + ".");
    if (killer.inEdgevilleWilderness() || killer.getArea().inPvpWorldUnsafe()) {
      killer.setCombatImmunity(34);
    }
    player.getCombat().setDeaths(player.getCombat().getDeaths() + 1);
    player.getCombat().setTotalDeaths(player.getCombat().getTotalKills() + 1);
    killer.getCombat().setKills(killer.getCombat().getKills() + 1);
    killer.getCombat().setTotalKills(killer.getCombat().getTotalKills() + 1);
    var isTargetKill = killer.getPlugin(BountyHunterPlugin.class).getTarget() == player;
    if (!player.getWorld().isPrimary()
        || !isTargetKill && player.getCombat().isRecentCombatantBlock(killer)) {
      return;
    }
    killer.getSkills().competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.WILD_KILLS, 1);
    if (killer.getArea().getWildernessLevel() >= 20) {
      killer.getSkills().competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.DEEP_WILD_KILLS,
          1);
    }
    killer.getCombat().setKillingSpree(killer.getCombat().getKillingSpree() + 1);
    var isHotspot = player.getWorld().getWorldEvent(WildernessHotspotEvent.class).inside(killer);
    if (killer.getSkills().getCombatLevel() < 50
        || !killer.getPlugin(BountyHunterPlugin.class).hasEmblem()) {
      isHotspot = false;
    }
    if (isHotspot) {
      killer.getSkills().competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.HOTSPOT_KILLS,
          1);
    }
    Killstreak killstreak = null;
    for (var aStreak : Killstreak.values()) {
      if (killer.getCombat().getKillingSpree() % aStreak.getIniterval() != 0) {
        continue;
      }
      killstreak = aStreak;
      break;
    }
    PKRaffle.addEntry(killer);
    var wildKillItem = RandomItem.getItem(WILD_KILL_DROP_TABLE);
    if (wildKillItem.getId() != -1) {
      player.getController().addMapItem(wildKillItem, player, killer);
    }
    if (isTargetKill) {
      killer.getSkills().competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.BOUNTY_KILLS, 1);
      if (!killer.getArea().inMultiCombat()) {
        killer.setCombatImmunity(34);
      }
      killer.getPlugin(BountyHunterPlugin.class).upgradeEmblem();
      var targetItem = RandomItem.getItem(BountyHunterPlugin.TARGET_DROP_TABLE);
      if (targetItem.getId() != -1) {
        player.getController().addMapItem(targetItem, player, killer);
        if (targetItem.getId() == ItemId.VESTAS_LONGSWORD
            || targetItem.getId() == ItemId.STATIUSS_WARHAMMER
            || targetItem.getId() == ItemId.VESTAS_SPEAR
            || targetItem.getId() == ItemId.MORRIGANS_JAVELIN
            || targetItem.getId() == ItemId.MORRIGANS_THROWING_AXE
            || targetItem.getId() == ItemId.ZURIELS_STAFF) {
          player.getWorld().sendItemDropNews(killer, targetItem.getId(),
              "PKing " + player.getUsername());
        }
      }
    }
    var holidayItemId = player.getWorld().getWorldEvent(HolidayBossEvent.class).getItemId();
    if (holidayItemId != -1 && PRandom.randomE(4) == 0) {
      player.getController().addMapItem(new Item(holidayItemId, 2 + PRandom.randomI(4)), player,
          killer);
    }
    var bloodMoneyAwarded = KILL_BLOOD_MONEY;
    if (isHotspot && isTargetKill) {
      bloodMoneyAwarded *= WildernessHotspotEvent.BLOOD_MONEY_MULTIPLIER;
    }
    if (killstreak != null) {
      bloodMoneyAwarded *= killstreak.getBloodMoneyMultiplier();
    }
    var carriedBloodMoney = player.getInventory().getCount(ItemId.BLOOD_MONEY);
    player.getInventory().deleteItem(ItemId.BLOOD_MONEY, Item.MAX_AMOUNT);
    if (!killer.isGameModeNormal() && !killer.isGameModeHard()) {
      carriedBloodMoney = 0;
    }
    bloodMoneyAwarded += carriedBloodMoney;
    if (bloodMoneyAwarded > 0 && Settings.getInstance().isEconomy()) {
      player.getController().addMapItem(new Item(ItemId.BLOOD_MONEY, bloodMoneyAwarded), player,
          killer);
    }
    if (killer.getCombat().getKillingSpree() % 5 == 0) {
      var spreeSprites = new int[] { 8, 7, 6, 5, 4 };
      var spreeMessages =
          new String[] { "on a killing spree", "on a killing frenzy", "running riot",
              "on a rampage", "untouchable", "invincible", "inconceivable", "unfriggenbelievable" };
      var spreeSprite = spreeSprites[Math.min(killer.getCombat().getKillingSpree() / 5 - 1,
          spreeSprites.length - 1)];
      var spreeUsername = "<img=" + spreeSprite + ">" + killer.getUsername();
      var spreeMessage = spreeMessages[Math.min(killer.getCombat().getKillingSpree() / 5 - 1,
          spreeMessages.length - 1)];
      player.getWorld().sendMessage("<col=ff0000>" + spreeUsername + " is " + spreeMessage + "!");
      JavaCord.sendMessage(DiscordChannel.GAME_ANNOUNCEMENTS,
          spreeUsername + " is " + spreeMessage + "!");
    }
    if (player.getCombat().getPKSkullDelay() > 0 && killer.getCombat().getPKSkullDelay() > 0) {
      var myRating = player.getCombat().getElo().getRating();
      var opponentRating = killer.getCombat().getElo().getRating();
      var myState = killer.getCombat().isDead() ? EloRating.DRAW : EloRating.LOSS;
      var opponentState = killer.getCombat().isDead() ? EloRating.DRAW : EloRating.WIN;
      player.getCombat().getElo().updateRating(opponentRating, myState, isTargetKill);
      killer.getCombat().getElo().updateRating(myRating, opponentState, isTargetKill);
      myRating = player.getCombat().getElo().getMonthlyRating();
      opponentRating = killer.getCombat().getElo().getMonthlyRating();
      player.getCombat().getElo().updateMonthlyRating(opponentRating, myState, isTargetKill);
      killer.getCombat().getElo().updateMonthlyRating(myRating, opponentState, isTargetKill);
      myRating = player.getCombat().getElo().getEventRating();
      opponentRating = killer.getCombat().getElo().getEventRating();
      if (!Settings.getInstance().isStaffOnly() && !Settings.getInstance().isBeta()) {
        player.getCombat().getElo().updateEventRating(opponentRating, myState, isTargetKill);
        killer.getCombat().getElo().updateEventRating(myRating, opponentState, isTargetKill);
      }
    }
    player.getCombat().setKillingSpree(0);
  }
}
