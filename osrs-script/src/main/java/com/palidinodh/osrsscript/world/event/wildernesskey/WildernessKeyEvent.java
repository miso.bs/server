package com.palidinodh.osrsscript.world.event.wildernesskey;

import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.Controller;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapItem;
import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PEvent;
import com.palidinodh.util.PTime;
import lombok.var;

public class WildernessKeyEvent extends PEvent {
  private static boolean ENABLED = true;
  private static final int SOON_MINUTES = 4;
  private static final String[] TIME = { "1:00", "3:00", "5:00", "7:00", "9:00", "11:00", "13:00",
      "15:00", "17:00", "19:00", "21:00", "23:00" };
  private static final int[] HOURS;
  private static final int[] MINUTES;
  private static int LAST_KNOWN_TIME = (int) PTime.minToTick(15);

  private transient KeySpawn spawn;
  private transient MapItem mapItem;
  private transient Tile lastKnownTile;
  private transient int lastKnownTileCountdown;

  public WildernessKeyEvent() {
    super(4);
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("wilderness_key_message")) {
      return getNextTimeText();
    }
    if (name.equals("wilderness_key_drop_keys")) {
      dropKeys((Player) args[0]);
    }
    return null;
  }

  @Override
  public void execute() {
    setTick(4);
    if (mapItem != null && !mapItem.isVisible()) {
      mapItem = null;
      lastKnownTileCountdown = LAST_KNOWN_TIME;
    }
    if (lastKnownTileCountdown > 0) {
      lastKnownTileCountdown -= getTick();
    }
    if (!canRun()) {
      return;
    }
    var currentHour = PTime.getHour24();
    var currentMinute = PTime.getMinute();
    var dayMinute = (int) PTime.hourToMin(currentHour) + currentMinute;
    var nextTime = getNextTime();
    if (nextTime == null) {
      return;
    }
    var remainingMinutes = PTime.getRemainingMinutes(dayMinute, nextTime[0] * 60 + nextTime[1]);
    if (remainingMinutes == SOON_MINUTES) {
      Main.getWorld()
          .sendNotice("<col=ff0000>A bloodier key will spawn in " + SOON_MINUTES + " minutes!");
      setTick(105);
    } else if (remainingMinutes == 0) {
      startEvent();
      setTick(105);
    }
  }

  public void addMapItem(int itemId, Tile tile, int totalTime, int appearTime) {
    var item = new Item(itemId);
    var player = tile instanceof Player ? (Player) tile : null;
    var id = player != null ? player.getId() : -1;
    var username = player != null ? player.getUsername() : null;
    var ip = player != null ? player.getIP() : "0.0.0.0";
    var inWilderness = Area.inWilderness(tile);
    if (inWilderness && item.getId() == ItemId.BLOODIER_KEY && totalTime != MapItem.LONG_TIME
        && username != null) {
      Main.getWorld().sendMessage("<col=ff0000>A bloodier key has been dropped at Level "
          + Area.getWildernessLevel(tile) + " for " + username + "!");
    }
    var controller = Controller.getDefaultController(tile);
    var mapItem = new MapItem(controller, item, tile, totalTime, appearTime, id, username, ip, -1);
    lastKnownTile = mapItem = Main.getWorld().addMapItem(mapItem).get(0);
    lastKnownTileCountdown = LAST_KNOWN_TIME;
    if (totalTime == MapItem.LONG_TIME) {
      this.mapItem = mapItem;
    }
    if (inWilderness && item.getId() == ItemId.BLOODIER_KEY) {
      controller.sendMapGraphic(tile, new Graphic(246));
    }
  }

  private void dropKeys(Player player) {
    if (player.carryingItem(ItemId.BLOODY_KEY)) {
      player.getInventory().deleteItem(ItemId.BLOODY_KEY, Item.MAX_AMOUNT);
      addMapItem(ItemId.BLOODY_KEY, player, MapItem.NORMAL_TIME, MapItem.ALWAYS_APPEAR);
    }
    if (player.carryingItem(ItemId.BLOODIER_KEY)) {
      player.getInventory().deleteItem(ItemId.BLOODIER_KEY, Item.MAX_AMOUNT);
      addMapItem(ItemId.BLOODIER_KEY, player, MapItem.NORMAL_TIME, MapItem.ALWAYS_APPEAR);
    }
  }

  private String getNextTimeText() {
    if (mapItem != null && mapItem.isVisible()) {
      return spawn.getShortName();
    }
    if (lastKnownTileCountdown > 0) {
      return "Level " + Area.getWildernessLevel(lastKnownTile);
    }
    if (!canRun()) {
      return "Unavailable";
    }
    int currentHour = PTime.getHour24();
    int currentMinute = PTime.getMinute();
    int dayMinute = (int) PTime.hourToMin(currentHour) + currentMinute;
    int[] nextTime = getNextTime();
    int remainingMinutes = PTime.getRemainingMinutes(dayMinute, nextTime[0] * 60 + nextTime[1]);
    return PTime.ticksToLongDuration(PTime.minToTick(remainingMinutes));
  }

  private void startEvent() {
    if (!canRun()) {
      return;
    }
    spawn = PRandom.arrayRandom(KeySpawn.values());
    addMapItem(ItemId.BLOODIER_KEY, spawn.getTile(), MapItem.LONG_TIME, MapItem.ALWAYS_APPEAR);
    Main.getWorld().sendBroadcast("A bloodier key has spawned " + spawn.getName() + "!");
  }

  private int[] getNextTime() {
    if (!canRun()) {
      return null;
    }
    var currentHour = PTime.getHour24();
    var currentMinute = PTime.getMinute();
    for (var i = 0; i < HOURS.length; i++) {
      var hour = HOURS[i];
      var minute = MINUTES[i];
      if (currentHour > hour || currentHour == hour && currentMinute > minute) {
        continue;
      }
      return new int[] { hour, minute };
    }
    return new int[] { HOURS[0], MINUTES[0] };
  }

  public boolean canRun() {
    return ENABLED && TIME != null && TIME.length > 0 && Main.getWorld().isPrimary();
  }

  static {
    if (TIME == null || TIME.length == 0) {
      HOURS = null;
      MINUTES = null;
    } else {
      HOURS = new int[TIME.length];
      MINUTES = new int[TIME.length];
      for (var i = 0; i < TIME.length; i++) {
        var data = TIME[i].split(":");
        HOURS[i] = Integer.parseInt(data[0]);
        MINUTES[i] = Integer.parseInt(data[1]);
      }
    }
  }
}
