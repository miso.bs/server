package com.palidinodh.osrsscript.world.event.competitivehiscores;

import java.util.Calendar;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum CompetitiveHiscoresDurationType {
  HOURS_1("Hour", "Current hour", "Previous hour", Calendar.HOUR_OF_DAY, 1, true),
  HOURS_3("3 Hr", "12AM-3AM, 3AM-6AM, 6AM-9AM, 9AM-12PM, 12PM-3PM, 3PM-6PM, 6PM-9PM, 9PM-12AM",
      "Previous 3 hours", Calendar.HOUR_OF_DAY, 3, false),
  HOURS_6("6 Hr", "12AM-6AM, 6AM-12PM, 12PM-6PM, 6PM-12AM", "Previous 6 hours",
      Calendar.HOUR_OF_DAY, 6, false),
  HOURS_12("12 Hr", "12AM-12PM, 12PM-12AM", "Previous 12 hours", Calendar.HOUR_OF_DAY, 12, true),
  DAY("Today", "Today", "Yesterday", Calendar.DAY_OF_WEEK, -1, false),
  WEEK("Week", "This week", "Previous week", Calendar.WEEK_OF_MONTH, -1, false),
  MONTH("Month", "This month", "Previous month", Calendar.MONTH, -1, false);

  private String currentName;
  private String currentDescription;
  private String previousDescription;
  private int calendarIdentifier;
  private int hours = -1;
  private boolean hidden;
}
