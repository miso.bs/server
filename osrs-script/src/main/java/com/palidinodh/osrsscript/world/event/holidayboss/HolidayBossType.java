package com.palidinodh.osrsscript.world.event.holidayboss;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum HolidayBossType {
  ANTI_SANTA(NpcId.ANTI_SANTA_16019, 0, new Tile(2269, 4062, 4), new Tile(2272, 4045, 4),
      ItemId.SNOWBALL_TOKEN_32339);

  private int npcId;
  private int npcMoveDistance;
  private Tile npcTile;
  private Tile teleportTile;
  private int itemId;
}
