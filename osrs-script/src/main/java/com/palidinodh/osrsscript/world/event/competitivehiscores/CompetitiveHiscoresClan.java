package com.palidinodh.osrsscript.world.event.competitivehiscores;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.world.CompetitiveHiscoresCategoryOrderType;
import com.palidinodh.osrscore.world.CompetitiveHiscoresCategoryType;
import com.palidinodh.util.PNumber;
import com.palidinodh.util.PString;
import lombok.Getter;
import lombok.var;

@Getter
class CompetitiveHiscoresClan {
  private int ownerUserId;
  private String ownerUsername;
  private String name;
  private Map<Integer, CompetitiveHiscoresIndividual> individuals = new HashMap<>();

  public CompetitiveHiscoresClan(Player player) {
    ownerUserId = player.getMessaging().getClanChatUserId();
    update(player);
  }

  public void update(Player player) {
    ownerUsername = PString.formatName(player.getMessaging().getClanChatUsername());
    name = PString.formatName(player.getMessaging().getClanChatName());
  }

  public List<CompetitiveHiscoresIndividual> getIndividualsAsList() {
    var list = new ArrayList<CompetitiveHiscoresIndividual>();
    list.addAll(individuals.values());
    return list;
  }

  public long getTotal(CompetitiveHiscoresCategoryType category) {
    var value = 0L;
    for (var individual : getIndividualsAsList()) {
      value += individual.getTotal(category);
    }
    return value;
  }

  public String getInformation(CompetitiveHiscoresCategoryType category) {
    var information = new StringBuilder();
    for (var individual : getIndividualsAsList()) {
      var individualTotal = individual.getTotal(category);
      if (individualTotal == 0) {
        continue;
      }
      information.append(", ").append(individual.getUsername()).append(": ")
          .append(PNumber.formatNumber(individualTotal));
    }
    return information.substring(2).toString();
  }

  public static List<CompetitiveHiscoresClan> sort(List<CompetitiveHiscoresClan> clans,
      CompetitiveHiscoresCategoryType category) {
    clans.sort((c1, c2) -> {
      var total1 = c1.getTotal(category);
      var total2 = c2.getTotal(category);
      if (category.getOrder() == CompetitiveHiscoresCategoryOrderType.HIGH_LOW) {
        return Long.compare(total2, total1);
      } else if (category.getOrder() == CompetitiveHiscoresCategoryOrderType.LOW_HIGH) {
        return Long.compare(total1, total2);
      }
      return 0;
    });
    return clans;
  }
}
