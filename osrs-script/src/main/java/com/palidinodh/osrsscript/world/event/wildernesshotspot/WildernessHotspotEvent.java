package com.palidinodh.osrsscript.world.event.wildernesshotspot;

import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PEvent;
import com.palidinodh.util.PTime;

public class WildernessHotspotEvent extends PEvent {
  public static final double BLOOD_MONEY_MULTIPLIER = 3.0;
  private static boolean ENABLED = true;
  private static WildernessHotspot[] HOTSPOTS = { new EdgevilleHotspot() };

  private transient WildernessHotspot hotspot;

  public WildernessHotspotEvent() {
    super(4);
    configureHotspot();
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("wilderness_hotspot_message")) {
      return hotspot != null ? hotspot.getName() : "None";
    }
    if (name.equals("wilderness_hotspot_inside")) {
      return inside((Tile) args[0]);
    }
    return null;
  }

  @Override
  public void execute() {
    setTick(4);
    if (!canRun()) {
      return;
    }
    if (PTime.getMinute() == 30) {
      configureHotspot();
      setTick(105);
    }
  }

  public boolean inside(Tile tile) {
    return hotspot != null && Area.inWilderness(tile) ? hotspot.inside(tile) : false;
  }

  private void configureHotspot() {
    hotspot = PRandom.arrayRandom(HOTSPOTS);
  }

  private boolean canRun() {
    return ENABLED && HOTSPOTS != null && HOTSPOTS.length > 0 && Main.getWorld().isPrimary();
  }
}
