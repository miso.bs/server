package com.palidinodh.osrsscript.world.event.wildernesshotspot;

import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.util.PPolygon;

class EdgevilleHotspot implements WildernessHotspot {
  private PPolygon polygon = new PPolygon(PPolygon.getPoint(3025, 3555),
      PPolygon.getPoint(3025, 3540), PPolygon.getPoint(3032, 3528), PPolygon.getPoint(3040, 3520),
      PPolygon.getPoint(3125, 3520), PPolygon.getPoint(3122, 3532), PPolygon.getPoint(3108, 3555));

  @Override
  public String getName() {
    return "Edgeville";
  }

  @Override
  public boolean inside(Tile tile) {
    return polygon.contains(tile.getPoint());
  }
}
