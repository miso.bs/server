package com.palidinodh.osrsscript.world.event.competitivehiscores;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.ScriptId;
import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.world.CompetitiveHiscoresCategoryFormatType;
import com.palidinodh.osrscore.world.CompetitiveHiscoresCategoryGroupType;
import com.palidinodh.osrscore.world.CompetitiveHiscoresCategoryType;
import com.palidinodh.rs.adaptive.RsFriend;
import com.palidinodh.util.PEvent;
import com.palidinodh.util.PNumber;
import com.palidinodh.util.PTime;
import lombok.var;

public class CompetitiveHiscoresEvent extends PEvent {
  private static final int DURATION_CHILD_COUNT = 1, FIRST_DURATION_CHILD = 40,
      LAST_DURATION_CHILD = 58;
  private static final int CATEGORY_GROUP_CHILD_COUNT = 1, FIRST_CATEGORY_GROUP_CHILD = 32,
      LAST_CATEGORY_GROUP_CHILD = 38;
  private static final int CATEGORY_CHILD_COUNT = 2, FIRST_CATEGORY_CHILD = 72,
      LAST_CATEGORY_CHILD = 159;
  private static final int USER_CHILD_COUNT = 5, FIRST_USER_CHILD = 169, LAST_USER_CHILD = 313;

  private List<CompetitiveHiscoresTracking> trackings = new ArrayList<>();

  public CompetitiveHiscoresEvent() {
    super(50);
    for (var duration : CompetitiveHiscoresDurationType.values()) {
      trackings.add(new CompetitiveHiscoresTracking(duration));
    }
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("competitive_hiscores_update")) {
      update((Player) args[0], (CompetitiveHiscoresCategoryType) args[1], (long) args[2]);
    }
    return null;
  }

  @Override
  public void execute() {
    trackings.forEach(d -> d.checkExpiration());
  }

  public void update(Player player, CompetitiveHiscoresCategoryType category, long value) {
    trackings.forEach(d -> d.update(player, category, value));
  }

  public void open(Player player) {
    player.putAttribute("hiscores_sorting", CompetitiveHiscoresSortingType.INDIVIDUALS);
    player.putAttribute("hiscores_category", 0);
    var groupType = CompetitiveHiscoresCategoryGroupType.ALL;
    player.putAttribute("hiscores_category_group", groupType);
    player.putAttribute("hiscores_category_list",
        CompetitiveHiscoresCategoryType.getByGroup(groupType));
    player.putAttribute("hiscores_tracking_entries", getTracking(0).getCurrent());
    player.getWidgetManager().sendInteractiveOverlay(WidgetId.HISCORES_1009);
    player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009, 69, "");
    player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009, 70, "");
    sendSorting(player);
    sendDurations(player);
    sendCategoryGroups(player);
    sendCategories(player);
    sendTotals(player);
  }

  public void hiscoresWidgetHandler(Player player, int option, int childId, int slot, int itemId) {
    if (childId >= FIRST_DURATION_CHILD && childId <= LAST_DURATION_CHILD + DURATION_CHILD_COUNT) {
      var displayIndex = childToDuration(childId);
      var duration = getTracking(displayIndex);
      if (duration == null) {
        return;
      }
      player.putAttribute("hiscores_tracking_entries",
          displayIndex % 2 != 0 ? duration.getPrevious() : duration.getCurrent());
      sendDurations(player);
    }
    if (childId >= FIRST_CATEGORY_CHILD && childId <= LAST_CATEGORY_CHILD + CATEGORY_CHILD_COUNT) {
      var categories =
          player.getAttributeList("hiscores_category_list", CompetitiveHiscoresCategoryType.class);
      var displayIndex = childToCategory(childId);
      var category = getCategory(categories, displayIndex);
      if (category == null) {
        return;
      }
      player.putAttribute("hiscores_category", displayIndex);
      sendCategories(player);
    }
    if (childId >= FIRST_USER_CHILD && childId <= LAST_USER_CHILD + USER_CHILD_COUNT) {
      var sorting = (CompetitiveHiscoresSortingType) player.getAttribute("hiscores_sorting");
      var displayIndex = childToUser(childId);
      if (sorting == CompetitiveHiscoresSortingType.CLANS) {
        var categories = player.getAttributeList("hiscores_category_list",
            CompetitiveHiscoresCategoryType.class);
        var selectedCategory = getCategory(categories, player.getAttributeInt("hiscores_category"));
        var entries =
            (CompetitiveHiscoresTrackingEntries) player.getAttribute("hiscores_tracking_entries");
        var clans = CompetitiveHiscoresClan.sort(entries.getClansAsList(), selectedCategory);
        var clan = displayIndex < clans.size() ? clans.get(displayIndex) : null;
        if (clan == null) {
          return;
        }
        var info = clan.getInformation(selectedCategory);
        if (info.length() > 128) {
          info = info.substring(0, 128) + "...";
        }
        player.getGameEncoder().sendMessage(info);
      }
    }
    if (childId >= FIRST_CATEGORY_GROUP_CHILD
        && childId <= LAST_CATEGORY_GROUP_CHILD + CATEGORY_GROUP_CHILD_COUNT) {
      var displayIndex = (childId - FIRST_CATEGORY_GROUP_CHILD) / (CATEGORY_GROUP_CHILD_COUNT + 1);
      var sortingType = CompetitiveHiscoresCategoryGroupType.values()[displayIndex];
      player.putAttribute("hiscores_category_group", sortingType);
      player.putAttribute("hiscores_category_list",
          CompetitiveHiscoresCategoryType.getByGroup(sortingType));
      player.putAttribute("hiscores_category", 0);
      sendCategoryGroups(player);
      sendCategories(player);
    }
    switch (childId) {
      case 16:
        player.putAttribute("hiscores_sorting", CompetitiveHiscoresSortingType.INDIVIDUALS);
        sendSorting(player);
        break;
      case 19:
        player.putAttribute("hiscores_sorting", CompetitiveHiscoresSortingType.FRIENDS);
        sendSorting(player);
        break;
      case 23:
        player.putAttribute("hiscores_sorting", CompetitiveHiscoresSortingType.CLANS);
        sendSorting(player);
        break;
      case 28:
        player.putAttribute("hiscores_sorting", CompetitiveHiscoresSortingType.SELF);
        sendSorting(player);
        break;
    }
    sendTotals(player);
  }

  private void sendSorting(Player player) {
    var sorting = (CompetitiveHiscoresSortingType) player.getAttribute("hiscores_sorting");
    player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009, 18,
        sorting == CompetitiveHiscoresSortingType.INDIVIDUALS ? "*" : "");
    player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009, 22,
        sorting == CompetitiveHiscoresSortingType.FRIENDS ? "*" : "");
    player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009, 27,
        sorting == CompetitiveHiscoresSortingType.CLANS ? "*" : "");
    player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009, 30,
        sorting == CompetitiveHiscoresSortingType.SELF ? "*" : "");
  }

  private void sendDurations(Player player) {
    var selectedEntries =
        (CompetitiveHiscoresTrackingEntries) player.getAttribute("hiscores_tracking_entries");
    for (var displayIndex = 0;; displayIndex += 2) {
      var currentChildren = durationToChildren(displayIndex);
      var previousChildren = durationToChildren(displayIndex + 1);
      if (currentChildren == null || previousChildren == null) {
        break;
      }
      var currentText = "";
      var previousText = "";
      var tracking = getTracking(displayIndex);
      player.getGameEncoder().sendHideWidget(WidgetId.HISCORES_1009, currentChildren[0],
          tracking == null);
      player.getGameEncoder().sendHideWidget(WidgetId.HISCORES_1009, previousChildren[0],
          tracking == null);
      if (tracking == null) {
        continue;
      }
      currentText = tracking.getDuration().getCurrentName();
      previousText = "Prev";
      if (tracking.getCurrent() == selectedEntries) {
        currentText = "<col=ffffff>" + currentText + "</col>";
      }
      if (tracking.getPrevious() == selectedEntries) {
        previousText = "<col=ffffff>" + previousText + "</col>";
      }
      player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009, currentChildren[1],
          currentText);
      player.getGameEncoder().sendClientscript(ScriptId.SET_WIDGET_TOOLTIP_8192,
          WidgetId.HISCORES_1009 << 16 | currentChildren[1], WidgetId.HISCORES_1009 << 16 | 320,
          tracking.getDuration().getCurrentDescription(), 500);
      player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009, previousChildren[1],
          previousText);
      player.getGameEncoder().sendClientscript(ScriptId.SET_WIDGET_TOOLTIP_8192,
          WidgetId.HISCORES_1009 << 16 | previousChildren[1], WidgetId.HISCORES_1009 << 16 | 320,
          tracking.getDuration().getPreviousDescription(), 500);
    }
  }

  private void sendCategoryGroups(Player player) {
    var groups = CompetitiveHiscoresCategoryGroupType.values();
    for (var displayIndex = 0; displayIndex < groups.length; displayIndex++) {
      var groupType = groups[displayIndex];
      var text = groupType.getFormattedName();
      if (groupType == player.getAttribute("hiscores_category_group")) {
        text = "<col=ffffff>" + text + "</col>";
      }
      player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009,
          FIRST_CATEGORY_GROUP_CHILD + (displayIndex * (CATEGORY_GROUP_CHILD_COUNT + 1)) + 1, text);
    }
  }

  private void sendCategories(Player player) {
    var categories =
        player.getAttributeList("hiscores_category_list", CompetitiveHiscoresCategoryType.class);
    for (var displayIndex = 0;; displayIndex++) {
      var children = categoryToChildren(displayIndex);
      if (children == null) {
        break;
      }
      var text = "";
      var category = getCategory(categories, displayIndex);
      player.getGameEncoder().sendHideWidget(WidgetId.HISCORES_1009, children[0], category == null);
      if (category == null) {
        continue;
      }
      text = category.getFormattedName();
      if (displayIndex == player.getAttributeInt("hiscores_category")) {
        text = "<col=ffffff>" + text + "</col>";
      }
      player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009, children[1], text);
    }
  }

  private void sendTotals(Player player) {
    var categories =
        player.getAttributeList("hiscores_category_list", CompetitiveHiscoresCategoryType.class);
    var sorting = (CompetitiveHiscoresSortingType) player.getAttribute("hiscores_sorting");
    var selectedCategory = getCategory(categories, player.getAttributeInt("hiscores_category"));
    var formatting = selectedCategory.getFormatting();
    var entries =
        (CompetitiveHiscoresTrackingEntries) player.getAttribute("hiscores_tracking_entries");
    var individuals =
        CompetitiveHiscoresIndividual.sort(entries.getIndividualsAsList(), selectedCategory);
    var clans = CompetitiveHiscoresClan.sort(entries.getClansAsList(), selectedCategory);
    var friends = player.getMessaging().getFriends();
    for (var displayIndex = 0;; displayIndex++) {
      var children = userToChildren(displayIndex);
      if (children == null) {
        break;
      }
      var text = "";
      var total = "";
      if (sorting == CompetitiveHiscoresSortingType.CLANS) {
        var clan = displayIndex < clans.size() ? clans.get(displayIndex) : null;
        player.getGameEncoder().sendHideWidget(WidgetId.HISCORES_1009, children[0], clan == null);
        if (clan != null) {
          var clanName = clan.getName();
          if (clanName.length() > 10) {
            clanName = clanName.substring(0, 10);
          }
          if (!clan.getName().equalsIgnoreCase(clan.getOwnerUsername())) {
            clanName += " (" + clan.getOwnerUsername() + ")";
          }
          text = clanName;
          if (formatting == CompetitiveHiscoresCategoryFormatType.FORMAT_NUMBER) {
            total = PNumber.formatNumber(clan.getTotal(selectedCategory));
          } else if (formatting == CompetitiveHiscoresCategoryFormatType.TICKS_TO_DURATION) {
            total = PTime.ticksToDuration(clan.getTotal(selectedCategory));
          }
          player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009, children[1], text);
          player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009, children[2], total);
        }
      } else {
        var individual = sorting == CompetitiveHiscoresSortingType.SELF && displayIndex > 0 ? null
            : getIndividual(individuals, displayIndex, sorting, player.getUsername(), friends);
        player.getGameEncoder().sendHideWidget(WidgetId.HISCORES_1009, children[0],
            individual == null);
        if (individual != null) {
          text = individual.getIconImage() + individual.getUsername();
          if (formatting == CompetitiveHiscoresCategoryFormatType.FORMAT_NUMBER) {
            total = PNumber.formatNumber(individual.getTotal(selectedCategory));
          } else if (formatting == CompetitiveHiscoresCategoryFormatType.TICKS_TO_DURATION) {
            total = PTime.ticksToDuration(individual.getTotal(selectedCategory));
          }
          player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009, children[1], text);
          player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009, children[2], total);
        }
      }
    }
  }

  private CompetitiveHiscoresTracking getTracking(int atDisplayIndex) {
    var displayIndex = 0;
    for (var tracking : trackings) {
      if (tracking.getDuration().isHidden()) {
        continue;
      }
      if (displayIndex++ == atDisplayIndex || displayIndex++ == atDisplayIndex) {
        return tracking;
      }
    }
    return null;
  }

  private CompetitiveHiscoresCategoryType getCategory(
      List<CompetitiveHiscoresCategoryType> categories, int atDisplayIndex) {
    var displayIndex = 0;
    for (var category : categories) {
      if (category.isHidden()) {
        continue;
      }
      if (displayIndex++ == atDisplayIndex) {
        return category;
      }
    }
    return null;
  }

  private CompetitiveHiscoresIndividual getIndividual(
      List<CompetitiveHiscoresIndividual> individuals, int atDisplayIndex,
      CompetitiveHiscoresSortingType sorting, String username, List<RsFriend> friends) {
    var displayIndex = 0;
    for (var individual : individuals) {
      if (individual.isHidden()) {
        continue;
      }
      if (sorting == CompetitiveHiscoresSortingType.FRIENDS
          && !username.equalsIgnoreCase(individual.getUsername())
          && !friends.contains(new RsFriend(individual.getUsername()))) {
        continue;
      }
      if (sorting == CompetitiveHiscoresSortingType.SELF
          && !username.equalsIgnoreCase(individual.getUsername())) {
        continue;
      }
      if (displayIndex++ == atDisplayIndex) {
        return individual;
      }
    }
    return null;
  }

  private int[] durationToChildren(int displayIndex) {
    var buttonChildId = FIRST_DURATION_CHILD;
    var textChildId = buttonChildId + 1;
    for (var i = 1; i <= displayIndex; i++) {
      buttonChildId += DURATION_CHILD_COUNT + 1;
      textChildId = buttonChildId + 1;
    }
    return buttonChildId <= LAST_DURATION_CHILD ? new int[] { buttonChildId, textChildId } : null;
  }

  private int childToDuration(int childId) {
    if (childId < FIRST_DURATION_CHILD || childId > LAST_DURATION_CHILD + DURATION_CHILD_COUNT) {
      return -1;
    }
    return (childId - FIRST_DURATION_CHILD) / (DURATION_CHILD_COUNT + 1);
  }


  private int[] categoryToChildren(int displayIndex) {
    var buttonChildId = FIRST_CATEGORY_CHILD;
    var textChildId = buttonChildId + 2;
    for (var i = 1; i <= displayIndex; i++) {
      buttonChildId += CATEGORY_CHILD_COUNT + 1;
      textChildId = buttonChildId + 2;
    }
    return buttonChildId <= LAST_CATEGORY_CHILD ? new int[] { buttonChildId, textChildId } : null;
  }

  private int childToCategory(int childId) {
    if (childId < FIRST_CATEGORY_CHILD || childId > LAST_CATEGORY_CHILD + CATEGORY_CHILD_COUNT) {
      return -1;
    }
    return (childId - FIRST_CATEGORY_CHILD) / (CATEGORY_CHILD_COUNT + 1);
  }

  private int[] userToChildren(int displayIndex) {
    var buttonChildId = FIRST_USER_CHILD;
    var textChildId = buttonChildId + 4;
    var textChildId2 = buttonChildId + 5;
    for (var i = 1; i <= displayIndex; i++) {
      buttonChildId += USER_CHILD_COUNT + 1;
      textChildId = buttonChildId + 4;
      textChildId2 = buttonChildId + 5;
    }
    return buttonChildId <= LAST_USER_CHILD ? new int[] { buttonChildId, textChildId, textChildId2 }
        : null;
  }

  private int childToUser(int childId) {
    if (childId < FIRST_USER_CHILD || childId > LAST_USER_CHILD + USER_CHILD_COUNT) {
      return -1;
    }
    return (childId - FIRST_USER_CHILD) / (USER_CHILD_COUNT + 1);
  }
}
