package com.palidinodh.osrsscript.world.event.competitivehiscores;

public enum CompetitiveHiscoresSortingType {
  INDIVIDUALS, FRIENDS, CLANS, SELF
}
