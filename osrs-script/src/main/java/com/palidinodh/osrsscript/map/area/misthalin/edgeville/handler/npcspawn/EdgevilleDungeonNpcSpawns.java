package com.palidinodh.osrsscript.map.area.misthalin.edgeville.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class EdgevilleDungeonNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.SKELETON_22, new Tile(3130, 9904), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETON_22, new Tile(3134, 9905), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETON_22, new Tile(3132, 9908), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETON_22, new Tile(3130, 9911), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETON_22, new Tile(3133, 9912), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETON_22, new Tile(3130, 9915), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(3148, 9907), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(3147, 9902), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(3143, 9900), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(3142, 9904), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(3140, 9908), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(3147, 9897), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(3150, 9893), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(3150, 9886), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(3148, 9882), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(3144, 9884), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(3139, 9886), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(3139, 9891), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(3145, 9891), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(3146, 9887), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(3140, 9895), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(3125, 9864), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(3127, 9862), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(3130, 9861), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(3128, 9865), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(3124, 9861), 4));
    spawns.add(new NpcSpawn(NpcId.HOBGOBLIN_28, new Tile(3116, 9870), 4));
    spawns.add(new NpcSpawn(NpcId.HOBGOBLIN_28, new Tile(3119, 9872), 4));
    spawns.add(new NpcSpawn(NpcId.HOBGOBLIN_28, new Tile(3122, 9874), 4));
    spawns.add(new NpcSpawn(NpcId.HOBGOBLIN_28, new Tile(3123, 9878), 4));
    spawns.add(new NpcSpawn(NpcId.HOBGOBLIN_28, new Tile(3125, 9874), 4));
    spawns.add(new NpcSpawn(NpcId.HOBGOBLIN_28, new Tile(3118, 9867), 4));
    spawns.add(new NpcSpawn(NpcId.HOBGOBLIN_28, new Tile(3128, 9874), 4));
    spawns.add(new NpcSpawn(NpcId.HILL_GIANT_28, new Tile(3118, 9849), 4));
    spawns.add(new NpcSpawn(NpcId.HILL_GIANT_28, new Tile(3121, 9844), 4));
    spawns.add(new NpcSpawn(NpcId.HILL_GIANT_28, new Tile(3117, 9840), 4));
    spawns.add(new NpcSpawn(NpcId.HILL_GIANT_28, new Tile(3114, 9836), 4));
    spawns.add(new NpcSpawn(NpcId.HILL_GIANT_28, new Tile(3119, 9833), 4));
    spawns.add(new NpcSpawn(NpcId.HILL_GIANT_28, new Tile(3112, 9831), 4));
    spawns.add(new NpcSpawn(NpcId.HILL_GIANT_28, new Tile(3107, 9827), 4));
    spawns.add(new NpcSpawn(NpcId.HILL_GIANT_28, new Tile(3106, 9832), 4));
    spawns.add(new NpcSpawn(NpcId.HILL_GIANT_28, new Tile(3107, 9838), 4));
    spawns.add(new NpcSpawn(NpcId.HILL_GIANT_28, new Tile(3100, 9835), 4));
    spawns.add(new NpcSpawn(NpcId.HILL_GIANT_28, new Tile(3098, 9829), 4));
    spawns.add(new NpcSpawn(NpcId.HILL_GIANT_28, new Tile(3110, 9844), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETON_22, new Tile(3097, 9910), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETON_22, new Tile(3099, 9913), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETON_22, new Tile(3101, 9908), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETON_22, new Tile(3099, 9905), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETON_22, new Tile(3096, 9906), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_SPIDER_2, new Tile(3095, 9881), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_SPIDER_2, new Tile(3095, 9884), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_SPIDER_2, new Tile(3098, 9882), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_SPIDER_2, new Tile(3101, 9884), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_SPIDER_2, new Tile(3101, 9881), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_SPIDER_2, new Tile(3105, 9883), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_RAT_3, new Tile(3117, 9891), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_RAT_3, new Tile(3120, 9893), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_RAT_3, new Tile(3122, 9889), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_RAT_3, new Tile(3119, 9886), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_RAT_3, new Tile(3120, 9890), 4));

    return spawns;
  }
}
