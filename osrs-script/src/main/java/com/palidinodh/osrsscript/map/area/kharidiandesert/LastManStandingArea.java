package com.palidinodh.osrsscript.map.area.kharidiandesert;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceIdSet;

@ReferenceIdSet(primary = 13617, secondary = { 3, 4, 5, 6, 19, 20, 21, 22, 36, 37, 38, 53, 54 })
public class LastManStandingArea extends Area {
}
