package com.palidinodh.osrsscript.map.area.kandarin;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 10039, 10295 })
public class BarbarianOutpostArea extends Area {
}
