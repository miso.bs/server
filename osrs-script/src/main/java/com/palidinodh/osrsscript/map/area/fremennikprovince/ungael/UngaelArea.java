package com.palidinodh.osrsscript.map.area.fremennikprovince.ungael;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(9023)
public class UngaelArea extends Area {
}
