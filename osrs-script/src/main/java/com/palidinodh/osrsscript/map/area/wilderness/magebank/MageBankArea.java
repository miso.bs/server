package com.palidinodh.osrsscript.map.area.wilderness.magebank;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(10057)
public class MageBankArea extends Area {
}
