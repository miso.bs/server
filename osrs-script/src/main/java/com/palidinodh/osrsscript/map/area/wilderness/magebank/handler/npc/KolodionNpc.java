package com.palidinodh.osrsscript.map.area.wilderness.magebank.handler.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.KOLODION)
class KolodionNpc implements NpcHandler {
  @Override
  public void npcOption(Player player, int option, Npc npc) {
    player.openDialogue("kolodion", 0);
  }
}
