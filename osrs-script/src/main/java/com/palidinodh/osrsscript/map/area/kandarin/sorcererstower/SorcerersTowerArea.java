package com.palidinodh.osrsscript.map.area.kandarin.sorcererstower;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(10805)
public class SorcerersTowerArea extends Area {
}
