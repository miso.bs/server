package com.palidinodh.osrsscript.map.area.entrana;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.reference.ReferenceIdSet;

@ReferenceId({ 11060, 11316 })
@ReferenceIdSet(primary = 11317, secondary = { 32, 48 })
public class EntranaArea extends Area {
}
