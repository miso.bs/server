package com.palidinodh.osrsscript.map.area.kandarin.ourania.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class OuraniaNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.RED_SALAMANDER, new Tile(2448, 3230), 4));
    spawns.add(new NpcSpawn(NpcId.RED_SALAMANDER, new Tile(2448, 3227), 4));
    spawns.add(new NpcSpawn(NpcId.RED_SALAMANDER, new Tile(2449, 3224), 4));
    spawns.add(new NpcSpawn(NpcId.RED_SALAMANDER, new Tile(2452, 3221), 4));
    spawns.add(new NpcSpawn(NpcId.RED_SALAMANDER, new Tile(2456, 3220), 4));
    spawns.add(new NpcSpawn(NpcId.RED_SALAMANDER, new Tile(2470, 3241), 4));
    spawns.add(new NpcSpawn(NpcId.RED_SALAMANDER, new Tile(2473, 3240), 4));
    spawns.add(new NpcSpawn(NpcId.RED_SALAMANDER, new Tile(2477, 3237), 4));
    spawns.add(new NpcSpawn(NpcId.RED_SALAMANDER, new Tile(2475, 3238), 4));
    spawns.add(new NpcSpawn(NpcId.RED_SALAMANDER, new Tile(2469, 3244), 4));

    return spawns;
  }
}
