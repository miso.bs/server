package com.palidinodh.osrsscript.map.area.fremennikprovince.fremennikslayerdungeon.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class FremennikSlayerDungeonNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.CAVE_CRAWLER_23, new Tile(2798, 9996), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_CRAWLER_23, new Tile(2792, 9999), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_CRAWLER_23, new Tile(2793, 9993), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_CRAWLER_23, new Tile(2787, 9992), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_CRAWLER_23, new Tile(2790, 9996), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_CRAWLER_23, new Tile(2783, 9993), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_CRAWLER_23, new Tile(2785, 9997), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_CRAWLER_23, new Tile(2782, 10000), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_CRAWLER_23, new Tile(2778, 10000), 4));
    spawns.add(new NpcSpawn(NpcId.ROCKSLUG_29, new Tile(2790, 10018), 4));
    spawns.add(new NpcSpawn(NpcId.ROCKSLUG_29, new Tile(2794, 10015), 4));
    spawns.add(new NpcSpawn(NpcId.ROCKSLUG_29, new Tile(2795, 10019), 4));
    spawns.add(new NpcSpawn(NpcId.ROCKSLUG_29, new Tile(2799, 10013), 4));
    spawns.add(new NpcSpawn(NpcId.ROCKSLUG_29, new Tile(2803, 10014), 4));
    spawns.add(new NpcSpawn(NpcId.ROCKSLUG_29, new Tile(2806, 10017), 4));
    spawns.add(new NpcSpawn(NpcId.ROCKSLUG_29, new Tile(2806, 10022), 4));
    spawns.add(new NpcSpawn(NpcId.ROCKSLUG_29, new Tile(2801, 10022), 4));
    spawns.add(new NpcSpawn(NpcId.ROCKSLUG_29, new Tile(2798, 10017), 4));
    spawns.add(new NpcSpawn(NpcId.ROCKSLUG_29, new Tile(2803, 10019), 4));
    spawns.add(new NpcSpawn(NpcId.COCKATRICE_37, new Tile(2802, 10030), 4));
    spawns.add(new NpcSpawn(NpcId.COCKATRICE_37, new Tile(2800, 10035), 4));
    spawns.add(new NpcSpawn(NpcId.COCKATRICE_37, new Tile(2794, 10038), 4));
    spawns.add(new NpcSpawn(NpcId.COCKATRICE_37, new Tile(2784, 10038), 4));
    spawns.add(new NpcSpawn(NpcId.COCKATRICE_37, new Tile(2782, 10034), 4));
    spawns.add(new NpcSpawn(NpcId.COCKATRICE_37, new Tile(2788, 10031), 4));
    spawns.add(new NpcSpawn(NpcId.COCKATRICE_37, new Tile(2794, 10030), 4));
    spawns.add(new NpcSpawn(NpcId.COCKATRICE_37, new Tile(2798, 10033), 4));
    spawns.add(new NpcSpawn(NpcId.COCKATRICE_37, new Tile(2792, 10034), 4));
    spawns.add(new NpcSpawn(NpcId.COCKATRICE_37, new Tile(2787, 10036), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(2761, 10012), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(2757, 10010), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(2763, 10007), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(2766, 10002), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(2765, 9998), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(2761, 9996), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(2757, 9995), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(2757, 10000), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(2756, 10005), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(2760, 10005), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(2761, 10000), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_61, new Tile(2743, 10001), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_61, new Tile(2738, 10006), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_61, new Tile(2747, 10006), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_61, new Tile(2739, 10013), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_61, new Tile(2744, 10017), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_61, new Tile(2745, 10012), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_61, new Tile(2742, 10010), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_61, new Tile(2742, 10005), 4));
    spawns.add(new NpcSpawn(NpcId.JELLY_78, new Tile(2712, 10030), 4));
    spawns.add(new NpcSpawn(NpcId.JELLY_78, new Tile(2706, 10031), 4));
    spawns.add(new NpcSpawn(NpcId.JELLY_78, new Tile(2700, 10031), 4));
    spawns.add(new NpcSpawn(NpcId.JELLY_78, new Tile(2697, 10026), 4));
    spawns.add(new NpcSpawn(NpcId.JELLY_78, new Tile(2700, 10022), 4));
    spawns.add(new NpcSpawn(NpcId.JELLY_78, new Tile(2705, 10021), 4));
    spawns.add(new NpcSpawn(NpcId.JELLY_78, new Tile(2709, 10022), 4));
    spawns.add(new NpcSpawn(NpcId.JELLY_78, new Tile(2712, 10025), 4));
    spawns.add(new NpcSpawn(NpcId.JELLY_78, new Tile(2708, 10027), 4));
    spawns.add(new NpcSpawn(NpcId.JELLY_78, new Tile(2702, 10027), 4));
    spawns.add(new NpcSpawn(NpcId.TUROTH_89, new Tile(2715, 10010), 4));
    spawns.add(new NpcSpawn(NpcId.TUROTH_89, new Tile(2720, 10012), 4));
    spawns.add(new NpcSpawn(NpcId.TUROTH_89, new Tile(2726, 10010), 4));
    spawns.add(new NpcSpawn(NpcId.TUROTH_89, new Tile(2728, 10004), 4));
    spawns.add(new NpcSpawn(NpcId.TUROTH_89, new Tile(2730, 9998), 4));
    spawns.add(new NpcSpawn(NpcId.TUROTH_89, new Tile(2726, 9995), 4));
    spawns.add(new NpcSpawn(NpcId.TUROTH_89, new Tile(2721, 9996), 4));
    spawns.add(new NpcSpawn(NpcId.TUROTH_89, new Tile(2718, 10001), 4));
    spawns.add(new NpcSpawn(NpcId.TUROTH_89, new Tile(2716, 10005), 4));
    spawns.add(new NpcSpawn(NpcId.TUROTH_89, new Tile(2722, 10006), 4));
    spawns.add(new NpcSpawn(NpcId.TUROTH_89, new Tile(2724, 10000), 4));
    spawns.add(new NpcSpawn(NpcId.KURASK_106, new Tile(2705, 9994), 4));
    spawns.add(new NpcSpawn(NpcId.KURASK_106, new Tile(2701, 9992), 4));
    spawns.add(new NpcSpawn(NpcId.KURASK_106, new Tile(2695, 9993), 4));
    spawns.add(new NpcSpawn(NpcId.KURASK_106, new Tile(2693, 10000), 4));
    spawns.add(new NpcSpawn(NpcId.KURASK_106, new Tile(2699, 10002), 4));
    spawns.add(new NpcSpawn(NpcId.KURASK_106, new Tile(2704, 10000), 4));
    spawns.add(new NpcSpawn(NpcId.KURASK_106, new Tile(2697, 9999), 4));
    spawns.add(new NpcSpawn(NpcId.KURASK_106, new Tile(2702, 9997), 4));

    return spawns;
  }
}
