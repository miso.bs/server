package com.palidinodh.osrsscript.map.area.kandarin.observatory;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(9777)
public class ObservatoryArea extends Area {
}
