package com.palidinodh.osrsscript.map.area.fremennikprovince.jormungandsprison;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 9634, 9635, 9890, 9891 })
public class JormungandsPrisonArea extends Area {
}
