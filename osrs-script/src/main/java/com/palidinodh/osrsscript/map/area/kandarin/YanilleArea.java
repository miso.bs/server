package com.palidinodh.osrsscript.map.area.kandarin;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 10032, 10288, 10544 })
public class YanilleArea extends Area {
}
