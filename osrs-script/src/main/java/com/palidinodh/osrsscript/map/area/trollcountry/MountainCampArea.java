package com.palidinodh.osrsscript.map.area.trollcountry;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(11065)
public class MountainCampArea extends Area {
}
