package com.palidinodh.osrsscript.map.area.fremennikprovince.waterbirth;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(10042)
public class WaterbirthIslandArea extends Area {
}
