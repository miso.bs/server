package com.palidinodh.osrsscript.map.area.morytania.canifis;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(14133)
public class CanifisHunterArea extends Area {
}
