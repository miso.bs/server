package com.palidinodh.osrsscript.map.area.trollcountry.deathplateau;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.reference.ReferenceIdSet;

@ReferenceId({ 11320, 11576 })
@ReferenceIdSet(primary = 11319, secondary = { 87, 103, 119 })
public class DeathPlateauArea extends Area {
}
