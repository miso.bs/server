package com.palidinodh.osrsscript.map.area.wilderness.magebank.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class MageBankNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.KOLODION, new Tile(2542, 4716), 4));
    spawns.add(new NpcSpawn(NpcId.GUNDAI, new Tile(2534, 4716), 2));
    spawns.add(new NpcSpawn(NpcId.LUNDAIL, new Tile(2537, 4716), 4));

    return spawns;
  }
}
