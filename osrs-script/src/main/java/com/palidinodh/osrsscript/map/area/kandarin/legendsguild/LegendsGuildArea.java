package com.palidinodh.osrsscript.map.area.kandarin.legendsguild;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.reference.ReferenceIdSet;

@ReferenceId(10804)
@ReferenceIdSet(primary = 11060, secondary = { 7 })
public class LegendsGuildArea extends Area {
}
