package com.palidinodh.osrsscript.map.area.morytania.sisterhoodsanctuary;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.MessageDialogue;
import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrsscript.player.plugin.boss.BossInstanceController;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PEventTasks;
import lombok.var;

@ReferenceId({ 14999, 15000, 15001, 15255, 15256, 15257, 15511, 15512, 15513, 15515 })
public class SisterhoodSanctuaryArea extends Area {
  public static final Tile NIGHTMARE_CENTER_TILE = new Tile(3870, 9949, 3);
  private static final NpcSpawn BOSS_SPAWN =
      new NpcSpawn(NpcId.THE_NIGHTMARE_814_9433, NIGHTMARE_CENTER_TILE);

  @Override
  public boolean inMultiCombat() {
    return true;
  }

  public Npc getNightmare() {
    return getEntity().getController().getNpc(NpcId.THE_NIGHTMARE_814, NpcId.THE_NIGHTMARE_814_9426,
        NpcId.THE_NIGHTMARE_814_9427, NpcId.THE_NIGHTMARE_814_9428, NpcId.THE_NIGHTMARE_814_9429,
        NpcId.THE_NIGHTMARE_814_9430, NpcId.THE_NIGHTMARE_814_9431);
  }

  public void joinPublic(Player player) {
    if (getNightmare() != null) {
      player.openDialogue(new MessageDialogue(
          "A group is already fighting the Nightmare. You'll have to wait until they are done."));
      return;
    }
    player.setController(new BossInstanceController());
    if (player.getController().getNpc(NpcId.THE_NIGHTMARE_814_9432,
        NpcId.THE_NIGHTMARE_814_9433) == null) {
      player.getController().addNpc(BOSS_SPAWN);
    }
    enter(player);
  }

  private void enter(Player player) {
    player.openDialogue(
        new MessageDialogue("The Nightmare pulls you into her dream as you approach her.", true));
    player.setAnimation(8584);
    player.getGameEncoder().sendFadeOut();
    player.lock();
    var tasks = new PEventTasks();
    tasks.execute(2,
        t -> player.getMovement().teleport(new Tile(3870 + PRandom.randomI(4), 9948, 3)));
    tasks.execute(2, t -> {
      player.setAnimation(8583);
      player.getGameEncoder().sendFadeIn();
    });
    tasks.execute(2, t -> {
      player.unlock();
      player.openDialogue(
          new MessageDialogue("The Nightmare pulls you into her dream as you approach her."));
    });
    player.getController().addEvent(tasks);
  }
}
