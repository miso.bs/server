package com.palidinodh.osrsscript.map.area.misthalin.edgeville.handler.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrsscript.player.plugin.bond.BondPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.CAPT_BOND_16018)
class BondsNpc implements NpcHandler {
  @Override
  public void npcOption(Player player, int option, Npc npc) {
    player.getPlugin(BondPlugin.class).sendPouch();
  }
}
