package com.palidinodh.osrsscript.map.area.kandarin.fishingguild.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class FishingGuildNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(2615, 3398, 1)));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(2584, 3422), Tile.Direction.EAST));
    spawns.add(new NpcSpawn(NpcId.BANKER_395, new Tile(2584, 3421), Tile.Direction.EAST));
    spawns.add(new NpcSpawn(NpcId.BANKER_395, new Tile(2584, 3418), Tile.Direction.EAST));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(2584, 3419), Tile.Direction.EAST));
    spawns.add(new NpcSpawn(NpcId.ROACHEY, new Tile(2597, 3400), 4));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1514, new Tile(2612, 3412)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1514, new Tile(2612, 3411)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1514, new Tile(2608, 3410)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1514, new Tile(2607, 3410)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1514, new Tile(2606, 3410)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1514, new Tile(2605, 3410)));
    spawns.add(new NpcSpawn(NpcId.ROD_FISHING_SPOT, new Tile(2602, 3411)));
    spawns.add(new NpcSpawn(NpcId.ROD_FISHING_SPOT, new Tile(2602, 3412)));
    spawns.add(new NpcSpawn(NpcId.ROD_FISHING_SPOT, new Tile(2602, 3413)));
    spawns.add(new NpcSpawn(NpcId.ROD_FISHING_SPOT, new Tile(2602, 3414)));
    spawns.add(new NpcSpawn(NpcId.ROD_FISHING_SPOT, new Tile(2602, 3415)));
    spawns.add(new NpcSpawn(NpcId.ROD_FISHING_SPOT, new Tile(2602, 3416)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1510, new Tile(2605, 3416)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1510, new Tile(2606, 3416)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1510, new Tile(2607, 3416)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1510, new Tile(2608, 3416)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1510, new Tile(2609, 3416)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1510, new Tile(2610, 3416)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1510, new Tile(2611, 3416)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1510, new Tile(2612, 3415)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1510, new Tile(2612, 3414)));
    spawns.add(new NpcSpawn(NpcId.ROD_FISHING_SPOT, new Tile(2603, 3417)));
    spawns.add(new NpcSpawn(NpcId.ROD_FISHING_SPOT, new Tile(2604, 3417)));
    spawns.add(new NpcSpawn(NpcId.ROD_FISHING_SPOT, new Tile(2605, 3413)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1510, new Tile(2611, 3413)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1514, new Tile(2610, 3413)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1514, new Tile(2609, 3413)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1510, new Tile(2608, 3413)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1510, new Tile(2607, 3413)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1514, new Tile(2606, 3413)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_4316, new Tile(2604, 3419)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_4316, new Tile(2603, 3419)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_4316, new Tile(2602, 3419)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_4316, new Tile(2605, 3420)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_4316, new Tile(2605, 3421)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_4316, new Tile(2604, 3422)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_4316, new Tile(2603, 3422)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_4316, new Tile(2602, 3422)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_4316, new Tile(2601, 3422)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_4712, new Tile(2600, 3419)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_4712, new Tile(2599, 3419)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_4712, new Tile(2598, 3419)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1511, new Tile(2601, 3423)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1511, new Tile(2602, 3423)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1511, new Tile(2603, 3423)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1511, new Tile(2604, 3423)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1511, new Tile(2605, 3424)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1511, new Tile(2605, 3425)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1511, new Tile(2604, 3426)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1511, new Tile(2603, 3426)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1511, new Tile(2602, 3426)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1510, new Tile(2598, 3423)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_1510, new Tile(2598, 3422)));
    spawns.add(new NpcSpawn(NpcId.KYLIE_MINNOW_7728, new Tile(2614, 3445), 2));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_7730, new Tile(2610, 3443)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_7731, new Tile(2611, 3444)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_7730, new Tile(2618, 3443)));
    spawns.add(new NpcSpawn(NpcId.FISHING_SPOT_7731, new Tile(2619, 3444)));

    return spawns;
  }
}
