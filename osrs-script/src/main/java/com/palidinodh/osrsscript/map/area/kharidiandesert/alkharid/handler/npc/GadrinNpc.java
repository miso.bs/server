package com.palidinodh.osrsscript.map.area.kharidiandesert.alkharid.handler.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrsscript.player.plugin.magic.SpellTeleport;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.GADRIN)
class GadrinNpc implements NpcHandler {
  @Override
  public void npcOption(Player player, int option, Npc npc) {
    SpellTeleport.normalTeleport(player, new Tile(2467, 9905));
  }
}
