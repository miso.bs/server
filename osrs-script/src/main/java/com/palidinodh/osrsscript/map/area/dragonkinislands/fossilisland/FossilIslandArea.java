package com.palidinodh.osrsscript.map.area.dragonkinislands.fossilisland;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 14393, 14394, 14395, 14396, 14397, 14649, 14650, 14651, 14652, 14653, 14654, 14905,
    14906, 14907, 14908, 15909, 14910, 15161, 15162, 15163, 15164, 15165, 15418 })
public class FossilIslandArea extends Area {
}
