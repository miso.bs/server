package com.palidinodh.osrsscript.map.area.asgarnia.asgarnianicedungeon.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class AsgarnianIceDungeonNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.MUGGER_6, new Tile(2993, 9546), 4));
    spawns.add(new NpcSpawn(NpcId.MUGGER_6, new Tile(2998, 9550), 4));
    spawns.add(new NpcSpawn(NpcId.MUGGER_6, new Tile(2994, 9550), 4));
    spawns.add(new NpcSpawn(NpcId.MUGGER_6, new Tile(2999, 9544), 4));
    spawns.add(new NpcSpawn(NpcId.MUGGER_6, new Tile(2996, 9546), 4));
    spawns.add(new NpcSpawn(NpcId.PIRATE_23_2880, new Tile(2992, 9573), 4));
    spawns.add(new NpcSpawn(NpcId.PIRATE_23_2880, new Tile(2986, 9577), 4));
    spawns.add(new NpcSpawn(NpcId.PIRATE_23_2880, new Tile(2989, 9582), 4));
    spawns.add(new NpcSpawn(NpcId.PIRATE_23_2880, new Tile(2994, 9583), 4));
    spawns.add(new NpcSpawn(NpcId.PIRATE_23_2880, new Tile(2999, 9578), 4));
    spawns.add(new NpcSpawn(NpcId.PIRATE_23_2880, new Tile(2995, 9576), 4));
    spawns.add(new NpcSpawn(NpcId.PIRATE_23_2880, new Tile(2995, 9570), 4));
    spawns.add(new NpcSpawn(NpcId.HOBGOBLIN_28, new Tile(3019, 9582), 4));
    spawns.add(new NpcSpawn(NpcId.HOBGOBLIN_28, new Tile(3024, 9582), 4));
    spawns.add(new NpcSpawn(NpcId.HOBGOBLIN_28, new Tile(3019, 9577), 4));
    spawns.add(new NpcSpawn(NpcId.HOBGOBLIN_28, new Tile(3023, 9578), 4));
    spawns.add(new NpcSpawn(NpcId.HOBGOBLIN_28, new Tile(3025, 9573), 4));
    spawns.add(new NpcSpawn(NpcId.HOBGOBLIN_28, new Tile(3021, 9581), 4));
    spawns.add(new NpcSpawn(NpcId.HOBGOBLIN_42, new Tile(3023, 9589), 4));
    spawns.add(new NpcSpawn(NpcId.HOBGOBLIN_42, new Tile(3023, 9593), 4));
    spawns.add(new NpcSpawn(NpcId.HOBGOBLIN_42, new Tile(3018, 9593), 4));
    spawns.add(new NpcSpawn(NpcId.HOBGOBLIN_42, new Tile(3014, 9594), 4));
    spawns.add(new NpcSpawn(NpcId.HOBGOBLIN_42, new Tile(3026, 9592), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_GIANT_53, new Tile(3063, 9582), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_GIANT_53, new Tile(3059, 9586), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_GIANT_53, new Tile(3063, 9576), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_GIANT_53, new Tile(3058, 9576), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_GIANT_53, new Tile(3064, 9571), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_GIANT_53, new Tile(3057, 9571), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_GIANT_53, new Tile(3054, 9585), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_GIANT_53, new Tile(3055, 9578), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57, new Tile(3039, 9582), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57, new Tile(3041, 9586), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57, new Tile(3044, 9588), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57, new Tile(3043, 9582), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57, new Tile(3047, 9581), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57, new Tile(3050, 9584), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_GIANT_53, new Tile(3052, 9590), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_GIANT_53, new Tile(3047, 9589), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57, new Tile(3051, 9578), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57, new Tile(3042, 9576), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57, new Tile(3046, 9574), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57, new Tile(3050, 9573), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57, new Tile(3059, 9568), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETAL_WYVERN_140, new Tile(3047, 9552), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETAL_WYVERN_140, new Tile(3062, 9551), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETAL_WYVERN_140, new Tile(3065, 9543), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETAL_WYVERN_140, new Tile(3058, 9540), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETAL_WYVERN_140, new Tile(3049, 9544), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETAL_WYVERN_140, new Tile(3041, 9542), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETAL_WYVERN_140, new Tile(3027, 9543), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETAL_WYVERN_140, new Tile(3034, 9553), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETAL_WYVERN_140, new Tile(3026, 9552), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETAL_WYVERN_140, new Tile(3034, 9539), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETAL_WYVERN_140, new Tile(3039, 9568), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETAL_WYVERN_140, new Tile(3042, 9560), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETAL_WYVERN_140, new Tile(3050, 9559), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETAL_WYVERN_140, new Tile(3072, 9552), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETAL_WYVERN_140, new Tile(3077, 9559), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETAL_WYVERN_140, new Tile(3068, 9562), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETAL_WYVERN_140, new Tile(3076, 9567), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETAL_WYVERN_140, new Tile(3072, 9576), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETAL_WYVERN_140, new Tile(3078, 9583), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETAL_WYVERN_140, new Tile(3070, 9584), 4));

    return spawns;
  }
}
