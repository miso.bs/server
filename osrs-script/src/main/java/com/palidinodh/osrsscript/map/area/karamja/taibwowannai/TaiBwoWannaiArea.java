package com.palidinodh.osrsscript.map.area.karamja.taibwowannai;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 11055, 11056, 11312 })
public class TaiBwoWannaiArea extends Area {
}
