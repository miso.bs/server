package com.palidinodh.osrsscript.map.area.kharidiandesert.clanwars.handler.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrsscript.map.area.kharidiandesert.clanwars.ClanWarsFreeForAllArea;
import com.palidinodh.osrsscript.player.plugin.clanwars.ClanWarsFreeForAllController;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.DOOMSAYER)
class DoomsayerNpc implements NpcHandler {
  @Override
  public void npcOption(Player player, int option, Npc npc) {
    if (!player.getArea().isArea(ClanWarsFreeForAllArea.class)) {
      return;
    }
    if (!player.getController().isController(ClanWarsFreeForAllController.class)) {
      return;
    }
    player.openShop("clan_wars_free_for_all");
  }
}
