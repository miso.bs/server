package com.palidinodh.osrsscript.map.area.kandarin;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(10036)
public class CombatTrainingCampArea extends Area {
}
