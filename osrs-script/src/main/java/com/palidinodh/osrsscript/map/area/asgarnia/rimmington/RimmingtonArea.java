package com.palidinodh.osrsscript.map.area.asgarnia.rimmington;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 11570, 11826 })
public class RimmingtonArea extends Area {
}
