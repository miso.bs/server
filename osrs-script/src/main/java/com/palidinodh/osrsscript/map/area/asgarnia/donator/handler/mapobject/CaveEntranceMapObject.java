package com.palidinodh.osrsscript.map.area.asgarnia.donator.handler.mapobject;

import com.palidinodh.osrscore.io.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ ObjectId.CAVE_ENTRANCE_2123, ObjectId.CAVE_ENTRANCE_5007 })
class CaveEntranceMapObject implements MapObjectHandler {
  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    switch (mapObject.getId()) {
      case ObjectId.CAVE_ENTRANCE_2123:
        player.getGameEncoder().sendMessage("I wonder what's inside...");
        break;

      case ObjectId.CAVE_ENTRANCE_5007:
        player.getGameEncoder().sendMessage("I wonder what's inside...");
        break;
    }
  }
}
