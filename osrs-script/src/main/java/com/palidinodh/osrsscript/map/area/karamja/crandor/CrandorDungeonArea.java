package com.palidinodh.osrsscript.map.area.karamja.crandor;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(11414)
public class CrandorDungeonArea extends Area {
}
