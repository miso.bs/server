package com.palidinodh.osrsscript.map.area.dragonkinislands.lithkren;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 14142, 14398 })
public class LithkrenArea extends Area {
}
