package com.palidinodh.osrsscript.map.area.fremennikprovince.rellekka.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class RellekkaHunterNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(2732, 3793)));
    spawns.add(new NpcSpawn(NpcId.CERULEAN_TWITCH, new Tile(2726, 3773), 8));
    spawns.add(new NpcSpawn(NpcId.CERULEAN_TWITCH, new Tile(2729, 3770), 8));
    spawns.add(new NpcSpawn(NpcId.CERULEAN_TWITCH, new Tile(2733, 3768), 8));
    spawns.add(new NpcSpawn(NpcId.CERULEAN_TWITCH, new Tile(2735, 3772), 8));
    spawns.add(new NpcSpawn(NpcId.CERULEAN_TWITCH, new Tile(2732, 3775), 8));
    spawns.add(new NpcSpawn(NpcId.CERULEAN_TWITCH, new Tile(2739, 3771), 8));
    spawns.add(new NpcSpawn(NpcId.CERULEAN_TWITCH, new Tile(2732, 3762), 8));
    spawns.add(new NpcSpawn(NpcId.CERULEAN_TWITCH, new Tile(2727, 3764), 8));
    spawns.add(new NpcSpawn(NpcId.CERULEAN_TWITCH, new Tile(2724, 3779), 8));
    spawns.add(new NpcSpawn(NpcId.CERULEAN_TWITCH, new Tile(2729, 3785), 8));
    spawns.add(new NpcSpawn(NpcId.CERULEAN_TWITCH, new Tile(2735, 3779), 8));
    spawns.add(new NpcSpawn(NpcId.CERULEAN_TWITCH, new Tile(2740, 3780), 8));
    spawns.add(new NpcSpawn(NpcId.CERULEAN_TWITCH, new Tile(2741, 3785), 8));
    spawns.add(new NpcSpawn(NpcId.CERULEAN_TWITCH, new Tile(2733, 3785), 8));
    spawns.add(new NpcSpawn(NpcId.SAPPHIRE_GLACIALIS, new Tile(2703, 3782), 8));
    spawns.add(new NpcSpawn(NpcId.SNOWY_KNIGHT, new Tile(2708, 3784), 8));
    spawns.add(new NpcSpawn(NpcId.SAPPHIRE_GLACIALIS, new Tile(2711, 3788), 8));
    spawns.add(new NpcSpawn(NpcId.SNOWY_KNIGHT, new Tile(2709, 3792), 8));
    spawns.add(new NpcSpawn(NpcId.SAPPHIRE_GLACIALIS, new Tile(2702, 3791), 8));
    spawns.add(new NpcSpawn(NpcId.SNOWY_KNIGHT, new Tile(2697, 3788), 8));
    spawns.add(new NpcSpawn(NpcId.SAPPHIRE_GLACIALIS, new Tile(2698, 3783), 8));
    spawns.add(new NpcSpawn(NpcId.SNOWY_KNIGHT, new Tile(2697, 3796), 8));
    spawns.add(new NpcSpawn(NpcId.SAPPHIRE_GLACIALIS, new Tile(2700, 3800), 8));
    spawns.add(new NpcSpawn(NpcId.SAPPHIRE_GLACIALIS, new Tile(2701, 3807), 8));
    spawns.add(new NpcSpawn(NpcId.SNOWY_KNIGHT, new Tile(2708, 3806), 8));
    spawns.add(new NpcSpawn(NpcId.SAPPHIRE_GLACIALIS, new Tile(2693, 3803), 8));
    spawns.add(new NpcSpawn(NpcId.SNOWY_KNIGHT, new Tile(2694, 3798), 8));
    spawns.add(new NpcSpawn(NpcId.SAPPHIRE_GLACIALIS, new Tile(2710, 3798), 8));
    spawns.add(new NpcSpawn(NpcId.SAPPHIRE_GLACIALIS, new Tile(2737, 3791), 8));
    spawns.add(new NpcSpawn(NpcId.SNOWY_KNIGHT, new Tile(2741, 3785), 8));
    spawns.add(new NpcSpawn(NpcId.SAPPHIRE_GLACIALIS, new Tile(2732, 3786), 8));
    spawns.add(new NpcSpawn(NpcId.SNOWY_KNIGHT, new Tile(2727, 3785), 8));
    spawns.add(new NpcSpawn(NpcId.SAPPHIRE_GLACIALIS, new Tile(2721, 3784), 8));
    spawns.add(new NpcSpawn(NpcId.SAPPHIRE_GLACIALIS, new Tile(2720, 3789), 8));
    spawns.add(new NpcSpawn(NpcId.SAPPHIRE_GLACIALIS, new Tile(2724, 3772), 8));
    spawns.add(new NpcSpawn(NpcId.SNOWY_KNIGHT, new Tile(2731, 3771), 8));

    return spawns;
  }
}
