package com.palidinodh.osrsscript.map.area.feldiphills.hunterarea;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 9773, 10029, 10285, 10129 })
public class FeldipHunterArea extends Area {
}
