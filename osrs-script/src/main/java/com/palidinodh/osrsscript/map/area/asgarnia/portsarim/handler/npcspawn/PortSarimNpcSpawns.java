package com.palidinodh.osrsscript.map.area.asgarnia.portsarim.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class PortSarimNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.THIEF_16, new Tile(3014, 3195), 2));
    spawns.add(new NpcSpawn(NpcId.CAPN_HAND, new Tile(3013, 3193), 1));
    spawns.add(new NpcSpawn(NpcId.PIRATE_23, new Tile(3014, 3191), 1));
    spawns.add(new NpcSpawn(NpcId.WORMBRAIN_2, new Tile(3014, 3189), 2));
    spawns.add(new NpcSpawn(NpcId.MUGGER_6, new Tile(3018, 3189), 2));
    spawns.add(new NpcSpawn(NpcId.GUARD_1551, new Tile(3019, 3185)));
    spawns.add(new NpcSpawn(NpcId.BLACK_KNIGHT_33, new Tile(3018, 3180), 1));
    spawns.add(new NpcSpawn(NpcId.GUARD_21, new Tile(3012, 3184, 1), 4));
    spawns.add(new NpcSpawn(NpcId.GUARD_21_1547, new Tile(3016, 3183, 1), 4));
    spawns.add(new NpcSpawn(NpcId.GUARD_21_1548, new Tile(3012, 3181, 1), 4));
    spawns.add(new NpcSpawn(NpcId.GUARD_21_1549, new Tile(3018, 3180, 1), 4));
    spawns.add(new NpcSpawn(NpcId.GUARD_21_1550, new Tile(3019, 3186, 1), 4));
    spawns.add(new NpcSpawn(NpcId.SECURITY_GUARD, new Tile(3013, 3190, 1), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_RAT_26, new Tile(2999, 3190), 2));
    spawns.add(new NpcSpawn(NpcId.GIANT_RAT_26, new Tile(2998, 3194), 2));
    spawns.add(new NpcSpawn(NpcId.TOOL_LEPRECHAUN, new Tile(3060, 3263)));
    spawns.add(new NpcSpawn(NpcId.FRIZZY_SKERNIP, new Tile(3063, 3259), 2));
    spawns.add(new NpcSpawn(NpcId.LONGBOW_BEN, new Tile(3047, 3258), 4));
    spawns.add(new NpcSpawn(NpcId.JACK_SEAGULL, new Tile(3051, 3256), 4));
    spawns.add(new NpcSpawn(NpcId.AHAB, new Tile(3049, 3256)));
    spawns.add(new NpcSpawn(NpcId.BARTENDER_1313, new Tile(3045, 3257), 2));
    spawns.add(new NpcSpawn(NpcId.REDBEARD_FRANK, new Tile(3052, 3247), 4));
    spawns.add(new NpcSpawn(NpcId.MONK_OF_ENTRANA_1166, new Tile(3049, 3235), 4));
    spawns.add(new NpcSpawn(NpcId.MONK_OF_ENTRANA_1166, new Tile(3047, 3236), 4));
    spawns.add(new NpcSpawn(NpcId.MONK_OF_ENTRANA_1167, new Tile(3044, 3235), 4));
    spawns.add(new NpcSpawn(NpcId.SEAGULL_2, new Tile(3028, 3235), 4));
    spawns.add(new NpcSpawn(NpcId.SEAGULL_2, new Tile(3026, 3233), 4));
    spawns.add(new NpcSpawn(NpcId.SEAGULL_2, new Tile(3028, 3231), 4));
    spawns.add(new NpcSpawn(NpcId.SEAMAN_LORRIS, new Tile(3027, 3219), 4));
    spawns.add(new NpcSpawn(NpcId.CAPTAIN_TOBIAS, new Tile(3026, 3216), 4));
    spawns.add(new NpcSpawn(NpcId.SEAMAN_THRESNOR, new Tile(3028, 3215), 4));
    spawns.add(new NpcSpawn(NpcId.SEAGULL_2, new Tile(3025, 3207), 4));
    spawns.add(new NpcSpawn(NpcId.SEAGULL_2, new Tile(3028, 3205), 4));
    spawns.add(new NpcSpawn(NpcId.SEAGULL_2, new Tile(3026, 3202), 4));
    spawns.add(new NpcSpawn(NpcId.SEAGULL_2, new Tile(3029, 3202), 4));
    spawns.add(new NpcSpawn(NpcId.WYDIN_1791, new Tile(3014, 3206), 2));
    spawns.add(new NpcSpawn(NpcId.GERRANT_1790, new Tile(3014, 3224), 2));
    spawns.add(new NpcSpawn(NpcId.THE_FACE, new Tile(3020, 3229), 2));
    spawns.add(new NpcSpawn(NpcId.ZAHUR, new Tile(3017, 3231), 2));
    spawns.add(new NpcSpawn(NpcId.WOMAN_2_3083, new Tile(3012, 3232), 4));
    spawns.add(new NpcSpawn(NpcId.MAN_2_3078, new Tile(3022, 3237), 4));
    spawns.add(new NpcSpawn(NpcId.THIEF_16_3093, new Tile(3017, 3238), 4));
    spawns.add(new NpcSpawn(NpcId.THIEF_16_3093, new Tile(3014, 3231), 4));
    spawns.add(new NpcSpawn(NpcId.GRUM, new Tile(3013, 3246), 2));
    spawns.add(new NpcSpawn(NpcId.BRIAN, new Tile(3028, 3248), 2));
    spawns.add(new NpcSpawn(NpcId.BETTY, new Tile(3014, 3258), 2));

    return spawns;
  }
}
