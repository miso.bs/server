package com.palidinodh.osrsscript.map.area.morytania.mosleharmless.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class MosLeHarmlessCaveNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3738, 9376), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3731, 9369), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3725, 9375), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3728, 9357), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3732, 9352), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3718, 9357), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3722, 9367), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3726, 9382), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3738, 9386), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3731, 9390), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3734, 9399), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3745, 9397), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3755, 9398), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3754, 9388), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3767, 9397), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3762, 9405), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3751, 9407), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3743, 9410), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3733, 9413), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3731, 9421), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3722, 9419), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3720, 9429), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3733, 9430), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3741, 9427), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3749, 9423), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3761, 9424), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3764, 9416), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3765, 9433), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3759, 9441), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3750, 9437), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3737, 9440), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3730, 9443), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3723, 9440), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3726, 9453), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3737, 9450), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3732, 9460), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3744, 9461), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3749, 9450), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3755, 9459), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3762, 9452), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3768, 9461), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3772, 9450), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3779, 9437), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3786, 9443), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3789, 9455), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3782, 9460), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3800, 9460), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3799, 9445), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3807, 9439), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3794, 9433), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3793, 9422), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3807, 9427), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3802, 9415), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3824, 9414), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3833, 9419), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3826, 9427), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3816, 9431), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3833, 9434), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_HORROR_80, new Tile(3822, 9446), 4));

    return spawns;
  }
}
