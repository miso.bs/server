package com.palidinodh.osrsscript.map.area.fremennikprovince.ungael.handler.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.VORKATH_8059)
class VorkathNpc implements NpcHandler {
  @Override
  public void npcOption(Player player, int option, Npc npc) {
    if (npc.isLocked()) {
      return;
    }
    npc.setId(NpcId.VORKATH_732);
  }
}
