package com.palidinodh.osrsscript.map.area.misthalin.edgeville.handler.mapobject;

import com.palidinodh.osrscore.io.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrsscript.player.plugin.clanwars.ClanWarsFreeForAllController;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.FREE_FOR_ALL_PORTAL)
class FreeForAllMapObject implements MapObjectHandler {
  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    player.openDialogue(new OptionsDialogue(new DialogueOption("Safe Free-For-All", (c, s) -> {
      if (!player.getInventory().isEmpty() || !player.getEquipment().isEmpty()) {
        player.getGameEncoder().sendMessage("You can't take items into the free-for-all arena.");
        return;
      }
      player.setController(new ClanWarsFreeForAllController());
    }), new DialogueOption("Risk Zone", (c, s) -> {
      player.getMovement().teleport(2655, 5471);
    })));
  }
}
