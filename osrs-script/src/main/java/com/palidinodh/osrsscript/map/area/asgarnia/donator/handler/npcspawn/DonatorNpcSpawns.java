package com.palidinodh.osrsscript.map.area.asgarnia.donator.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class DonatorNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.MARTIN_THWAIT, new Tile(3049, 3500), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.NIEVE, new Tile(3064, 3497), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.LLIANN, new Tile(3058, 3515), Tile.Direction.SOUTH));
    spawns.add(new NpcSpawn(NpcId.HONEST_JIMMY, new Tile(3061, 3514), Tile.Direction.SOUTH));
    spawns.add(new NpcSpawn(NpcId.LOGAVA, new Tile(3062, 3512), Tile.Direction.SOUTH));
    spawns.add(new NpcSpawn(NpcId.HORVIK, new Tile(3040, 3496), Tile.Direction.NORTH));


    return spawns;
  }
}
