package com.palidinodh.osrsscript.map.area.asgarnia.cerberuslair;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 4883, 5139, 5140, 5395 })
public class CerberusLairArea extends Area {
}
