package com.palidinodh.osrsscript.map.area.asgarnia.falador;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 11572, 11827, 11828, 11829, 12083, 12084 })
public class FaladorArea extends Area {
}
