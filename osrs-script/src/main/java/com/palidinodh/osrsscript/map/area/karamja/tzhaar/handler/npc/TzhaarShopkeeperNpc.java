package com.palidinodh.osrsscript.map.area.karamja.tzhaar.handler.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ NpcId.TZHAAR_HUR_LEK, NpcId.TZHAAR_HUR_TEL, NpcId.TZHAAR_MEJ_ROH })
class TzhaarShopkeeperNpc implements NpcHandler {
  @Override
  public void npcOption(Player player, int option, Npc npc) {
    switch (npc.getId()) {
      case NpcId.TZHAAR_HUR_LEK:
        player.openShop("tzhaar_rocks");
        break;
      case NpcId.TZHAAR_HUR_TEL:
        player.openShop("tzhaar_equipment");
        break;
      case NpcId.TZHAAR_MEJ_ROH:
        player.openShop("tzhaar_runes");
        break;
    }
  }
}
