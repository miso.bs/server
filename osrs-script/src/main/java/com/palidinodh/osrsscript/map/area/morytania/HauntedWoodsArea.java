package com.palidinodh.osrsscript.map.area.morytania;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 14134, 14390 })
public class HauntedWoodsArea extends Area {
}
