package com.palidinodh.osrsscript.map.area.puropuro.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class PuroPuroNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.ELNOCK_INQUISITOR, new Tile(2590, 4319), 4));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(2588, 4323)));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2569, 4342), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2569, 4333), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2569, 4324), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2569, 4317), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2569, 4310), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2569, 4302), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2572, 4297), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2578, 4297), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2586, 4297), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2594, 4297), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2602, 4297), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2610, 4297), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2614, 4302), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2614, 4309), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2614, 4317), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2614, 4326), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2614, 4335), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2611, 4342), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2603, 4342), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2595, 4342), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2587, 4342), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2579, 4342), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2573, 4338), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2573, 4329), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2573, 4320), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2573, 4312), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2573, 4305), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2576, 4301), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2582, 4301), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2591, 4301), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2599, 4301), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2607, 4301), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2610, 4306), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2610, 4314), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2610, 4322), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2599, 4338), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2610, 4338), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2590, 4338), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2582, 4338), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2580, 4332), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2596, 4332), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2604, 4321), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2598, 4307), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2581, 4307), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2579, 4319), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2597, 4314), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2586, 4314), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2586, 4325), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2597, 4325), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2602, 4330), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2586, 4330), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2581, 4325), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2581, 4313), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2588, 4309), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2602, 4309), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2602, 4316), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2606, 4329), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2586, 4334), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2577, 4327), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2600, 4305), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2617, 4345), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2617, 4294), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2566, 4294), 16));
    spawns.add(new NpcSpawn(NpcId.BABY_IMPLING, new Tile(2566, 4345), 16));

    return spawns;
  }
}
