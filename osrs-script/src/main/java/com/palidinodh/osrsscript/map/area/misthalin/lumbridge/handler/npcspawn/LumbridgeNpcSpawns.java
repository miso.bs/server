package com.palidinodh.osrsscript.map.area.misthalin.lumbridge.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class LumbridgeNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.FAYETH, new Tile(3190, 3232), 2));
    spawns.add(new NpcSpawn(NpcId.TOOL_LEPRECHAUN, new Tile(3189, 3234)));
    spawns.add(new NpcSpawn(NpcId.SHOP_KEEPER, new Tile(3212, 3248), 4));
    spawns.add(new NpcSpawn(NpcId.DONIE, new Tile(3217, 3249), 4));
    spawns.add(new NpcSpawn(NpcId.DONIE, new Tile(3223, 3237), 4));
    spawns.add(new NpcSpawn(NpcId.MAN_2_3078, new Tile(3214, 3220), 4));
    spawns.add(new NpcSpawn(NpcId.COOK_4626, new Tile(3210, 3215), 4));
    spawns.add(new NpcSpawn(NpcId.HANS, new Tile(3221, 3222), 4));
    spawns.add(new NpcSpawn(NpcId.MAN_2_3080, new Tile(3222, 3216), 4));
    spawns.add(new NpcSpawn(NpcId.WOMAN_2_3083, new Tile(3216, 3206), 4));
    spawns.add(new NpcSpawn(NpcId.PERDU, new Tile(3230, 3215), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.GUIDE, new Tile(3238, 3220), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.MAN_2_3079, new Tile(3234, 3221), 4));
    spawns.add(new NpcSpawn(NpcId.HATIUS_COSAINTUS, new Tile(3234, 3216), 4));
    spawns.add(new NpcSpawn(NpcId.MAN_2_3078, new Tile(3231, 3208), 4));
    spawns.add(new NpcSpawn(NpcId.BOB, new Tile(3230, 3203), 4));
    spawns.add(new NpcSpawn(NpcId.MAN_2_3080, new Tile(3235, 3202), 4));
    spawns.add(new NpcSpawn(NpcId.WOMAN_2_3083, new Tile(3236, 3205), 4));
    spawns.add(new NpcSpawn(NpcId.WOMAN_2_3084, new Tile(3235, 3208), 4));
    spawns.add(new NpcSpawn(NpcId.FATHER_AERECK, new Tile(3243, 3210), 4));
    spawns.add(new NpcSpawn(NpcId.COUNT_CHECK, new Tile(3238, 3199), Tile.Direction.EAST));
    spawns.add(new NpcSpawn(NpcId.DOOMSAYER, new Tile(3232, 3232)));
    spawns.add(new NpcSpawn(NpcId.MAN_2_3078, new Tile(3227, 3240), 4));
    spawns.add(new NpcSpawn(NpcId.MAN_2_3079, new Tile(3230, 3237), 4));
    spawns.add(new NpcSpawn(NpcId.WOMAN_2_3085, new Tile(3231, 3239), 4));
    spawns.add(new NpcSpawn(NpcId.IMP_7, new Tile(3226, 3235), 8));
    spawns.add(new NpcSpawn(NpcId.GEE, new Tile(3217, 3237), 4));

    return spawns;
  }
}
