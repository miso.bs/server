package com.palidinodh.osrsscript.map.area.trollcountry.godwars;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 11578, 11579 })
public class GodWarsDungeonEntranceArea extends Area {
}
