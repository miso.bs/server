package com.palidinodh.osrsscript.map.area.kandarin.krakencove;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(9116)
public class KrakenCoveArea extends Area {
}
