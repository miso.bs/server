package com.palidinodh.osrsscript.map.area.karamja.viyeldicaves.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class ViyeldiCavesNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.DEATH_WING_83, new Tile(2810, 9309), 4));
    spawns.add(new NpcSpawn(NpcId.DEATH_WING_83, new Tile(2812, 9299), 4));
    spawns.add(new NpcSpawn(NpcId.DEATH_WING_83, new Tile(2800, 9307), 4));
    spawns.add(new NpcSpawn(NpcId.DEATH_WING_83, new Tile(2800, 9301), 4));
    spawns.add(new NpcSpawn(NpcId.DEATH_WING_83, new Tile(2803, 9295), 4));
    spawns.add(new NpcSpawn(NpcId.DEATH_WING_83, new Tile(2810, 9294), 4));
    spawns.add(new NpcSpawn(NpcId.DEATH_WING_83, new Tile(2806, 9292), 4));
    spawns.add(new NpcSpawn(NpcId.UNGADULU_70, new Tile(2792, 9327), 2));
    spawns.add(new NpcSpawn(NpcId.SAN_TOJALON_106, new Tile(2393, 4712), 4));
    spawns.add(new NpcSpawn(NpcId.SAN_TOJALON_106, new Tile(2385, 4706), 4));
    spawns.add(new NpcSpawn(NpcId.SAN_TOJALON_106, new Tile(2403, 4711), 4));
    spawns.add(new NpcSpawn(NpcId.IRVIG_SENAY_100, new Tile(2406, 4703), 4));
    spawns.add(new NpcSpawn(NpcId.IRVIG_SENAY_100, new Tile(2416, 4703), 4));
    spawns.add(new NpcSpawn(NpcId.IRVIG_SENAY_100, new Tile(2413, 4710), 4));
    spawns.add(new NpcSpawn(NpcId.RANALPH_DEVERE_92, new Tile(2423, 4705), 4));
    spawns.add(new NpcSpawn(NpcId.RANALPH_DEVERE_92, new Tile(2421, 4713), 4));
    spawns.add(new NpcSpawn(NpcId.RANALPH_DEVERE_92, new Tile(2417, 4723), 4));
    spawns.add(new NpcSpawn(NpcId.LESSER_DEMON_82, new Tile(2419, 4681), 4));
    spawns.add(new NpcSpawn(NpcId.LESSER_DEMON_82, new Tile(2409, 4682), 4));
    spawns.add(new NpcSpawn(NpcId.LESSER_DEMON_82, new Tile(2412, 4677), 4));
    spawns.add(new NpcSpawn(NpcId.BOULDER_3967, new Tile(2391, 4677)));

    return spawns;
  }
}
