package com.palidinodh.osrsscript.map.area.kharidiandesert.alkharid.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class AlKharidNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.WIZARD_4399, new Tile(3272, 3170), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.GADRIN, new Tile(3272, 3164), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.MILES, new Tile(3287, 3178), Tile.Direction.EAST));
    spawns.add(new NpcSpawn(NpcId.AL_KHARID_WARRIOR_9, new Tile(3288, 3176), 4));
    spawns.add(new NpcSpawn(NpcId.AL_KHARID_WARRIOR_9, new Tile(3297, 3176), 4));
    spawns.add(new NpcSpawn(NpcId.AL_KHARID_WARRIOR_9, new Tile(3297, 3172), 4));
    spawns.add(new NpcSpawn(NpcId.AL_KHARID_WARRIOR_9, new Tile(3297, 3168), 4));
    spawns.add(new NpcSpawn(NpcId.AL_KHARID_WARRIOR_9, new Tile(3288, 3168), 4));
    spawns.add(new NpcSpawn(NpcId.AL_KHARID_WARRIOR_9, new Tile(3288, 3172), 4));
    spawns.add(new NpcSpawn(NpcId.AL_KHARID_WARRIOR_9, new Tile(3293, 3168), 4));
    spawns.add(new NpcSpawn(NpcId.AL_KHARID_WARRIOR_9, new Tile(3292, 3176), 4));
    spawns.add(new NpcSpawn(NpcId.AL_KHARID_WARRIOR_9, new Tile(3284, 3173), 4));
    spawns.add(new NpcSpawn(NpcId.AL_KHARID_WARRIOR_9, new Tile(3285, 3169), 4));
    spawns.add(new NpcSpawn(NpcId.AL_KHARID_WARRIOR_9, new Tile(3300, 3175), 4));
    spawns.add(new NpcSpawn(NpcId.AL_KHARID_WARRIOR_9, new Tile(3301, 3170), 4));
    spawns.add(new NpcSpawn(NpcId.AL_KHARID_WARRIOR_9, new Tile(3286, 3161), 4));
    spawns.add(new NpcSpawn(NpcId.AL_KHARID_WARRIOR_9, new Tile(3290, 3163), 4));
    spawns.add(new NpcSpawn(NpcId.AL_KHARID_WARRIOR_9, new Tile(3297, 3162), 4));
    spawns.add(new NpcSpawn(NpcId.AL_KHARID_WARRIOR_9, new Tile(3301, 3164), 4));
    spawns.add(new NpcSpawn(NpcId.ZEKE, new Tile(3288, 3190), 4));
    spawns.add(new NpcSpawn(NpcId.MAN_2_3078, new Tile(3294, 3194), 4));
    spawns.add(new NpcSpawn(NpcId.MAN_2_3078, new Tile(3303, 3199), 4));
    spawns.add(new NpcSpawn(NpcId.MAN_2_3078, new Tile(3298, 3200), 4));
    spawns.add(new NpcSpawn(NpcId.SILK_TRADER, new Tile(3300, 3203)));
    spawns.add(new NpcSpawn(NpcId.ELLY_THE_CAMEL, new Tile(3308, 3210), 4));
    spawns.add(new NpcSpawn(NpcId.AYESHA, new Tile(3315, 3205), 4));
    spawns.add(new NpcSpawn(NpcId.DOMMIK, new Tile(3321, 3193), 4));
    spawns.add(new NpcSpawn(NpcId.LOUIE_LEGS, new Tile(3316, 3175), 4));
    spawns.add(new NpcSpawn(NpcId.SHOP_KEEPER, new Tile(3315, 3180), 4));
    spawns.add(new NpcSpawn(NpcId.RANAEL, new Tile(3314, 3162), 4));
    spawns.add(new NpcSpawn(NpcId.ALI_MORRISANE, new Tile(3304, 3211), Tile.Direction.EAST));
    spawns.add(new NpcSpawn(NpcId.CAM_THE_CAMEL, new Tile(3298, 3227), 4));
    spawns.add(new NpcSpawn(NpcId.OLLIE_THE_CAMEL, new Tile(3290, 3208), 4));
    spawns.add(new NpcSpawn(NpcId.GEM_TRADER, new Tile(3289, 3211), 4));
    spawns.add(new NpcSpawn(NpcId.CAPTAIN_DALBUR, new Tile(3284, 3212)));
    spawns.add(new NpcSpawn(NpcId.MAN_2_3078, new Tile(3287, 3204), 4));
    spawns.add(new NpcSpawn(NpcId.CAMEL, new Tile(3285, 3198), 4));
    spawns.add(new NpcSpawn(NpcId.MAN_2_3078, new Tile(3282, 3192), 4));
    spawns.add(new NpcSpawn(NpcId.MAN_2_3078, new Tile(3274, 3192), 4));
    spawns.add(new NpcSpawn(NpcId.ELLIS, new Tile(3273, 3190), 4));
    spawns.add(new NpcSpawn(NpcId.KARIM, new Tile(3273, 3180), 4));
    spawns.add(new NpcSpawn(NpcId.AL_THE_CAMEL, new Tile(3277, 3165), 4));
    spawns.add(new NpcSpawn(NpcId.PING_839, new Tile(3272, 3159), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPION_14, new Tile(3277, 3143), 4));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(3267, 3164), Tile.Direction.EAST));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(3267, 3167), Tile.Direction.EAST));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(3267, 3169), Tile.Direction.EAST));
    spawns.add(new NpcSpawn(NpcId.BANKER_395, new Tile(3267, 3166), Tile.Direction.EAST));
    spawns.add(new NpcSpawn(NpcId.BANKER_395, new Tile(3267, 3168), Tile.Direction.EAST));

    return spawns;
  }
}
