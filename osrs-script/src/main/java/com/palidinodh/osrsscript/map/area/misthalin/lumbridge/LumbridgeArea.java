package com.palidinodh.osrsscript.map.area.misthalin.lumbridge;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.reference.ReferenceIdSet;

@ReferenceId({ 12594, 12850, 12851 })
@ReferenceIdSet(primary = 12849, secondary = { 55, 71, 86, 87, 102, 103 })
public class LumbridgeArea extends Area {
}
