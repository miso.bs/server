package com.palidinodh.osrsscript.map.area.misthalin.edgeville.handler.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.ADAM)
class AdamNpc implements NpcHandler {
  @Override
  public void npcOption(Player player, int option, Npc npc) {
    if (option == 0) {
      if (player.isGameModeIronman() || player.isGameModeGroupIronman()) {
        player.openDialogue("ironadam", 0);
      } else {
        player.getGameEncoder().sendMessage("Adam has no reason to talk to you.");
      }
    } else if (option == 2) {
      if (player.isGameModeIronman() || player.isGameModeGroupIronman()) {
        player.openShop("ironman");
      } else if (player.isGameModeHardcoreIronman()) {
        player.openShop("hardcore_ironman");
      } else {
        player.getGameEncoder().sendMessage("Adam has no reason to trade you.");
      }
    }
  }
}
