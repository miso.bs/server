package com.palidinodh.osrsscript.map.area.karamja.brimhaven.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class BrimhavenNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.GARTH, new Tile(2766, 3215), 2));
    spawns.add(new NpcSpawn(NpcId.TOOL_LEPRECHAUN, new Tile(2764, 3210), Tile.Direction.NORTH));

    return spawns;
  }
}
