package com.palidinodh.osrsscript.map.area.asgarnia.portsarim;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 11825, 12081, 12082 })
public class PortSarimArea extends Area {
}
