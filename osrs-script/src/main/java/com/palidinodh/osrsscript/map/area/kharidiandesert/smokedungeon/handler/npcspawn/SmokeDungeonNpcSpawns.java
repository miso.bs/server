package com.palidinodh.osrsscript.map.area.kharidiandesert.smokedungeon.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class SmokeDungeonNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.FIRE_ELEMENTAL_35, new Tile(3210, 9378), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_ELEMENTAL_35, new Tile(3206, 9368), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_ELEMENTAL_35, new Tile(3212, 9362), 4));
    spawns.add(new NpcSpawn(NpcId.DUST_DEVIL_93, new Tile(3218, 9365), 4));
    spawns.add(new NpcSpawn(NpcId.DUST_DEVIL_93, new Tile(3215, 9357), 4));
    spawns.add(new NpcSpawn(NpcId.DUST_DEVIL_93, new Tile(3213, 9351), 4));
    spawns.add(new NpcSpawn(NpcId.DUST_DEVIL_93, new Tile(3221, 9347), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_ELEMENTAL_35, new Tile(3226, 9353), 4));
    spawns.add(new NpcSpawn(NpcId.DUST_DEVIL_93, new Tile(3234, 9356), 4));
    spawns.add(new NpcSpawn(NpcId.DUST_DEVIL_93, new Tile(3235, 9362), 4));
    spawns.add(new NpcSpawn(NpcId.DUST_DEVIL_93, new Tile(3230, 9367), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(3235, 9368), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(3245, 9370), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_GIANT_86, new Tile(3251, 9369), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_GIANT_86, new Tile(3243, 9360), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_GIANT_86, new Tile(3252, 9358), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(3250, 9364), 4));
    spawns.add(new NpcSpawn(NpcId.DUST_DEVIL_93, new Tile(3239, 9381), 4));
    spawns.add(new NpcSpawn(NpcId.DUST_DEVIL_93, new Tile(3241, 9386), 4));
    spawns.add(new NpcSpawn(NpcId.DUST_DEVIL_93, new Tile(3237, 9390), 4));
    spawns.add(new NpcSpawn(NpcId.DUST_DEVIL_93, new Tile(3225, 9377), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_ELEMENTAL_35, new Tile(3221, 9381), 4));
    spawns.add(new NpcSpawn(NpcId.DUST_DEVIL_93, new Tile(3229, 9394), 4));
    spawns.add(new NpcSpawn(NpcId.DUST_DEVIL_93, new Tile(3233, 9400), 4));
    spawns.add(new NpcSpawn(NpcId.DUST_DEVIL_93, new Tile(3223, 9398), 4));
    spawns.add(new NpcSpawn(NpcId.DUST_DEVIL_93, new Tile(3216, 9400), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_ELEMENTAL_35, new Tile(3208, 9400), 4));
    spawns.add(new NpcSpawn(NpcId.DUST_DEVIL_93, new Tile(3247, 9400), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(3258, 9399), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_ELEMENTAL_35, new Tile(3262, 9399), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_ELEMENTAL_35, new Tile(3257, 9392), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(3258, 9385), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(3260, 9379), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(3259, 9371), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(3261, 9363), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(3259, 9356), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(3258, 9350), 4));
    spawns.add(new NpcSpawn(NpcId.DUST_DEVIL_93, new Tile(3248, 9349), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(3238, 9349), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(3274, 9397), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_ELEMENTAL_35, new Tile(3271, 9386), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(3272, 9379), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(3286, 9384), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(3287, 9392), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(3286, 9398), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_ELEMENTAL_35, new Tile(3283, 9400), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(3280, 9369), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(3272, 9364), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(3273, 9356), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_ELEMENTAL_35, new Tile(3279, 9352), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(3285, 9357), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(3291, 9364), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_GIANT_86, new Tile(3294, 9374), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(3301, 9389), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_GIANT_86, new Tile(3302, 9398), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_ELEMENTAL_35, new Tile(3308, 9401), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(3314, 9400), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_GIANT_86, new Tile(3300, 9353), 4));
    spawns.add(new NpcSpawn(NpcId.PYREFIEND_43, new Tile(3311, 9351), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_ELEMENTAL_35, new Tile(3318, 9352), 4));

    return spawns;
  }
}
