package com.palidinodh.osrsscript.map.area.zeah.chasmoffire.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class ChasmOfFireNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.LESSER_DEMON_82, new Tile(1435, 10080, 3), 4));
    spawns.add(new NpcSpawn(NpcId.LESSER_DEMON_82, new Tile(1438, 10082, 3), 4));
    spawns.add(new NpcSpawn(NpcId.LESSER_DEMON_82, new Tile(1435, 10085, 3), 4));
    spawns.add(new NpcSpawn(NpcId.LESSER_DEMON_82, new Tile(1440, 10086, 3), 4));
    spawns.add(new NpcSpawn(NpcId.GREATER_DEMON_92, new Tile(1433, 10091, 2), 4));
    spawns.add(new NpcSpawn(NpcId.GREATER_DEMON_92, new Tile(1438, 10088, 2), 4));
    spawns.add(new NpcSpawn(NpcId.GREATER_DEMON_92, new Tile(1443, 10093, 2), 4));
    spawns.add(new NpcSpawn(NpcId.GREATER_DEMON_92, new Tile(1431, 10078, 2), 4));
    spawns.add(new NpcSpawn(NpcId.GREATER_DEMON_92, new Tile(1427, 10082, 2), 4));
    spawns.add(new NpcSpawn(NpcId.GREATER_DEMON_92, new Tile(1427, 10077, 2), 4));
    spawns.add(new NpcSpawn(NpcId.GREATER_DEMON_92, new Tile(1428, 10073, 2), 4));
    spawns.add(new NpcSpawn(NpcId.GREATER_DEMON_92, new Tile(1420, 10087, 2), 4));
    spawns.add(new NpcSpawn(NpcId.GREATER_DEMON_92, new Tile(1431, 10060, 2), 4));
    spawns.add(new NpcSpawn(NpcId.GREATER_DEMON_92, new Tile(1438, 10061, 2), 4));
    spawns.add(new NpcSpawn(NpcId.GREATER_DEMON_92, new Tile(1441, 10075, 2), 4));
    spawns.add(new NpcSpawn(NpcId.GREATER_DEMON_92, new Tile(1445, 10078, 2), 4));
    spawns.add(new NpcSpawn(NpcId.GREATER_DEMON_92, new Tile(1449, 10072, 2), 4));
    spawns.add(new NpcSpawn(NpcId.GREATER_DEMON_92, new Tile(1450, 10081, 2), 4));
    spawns.add(new NpcSpawn(NpcId.GREATER_DEMON_92, new Tile(1447, 10086, 2), 4));
    spawns.add(new NpcSpawn(NpcId.GREATER_DEMON_92, new Tile(1453, 10090, 2), 4));
    spawns.add(new NpcSpawn(NpcId.GREATER_DEMON_92, new Tile(1450, 10091, 2), 4));
    spawns.add(new NpcSpawn(NpcId.LESSER_DEMON_82, new Tile(1440, 10073, 3), 4));
    spawns.add(new NpcSpawn(NpcId.LESSER_DEMON_82, new Tile(1442, 10076, 3), 4));
    spawns.add(new NpcSpawn(NpcId.LESSER_DEMON_82, new Tile(1445, 10075, 3), 4));
    spawns.add(new NpcSpawn(NpcId.LESSER_DEMON_82, new Tile(1444, 10079, 3), 4));
    spawns.add(new NpcSpawn(NpcId.LESSER_DEMON_82, new Tile(1446, 10082, 3), 4));
    spawns.add(new NpcSpawn(NpcId.BLACK_DEMON_172, new Tile(1448, 10061, 1), 4));
    spawns.add(new NpcSpawn(NpcId.BLACK_DEMON_172, new Tile(1442, 10063, 1), 4));
    spawns.add(new NpcSpawn(NpcId.BLACK_DEMON_172, new Tile(1447, 10067, 1), 4));
    spawns.add(new NpcSpawn(NpcId.BLACK_DEMON_172, new Tile(1436, 10068, 1), 4));
    spawns.add(new NpcSpawn(NpcId.BLACK_DEMON_172, new Tile(1428, 10065, 1), 4));
    spawns.add(new NpcSpawn(NpcId.BLACK_DEMON_172, new Tile(1422, 10062, 1), 4));
    spawns.add(new NpcSpawn(NpcId.BLACK_DEMON_172, new Tile(1420, 10071, 1), 4));
    spawns.add(new NpcSpawn(NpcId.BLACK_DEMON_172, new Tile(1426, 10075, 1), 4));
    spawns.add(new NpcSpawn(NpcId.BLACK_DEMON_172, new Tile(1427, 10082, 1), 4));
    spawns.add(new NpcSpawn(NpcId.BLACK_DEMON_172, new Tile(1423, 10091, 1), 4));
    spawns.add(new NpcSpawn(NpcId.BLACK_DEMON_172, new Tile(1432, 10088, 1), 4));
    spawns.add(new NpcSpawn(NpcId.BLACK_DEMON_172, new Tile(1429, 10096, 1), 4));
    spawns.add(new NpcSpawn(NpcId.BLACK_DEMON_172, new Tile(1443, 10086, 1), 4));
    spawns.add(new NpcSpawn(NpcId.BLACK_DEMON_172, new Tile(1449, 10094, 1), 4));
    spawns.add(new NpcSpawn(NpcId.BLACK_DEMON_172, new Tile(1452, 10083, 1), 4));
    spawns.add(new NpcSpawn(NpcId.BLACK_DEMON_172, new Tile(1447, 10077, 1), 4));
    spawns.add(new NpcSpawn(NpcId.BLACK_DEMON_172, new Tile(1452, 10073, 1), 4));

    return spawns;
  }
}
