package com.palidinodh.osrsscript.map.area.fremennikprovince;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(11064)
public class GoldenAppleTreeArea extends Area {
}
