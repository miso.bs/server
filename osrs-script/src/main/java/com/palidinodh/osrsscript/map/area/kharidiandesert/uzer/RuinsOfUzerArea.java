package com.palidinodh.osrsscript.map.area.kharidiandesert.uzer;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.reference.ReferenceIdSet;

@ReferenceId(13872)
@ReferenceIdSet(primary = 13873, secondary = { 0, 1, 16, 17, 32 })
public class RuinsOfUzerArea extends Area {
}
