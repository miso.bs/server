package com.palidinodh.osrsscript.map.area.karamja;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.reference.ReferenceIdSet;

@ReferenceId({ 11313, 11569 })
@ReferenceIdSet(primary = 11825,
    secondary = { 0, 1, 2, 3, 4, 16, 17, 18, 19, 32, 33, 34, 35, 48, 49, 50, 51 })
@ReferenceIdSet(primary = 11314, secondary = { 16, 32, 48, 64, 80 })
public class MusaPointArea extends Area {
}
