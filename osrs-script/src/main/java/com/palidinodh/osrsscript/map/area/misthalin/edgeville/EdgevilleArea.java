package com.palidinodh.osrsscript.map.area.misthalin.edgeville;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.osrsscript.world.event.pvptournament.PvpTournamentEvent;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.reference.ReferenceIdSet;
import lombok.var;

@ReferenceId(12342)
@ReferenceIdSet(primary = 12086, secondary = { 119 })
public class EdgevilleArea extends Area {
  @Override
  public void tickPlayer() {
    var player = getPlayer();
    var tournament = player.getWorld().getWorldEvent(PvpTournamentEvent.class);
    tournament.sendWidgetText(player);
  }

  @Override
  public boolean inLoadoutZone() {
    if (getTile().getX() > 3100) {
      return false;
    }
    return !inWilderness();
  }
}
