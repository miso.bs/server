package com.palidinodh.osrsscript.map.area.kandarin.grandtreetunnels.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class GrandTreeTunnelsNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(2447, 9909), Tile.Direction.EAST));
    spawns.add(new NpcSpawn(NpcId.BANKER_395, new Tile(2479, 9918)));

    return spawns;
  }
}
