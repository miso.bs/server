package com.palidinodh.osrsscript.map.area.misthalin.edgeville.handler.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.VOTE_MANAGER)
class VoteManagerNpc implements NpcHandler {
  @Override
  public void npcOption(Player player, int option, Npc npc) {
    if (option == 0) {
      player.openDialogue("vote", 0);
    } else if (option == 3) {
      player.openShop("vote");
    }
  }
}
