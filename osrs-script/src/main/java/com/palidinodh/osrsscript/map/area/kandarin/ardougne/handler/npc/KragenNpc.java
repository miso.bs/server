package com.palidinodh.osrsscript.map.area.kandarin.ardougne.handler.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.KRAGEN)
class KragenNpc implements NpcHandler {
  @Override
  public void npcOption(Player player, int option, Npc npc) {
    if (option == 0) {
      player.getGameEncoder().sendMessage("This gardener will protect your patches for a fee.");
    } else {
      player.getFarming().gardenerProtection(npc, option - 2);
    }
  }
}
