package com.palidinodh.osrsscript.map.area.feldiphills.ogreenclave.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class OgreEnclaveNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.BLUE_DRAGON_111, new Tile(2592, 9461), 4));
    spawns.add(new NpcSpawn(NpcId.BLUE_DRAGON_111, new Tile(2607, 9459), 4));
    spawns.add(new NpcSpawn(NpcId.BLUE_DRAGON_111, new Tile(2607, 9434), 4));
    spawns.add(new NpcSpawn(NpcId.BLUE_DRAGON_111, new Tile(2594, 9431), 4));
    spawns.add(new NpcSpawn(NpcId.BLUE_DRAGON_111, new Tile(2587, 9424), 4));
    spawns.add(new NpcSpawn(NpcId.BLUE_DRAGON_111, new Tile(2581, 9435), 4));
    spawns.add(new NpcSpawn(NpcId.BLUE_DRAGON_111, new Tile(2569, 9435), 4));
    spawns.add(new NpcSpawn(NpcId.BLUE_DRAGON_111, new Tile(2572, 9447), 4));
    spawns.add(new NpcSpawn(NpcId.BLUE_DRAGON_111, new Tile(2582, 9459), 4));
    spawns.add(new NpcSpawn(NpcId.BLUE_DRAGON_111, new Tile(2613, 9444), 4));
    spawns.add(new NpcSpawn(NpcId.BLUE_DRAGON_111, new Tile(2614, 9421), 4));

    return spawns;
  }
}
