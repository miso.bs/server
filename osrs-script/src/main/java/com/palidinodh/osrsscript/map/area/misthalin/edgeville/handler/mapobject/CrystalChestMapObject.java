package com.palidinodh.osrsscript.map.area.misthalin.edgeville.handler.mapobject;

import com.palidinodh.osrscore.io.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrsscript.map.area.asgarnia.taverley.TaverleyCrystalChest;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.CLOSED_CHEST_172)
class CrystalChestMapObject implements MapObjectHandler {
  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    TaverleyCrystalChest.open(player, mapObject);
  }
}
