package com.palidinodh.osrsscript.map.area.voidknightsoutpost.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class VoidKnightsOutpostNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.VOID_KNIGHT, new Tile(2661, 2648), 4));
    spawns.add(new NpcSpawn(NpcId.SQUIRE_NOVICE, new Tile(2657, 2637), Tile.Direction.NORTH));
    spawns.add(new NpcSpawn(NpcId.SQUIRE_1764, new Tile(2668, 2651), 1));
    spawns.add(new NpcSpawn(NpcId.SQUIRE_1765, new Tile(2664, 2660), 1));
    spawns.add(new NpcSpawn(NpcId.SQUIRE_1767, new Tile(2658, 2654), 1));
    spawns.add(new NpcSpawn(NpcId.SQUIRE_1766, new Tile(2651, 2659), 1));
    spawns.add(new NpcSpawn(NpcId.SQUIRE_1768, new Tile(2651, 2664), 1));
    spawns.add(new NpcSpawn(NpcId.SQUIRE_1770, new Tile(2659, 2673), 4));
    spawns.add(new NpcSpawn(NpcId.SQUIRE, new Tile(2657, 2663), 4));
    spawns.add(new NpcSpawn(NpcId.SQUIRE_1761, new Tile(2660, 2658), 4));

    return spawns;
  }
}
