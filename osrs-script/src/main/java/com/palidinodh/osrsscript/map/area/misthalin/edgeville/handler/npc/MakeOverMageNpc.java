package com.palidinodh.osrsscript.map.area.misthalin.edgeville.handler.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.MAKE_OVER_MAGE)
class MageOverMageNpc implements NpcHandler {
  @Override
  public void npcOption(Player player, int option, Npc npc) {
    player.getWidgetManager().sendInteractiveOverlay(WidgetId.CHARACTER_DESIGN);
  }
}
