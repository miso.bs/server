package com.palidinodh.osrsscript.map.area.karamja.shilovillage;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(11310)
public class ShiloVillageArea extends Area {
}
