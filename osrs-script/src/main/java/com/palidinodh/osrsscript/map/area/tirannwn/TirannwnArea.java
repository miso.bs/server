package com.palidinodh.osrsscript.map.area.tirannwn;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 8498, 9009, 9010 })
public class TirannwnArea extends Area {
}
