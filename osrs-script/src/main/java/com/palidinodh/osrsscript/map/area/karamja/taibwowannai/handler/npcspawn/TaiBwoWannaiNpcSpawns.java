package com.palidinodh.osrsscript.map.area.karamja.taibwowannai.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class TaiBwoWannaiNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.TOOL_LEPRECHAUN, new Tile(2799, 3101), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.IMIAGO, new Tile(2799, 3097), 2));

    return spawns;
  }
}
