package com.palidinodh.osrsscript.map.area.asgarnia.icequeenslair.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class IceQueensLairNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57, new Tile(2841, 9913), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57, new Tile(2836, 9905), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57, new Tile(2834, 9920), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57, new Tile(2833, 9934), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57, new Tile(2834, 9946), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57, new Tile(2839, 9961), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57, new Tile(2825, 9900), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57, new Tile(2820, 9916), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57, new Tile(2819, 9926), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57, new Tile(2819, 9937), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_SPIDER_61, new Tile(2827, 9933), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_SPIDER_61, new Tile(2826, 9951), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_SPIDER_61, new Tile(2858, 9971), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_SPIDER_61, new Tile(2873, 9971), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_GIANT_53, new Tile(2883, 9964), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_GIANT_53, new Tile(2884, 9959), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_GIANT_53, new Tile(2887, 9954), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_GIANT_53, new Tile(2891, 9949), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_GIANT_53, new Tile(2891, 9943), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_GIANT_53, new Tile(2885, 9935), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_GIANT_53, new Tile(2880, 9927), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_SPIDER_61, new Tile(2860, 9916), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_QUEEN_111, new Tile(2866, 9955), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57_2851, new Tile(2863, 9952), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57_2851, new Tile(2868, 9952), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57_2851, new Tile(2866, 9949), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57_2851, new Tile(2863, 9957), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57_2851, new Tile(2868, 9957), 4));
    spawns.add(new NpcSpawn(NpcId.ICE_WARRIOR_57_2851, new Tile(2865, 9962), 4));

    return spawns;
  }
}
