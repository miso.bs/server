package com.palidinodh.osrsscript.map.area.morytania.slayertower.handler.mapobject;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerUnlock;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrsscript.player.plugin.boss.BossPlugin;
import com.palidinodh.osrsscript.player.plugin.slayer.SlayerPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.setting.Settings;
import lombok.var;

@ReferenceId(ObjectId.ROOF_ENTRANCE_31681)
class RoofEntranceMapObject implements MapObjectHandler {
  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    var plugin = player.getPlugin(SlayerPlugin.class);
    if (!Settings.getInstance().isBeta() && !Settings.getInstance().isSpawn()
        && !plugin.isUnlocked(SlayerUnlock.GROTESQUE_GUARDIANS)) {
      if (!player.getInventory().hasItem(ItemId.BRITTLE_KEY)) {
        player.getGameEncoder().sendMessage("You need a brittle key to unlock this.");
        return;
      }
      player.getInventory().deleteItem(ItemId.BRITTLE_KEY);
      plugin.unlock(SlayerUnlock.GROTESQUE_GUARDIANS);
      return;
    }
    if (!player.getSkills().isAnySlayerTask(NpcId.DUSK_248) && !Settings.getInstance().isLocal()
        && !Settings.getInstance().isBeta() && !Settings.getInstance().isSpawn()) {
      player.getGameEncoder().sendMessage("You need an appropriate task to enter.");
      return;
    }
    player.getPlugin(BossPlugin.class).start(NpcId.DUSK_248, true);
  }
}
