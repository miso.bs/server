package com.palidinodh.osrsscript.map.area.kandarin.krakencove.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class KrakenCoveNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.WHIRLPOOL, new Tile(2278, 10035)));
    spawns.add(new NpcSpawn(NpcId.WHIRLPOOL_127, new Tile(2245, 10013), 2));
    spawns.add(new NpcSpawn(NpcId.WHIRLPOOL_127, new Tile(2245, 10026), 2));
    spawns.add(new NpcSpawn(NpcId.WHIRLPOOL_127, new Tile(2246, 10019), 2));
    spawns.add(new NpcSpawn(NpcId.WHIRLPOOL_127, new Tile(2266, 10013), 2));
    spawns.add(new NpcSpawn(NpcId.WHIRLPOOL_127, new Tile(2261, 10016), 2));
    spawns.add(new NpcSpawn(NpcId.WHIRLPOOL_127, new Tile(2259, 10020), 2));
    spawns.add(new NpcSpawn(NpcId.WHIRLPOOL_127, new Tile(2255, 10024), 2));
    spawns.add(new NpcSpawn(NpcId.WHIRLPOOL_127, new Tile(2255, 10029), 2));
    spawns.add(new NpcSpawn(NpcId.WHIRLPOOL_127, new Tile(2254, 10035), 2));
    spawns.add(new NpcSpawn(NpcId.WHIRLPOOL_127, new Tile(2257, 10039), 2));
    spawns.add(new NpcSpawn(NpcId.WHIRLPOOL_127, new Tile(2264, 10003), 2));
    spawns.add(new NpcSpawn(NpcId.WHIRLPOOL_127, new Tile(2259, 9996), 2));
    spawns.add(new NpcSpawn(NpcId.WHIRLPOOL_127, new Tile(2270, 9990), 2));
    spawns.add(new NpcSpawn(NpcId.WHIRLPOOL_127, new Tile(2266, 9992), 2));
    spawns.add(new NpcSpawn(NpcId.WHIRLPOOL_127, new Tile(2269, 9994), 2));
    spawns.add(new NpcSpawn(NpcId.WHIRLPOOL_127, new Tile(2275, 10009), 2));
    spawns.add(new NpcSpawn(NpcId.WHIRLPOOL_127, new Tile(2279, 10010), 2));
    spawns.add(new NpcSpawn(NpcId.WHIRLPOOL_127, new Tile(2285, 10005), 2));
    spawns.add(new NpcSpawn(NpcId.WHIRLPOOL_127, new Tile(2293, 9991), 2));
    spawns.add(new NpcSpawn(NpcId.WHIRLPOOL_127, new Tile(2297, 9990), 2));
    spawns.add(new NpcSpawn(NpcId.WHIRLPOOL_127, new Tile(2297, 9998), 2));
    spawns.add(new NpcSpawn(NpcId.WHIRLPOOL_127, new Tile(2296, 10002), 2));
    spawns.add(new NpcSpawn(NpcId.WHIRLPOOL_127, new Tile(2299, 10011), 2));
    spawns.add(new NpcSpawn(NpcId.WHIRLPOOL_127, new Tile(2298, 10017), 2));
    spawns.add(new NpcSpawn(NpcId.WHIRLPOOL_127, new Tile(2291, 10016), 2));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(2288, 9988), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(2286, 9989), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(2286, 9987), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(2281, 9988), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(2281, 9990), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(2264, 9988), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(2261, 9987), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(2259, 9990), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(2258, 9988), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(2255, 9989), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(2249, 9991), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(2251, 9993), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(2248, 9994), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(2250, 9995), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(2248, 9997), 4));

    return spawns;
  }
}
