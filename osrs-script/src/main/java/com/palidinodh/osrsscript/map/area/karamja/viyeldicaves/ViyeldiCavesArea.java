package com.palidinodh.osrsscript.map.area.karamja.viyeldicaves;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 9545, 11153 })
public class ViyeldiCavesArea extends Area {
}
