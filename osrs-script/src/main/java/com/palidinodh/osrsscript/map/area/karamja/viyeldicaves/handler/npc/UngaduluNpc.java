package com.palidinodh.osrsscript.map.area.karamja.viyeldicaves.handler.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(NpcId.UNGADULU_70)
class UngaduluNpc implements NpcHandler {
  @Override
  public void npcOption(Player player, int option, Npc npc) {
    if (player.getCombat().getLegendsQuest() == 0 && player.carryingItem(730)
        && player.getWorld().getTargetNpc(player, NpcId.NEZIKCHENED_187) == null) {
      player.getGameEncoder()
          .sendMessage("You open the book and a light starts emanating, illuminating Ungadulu.");
      player.getPrayer().changePoints(-99);
      var nezikchened = player.getController()
          .addNpc(new NpcSpawn(NpcId.NEZIKCHENED_187, new Tile(2792, 9328, player.getHeight())));
      nezikchened.setForceMessage("Your faith will help you little here.");
      nezikchened.getCombat().setTarget(player);
    } else if (player.getCombat().getLegendsQuest() == 2 && player.carryingItem(746)) {
      player.getInventory().deleteItem(746, 1);
      player.getInventory().addItem(748, 1);
      player.getMovement().teleport(2792, 9337, 0);
      player.getGameEncoder()
          .sendMessage("Ungadulu gives you a water purifying spell in exchange for the dagger.");
    } else {
      player.getGameEncoder().sendMessage("You try to speak to Ungadulu, but he ignores you.");
    }
  }
}
