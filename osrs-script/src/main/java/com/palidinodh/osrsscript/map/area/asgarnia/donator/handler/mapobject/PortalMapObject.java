package com.palidinodh.osrsscript.map.area.asgarnia.donator.handler.mapobject;

import com.palidinodh.osrscore.io.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ ObjectId.PORTAL_OF_CHAMPIONS, ObjectId.PORTAL_OF_LEGENDS, ObjectId.PORTAL_OF_HEROES,
    ObjectId.PORTAL_7315 })
class PortalMapObject implements MapObjectHandler {
  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    switch (mapObject.getId()) {
      case ObjectId.PORTAL_OF_CHAMPIONS:
        player.getGameEncoder()
            .sendMessage("Where should this teleport to? ID: " + mapObject.getId());
        break;
      case ObjectId.PORTAL_OF_LEGENDS:
        player.getGameEncoder()
            .sendMessage("Where should this teleport to? ID: " + mapObject.getId());
        break;
      case ObjectId.PORTAL_OF_HEROES:
        player.getGameEncoder()
            .sendMessage("Where should this teleport to? ID: " + mapObject.getId());
        break;
      case ObjectId.PORTAL_7315:
        player.getGameEncoder()
            .sendMessage("Where should this teleport to? ID: " + mapObject.getId());
        break;
    }
  }
}
