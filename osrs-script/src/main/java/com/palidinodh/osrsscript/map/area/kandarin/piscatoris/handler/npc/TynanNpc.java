package com.palidinodh.osrsscript.map.area.kandarin.piscatoris.handler.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.TYNAN)
class TynanNpc implements NpcHandler {
  @Override
  public void npcOption(Player player, int option, Npc npc) {
    player.openShop("skilling");
  }
}
