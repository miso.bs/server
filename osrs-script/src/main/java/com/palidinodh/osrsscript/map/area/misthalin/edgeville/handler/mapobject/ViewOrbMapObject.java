package com.palidinodh.osrsscript.map.area.misthalin.edgeville.handler.mapobject;

import com.palidinodh.osrscore.io.cache.id.ObjectId;
import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrsscript.player.plugin.clanwars.ClanWarsPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ObjectId.VIEWING_ORB_26743, ObjectId.VIEWING_ORB_26745 })
class ViewOrbMapObject implements MapObjectHandler {
  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    switch (mapObject.getId()) {
      case ObjectId.VIEWING_ORB_26743:
        var plugin = player.getPlugin(ClanWarsPlugin.class);
        plugin.teleportViewing(0);
        break;
      case ObjectId.VIEWING_ORB_26745:
        player.openDialogue(new OptionsDialogue("Which arena would you like to view?",
            DialogueOption.toOptions("Arena 1 (No movement)", "Arena 2")).action((c, s) -> {
              player.getWidgetManager().sendInventoryOverlay(WidgetId.UNMORPH);
              player.getGameEncoder().sendWidgetText(WidgetId.UNMORPH, 5, "");
              if (s == 0) {
                player.getMovement().setViewing(3345, 3214, 0);
                player.getGameEncoder().sendWidgetText(WidgetId.UNMORPH, 4,
                    "Arena 1 (No movement)");
              } else if (s == 1) {
                player.getMovement().setViewing(3344, 3251, 0);
                player.getGameEncoder().sendWidgetText(WidgetId.UNMORPH, 4, "Arena 2");
              }
            }));
        break;
    }
  }
}
