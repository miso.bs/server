package com.palidinodh.osrsscript.map.area.karamja.crandor.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class CrandorDungeonNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.SKELETON_22, new Tile(2834, 9654), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETON_22, new Tile(2838, 9652), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETON_22, new Tile(2841, 9649), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETON_22, new Tile(2841, 9645), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETON_22, new Tile(2843, 9641), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETON_22, new Tile(2840, 9639), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETON_22, new Tile(2843, 9634), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETON_22, new Tile(2841, 9631), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETON_22, new Tile(2844, 9629), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETON_22, new Tile(2840, 9625), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETON_22, new Tile(2837, 9623), 4));
    spawns.add(new NpcSpawn(NpcId.LESSER_DEMON_82, new Tile(2839, 9613), 4));
    spawns.add(new NpcSpawn(NpcId.LESSER_DEMON_82, new Tile(2839, 9609), 4));
    spawns.add(new NpcSpawn(NpcId.LESSER_DEMON_82, new Tile(2842, 9608), 4));
    spawns.add(new NpcSpawn(NpcId.LESSER_DEMON_82, new Tile(2838, 9604), 4));
    spawns.add(new NpcSpawn(NpcId.LESSER_DEMON_82, new Tile(2835, 9601), 4));
    spawns.add(new NpcSpawn(NpcId.ELVARG_83_6349, new Tile(2852, 9636), 8));

    return spawns;
  }
}
