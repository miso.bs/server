package com.palidinodh.osrsscript.map.area.morytania.abandonedmine;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 13618, 10821, 10822, 10823, 11077, 11078, 11079, 13718 })
public class AbandonedMineArea extends Area {
}
