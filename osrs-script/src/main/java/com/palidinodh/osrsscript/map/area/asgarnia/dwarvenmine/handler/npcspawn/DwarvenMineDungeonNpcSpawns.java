package com.palidinodh.osrsscript.map.area.asgarnia.dwarvenmine.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class DwarvenMineDungeonNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.SCORPION_14, new Tile(3054, 9773), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPION_14, new Tile(3047, 9766), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPION_14, new Tile(3041, 9779), 4));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(3047, 9757), Tile.Direction.NORTH));
    spawns.add(new NpcSpawn(NpcId.BANKER_395, new Tile(3047, 9756)));
    spawns.add(new NpcSpawn(NpcId.BELONA, new Tile(3019, 9726), 4));

    return spawns;
  }
}
