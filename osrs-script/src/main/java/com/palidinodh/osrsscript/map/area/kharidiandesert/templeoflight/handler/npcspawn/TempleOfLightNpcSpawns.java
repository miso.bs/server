package com.palidinodh.osrsscript.map.area.kharidiandesert.templeoflight.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class TempleOfLightNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.DARK_BEAST_182, new Tile(1989, 4663), 4));
    spawns.add(new NpcSpawn(NpcId.DARK_BEAST_182, new Tile(1996, 4663), 4));
    spawns.add(new NpcSpawn(NpcId.DARK_BEAST_182, new Tile(1988, 4658), 4));
    spawns.add(new NpcSpawn(NpcId.DARK_BEAST_182, new Tile(1996, 4658), 4));
    spawns.add(new NpcSpawn(NpcId.DARK_BEAST_182, new Tile(1992, 4660), 4));
    spawns.add(new NpcSpawn(NpcId.DARK_BEAST_182, new Tile(1989, 4652), 4));
    spawns.add(new NpcSpawn(NpcId.DARK_BEAST_182, new Tile(1994, 4651), 4));
    spawns.add(new NpcSpawn(NpcId.DARK_BEAST_182, new Tile(2021, 4664), 4));
    spawns.add(new NpcSpawn(NpcId.DARK_BEAST_182, new Tile(2022, 4658), 4));
    spawns.add(new NpcSpawn(NpcId.DARK_BEAST_182, new Tile(2027, 4655), 4));
    spawns.add(new NpcSpawn(NpcId.DARK_BEAST_182, new Tile(2032, 4659), 4));
    spawns.add(new NpcSpawn(NpcId.DARK_BEAST_182, new Tile(2034, 4665), 4));
    spawns.add(new NpcSpawn(NpcId.DARK_BEAST_182, new Tile(2027, 4664), 4));
    spawns.add(new NpcSpawn(NpcId.DARK_BEAST_182, new Tile(2027, 4660), 4));
    spawns.add(new NpcSpawn(NpcId.DARK_BEAST_182, new Tile(2003, 4614), 4));
    spawns.add(new NpcSpawn(NpcId.DARK_BEAST_182, new Tile(2008, 4615), 4));
    spawns.add(new NpcSpawn(NpcId.DARK_BEAST_182, new Tile(2007, 4620), 4));
    spawns.add(new NpcSpawn(NpcId.DARK_BEAST_182, new Tile(1992, 4625), 4));
    spawns.add(new NpcSpawn(NpcId.DARK_BEAST_182, new Tile(1997, 4629), 4));
    spawns.add(new NpcSpawn(NpcId.DARK_BEAST_182, new Tile(1999, 4635), 4));

    return spawns;
  }
}
