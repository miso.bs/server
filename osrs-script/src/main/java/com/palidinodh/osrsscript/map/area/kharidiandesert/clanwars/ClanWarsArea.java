package com.palidinodh.osrsscript.map.area.kharidiandesert.clanwars;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 13361, 12621, 12622, 12623, 13133, 13134, 13135, 13391, 13641, 13642, 13643, 13644,
    13645, 13646, 13647, 13898, 13899, 13900, 14155, 14156 })
public class ClanWarsArea extends Area {
  @Override
  public boolean inMultiCombat() {
    return isEntity() ? getEntity().getController().isInstanced() : false;
  }
}
