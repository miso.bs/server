package com.palidinodh.osrsscript.map.area.zeah.woodcuttingguild;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 6198, 6454 })
public class WoodcuttingGuildArea extends Area {
}
