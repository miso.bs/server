package com.palidinodh.osrsscript.map.area.zeah.handler.mapobject;

import com.palidinodh.osrscore.io.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrsscript.player.skill.woodcutting.Woodcutting;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.SHRINE)
class ShrineMapObject implements MapObjectHandler {
  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    SkillContainer.getContainer(Woodcutting.class).checkShrine(player);
  }
}
