package com.palidinodh.osrsscript.map.area.wilderness.lavamazedungeon;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(12192)
public class LavaMazeDungeonArea extends Area {
  @Override
  public boolean inWilderness() {
    return true;
  }

  @Override
  public int getWildernessLevel() {
    return (getTile().getY() - 9920) / 8 + 1;
  }
}
