package com.palidinodh.osrsscript.map.area.kharidiandesert.smokedungeon;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 12946, 13202 })
public class SmokeDungeonArea extends Area {
}
