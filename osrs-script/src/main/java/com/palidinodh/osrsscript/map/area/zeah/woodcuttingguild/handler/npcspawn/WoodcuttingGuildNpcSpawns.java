package com.palidinodh.osrsscript.map.area.zeah.woodcuttingguild.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class WoodcuttingGuildNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.PERRY, new Tile(1651, 3499), 2));
    spawns.add(new NpcSpawn(NpcId.MURFET, new Tile(1607, 3508), 2));
    spawns.add(new NpcSpawn(NpcId.FORESTER_15_7238, new Tile(1624, 3515), 2));
    spawns.add(new NpcSpawn(NpcId.FORESTER_15_7238, new Tile(1615, 3489), 2));
    spawns.add(new NpcSpawn(NpcId.FORESTER_15_7238, new Tile(1593, 3490), 2));

    return spawns;
  }
}
