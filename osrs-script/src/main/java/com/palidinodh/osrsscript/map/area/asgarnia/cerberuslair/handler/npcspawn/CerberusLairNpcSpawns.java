package com.palidinodh.osrsscript.map.area.asgarnia.cerberuslair.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class CerberusLairNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.CERBERUS_318, new Tile(1238, 1250)));
    spawns.add(new NpcSpawn(NpcId.CERBERUS_318, new Tile(1366, 1250)));
    spawns.add(new NpcSpawn(NpcId.CERBERUS_318, new Tile(1302, 1314)));

    return spawns;
  }
}
