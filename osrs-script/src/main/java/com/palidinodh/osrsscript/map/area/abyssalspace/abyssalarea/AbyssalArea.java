package com.palidinodh.osrsscript.map.area.abyssalspace.abyssalarea;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(12108)
public class AbyssalArea extends Area {
}
