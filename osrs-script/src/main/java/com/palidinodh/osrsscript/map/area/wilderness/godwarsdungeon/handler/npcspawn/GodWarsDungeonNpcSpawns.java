package com.palidinodh.osrsscript.map.area.wilderness.godwarsdungeon.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class GodWarsDungeonNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.BOULDER_6621, new Tile(3053, 10165, 3)));
    spawns.add(new NpcSpawn(NpcId.SPIRITUAL_MAGE_120, new Tile(3022, 10126), 4));
    spawns.add(new NpcSpawn(NpcId.SPIRITUAL_MAGE_120, new Tile(3020, 10121), 4));
    spawns.add(new NpcSpawn(NpcId.SPIRITUAL_MAGE_120, new Tile(3026, 10117), 4));
    spawns.add(new NpcSpawn(NpcId.SPIRITUAL_MAGE_120, new Tile(3032, 10123), 4));
    spawns.add(new NpcSpawn(NpcId.SPIRITUAL_MAGE_120, new Tile(3026, 10124), 4));
    spawns.add(new NpcSpawn(NpcId.SPIRITUAL_MAGE_120, new Tile(3030, 10129), 4));
    spawns.add(new NpcSpawn(NpcId.SPIRITUAL_MAGE_120, new Tile(3027, 10134), 4));
    spawns.add(new NpcSpawn(NpcId.SPIRITUAL_MAGE_121, new Tile(3059, 10120), 4));
    spawns.add(new NpcSpawn(NpcId.SPIRITUAL_MAGE_121, new Tile(3062, 10124), 4));
    spawns.add(new NpcSpawn(NpcId.SPIRITUAL_MAGE_121, new Tile(3054, 10124), 4));
    spawns.add(new NpcSpawn(NpcId.SPIRITUAL_MAGE_121, new Tile(3051, 10128), 4));
    spawns.add(new NpcSpawn(NpcId.SPIRITUAL_MAGE_121, new Tile(3058, 10128), 4));
    spawns.add(new NpcSpawn(NpcId.SPIRITUAL_MAGE_121, new Tile(3052, 10134), 4));
    spawns.add(new NpcSpawn(NpcId.SPIRITUAL_MAGE_121, new Tile(3047, 10122), 4));
    spawns.add(new NpcSpawn(NpcId.SPIRITUAL_MAGE_121_3161, new Tile(3042, 10147), 4));
    spawns.add(new NpcSpawn(NpcId.SPIRITUAL_MAGE_121_3161, new Tile(3041, 10142), 4));
    spawns.add(new NpcSpawn(NpcId.SPIRITUAL_MAGE_121_3161, new Tile(3045, 10140), 4));
    spawns.add(new NpcSpawn(NpcId.SPIRITUAL_MAGE_121_3161, new Tile(3049, 10144), 4));
    spawns.add(new NpcSpawn(NpcId.SPIRITUAL_MAGE_121_3161, new Tile(3035, 10144), 4));
    spawns.add(new NpcSpawn(NpcId.SPIRITUAL_MAGE_121_3161, new Tile(3039, 10136), 4));
    spawns.add(new NpcSpawn(NpcId.SPIRITUAL_MAGE_121_3161, new Tile(3038, 10151), 4));
    spawns.add(new NpcSpawn(NpcId.SARADOMIN_PRIEST_113, new Tile(3026, 10130), 4));
    spawns.add(new NpcSpawn(NpcId.KNIGHT_OF_SARADOMIN_103, new Tile(3031, 10126), 4));
    spawns.add(new NpcSpawn(NpcId.SARADOMIN_PRIEST_113, new Tile(3036, 10125), 4));
    spawns.add(new NpcSpawn(NpcId.KNIGHT_OF_SARADOMIN_103, new Tile(3037, 10130), 4));
    spawns.add(new NpcSpawn(NpcId.SARADOMIN_PRIEST_113, new Tile(3033, 10132), 4));
    spawns.add(new NpcSpawn(NpcId.KNIGHT_OF_SARADOMIN_103, new Tile(3023, 10119), 4));
    spawns.add(new NpcSpawn(NpcId.HELLHOUND_127, new Tile(3050, 10141), 4));
    spawns.add(new NpcSpawn(NpcId.WEREWOLF_93, new Tile(3043, 10134), 4));
    spawns.add(new NpcSpawn(NpcId.GORAK_149, new Tile(3036, 10139), 4));
    spawns.add(new NpcSpawn(NpcId.IMP_7, new Tile(3037, 10147), 4));
    spawns.add(new NpcSpawn(NpcId.WEREWOLF_93, new Tile(3046, 10148), 4));
    spawns.add(new NpcSpawn(NpcId.IMP_7, new Tile(3055, 10137), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_81, new Tile(3042, 10124), 4));
    spawns.add(new NpcSpawn(NpcId.ORK_107, new Tile(3043, 10130), 4));
    spawns.add(new NpcSpawn(NpcId.HOBGOBLIN_47, new Tile(3047, 10132), 4));
    spawns.add(new NpcSpawn(NpcId.JOGRE_58, new Tile(3047, 10136), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_81, new Tile(3057, 10133), 4));
    spawns.add(new NpcSpawn(NpcId.ORK_107, new Tile(3057, 10124), 4));
    spawns.add(new NpcSpawn(NpcId.HOBGOBLIN_47, new Tile(3063, 10121), 4));
    spawns.add(new NpcSpawn(NpcId.JOGRE_58, new Tile(3051, 10120), 4));
    spawns.add(new NpcSpawn(NpcId.AVIANSIE_69, new Tile(3034, 10152), 4));
    spawns.add(new NpcSpawn(NpcId.AVIANSIE_79, new Tile(3031, 10148), 4));
    spawns.add(new NpcSpawn(NpcId.AVIANSIE_92, new Tile(3027, 10144), 4));
    spawns.add(new NpcSpawn(NpcId.AVIANSIE_137, new Tile(3020, 10147), 4));
    spawns.add(new NpcSpawn(NpcId.AVIANSIE_69, new Tile(3018, 10154), 4));
    spawns.add(new NpcSpawn(NpcId.AVIANSIE_79, new Tile(3025, 10152), 4));
    spawns.add(new NpcSpawn(NpcId.AVIANSIE_92, new Tile(3023, 10157), 4));
    spawns.add(new NpcSpawn(NpcId.AVIANSIE_137, new Tile(3019, 10161), 4));
    spawns.add(new NpcSpawn(NpcId.AVIANSIE_69, new Tile(3030, 10156), 4));
    spawns.add(new NpcSpawn(NpcId.AVIANSIE_79, new Tile(3027, 10159), 4));
    spawns.add(new NpcSpawn(NpcId.AVIANSIE_92, new Tile(3024, 10164), 4));

    return spawns;
  }
}
