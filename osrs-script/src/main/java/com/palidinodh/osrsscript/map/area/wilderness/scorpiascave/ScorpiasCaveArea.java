package com.palidinodh.osrsscript.map.area.wilderness.scorpiascave;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(12961)
public class ScorpiasCaveArea extends Area {
  @Override
  public boolean inWilderness() {
    return true;
  }

  @Override
  public int getWildernessLevel() {
    return (getTile().getY() - 10328) / 8 + 52;
  }
}
