package com.palidinodh.osrsscript.map.area.feldiphills.corsaircove.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class CorsairCoveNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.WIZARD_4399, new Tile(2566, 2859), 4));
    spawns.add(new NpcSpawn(NpcId.MADAME_CALDARIUM, new Tile(2553, 2868)));
    spawns.add(new NpcSpawn(NpcId.YUSUF_7982, new Tile(2569, 2866)));
    spawns.add(new NpcSpawn(NpcId.SEAGULL_3, new Tile(2547, 2856), 4));
    spawns.add(new NpcSpawn(NpcId.SEAGULL_3, new Tile(2561, 2864), 4));
    spawns.add(new NpcSpawn(NpcId.SEAGULL_3, new Tile(2562, 2853), 4));
    spawns.add(new NpcSpawn(NpcId.SEAGULL_3, new Tile(2564, 2857), 4));
    spawns.add(new NpcSpawn(NpcId.SEAGULL_3, new Tile(2578, 2862), 4));
    spawns.add(new NpcSpawn(NpcId.SEAGULL_3, new Tile(2581, 2862), 4));
    spawns.add(new NpcSpawn(NpcId.SEAGULL_3, new Tile(2575, 2852), 4));
    spawns.add(new NpcSpawn(NpcId.SEAGULL_3, new Tile(2571, 2852), 4));
    spawns.add(new NpcSpawn(NpcId.SEAGULL_3, new Tile(2579, 2846), 4));
    spawns.add(new NpcSpawn(NpcId.SEAGULL_3, new Tile(2580, 2850), 4));
    spawns.add(new NpcSpawn(NpcId.SEAGULL_3, new Tile(2578, 2853), 4));
    spawns.add(new NpcSpawn(NpcId.SEAGULL_3, new Tile(2577, 2856), 4));

    return spawns;
  }
}
