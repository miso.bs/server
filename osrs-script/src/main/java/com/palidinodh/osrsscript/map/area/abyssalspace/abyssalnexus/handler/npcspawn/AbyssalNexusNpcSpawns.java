package com.palidinodh.osrsscript.map.area.abyssalspace.abyssalnexus.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class AbyssalNexusNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.ABYSSAL_SIRE_350, new Tile(2977, 4855)));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_SIRE_350, new Tile(3102, 4855)));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_SIRE_350, new Tile(2967, 4791)));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_SIRE_350, new Tile(3107, 4791)));

    return spawns;
  }
}
