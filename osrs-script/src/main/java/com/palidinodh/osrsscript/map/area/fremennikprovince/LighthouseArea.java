package com.palidinodh.osrsscript.map.area.fremennikprovince;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 9784, 9785, 10040, 10041, 10296 })
public class LighthouseArea extends Area {
}
