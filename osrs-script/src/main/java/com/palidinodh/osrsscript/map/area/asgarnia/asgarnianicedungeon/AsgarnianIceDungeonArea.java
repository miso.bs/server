package com.palidinodh.osrsscript.map.area.asgarnia.asgarnianicedungeon;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 11925, 12181, 12437 })
public class AsgarnianIceDungeonArea extends Area {
}
