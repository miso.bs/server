package com.palidinodh.osrsscript.map.area.morytania;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(15159)
public class DragontoothIsleArea extends Area {
}
