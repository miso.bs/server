package com.palidinodh.osrsscript.map.area.misthalin.varrock.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class VarrockNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.TOOL_LEPRECHAUN, new Tile(3229, 3454), Tile.Direction.NORTH));
    spawns.add(new NpcSpawn(NpcId.TREZNOR, new Tile(3226, 3457), 2));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(3222, 3394), Tile.Direction.NORTH));
    spawns.add(new NpcSpawn(NpcId.WIZARD_4399, new Tile(3224, 3394), Tile.Direction.NORTH));
    spawns.add(new NpcSpawn(NpcId.TANNER, new Tile(3226, 3397)));

    return spawns;
  }
}
