package com.palidinodh.osrsscript.map.area.morytania;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 14389, 14645 })
public class MortyaniaArea extends Area {
}
