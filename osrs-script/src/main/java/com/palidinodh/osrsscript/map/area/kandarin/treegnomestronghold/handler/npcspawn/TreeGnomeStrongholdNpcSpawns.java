package com.palidinodh.osrsscript.map.area.kandarin.treegnomestronghold.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class TreeGnomeStrongholdNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.TOOL_LEPRECHAUN, new Tile(2438, 3419)));
    spawns.add(new NpcSpawn(NpcId.GADRIN, new Tile(2446, 3426, 1), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.PRISSY_SCILLA, new Tile(2435, 3411), 2));
    spawns.add(new NpcSpawn(NpcId.BOLONGO, new Tile(2473, 3446), 2));
    spawns.add(new NpcSpawn(NpcId.TOOL_LEPRECHAUN, new Tile(2476, 3443), Tile.Direction.NORTH));
    spawns.add(new NpcSpawn(NpcId.WIZARD_4399, new Tile(2446, 3432, 1), 2));
    spawns.add(new NpcSpawn(NpcId.GNOME_BANKER, new Tile(2443, 3425, 1), Tile.Direction.EAST));
    spawns.add(new NpcSpawn(NpcId.GNOME_BANKER, new Tile(2443, 3424, 1), Tile.Direction.EAST));
    spawns.add(new NpcSpawn(NpcId.GNOME_BANKER, new Tile(2448, 3427, 1), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.GNOME_BANKER, new Tile(2448, 3424, 1), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.GNOME, new Tile(2480, 3431), 4));
    spawns.add(new NpcSpawn(NpcId.GNOME, new Tile(2479, 3422), 4));
    spawns.add(new NpcSpawn(NpcId.GNOME, new Tile(2470, 3436), 4));

    return spawns;
  }
}
