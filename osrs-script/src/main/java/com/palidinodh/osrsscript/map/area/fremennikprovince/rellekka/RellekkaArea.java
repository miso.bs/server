package com.palidinodh.osrsscript.map.area.fremennikprovince.rellekka;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 10297, 10552, 10553, 10554, 10808, 10809 })
public class RellekkaArea extends Area {
}
