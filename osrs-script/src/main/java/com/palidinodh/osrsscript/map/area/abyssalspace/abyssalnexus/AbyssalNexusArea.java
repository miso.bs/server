package com.palidinodh.osrsscript.map.area.abyssalspace.abyssalnexus;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 11850, 11851, 12106, 12362, 12363 })
public class AbyssalNexusArea extends Area {
}
