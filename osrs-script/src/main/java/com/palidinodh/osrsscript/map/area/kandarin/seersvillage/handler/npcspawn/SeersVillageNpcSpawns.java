package com.palidinodh.osrsscript.map.area.kandarin.seersvillage.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class SeersVillageNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(2721, 3495)));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(2724, 3495)));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(2728, 3495)));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(2729, 3495)));
    spawns.add(new NpcSpawn(NpcId.BANKER_395, new Tile(2722, 3495)));
    spawns.add(new NpcSpawn(NpcId.BANKER_395, new Tile(2726, 3496)));
    spawns.add(new NpcSpawn(NpcId.BANKER_395, new Tile(2727, 3495)));
    spawns.add(new NpcSpawn(NpcId.BANKER_395, new Tile(2729, 3496)));
    spawns.add(new NpcSpawn(NpcId.SEER, new Tile(2718, 3484), 4));
    spawns.add(new NpcSpawn(NpcId.SEER, new Tile(2705, 3480), 4));
    spawns.add(new NpcSpawn(NpcId.PHANTUWTI_FANSTUWI_FARSIGHT, new Tile(2701, 3474), 4));
    spawns.add(new NpcSpawn(NpcId.MAN_2_3078, new Tile(2707, 3476), 4));
    spawns.add(new NpcSpawn(NpcId.SEER, new Tile(2707, 3469), 4));
    spawns.add(new NpcSpawn(NpcId.SEER, new Tile(2718, 3474), 4));
    spawns.add(new NpcSpawn(NpcId.TOWN_CRIER_280, new Tile(2739, 3480), 4));
    spawns.add(new NpcSpawn(NpcId.GUARD_4218, new Tile(2737, 3468), 4));
    spawns.add(new NpcSpawn(NpcId.THE_WEDGE, new Tile(2760, 3476), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.ESTATE_AGENT, new Tile(2737, 3502), 4));
    spawns.add(new NpcSpawn(NpcId.TRAMP, new Tile(2683, 3486), 4));
    spawns.add(new NpcSpawn(NpcId.POISON_SALESMAN, new Tile(2696, 3496), 4));
    spawns.add(new NpcSpawn(NpcId.WOMAN_2_3083, new Tile(2694, 3497), 4));
    spawns.add(new NpcSpawn(NpcId.WOMAN_2_3083, new Tile(2696, 3494), 4));
    spawns.add(new NpcSpawn(NpcId.WOMAN_2_3083, new Tile(2694, 3492), 4));
    spawns.add(new NpcSpawn(NpcId.MAN_2_3078, new Tile(2691, 3489), 4));
    spawns.add(new NpcSpawn(NpcId.MAN_2_3078, new Tile(2696, 3490), 4));
    spawns.add(new NpcSpawn(NpcId.MAN_2_3078, new Tile(2694, 3495), 4));
    spawns.add(new NpcSpawn(NpcId.BARTENDER_1318, new Tile(2690, 3493), 4));

    return spawns;
  }
}
