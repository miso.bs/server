package com.palidinodh.osrsscript.map.area.kandarin.piscatoris;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 9017, 9273, 9529 })
public class PiscatorisFishingColonyArea extends Area {
}
