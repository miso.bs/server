package com.palidinodh.osrsscript.map.area.trollcountry;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 11324, 11325, 11580, 11581 })
public class WeissArea extends Area {
}
