package com.palidinodh.osrsscript.map.area.kandarin.legendsguild.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class LegendsGuildNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.SIEGFRIED_ERKLE, new Tile(2727, 3379), 6));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(2733, 3374), Tile.Direction.WEST));

    return spawns;
  }
}
