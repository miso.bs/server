package com.palidinodh.osrsscript.map.area.wilderness.lavamazedungeon.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class LavaMazeDungeonNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.BLACK_DRAGON_227, new Tile(3047, 10264), 2));
    spawns.add(new NpcSpawn(NpcId.BLACK_DRAGON_227, new Tile(3053, 10266), 2));
    spawns.add(new NpcSpawn(NpcId.BLACK_DRAGON_227, new Tile(3050, 10269), 2));
    spawns.add(new NpcSpawn(NpcId.BLACK_DRAGON_227, new Tile(3053, 10271), 2));
    spawns.add(new NpcSpawn(NpcId.BABY_DRAGON_83, new Tile(3051, 10277), 2));
    spawns.add(new NpcSpawn(NpcId.BABY_DRAGON_83, new Tile(3052, 10282), 2));
    spawns.add(new NpcSpawn(NpcId.BABY_DRAGON_83, new Tile(3054, 10286), 2));
    spawns.add(new NpcSpawn(NpcId.BABY_DRAGON_83, new Tile(3044, 10262), 2));
    spawns.add(new NpcSpawn(NpcId.BABY_DRAGON_83, new Tile(3050, 10260), 2));
    spawns.add(new NpcSpawn(NpcId.GREATER_DEMON_92, new Tile(3028, 10250), 2));
    spawns.add(new NpcSpawn(NpcId.GREATER_DEMON_92, new Tile(3032, 10245), 2));
    spawns.add(new NpcSpawn(NpcId.GREATER_DEMON_92, new Tile(3039, 10256), 2));
    spawns.add(new NpcSpawn(NpcId.GREATER_DEMON_92, new Tile(3036, 10260), 2));
    spawns.add(new NpcSpawn(NpcId.GREATER_DEMON_92, new Tile(3030, 10259), 2));
    spawns.add(new NpcSpawn(NpcId.GREATER_DEMON_92, new Tile(3032, 10255), 2));
    spawns.add(new NpcSpawn(NpcId.GREATER_DEMON_92, new Tile(3048, 10254), 2));

    return spawns;
  }
}
