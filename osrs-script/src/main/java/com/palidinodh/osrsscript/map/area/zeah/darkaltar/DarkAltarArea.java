package com.palidinodh.osrsscript.map.area.zeah.darkaltar;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 6716, 6972, 6715, 7228 })
public class DarkAltarArea extends Area {
}
