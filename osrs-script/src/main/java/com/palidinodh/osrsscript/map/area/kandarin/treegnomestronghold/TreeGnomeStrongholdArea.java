package com.palidinodh.osrsscript.map.area.kandarin.treegnomestronghold;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 9268, 9269, 9524, 9525, 9526, 9527, 9780, 9781, 9782, 9783 })
public class TreeGnomeStrongholdArea extends Area {
}
