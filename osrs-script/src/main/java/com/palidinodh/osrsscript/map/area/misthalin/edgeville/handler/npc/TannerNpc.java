package com.palidinodh.osrsscript.map.area.misthalin.edgeville.handler.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrsscript.player.skill.crafting.Crafting;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.TANNER)
class TannerNpc implements NpcHandler {
  @Override
  public void npcOption(Player player, int option, Npc npc) {
    SkillContainer.getContainer(Crafting.class).openTanning(player);
  }
}
