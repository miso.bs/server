package com.palidinodh.osrsscript.map.area.misthalin.edgeville;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.reference.ReferenceIdSet;

@ReferenceId({ 12441, 12442 })
@ReferenceIdSet(primary = 12698,
    secondary = { 0, 1, 2, 3, 4, 5, 6, 7, 16, 17, 18, 19, 20, 21, 22, 23 })
public class EdgevilleDungeonArea extends Area {
}
