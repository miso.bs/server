package com.palidinodh.osrsscript.map.area.wilderness.revenantcaves;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 12701, 12702, 12703, 12957, 12958, 12959 })
public class RevenantCavesArea extends Area {
  @Override
  public boolean inWilderness() {
    return true;
  }

  @Override
  public int getWildernessLevel() {
    return (getTile().getY() - 9920) / 8 + 1;
  }

  @Override
  public boolean inRevenantCaves() {
    return true;
  }
}
