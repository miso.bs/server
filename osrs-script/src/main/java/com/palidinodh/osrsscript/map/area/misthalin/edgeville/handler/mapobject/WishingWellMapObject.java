package com.palidinodh.osrsscript.map.area.misthalin.edgeville.handler.mapobject;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.world.WishingWell;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PNumber;

@ReferenceId(ObjectId.WISHING_WELL)
class WishingWellMapObject implements MapObjectHandler {
  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    openBaseDialogue(player);
  }

  @Override
  public void itemOnMapObject(Player player, int slot, int itemId, MapObject mapObject) {
    if (itemId == ItemId.COINS) {
      openCoinDonationDialogue(player);
      return;
    }
    if (!WishingWell.canDonateItem(itemId)) {
      player.getGameEncoder().sendMessage("The well won't take this item.");
      return;
    }
    openItemDonationDialogue(player, player.getInventory().getItem(slot));
  }

  public void openBaseDialogue(Player player) {
    player.openDialogue(new OptionsDialogue(new DialogueOption("View statistics", (c, s) -> {
      WishingWell.viewStats(player);
    }), new DialogueOption("Buy lottery entry", (c, s) -> {
      WishingWell.addEntry(player);
    }), new DialogueOption("Donate", (c, s) -> {
      openCoinDonationDialogue(player);
    }), new DialogueOption("Collect", (c, s) -> {
      WishingWell.collect(player);
    })));
  }

  public void openCoinDonationDialogue(Player player) {
    player
        .openDialogue(new OptionsDialogue(new DialogueOption("Donate directly into pot", (c, s) -> {
          player.getGameEncoder().sendEnterAmount(value -> {
            WishingWell.donate(player, value, false);
          });
        }), new DialogueOption("Donate for boosts", (c, s) -> {
          player.getGameEncoder().sendEnterAmount(value -> {
            WishingWell.donate(player, value, true);
          });
        })));
  }

  public void openItemDonationDialogue(Player player, Item item) {
    if (item == null || item.getId() == ItemId.BOND_32318) {
      openCoinDonationDialogue(player);
      return;
    }
    String title;
    if (item.getId() == ItemId.BOND_32318) {
      title = item.getName() + " x" + PNumber.formatNumber(item.getAmount()) + ": "
          + PNumber.formatNumber(
              PNumber.multiplyInt(WishingWell.BOND_VALUE, item.getAmount(), Item.MAX_AMOUNT));
    } else {
      title = item.getName() + " x" + PNumber.formatNumber(item.getAmount()) + ": "
          + PNumber
              .formatNumber(PNumber.multiplyInt(item.getInfoDef().getConfiguredExchangePrice() * 2,
                  item.getAmount(), Item.MAX_AMOUNT));
    }
    player.openDialogue(
        new OptionsDialogue(title, new DialogueOption("Put item in the well for boosts", (c, s) -> {
          player.getGameEncoder().sendEnterAmount(value -> {
            WishingWell.donateItemForBoost(player, item.getId());
          });
        }), new DialogueOption("Nevermind")));
  }
}
