package com.palidinodh.osrsscript.map.area.misthalin.edgeville.handler.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.MAGE_OF_ZAMORAK_2582)
class MageOfZamorakNpc implements NpcHandler {
  @Override
  public void npcOption(Player player, int option, Npc npc) {
    player.openDialogue("magezamorak", 0);
  }
}
