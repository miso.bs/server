package com.palidinodh.osrsscript.map.area.abyssalspace.abyssalarea.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class AbyssalAreaNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.ABYSSAL_DEMON_124, new Tile(3052, 4874), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_DEMON_124, new Tile(3045, 4874), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_DEMON_124, new Tile(3038, 4871), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_DEMON_124, new Tile(3030, 4874), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_DEMON_124, new Tile(3019, 4878), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_DEMON_124, new Tile(3016, 4891), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_DEMON_124, new Tile(3023, 4897), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_DEMON_124, new Tile(3036, 4883), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_DEMON_124, new Tile(3029, 4889), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_DEMON_124, new Tile(3044, 4885), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_DEMON_124, new Tile(3050, 4892), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_DEMON_124, new Tile(3062, 4886), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_DEMON_124, new Tile(3065, 4898), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_DEMON_124, new Tile(3060, 4906), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_DEMON_124, new Tile(3056, 4911), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_DEMON_124, new Tile(3051, 4918), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_DEMON_124, new Tile(3043, 4914), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_DEMON_124, new Tile(3049, 4900), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_DEMON_124, new Tile(3041, 4905), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_DEMON_124, new Tile(3033, 4911), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_DEMON_124, new Tile(3023, 4916), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_DEMON_124, new Tile(3019, 4907), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_DEMON_124, new Tile(3032, 4902), 4));

    return spawns;
  }
}
