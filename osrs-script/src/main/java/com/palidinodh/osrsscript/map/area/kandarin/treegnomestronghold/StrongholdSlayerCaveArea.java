package com.palidinodh.osrsscript.map.area.kandarin.treegnomestronghold;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 9624, 9625, 9880, 9881 })
public class StrongholdSlayerCaveArea extends Area {
}
