package com.palidinodh.osrsscript.map.area.misthalin.edgeville.handler.mapobject;

import com.palidinodh.osrscore.io.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrsscript.world.event.pvptournament.PvpTournamentEvent;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ ObjectId.GATE_26081, ObjectId.GATE_26082 })
class TournamentGateMapObject implements MapObjectHandler {
  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    player.getWorld().getWorldEvent(PvpTournamentEvent.class).addPlayer(player);
  }
}
