package com.palidinodh.osrsscript.map.area.kandarin.ourania.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class OuraniaCaveNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.ENIOLA, new Tile(3014, 5625), 4));
    spawns.add(new NpcSpawn(NpcId.ZAMORAK_WARRIOR_85, new Tile(3015, 5603), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_LIZARD_37, new Tile(3011, 5601), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_LIZARD_37, new Tile(3017, 5595), 4));
    spawns.add(new NpcSpawn(NpcId.ZAMORAK_RANGER_81, new Tile(3013, 5591), 4));
    spawns.add(new NpcSpawn(NpcId.ZAMORAK_CRAFTER_19, new Tile(3012, 5596), 4));
    spawns.add(new NpcSpawn(NpcId.ZAMORAK_CRAFTER_19, new Tile(3016, 5587), 4));
    spawns.add(new NpcSpawn(NpcId.ZAMORAK_WARRIOR_84, new Tile(3012, 5585), 4));
    spawns.add(new NpcSpawn(NpcId.ZAMORAK_MAGE_84, new Tile(3018, 5578), 4));
    spawns.add(new NpcSpawn(NpcId.ZAMORAK_MAGE_84, new Tile(3011, 5573), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_LIZARD_37, new Tile(3013, 5578), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_LIZARD_37, new Tile(3017, 5573), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_LIZARD_37, new Tile(3018, 5583), 4));
    spawns.add(new NpcSpawn(NpcId.ZAMORAK_WARRIOR_84, new Tile(3027, 5572), 4));
    spawns.add(new NpcSpawn(NpcId.ZAMORAK_MAGE_84, new Tile(3032, 5580), 4));
    spawns.add(new NpcSpawn(NpcId.ZAMORAK_RANGER_82, new Tile(3034, 5573), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_LIZARD_37, new Tile(3029, 5577), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_LIZARD_37, new Tile(3031, 5572), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_LIZARD_37, new Tile(3033, 5576), 4));
    spawns.add(new NpcSpawn(NpcId.CAVE_LIZARD_37, new Tile(3043, 5580), 4));
    spawns.add(new NpcSpawn(NpcId.ZAMORAK_RANGER_81, new Tile(3047, 5577), 4));

    return spawns;
  }
}
