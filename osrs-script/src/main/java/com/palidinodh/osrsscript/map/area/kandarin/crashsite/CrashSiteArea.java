package com.palidinodh.osrsscript.map.area.kandarin.crashsite;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 7766, 7767, 8022, 8023 })
public class CrashSiteArea extends Area {
}
