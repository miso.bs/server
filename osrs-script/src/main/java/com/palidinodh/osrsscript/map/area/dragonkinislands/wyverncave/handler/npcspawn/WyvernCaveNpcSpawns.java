package com.palidinodh.osrsscript.map.area.dragonkinislands.wyverncave.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class WyvernCaveNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.TALONED_WYVERN_147, new Tile(3605, 10215), 4));
    spawns.add(new NpcSpawn(NpcId.TALONED_WYVERN_147, new Tile(3613, 10214), 4));
    spawns.add(new NpcSpawn(NpcId.TALONED_WYVERN_147, new Tile(3612, 10205), 4));
    spawns.add(new NpcSpawn(NpcId.TALONED_WYVERN_147, new Tile(3604, 10202), 4));
    spawns.add(new NpcSpawn(NpcId.LONG_TAILED_WYVERN_152, new Tile(3598, 10186), 4));
    spawns.add(new NpcSpawn(NpcId.LONG_TAILED_WYVERN_152, new Tile(3604, 10191), 4));
    spawns.add(new NpcSpawn(NpcId.LONG_TAILED_WYVERN_152, new Tile(3597, 10196), 4));
    spawns.add(new NpcSpawn(NpcId.SPITTING_WYVERN_139, new Tile(3630, 10196), 4));
    spawns.add(new NpcSpawn(NpcId.SPITTING_WYVERN_139, new Tile(3627, 10187), 4));
    spawns.add(new NpcSpawn(NpcId.SPITTING_WYVERN_139, new Tile(3620, 10183), 4));
    spawns.add(new NpcSpawn(NpcId.SPITTING_WYVERN_139, new Tile(3621, 10193), 4));
    spawns.add(new NpcSpawn(NpcId.ANCIENT_WYVERN_210, new Tile(3633, 10212), 4));
    spawns.add(new NpcSpawn(NpcId.ANCIENT_WYVERN_210, new Tile(3635, 10206), 4));
    spawns.add(new NpcSpawn(NpcId.ANCIENT_WYVERN_210, new Tile(3626, 10246), 4));
    spawns.add(new NpcSpawn(NpcId.ANCIENT_WYVERN_210, new Tile(3636, 10246), 4));
    spawns.add(new NpcSpawn(NpcId.ANCIENT_WYVERN_210, new Tile(3632, 10253), 4));
    spawns.add(new NpcSpawn(NpcId.SPITTING_WYVERN_139, new Tile(3604, 10265), 4));
    spawns.add(new NpcSpawn(NpcId.SPITTING_WYVERN_139, new Tile(3602, 10273), 4));
    spawns.add(new NpcSpawn(NpcId.SPITTING_WYVERN_139, new Tile(3613, 10273), 4));
    spawns.add(new NpcSpawn(NpcId.SPITTING_WYVERN_139, new Tile(3611, 10281), 4));
    spawns.add(new NpcSpawn(NpcId.TALONED_WYVERN_147, new Tile(3622, 10291), 4));
    spawns.add(new NpcSpawn(NpcId.TALONED_WYVERN_147, new Tile(3626, 10285), 4));
    spawns.add(new NpcSpawn(NpcId.TALONED_WYVERN_147, new Tile(3634, 10276), 4));
    spawns.add(new NpcSpawn(NpcId.TALONED_WYVERN_147, new Tile(3636, 10283), 4));
    spawns.add(new NpcSpawn(NpcId.LONG_TAILED_WYVERN_152, new Tile(3596, 10249), 4));
    spawns.add(new NpcSpawn(NpcId.LONG_TAILED_WYVERN_152, new Tile(3603, 10251), 4));
    spawns.add(new NpcSpawn(NpcId.LONG_TAILED_WYVERN_152, new Tile(3614, 10249), 4));
    spawns.add(new NpcSpawn(NpcId.LONG_TAILED_WYVERN_152, new Tile(3609, 10252), 4));

    return spawns;
  }
}
