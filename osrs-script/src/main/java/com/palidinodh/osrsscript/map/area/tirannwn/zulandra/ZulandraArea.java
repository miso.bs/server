package com.palidinodh.osrsscript.map.area.tirannwn.zulandra;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(8751)
public class ZulandraArea extends Area {
}
