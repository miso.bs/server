package com.palidinodh.osrsscript.npc.combat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.google.inject.Inject;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.cache.id.VarbitId;
import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.util.PEvent;
import lombok.var;

class TzkalZukCombat extends NpcCombat {
  private static final int MAGE_RANGE_SPAWNS_1 = 81, MAGE_RANGE_SPAWNS_2 = 600,
      MAGE_RANGE_SPAWNS_3 = 960;
  private static final int SPAWN_PHASE_1 = 0, SPAWN_PHASE_2 = 1, SPAWN_PHASE_3 = 2;
  private static final Tile MAGE_TILE = new Tile(2266, 5351), RANGER_TILE = new Tile(2275, 5351),
      JAD_TILE = new Tile(2270, 5347);
  private static final Tile[] JAK_TILES =
      { new Tile(2262, 5363), new Tile(2266, 5363), new Tile(2276, 5363), new Tile(2280, 5363) };

  @Inject
  private Npc npc;
  private int ticks;
  private Npc glyph;
  private int spawnPhase;
  private boolean spawnedJad;
  private boolean spawnedHealers;
  private List<Npc> spawns = new ArrayList<>();

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.TZKAL_ZUK_1400);
    combat.spawn(NpcCombatSpawn.builder().lock(8).animation(7563).build());
    combat.hitpoints(
        NpcCombatHitpoints.builder().total(1200).barType(HitpointsBarType.GREEN_RED_160).build());
    combat.stats(NpcCombatStats.builder().attackLevel(350).magicLevel(150).rangedLevel(400)
        .defenceLevel(260).bonus(BonusType.ATTACK_CRUSH, 300).bonus(BonusType.ATTACK_MAGIC, 550)
        .bonus(BonusType.ATTACK_RANGED, 550).bonus(BonusType.DEFENCE_MAGIC, 350)
        .bonus(BonusType.DEFENCE_RANGED, 100).build());
    combat.aggression(
        NpcCombatAggression.builder().range(8).always(true).checkWhileAttacking(false).build());
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.focus(NpcCombatFocus.builder().disableFacingOpponent(true).disableFollowingOpponent(true)
        .build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(251)
        .applyType(NpcCombatDamage.ApplyType.UNCAPPED).ignorePrayer(true).build());
    style.animation(7566).attackSpeed(10).attackRange(40);
    style.projectile(NpcCombatProjectile.builder().id(1375).speedMinimumDistance(4).build());
    combat.style(style.build());


    return Arrays.asList(combat.build());
  }

  @Override
  public void spawnHook() {
    ticks = 0;
    spawnPhase = 0;
    spawnedJad = false;
    spawnedHealers = false;
    glyph = npc.getController()
        .addNpc(new NpcSpawn(NpcId.ANCESTRAL_GLYPH, new Tile(2270, 5363, npc.getHeight())));
    glyph.getCombat().script("zuk", npc);
    npc.getCombat().setHitDelay(8 + 13);
  }

  @Override
  public void despawnHook() {
    npc.getWorld().removeNpc(glyph);
    npc.getWorld().removeNpcs(spawns);
  }

  @Override
  public void tickStartHook() {
    if (!npc.isVisible() || npc.getCombat().isDead()) {
      return;
    }
    if (!spawns.isEmpty()) {
      for (var it = spawns.iterator(); it.hasNext();) {
        if (it.next().getCombat().isDead()) {
          it.remove();
        }
      }
    }
    ticks++;
    if (spawns.isEmpty() && (spawnPhase == SPAWN_PHASE_1 && ticks >= MAGE_RANGE_SPAWNS_1
        || spawnPhase == SPAWN_PHASE_2 && ticks >= MAGE_RANGE_SPAWNS_2
        || spawnPhase == SPAWN_PHASE_3 && ticks >= MAGE_RANGE_SPAWNS_3)) {
      var ranger = npc.getController().addNpc(new NpcSpawn(NpcId.JAL_XIL_370_7702,
          new Tile(RANGER_TILE.getX(), RANGER_TILE.getY(), npc.getHeight())));
      ranger.getMovement().setClipNpcs(true);
      ranger.setLargeVisibility();
      ranger.getCombat().startAttacking(glyph);
      spawns.add(ranger);
      var mage = npc.getController().addNpc(new NpcSpawn(NpcId.JAL_ZEK_490_7703,
          new Tile(MAGE_TILE.getX(), MAGE_TILE.getY(), npc.getHeight())));
      mage.getMovement().setClipNpcs(true);
      mage.setLargeVisibility();
      mage.getCombat().startAttacking(glyph);
      // List<Npc> monsters = new ArrayList<Npc>();
      // monsters.add(ranger);
      // mage.getCombat().script("monsters", monsters);
      spawns.add(mage);
      spawnPhase++;
    }
    if (npc.getCombat().getHitpoints() <= 480 && !spawnedJad) {
      spawnedJad = true;
      var jad = npc.getController().addNpc(new NpcSpawn(NpcId.JALTOK_JAD_900_7704,
          new Tile(JAD_TILE.getX(), JAD_TILE.getY(), npc.getHeight())));
      jad.setLargeVisibility();
      jad.getMovement().setClipNpcs(true);
      jad.getCombat().startAttacking(glyph);
      spawns.add(jad);
    }
    if (npc.getCombat().getHitpoints() <= 240 && !spawnedHealers) {
      spawnedHealers = true;
      for (var tile : JAK_TILES) {
        var jak = npc.getController().addNpc(new NpcSpawn(NpcId.JAL_MEJJAK_250,
            new Tile(tile.getX(), tile.getY(), npc.getHeight())));
        jak.getMovement().setClipNpcs(true);
        jak.setLargeVisibility();
        jak.getCombat().startAttacking(npc);
        jak.getCombat().script("spawn_healers_north", true);
        spawns.add(jak);
      }
    }
  }

  @Override
  public void npcApplyHitStartHook(Hit hit) {
    if (hit.getDamage() > 250) {
      hit.setDamage(250);
    }
  }

  @Override
  public void npcApplyHitEndHook(Hit hit) {
    updateHealth();
  }

  @Override
  public double damageInflictedHook(NpcCombatStyle combatStyle, Entity opponent, double damage) {
    if (entityIsSafe(opponent)) {
      return 0;
    }
    return damage;
  }

  @Override
  public Entity applyAttackEntityHook(NpcCombatStyle combatStyle, Entity opponent) {
    return entityIsSafe(opponent) ? glyph : opponent;
  }

  @Override
  public void applyAttackEndHook(NpcCombatStyle combatStyle, Entity opponent,
      int applyAttackLoopCount, HitEvent hitEvent) {
    if (opponent == glyph) {
      hitEvent.stop();
    }
  }

  @Override
  public void setTargetHook(Entity target) {
    updateHealth();
    addEvent(PEvent.singleEvent(1, e -> {
      target.asPlayer().getWidgetManager().sendOverlay(WidgetId.TZKAL_ZUK);
    }));
  }

  public boolean entityIsSafe(Entity entity) {
    if (!glyph.isLocked() && (entity.getX() + 1 == glyph.getX() || entity.getX() == glyph.getX()
        || entity.getX() - 1 == glyph.getX() || entity.getX() - 2 == glyph.getX()
        || entity.getX() - 3 == glyph.getX())) {
      return true;
    }
    return false;
  }

  public void updateHealth() {
    var player = getTarget().asPlayer();
    player.getGameEncoder().setVarbit(VarbitId.TZKAL_ZULK_HEALTH_OVERLAY_CURRENT, getHitpoints());
    player.getGameEncoder().setVarbit(VarbitId.TZKAL_ZULK_HEALTH_OVERLAY_TOTAL, getMaxHitpoints());
  }
}
