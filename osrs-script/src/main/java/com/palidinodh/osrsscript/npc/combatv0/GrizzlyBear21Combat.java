package com.palidinodh.osrsscript.npc.combatv0;

import java.util.Arrays;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.item.RandomItem;
import lombok.var;

class GrizzlyBear21Combat extends NpcCombat {
  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop = NpcCombatDrop.builder();
    var dropTable = NpcCombatDropTable.builder().chance(4.0);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ENSOULED_BEAR_HEAD_13463)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BEAR_RIBS)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BONES)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BEAR_FUR)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RAW_BEAR_MEAT)));
    drop.table(dropTable.build());


    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.GRIZZLY_BEAR_21);
    combat.hitpoints(NpcCombatHitpoints.total(27));
    combat.stats(
        NpcCombatStats.builder().attackLevel(17).defenceLevel(15).bonus(BonusType.ATTACK_STAB, 1)
            .bonus(BonusType.ATTACK_SLASH, 1).bonus(BonusType.ATTACK_CRUSH, 1)
            .bonus(BonusType.ATTACK_MAGIC, 1).bonus(BonusType.ATTACK_RANGED, 1)
            .bonus(BonusType.DEFENCE_SLASH, 1).bonus(BonusType.DEFENCE_CRUSH, 1)
            .bonus(BonusType.DEFENCE_MAGIC, 1).bonus(BonusType.DEFENCE_RANGED, 1).build());
    combat.aggression(NpcCombatAggression.PLAYERS);
    combat.deathAnimation(4929).blockAnimation(4927);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(3));
    style.animation(4925).attackSpeed(4);
    combat.style(style.build());


    return Arrays.asList(combat.build());
  }
}
