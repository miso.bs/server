package com.palidinodh.osrsscript.npc.combat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.google.inject.Inject;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.cache.id.ObjectId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.combat.HitMarkType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.combat.TileHitEvent;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatMulti;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.map.route.Route;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PCollection;
import com.palidinodh.util.PEvent;
import lombok.var;

class GreatOlmCombat extends NpcCombat {
  private static final int HEAD = 0;
  private static final int LEFT_HAND = 1;
  private static final int RIGHT_HAND = 2;
  private static final Tile[] EAST_OBJECT_TILES =
      { new Tile(3238, 5738), new Tile(3238, 5733), new Tile(3238, 5743) };
  private static final Tile[] EAST_NPC_TILES =
      { new Tile(3239, 5738), new Tile(3238, 5733), new Tile(3238, 5743) };
  private static final Tile[] WEST_OBJECT_TILES =
      { new Tile(3220, 5738), new Tile(3220, 5743), new Tile(3220, 5733) };
  private static final Tile[] WEST_NPC_TILES =
      { new Tile(3222, 5738), new Tile(3223, 5743), new Tile(3223, 5733) };
  private static final int[] OBJECT_IDS = { ObjectId.LARGE_HOLE_29881, ObjectId.LARGE_ROCK_29884,
      ObjectId.CRYSTALLINE_STRUCTURE_29887 };
  private static final int[] NPC_IDS =
      { NpcId.GREAT_OLM_1043, NpcId.GREAT_OLM_LEFT_CLAW_750, NpcId.GREAT_OLM_RIGHT_CLAW_549 };
  private static final int[] SPAWN_ANIMATIONS = { 7335, 7354, 7350 };
  private static final int[] DESPAWN_ANIMATIONS = { 7348, 7370, 7352 };
  private static final int[] DESPAWN_TIMES = { 2, 1, 1 };
  private static final int[] DEFAULT_ANIMATIONS = { 7336, 7355, 7351 };
  private static final Map<Integer, Integer> OLM_PHASE_3_ANIMATIONS =
      PCollection.toMap(7335, 7383, 7336, 7374, 7345, 7371);

  @Inject
  private Npc npc;
  private List<PEvent> events = new ArrayList<>();
  private boolean loaded;
  private boolean visible;
  private Npc[] olm;
  private MapObject[] objects;
  private int handHitpoints;
  private int phase = 1;
  private int normalUniqueAttackDelay;
  private int rotatingSpecials;
  private int attackRotation;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop = NpcCombatDrop.builder().additionalPlayers(255);


    var headCombat = NpcCombatDefinition.builder();
    headCombat.id(NpcId.GREAT_OLM_1043);
    headCombat.spawn(NpcCombatSpawn.builder().lock(8).build());
    headCombat.hitpoints(
        NpcCombatHitpoints.builder().total(680).barType(HitpointsBarType.GREEN_RED_100).build());
    headCombat.stats(NpcCombatStats.builder().attackLevel(250).magicLevel(250).rangedLevel(250)
        .defenceLevel(150).bonus(BonusType.ATTACK_MAGIC, 60).bonus(BonusType.ATTACK_RANGED, 60)
        .bonus(BonusType.MELEE_DEFENCE, 200).bonus(BonusType.DEFENCE_MAGIC, 200)
        .bonus(BonusType.DEFENCE_RANGED, 50).build());
    headCombat.aggression(NpcCombatAggression.builder().always(true).range(32).build());
    headCombat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    headCombat.focus(NpcCombatFocus.builder().disableFollowingOpponent(true).build());
    headCombat.killCount(
        NpcCombatKillCount.builder().asName("Chambers of Xeric").sendMessage(true).build());
    headCombat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.builder().maximum(33).prayerEffectiveness(0.16).build());
    style.animation(7345).attackSpeed(4).attackRange(32);
    style.projectile(NpcCombatProjectile.id(1340));
    style.multiCombat(NpcCombatMulti.nearOpponent(3));
    headCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(33).prayerEffectiveness(0.16).build());
    style.animation(7345).attackSpeed(4).attackRange(32);
    style.projectile(NpcCombatProjectile.id(1339));
    style.multiCombat(NpcCombatMulti.nearOpponent(3));
    headCombat.style(style.build());


    var leftClawCombat = NpcCombatDefinition.builder();
    leftClawCombat.id(NpcId.GREAT_OLM_LEFT_CLAW_750);
    leftClawCombat.hitpoints(
        NpcCombatHitpoints.builder().total(500).barType(HitpointsBarType.GREEN_RED_100).build());
    leftClawCombat.stats(NpcCombatStats.builder().attackLevel(250).magicLevel(175).rangedLevel(250)
        .defenceLevel(175).bonus(BonusType.ATTACK_MAGIC, 60).bonus(BonusType.ATTACK_RANGED, 60)
        .bonus(BonusType.MELEE_DEFENCE, 50).bonus(BonusType.DEFENCE_MAGIC, 50)
        .bonus(BonusType.DEFENCE_RANGED, 50).build());
    leftClawCombat.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());


    var rightClawCombat = NpcCombatDefinition.builder();
    rightClawCombat.id(NpcId.GREAT_OLM_RIGHT_CLAW_549);
    rightClawCombat.hitpoints(
        NpcCombatHitpoints.builder().total(500).barType(HitpointsBarType.GREEN_RED_100).build());
    rightClawCombat.stats(NpcCombatStats.builder().attackLevel(250).magicLevel(87).rangedLevel(250)
        .defenceLevel(175).bonus(BonusType.ATTACK_MAGIC, 60).bonus(BonusType.ATTACK_RANGED, 60)
        .bonus(BonusType.MELEE_DEFENCE, 200).bonus(BonusType.DEFENCE_MAGIC, 50)
        .bonus(BonusType.DEFENCE_RANGED, 200).build());
    rightClawCombat.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());


    return Arrays.asList(headCombat.build(), leftClawCombat.build(), rightClawCombat.build());
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("olm")) {
      olm = (Npc[]) args;
    } else if (name.equals("objects")) {
      objects = (MapObject[]) args;
    } else if (name.equals("phase")) {
      phase = (int) args[0];
    }
    return null;
  }

  @Override
  public void restoreHook() {
    if (npc.getId() != NPC_IDS[HEAD]) {
      return;
    }
    despawnNpc(HEAD, 0, true);
    despawnNpc(LEFT_HAND, 0, true);
    despawnNpc(RIGHT_HAND, 0, true);
    if (olm != null) {
      npc.getWorld().removeNpc(olm[LEFT_HAND]);
      npc.getWorld().removeNpc(olm[RIGHT_HAND]);
    }
  }

  @Override
  public void tickStartHook() {
    if (npc.getId() != NPC_IDS[HEAD]) {
      return;
    }
    if (!loaded) {
      loadProfile();
    }
    if (!npc.getController().isRegionLoaded()) {
      return;
    }
    if (!visible) {
      for (var player : npc.getController().getPlayers()) {
        if (npc.withinDistance(player, 6)) {
          visible = true;
          break;
        }
      }
      if (visible) {
        rotate(false);
      }
      return;
    }
    if (npc.isLocked()) {
      return;
    }
    if (normalUniqueAttackDelay > 0) {
      normalUniqueAttackDelay--;
    }
    if (npc.getCombat().getHitDelay() == 6 || npc.getCombat().getHitDelay() == 2) {
      setAnimation(HEAD, DEFAULT_ANIMATIONS[HEAD], true);
    }
    if (npc.isAttacking() && npc.getCombat().getHitDelay() == 0) {
      if ((attackRotation == 0 || attackRotation == 2) && normalUniqueAttackDelay == 0
          && PRandom.randomE(4) == 0) {
        var attack = PRandom.randomI(2);
        if (attack == 0) {
          spheres();
        } else if (attack == 1) {
          acidSpray();
        } else if (attack == 2) {
          acidDrip();
        }
        normalUniqueAttackDelay = 32;
      } else if (attackRotation == 1) {
        attackRotation++;
        if (!olm[LEFT_HAND].isLocked()) {
          if (rotatingSpecials == 0) {
            crystalBurst();
          } else if (rotatingSpecials == 1) {
            lightning();
          } else if (rotatingSpecials == 2) {
            swamp();
          }
          rotatingSpecials = (rotatingSpecials + 1) % 3;
        }
      }
    }
    if (phase < 3 && olm != null && !olm[LEFT_HAND].isVisible() && !olm[RIGHT_HAND].isVisible()) {
      rotateStart();
    }
  }

  @Override
  public int attackTickAnimationHook(NpcCombatStyle combatStyle, Entity opponent) {
    setAnimation(HEAD, combatStyle.getAnimation(), true);
    return -1;
  }

  @Override
  public int attackTickAttackSpeedHook(NpcCombatStyle combatStyle, Entity opponent) {
    if (attackRotation++ == 2) {
      attackRotation = 0;
      return combatStyle.getAttackSpeed() * 2;
    }
    return combatStyle.getAttackSpeed();
  }

  @Override
  public double damageReceivedHook(Entity opponent, double damage, HitStyleType hitStyleType,
      HitStyleType defenceType) {
    if (npc.getId() == NPC_IDS[RIGHT_HAND] && hitStyleType != HitStyleType.MAGIC) {
      damage = 0;
    }
    if (npc.getId() == NPC_IDS[LEFT_HAND] && phase < 3 && !olm[LEFT_HAND].isLocked()
        && !olm[RIGHT_HAND].isLocked() && !olm[RIGHT_HAND].getCombat().isDead()
        && (npc.getCombat().getHitpoints() * 0.8) <= olm[RIGHT_HAND].getCombat().getHitpoints()
        && damage > 0 && PRandom.randomE(16) == 0) {
      npc.setLock(44);
      for (var player : npc.getController().getPlayers()) {
        player.getGameEncoder()
            .sendMessage("The Great Olm's left claw clenches to protect itself temporarily.");
      }
      setAnimation(LEFT_HAND, 7360, false);
      var event = new PEvent(0) {
        @Override
        public void execute() {
          if (getExecutions() == 44 || !npc.isVisible()) {
            stop();
          }
          if (getExecutions() == 1) {
            setAnimation(LEFT_HAND, 7361, false);
          } else if (getExecutions() == 40) {
            setAnimation(LEFT_HAND, 7362, false);
          } else if (getExecutions() == 42) {
            setAnimation(LEFT_HAND, DEFAULT_ANIMATIONS[LEFT_HAND], false);
          }
        }
      };
      olm[HEAD].getCombat().addEvent(event);
    }
    return damage;

  }

  @Override
  public boolean canAttackEntityHook(NpcCombatStyle combatStyle, Entity opponent) {
    return visible;
  }

  @Override
  public boolean canBeAttackedHook(Entity opponent, boolean sendMessage,
      HitStyleType hitStyleType) {
    return npc.getId() != NPC_IDS[HEAD]
        || visible && phase == 3 && olm[LEFT_HAND].isLocked() && olm[RIGHT_HAND].isLocked();
  }

  @Override
  public void applyDeadStartHook(int deathDelay) {
    var index = getIndex();
    despawnNpc(index, DESPAWN_TIMES[index], false);
    if (index == HEAD) {
      for (var player : npc.getController().getPlayers()) {
        player.getGameEncoder().sendMessage(
            "As the Great Olm collapses, the crystal blocking your exit has been shattered.");
      }
      npc.getController().addMapObject(new MapObject(-1, 10, 0, new Tile(3232, 5749)));
      npc.getController().addMapObject(new MapObject(30028, 10, 0, new Tile(3233, 5751)));
    } else if (phase == 3 && olm[LEFT_HAND].getCombat().isDead()
        && olm[RIGHT_HAND].getCombat().isDead()) {
      fallingCrystals();
    }
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    var index = getIndex();
    if (index == HEAD && additionalPlayerLoopCount == 0) {
      player.getController().script("get_decide_rewards");
    }
  }

  public void loadProfile() {
    if (loaded || npc.getController().getPlayers().isEmpty()) {
      return;
    }
    loaded = true;
    var averageHP = 0;
    var playerMultiplier = 1.0;
    var players = npc.getController().getPlayers();
    for (var player : players) {
      averageHP += player.getCombat().getMaxHitpoints();
      playerMultiplier += 0.5;
    }
    averageHP /= players.size();
    var hitpoints = (int) ((300 + (players.size() * 50) + (averageHP * 2)) * playerMultiplier);
    npc.getCombat().setMaxHitpoints(hitpoints);
    npc.getCombat().setHitpoints(npc.getCombat().getMaxHitpoints());
    handHitpoints = (int) ((150 + (players.size() * 50) + (averageHP * 2)) * playerMultiplier);
  }

  public int getIndex() {
    if (npc.getId() == NPC_IDS[LEFT_HAND]) {
      return LEFT_HAND;
    } else if (npc.getId() == NPC_IDS[RIGHT_HAND]) {
      return RIGHT_HAND;
    }
    return HEAD;
  }

  public void despawnNpc(int index, int time, boolean force) {
    if (time != 0) {
      setAnimation(index, DESPAWN_ANIMATIONS[index], false);
    }
    var event = new PEvent(time) {
      @Override
      public void execute() {
        stop();
        var matchesEast = npc.matchesTile(EAST_NPC_TILES[index]);
        var matchesWest = npc.matchesTile(WEST_NPC_TILES[index]);
        if (matchesEast || !matchesEast && !matchesWest || force) {
          npc.getController()
              .addMapObject(new MapObject(OBJECT_IDS[index] - 1, 10, 1, EAST_OBJECT_TILES[index]));
        }
        if (matchesWest || !matchesEast && !matchesWest || force) {
          npc.getController()
              .addMapObject(new MapObject(OBJECT_IDS[index] - 1, 10, 3, WEST_OBJECT_TILES[index]));
        }
      }
    };
    if (time == 0) {
      event.execute();
    } else {
      olm[HEAD].getCombat().addEvent(event);
    }
  }

  public void setAnimation(int index, int animation, boolean all) {
    var animations = new int[] { DEFAULT_ANIMATIONS[HEAD], DEFAULT_ANIMATIONS[LEFT_HAND],
        DEFAULT_ANIMATIONS[RIGHT_HAND] };
    if (phase == 3 && OLM_PHASE_3_ANIMATIONS.containsKey(animation)) {
      animations[index] = OLM_PHASE_3_ANIMATIONS.get(animation);
    } else {
      animations[index] = animation;
    }
    for (var player : npc.getController().getPlayers()) {
      if (all) {
        for (var i = 0; i < objects.length; i++) {
          if (olm == null || olm[i].isLocked()) {
            continue;
          }
          player.getGameEncoder().sendMapObjectAnimation(objects[i], animations[i]);
        }
      } else {
        player.getGameEncoder().sendMapObjectAnimation(objects[index], animations[index]);
      }
    }
  }

  private List<Player> getAttackablePlayers() {
    var players = npc.getController().getPlayers();
    for (var it = players.iterator(); it.hasNext();) {
      var player = it.next();
      if (player.isLocked() || !npc.withinDistance(player, 32) || player.getY() < 5730) {
        it.remove();
      }
    }
    return players;
  }

  public void rotateStart() {
    var west = phase++ == 0 ? PRandom.randomI(1) == 0 : npc.matchesTile(EAST_NPC_TILES[HEAD]);
    olm[LEFT_HAND].getCombat().script("phase", phase);
    olm[RIGHT_HAND].getCombat().script("phase", phase);
    despawnNpc(HEAD, DESPAWN_TIMES[HEAD], false);
    npc.setVisible(false);
    npc.lock();
    var event = new PEvent(1) {
      @Override
      public void execute() {
        if (getExecutions() == 0) {
          return;
        } else if (getExecutions() == 17) {
          stop();
          npc.setVisible(true);
          npc.unlock();
          rotate(west);
        }
        var count = 1 + PRandom.randomI(3);
        for (var i = 0; i < count; i++) {
          var tile = new Tile(3228, 5730);
          tile.moveTile(PRandom.randomI(9), PRandom.randomI(18));
          npc.getController().sendMapProjectile(null, (new Tile(tile)).moveY(1), tile, 1357, 255,
              10, 0, 51 + 120, 0, 0);
          npc.getController().sendMapGraphic(tile, new Graphic(1358, 0, 51 + 100));
          npc.getController().sendMapGraphic(tile, new Graphic(1449));
          var the = new TileHitEvent(5, npc.getController(), tile, 20, HitStyleType.TYPELESS);
          the.setAdjacentHalfDamage(true);
          olm[HEAD].getCombat().addEvent(the);
        }
      }
    };
    olm[HEAD].getCombat().addEvent(event);
  }

  public void rotate(boolean west) {
    var objectTiles = west ? WEST_OBJECT_TILES : EAST_OBJECT_TILES;
    var npcTiles = west ? WEST_NPC_TILES : EAST_NPC_TILES;
    olm = new Npc[3];
    olm[0] = npc;
    olm[1] = npc.getController().addNpc(new NpcSpawn(NPC_IDS[LEFT_HAND], npcTiles[LEFT_HAND]));
    olm[2] = npc.getController().addNpc(new NpcSpawn(NPC_IDS[RIGHT_HAND], npcTiles[RIGHT_HAND]));
    npc.setLock(6);
    npc.getMovement().teleport(npcTiles[HEAD]);
    var hands = new Npc[] { olm[LEFT_HAND], olm[RIGHT_HAND] };
    for (var hand : hands) {
      hand.setLock(6);
      hand.getController().setMultiCombatFlag(true);
      hand.getCombat().setMaxHitpoints(handHitpoints);
      hand.getCombat().setHitpoints(hand.getCombat().getMaxHitpoints());
      hand.getCombat().script("olm", (Object[]) olm);
    }
    var direction = west ? 3 : 1;
    objects = new MapObject[] { new MapObject(OBJECT_IDS[HEAD], 10, direction, objectTiles[HEAD]),
        new MapObject(OBJECT_IDS[LEFT_HAND], 10, direction, objectTiles[LEFT_HAND]),
        new MapObject(OBJECT_IDS[RIGHT_HAND], 10, direction, objectTiles[RIGHT_HAND]) };
    olm[LEFT_HAND].getCombat().script("objects", (Object[]) objects);
    olm[RIGHT_HAND].getCombat().script("objects", (Object[]) objects);
    for (var player : npc.getController().getPlayers()) {
      for (var i = 0; i < objects.length; i++) {
        player.getGameEncoder().sendMapObjectAnimation(
            new MapObject(objects[i].getId() - 1, objects[i]), SPAWN_ANIMATIONS[i]);
      }
    }
    var event = new PEvent(4) {
      @Override
      public void execute() {
        stop();
        for (var mapObject : objects) {
          npc.getController().addMapObject(mapObject);
        }
      }
    };
    olm[HEAD].getCombat().addEvent(event);
    npc.getCombat().setHitpoints(npc.getCombat().getMaxHitpoints());
  }

  public void crystalBurst() {
    npc.getCombat().setHitDelay(4);
    setAnimation(LEFT_HAND, 7356, true);
    var crystals = new ArrayList<MapObject>();
    for (var player : getAttackablePlayers()) {
      var crystal =
          new MapObject(ObjectId.SMALL_CRYSTALS, 10, MapObject.getRandomDirection(), player);
      var hasTileMatch = false;
      for (var aCrystal : crystals) {
        if (!crystal.matchesTile(aCrystal)) {
          continue;
        }
        hasTileMatch = true;
        break;
      }
      if (hasTileMatch) {
        continue;
      }
      crystals.add(crystal);
      npc.getController().addMapObject(crystal);
    }
    var event = new PEvent(3) {
      @Override
      public void execute() {
        if (getExecutions() == 0) {
          setTick(1);
          var players = npc.getController().getPlayers();
          for (var crystal : crystals) {
            for (var player : players) {
              if (player.isLocked() || !crystal.withinDistance(player, 0)) {
                continue;
              }
              var hitEvent = new HitEvent(0, player, new Hit(30 + PRandom.randomI(15)));
              player.getCombat().addHitEvent(hitEvent);
              player.setInCombatDelay(Entity.COMBAT_DELAY);
              player.getGameEncoder().sendMessage(
                  "The crystal beneath your feet grows rapidly and shunts you to the side.");
            }
            npc.getController().addMapObject(new MapObject(ObjectId.LARGE_CRYSTALS, crystal));
          }
        } else {
          stop();
          for (var crystal : crystals) {
            npc.getController().addMapObject(new MapObject(-1, crystal));
            npc.getController().sendMapGraphic(crystal, new Graphic(1353));
          }
        }
      }
    };
    olm[HEAD].getCombat().addEvent(event);
  }

  public void lightning() {
    npc.getCombat().setHitDelay(4);
    setAnimation(LEFT_HAND, 7358, true);
    var tiles = PCollection.toList(new Tile(3228, 5748), new Tile(3229, 5748), new Tile(3230, 5748),
        new Tile(3231, 5748), new Tile(3232, 5748), new Tile(3233, 5748), new Tile(3234, 5748),
        new Tile(3235, 5748), new Tile(3236, 5748), new Tile(3237, 5747), new Tile(3228, 5731),
        new Tile(3229, 5731), new Tile(3230, 5731), new Tile(3231, 5730), new Tile(3232, 5730),
        new Tile(3233, 5730), new Tile(3234, 5730), new Tile(3235, 5731), new Tile(3236, 5731),
        new Tile(3237, 5731));
    Collections.shuffle(tiles);
    var selectedTiles = new Tile[] { tiles.get(0), tiles.get(1), tiles.get(2), tiles.get(3) };
    var directions = new ArrayList<Integer>();
    for (var tile : selectedTiles) {
      directions.add((tile.getY() > 5739) ? Tile.SOUTH : Tile.NORTH);
    }
    var event = new PEvent(0) {
      @Override
      public void execute() {
        var stillWorking = false;
        var players = npc.getController().getPlayers();
        for (var i = 0; i < selectedTiles.length; i++) {
          var tile = selectedTiles[i];
          var direction = directions.get(i);
          var toTile = (new Tile(tile)).moveY(direction == Tile.NORTH ? 1 : -1);
          var routeDirection = direction == Tile.NORTH ? Route.NORTH : Route.SOUTH;
          if (Route.blocked(npc, toTile, routeDirection)) {
            continue;
          }
          stillWorking = true;
          npc.getController().sendMapGraphic(tile, new Graphic(1356));
          for (var player : players) {
            if (player.isLocked() || !tile.withinDistance(player, 0)) {
              continue;
            }
            var hitEvent = new HitEvent(0, player, new Hit(PRandom.randomI(33)));
            player.getCombat().addHitEvent(hitEvent);
            player.setInCombatDelay(Entity.COMBAT_DELAY);
            player.getPrayer().deactivate("protect from magic");
            player.getPrayer().deactivate("protect from missiles");
            player.getPrayer().deactivate("protect from melee");
            player.getPrayer().setDamageProtectionPrayerBlock(8);
            player.getController().setMagicBind(8, npc);
            player.getGameEncoder()
                .sendMessage("<col=ff0000>You've been eletrocuted to the spot!</col>");
            player.getGameEncoder()
                .sendMessage("You've been injured and can't use protection prayers!");
          }
          tile.moveY(direction == Tile.NORTH ? 1 : -1);
        }
        if (!stillWorking) {
          stop();
        }
      }
    };
    olm[HEAD].getCombat().addEvent(event);
  }

  public void swamp() {
    npc.getCombat().setHitDelay(4);
    setAnimation(LEFT_HAND, 7359, true);
    var playerMap = new HashMap<Player, Tile>();
    var players = getAttackablePlayers();
    if (players.isEmpty()) {
      return;
    }
    Collections.shuffle(players);
    if (players.size() > 1 && (players.size() % 2) != 0) {
      players.remove(players.size() - 1);
    }
    if (players.size() == 1) {
      var tile = new Tile(3228, 5731);
      tile.moveTile(PRandom.randomI(9), PRandom.randomI(16));
      playerMap.put(players.get(0), tile);
      players.get(0).getGameEncoder().sendMessage(
          "You have been paired with <col=ff0000>a random location</col>! The magical power will enact soon.");
    } else {
      for (var i = 0; i < players.size(); i += 2) {
        var player1 = players.get(i);
        var player2 = players.get(i + 1);
        playerMap.put(player1, player2);
        player1.getGameEncoder().sendMessage("You have been paired with <col=ff0000>"
            + player2.getUsername() + "</col>! The magical power will enact soon.");
        player2.getGameEncoder().sendMessage("You have been paired with <col=ff0000>"
            + player1.getUsername() + "</col>! The magical power will enact soon.");
      }
    }
    var graphicIds = new int[] { 1359, 1360, 1361, 1362 };
    var event = new PEvent(0) {
      @Override
      public void execute() {
        var graphicIndex = -1;
        for (var entry : playerMap.entrySet()) {
          graphicIndex = (graphicIndex + 1) % graphicIds.length;
          var key = entry.getKey();
          var value = entry.getValue();
          if (!key.isVisible() || !value.isVisible() || !npc.withinDistance(key, 32)
              || !npc.withinDistance(value, 32) || key.getY() < 5730 || value.getY() < 5730) {
            continue;
          }
          if (getExecutions() == 8) {
            if (key.withinDistance(value, 0)) {
              key.getGameEncoder().sendMessage("The teleport attack has no effect!");
            } else {
              key.getMovement().teleport(value);
              key.setGraphic(1039);
              var hitEvent = new HitEvent(0, key, new Hit(key.getDistance(value) * 5));
              key.getCombat().addHitEvent(hitEvent);
              key.setInCombatDelay(Entity.COMBAT_DELAY);
              if (value instanceof Player) {
                var player = (Player) value;
                player.getMovement().teleport(key);
                player.setGraphic(1039);
                var hitEvent2 = new HitEvent(0, player, new Hit(player.getDistance(key) * 5));
                player.getCombat().addHitEvent(hitEvent2);
                player.setInCombatDelay(Entity.COMBAT_DELAY);
                key.getGameEncoder()
                    .sendMessage("Yourself and " + player.getUsername() + " have swapped places!");
                player.getGameEncoder()
                    .sendMessage("Yourself and " + key.getUsername() + " have swapped places!");
              }
            }
          } else {
            var graphic = new Graphic(graphicIds[graphicIndex]);
            key.setGraphic(graphic);
            if (value instanceof Player) {
              ((Player) value).setGraphic(graphic);
            } else {
              npc.getController().sendMapGraphic(value, graphic);
            }
          }
        }
        if (getExecutions() == 8) {
          stop();
        }
      }

      @Override
      public void stopHook() {
        playerMap.clear();
      }
    };
    olm[HEAD].getCombat().addEvent(event);
  }

  public void spheres() {
    npc.getCombat().setHitDelay(4);
    setAnimation(HEAD, 7345, true);
    var players = getAttackablePlayers();
    if (players.isEmpty()) {
      return;
    }
    var projectileIds = new int[] { -1, 1345, 1343, 1341 };
    var contactIds = new int[] { -1, 1346, 1344, 1342 };
    var types = new ArrayList<HitStyleType>();
    var speed = getProjectileSpeed(12);
    for (var player : players) {
      var hitStyleType =
          HitStyleType.getRandomType(HitStyleType.MELEE, HitStyleType.RANGED, HitStyleType.MAGIC);
      types.add(hitStyleType);
      var message = "";
      if (hitStyleType == HitStyleType.MELEE) {
        message = "<col=ff0000>The Great olm fires a sphere of aggression your way.</col>";
      } else if (hitStyleType == HitStyleType.RANGED) {
        message =
            "<col=00ff00>The Great olm fires a sphere of accuracy and dexterity your way.</col>";
      } else if (hitStyleType == HitStyleType.MAGIC) {
        message = "<col=0000ff>The Great olm fires a sphere of magical power your way.</col>";
      }
      if (player.getPrayer().hasActive("protect from magic")
          || player.getPrayer().hasActive("protect from missiles")
          || player.getPrayer().hasActive("protect from melee")) {
        message += " Your prayers have been sapped.";
        player.getPrayer().deactivate("protect from magic");
        player.getPrayer().deactivate("protect from missiles");
        player.getPrayer().deactivate("protect from melee");
        player.getPrayer().changePoints(-(player.getPrayer().getPoints() / 2));
      }
      player.getGameEncoder().sendMessage(message);
      sendMapProjectile(Graphic.Projectile.builder().id(projectileIds[hitStyleType.ordinal()])
          .speed(speed).startTile(npc).entity(player).build());
      player.setGraphic(
          new Graphic(contactIds[hitStyleType.ordinal()], 124, speed.getContactDelay()));
    }
    var event = new PEvent(speed.getEventDelay()) {
      @Override
      public void execute() {
        stop();
        for (var i = 0; i < players.size(); i++) {
          var player = players.get(i);
          var type = types.get(i);
          if (!player.isVisible()
              || type == HitStyleType.MELEE && player.getPrayer().hasActive("protect from melee")
              || type == HitStyleType.RANGED
                  && player.getPrayer().hasActive("protect from missiles")
              || type == HitStyleType.MAGIC && player.getPrayer().hasActive("protect from magic")) {
            continue;
          }
          var hitEvent = new HitEvent(0, player, new Hit(player.getCombat().getHitpoints() / 2));
          player.getCombat().addHitEvent(hitEvent);
          player.setInCombatDelay(Entity.COMBAT_DELAY);
        }
      }
    };
    olm[HEAD].getCombat().addEvent(event);
  }

  public void acidSpray() {
    npc.getCombat().setHitDelay(4);
    setAnimation(HEAD, 7345, true);
    var speed = getProjectileSpeed(10);
    var pools = new ArrayList<MapObject>();
    var poolTiles = new ArrayList<Tile>();
    for (var i = 0; i < 10; i++) {
      var tile = new Tile(3228, 5730);
      tile.moveTile(PRandom.randomI(9), PRandom.randomI(18));
      if (npc.getController().getMapObjectByType(10, tile.getX(), tile.getY(),
          tile.getHeight()) != null) {
        continue;
      }
      poolTiles.add(tile);
      sendMapProjectile(
          Graphic.Projectile.builder().id(1354).speed(speed).startTile(npc).endTile(tile).build());
    }
    var event = new PEvent(speed.getEventDelay()) {
      @Override
      public void execute() {
        if (getExecutions() == 0) {
          setTick(0);
          for (var poolTile : poolTiles) {
            var pool =
                new MapObject(ObjectId.ACID_POOL, 10, MapObject.getRandomDirection(), poolTile);
            pools.add(pool);
            npc.getController().addMapObject(pool);
          }
        } else if (getExecutions() < 14) {
          for (var player : npc.getController().getPlayers()) {
            for (var pool : pools) {
              if (player.isLocked() || !pool.withinDistance(player, 0)) {
                continue;
              }
              var hitEvent =
                  new HitEvent(0, player, new Hit(3 + PRandom.randomI(3), HitMarkType.GREEN));
              player.getCombat().addHitEvent(hitEvent);
              player.setInCombatDelay(Entity.COMBAT_DELAY);
            }
          }
        } else {
          stop();
          for (var pool : pools) {
            npc.getController().addMapObject(new MapObject(-1, pool));
          }
        }
      }
    };
    olm[HEAD].getCombat().addEvent(event);
  }

  public void acidDrip() {
    var players = getAttackablePlayers();
    if (players.isEmpty()) {
      return;
    }
    Collections.shuffle(players);
    npc.getCombat().setHitDelay(4);
    setAnimation(HEAD, 7345, true);
    var selectedPlayer = players.get(0);
    var projectile = Graphic.Projectile.builder().id(1354).speed(getProjectileSpeed(selectedPlayer))
        .startTile(npc).entity(selectedPlayer).build();
    sendMapProjectile(projectile);
    selectedPlayer.getGameEncoder().sendMessage(
        "<col=ff0000>The Great Olm has smothered you in acid. It starts to drip off slowly.</col");
    if (!selectedPlayer.isPoisonImmune()) {
      selectedPlayer.setPoison(2);
    }
    var pools = new ArrayList<MapObject>();
    var times = new ArrayList<Integer>();
    var event = new PEvent(projectile.getEventDelay()) {
      @Override
      public void execute() {
        setTick(0);
        MapObject addedPool = null;
        MapObject addedPool2 = null;
        if (getExecutions() < 22 && selectedPlayer.isVisible() && !selectedPlayer.isLocked()
            && npc.withinDistance(selectedPlayer, 32) && selectedPlayer.getY() >= 5730
            && npc.getController().getMapObjectByType(10, selectedPlayer) == null) {
          addedPool =
              new MapObject(ObjectId.ACID_POOL, 10, MapObject.getRandomDirection(), selectedPlayer);
          pools.add(addedPool);
          times.add(16);
          npc.getController().addMapObject(addedPool);
          if (selectedPlayer.getMovement().isRunning()
              && selectedPlayer.getMovement().isRouting()) {
            var nextTile = selectedPlayer.getMovement().getNextTile();
            if (npc.getController().getMapObjectByType(10, nextTile) == null) {
              addedPool2 =
                  new MapObject(ObjectId.ACID_POOL, 10, MapObject.getRandomDirection(), nextTile);
              pools.add(addedPool2);
              times.add(16);
              npc.getController().addMapObject(addedPool2);
            }
          }
        }
        for (var i = 0; i < times.size(); i++) {
          if (times.get(i) == 0) {
            npc.getController().addMapObject(new MapObject(-1, pools.get(i)));
            pools.get(i).setVisible(false);
          }
          times.set(i, times.get(i) - 1);
        }
        for (var player : npc.getController().getPlayers()) {
          for (var pool : pools) {
            if (addedPool == pool || addedPool2 == pool || player.isLocked()
                || !pool.withinDistance(player, 0)) {
              continue;
            }
            var hitEvent =
                new HitEvent(0, player, new Hit(3 + PRandom.randomI(3), HitMarkType.GREEN));
            player.getCombat().addHitEvent(hitEvent);
            player.setInCombatDelay(Entity.COMBAT_DELAY);
          }
        }
        if (getExecutions() == 38) {
          stop();
          for (var pool : pools) {
            if (pool.isVisible()) {
              npc.getController().addMapObject(new MapObject(-1, pool));
            }
          }
        }
      }
    };
    olm[HEAD].getCombat().addEvent(event);
  }

  public void fallingCrystals() {
    var event = new PEvent(1) {
      @Override
      public void execute() {
        var count = 1 + PRandom.randomI(3);
        for (var i = 0; i < count; i++) {
          var tile = new Tile(3228, 5730);
          tile.moveTile(PRandom.randomI(9), PRandom.randomI(18));
          npc.getController().sendMapProjectile(null, (new Tile(tile)).moveY(1), tile, 1357, 255,
              10, 0, 51 + 120, 0, 0);
          npc.getController().sendMapGraphic(tile, new Graphic(1358, 0, 51 + 100));
          npc.getController().sendMapGraphic(tile, new Graphic(1449));
          var the = new TileHitEvent(5, npc.getController(), tile, 20, HitStyleType.TYPELESS);
          the.setAdjacentHalfDamage(true);
          olm[HEAD].getController().addEvent(the);
        }
      }
    };
    events.add(olm[HEAD].getController().addEvent(event));
  }
}
