package com.palidinodh.osrsscript.npc.plugin;

import com.google.inject.Inject;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NullNpcId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.NpcPlugin;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ NullNpcId.NULL_8032 })
class RuneDragonSpawnPlugin implements NpcPlugin {
  @Inject
  private Npc npc;
  private int countdown;

  @Override
  public void spawn() {
    countdown = 8;
  }

  @Override
  public void tick() {
    if (--countdown == 0) {
      npc.getCombat().timedDeath();
    }
    if (npc.isLocked()) {
      return;
    }
    var following = npc.getMovement().getFollowing();
    if (npc.withinDistance(following, 1) && following instanceof Player) {
      var player = (Player) following;
      player.getCombat().addHitEvent(new HitEvent(0, player, null, new Hit(
          PRandom.randomI(player.getEquipment().getFootId() == ItemId.INSULATED_BOOTS ? 2 : 8))));
    }
  }
}
