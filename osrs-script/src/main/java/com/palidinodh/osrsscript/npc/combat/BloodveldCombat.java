package com.palidinodh.osrsscript.npc.combat;

import java.util.Arrays;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSlayer;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.item.RandomItem;
import lombok.var;

class BloodveldCombat extends NpcCombat {
  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var defaultDrop = NpcCombatDrop.builder().rareDropTableRate(NpcCombatDropTable.CHANCE_1_IN_256)
        .clue(NpcCombatDrop.ClueScroll.HARD, NpcCombatDropTable.CHANCE_1_IN_256);
    var dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_1_IN_35);
    dropTable
        .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ENSOULED_BLOODVELD_HEAD_13496)));
    defaultDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLACK_BOOTS)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_MED_HELM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_CADANTINE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_LANTADYME)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_DWARF_WEED)));
    defaultDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_SQ_SHIELD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_CHAINBODY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_GUAM_LEAF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_MARRENTILL)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_TARROMIN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_HARRALANDER)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_RANARR_WEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_IRIT_LEAF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_AVANTOE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_KWUARM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BONES, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GOLD_ORE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MEAT_PIZZA)));
    defaultDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_AXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_FULL_HELM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_SCIMITAR)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.FIRE_RUNE, 60)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 10, 460)));
    defaultDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BONES)));
    defaultDrop.table(dropTable.build());


    var defaultCombat = NpcCombatDefinition.builder();
    defaultCombat.id(NpcId.BLOODVELD_76);
    defaultCombat.hitpoints(NpcCombatHitpoints.total(120));
    defaultCombat.stats(NpcCombatStats.builder().attackLevel(75).defenceLevel(30).build());
    defaultCombat.slayer(
        NpcCombatSlayer.builder().level(50).superiorId(NpcId.INSATIABLE_BLOODVELD_202).build());
    defaultCombat.deathAnimation(1553).blockAnimation(1550);
    defaultCombat.drop(defaultDrop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.builder().hitStyleType(HitStyleType.MELEE)
        .subHitStyleType(HitStyleType.MAGIC).build());
    style.damage(NpcCombatDamage.maximum(5));
    style.animation(1552).attackSpeed(4);
    defaultCombat.style(style.build());


    var mutatedDrop = NpcCombatDrop.builder().rareDropTableRate(NpcCombatDropTable.CHANCE_1_IN_256)
        .clue(NpcCombatDrop.ClueScroll.HARD, NpcCombatDropTable.CHANCE_1_IN_128);
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_1_IN_35);
    dropTable
        .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ENSOULED_BLOODVELD_HEAD_13496)));
    mutatedDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLACK_BOOTS)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_MED_HELM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_DAGGER)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_BATTLEAXE)));
    mutatedDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_BATTLEAXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_PLATEBODY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_KNIFE, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_CHAINBODY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_FULL_HELM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_LONGSWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_SCIMITAR)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SOUL_RUNE, 4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GOLD_ORE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MEAT_PIE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_BAR)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUBY_AMULET)));
    mutatedDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLACK_MED_HELM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_AXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.AIR_RUNE, 105)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.FIRE_RUNE, 75)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BOW_STRING)));
    mutatedDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BONES)));
    mutatedDrop.table(dropTable.build());


    var mutatedCombat = NpcCombatDefinition.builder();
    mutatedCombat.id(NpcId.MUTATED_BLOODVELD_123);
    mutatedCombat.hitpoints(NpcCombatHitpoints.total(170));
    mutatedCombat.stats(NpcCombatStats.builder().attackLevel(110).defenceLevel(30).build());
    mutatedCombat.slayer(NpcCombatSlayer.builder().level(50)
        .superiorId(NpcId.INSATIABLE_MUTATED_BLOODVELD_278).build());
    mutatedCombat.killCount(NpcCombatKillCount.builder().asName("Bloodveld").build());
    mutatedCombat.deathAnimation(1553).blockAnimation(1550);
    mutatedCombat.drop(mutatedDrop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.builder().hitStyleType(HitStyleType.MELEE)
        .subHitStyleType(HitStyleType.MAGIC).build());
    style.damage(NpcCombatDamage.maximum(12));
    style.animation(1552).attackSpeed(4);
    mutatedCombat.style(style.build());


    var insatiableCombat = NpcCombatDefinition.builder();
    insatiableCombat.id(NpcId.INSATIABLE_BLOODVELD_202);
    insatiableCombat.hitpoints(
        NpcCombatHitpoints.builder().total(380).barType(HitpointsBarType.GREEN_RED_60).build());
    insatiableCombat.stats(NpcCombatStats.builder().attackLevel(190).defenceLevel(85).build());
    insatiableCombat.slayer(NpcCombatSlayer.builder().level(50).experience(2900).build());
    insatiableCombat
        .killCount(NpcCombatKillCount.builder().asName("Superior Slayer Creature").build());
    insatiableCombat.deathAnimation(1553).blockAnimation(1550);
    insatiableCombat.drop(defaultDrop.rolls(3).build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.builder().hitStyleType(HitStyleType.MELEE)
        .subHitStyleType(HitStyleType.MAGIC).build());
    style.damage(NpcCombatDamage.maximum(15));
    style.animation(1552).attackSpeed(4);
    insatiableCombat.style(style.build());


    var insatiableMutatedCombat = NpcCombatDefinition.builder();
    insatiableMutatedCombat.id(NpcId.INSATIABLE_MUTATED_BLOODVELD_278);
    insatiableMutatedCombat.hitpoints(
        NpcCombatHitpoints.builder().total(410).barType(HitpointsBarType.GREEN_RED_60).build());
    insatiableMutatedCombat
        .stats(NpcCombatStats.builder().attackLevel(250).defenceLevel(130).build());
    insatiableMutatedCombat.slayer(NpcCombatSlayer.builder().level(50).experience(4100).build());
    insatiableMutatedCombat
        .killCount(NpcCombatKillCount.builder().asName("Superior Slayer Creature").build());
    insatiableMutatedCombat.deathAnimation(1553).blockAnimation(1550);
    insatiableMutatedCombat.drop(mutatedDrop.rolls(3).build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.builder().hitStyleType(HitStyleType.MELEE)
        .subHitStyleType(HitStyleType.MAGIC).build());
    style.damage(NpcCombatDamage.maximum(20));
    style.animation(1552).attackSpeed(4);
    insatiableMutatedCombat.style(style.build());


    return Arrays.asList(defaultCombat.build(), mutatedCombat.build(), insatiableCombat.build(),
        insatiableMutatedCombat.build());
  }
}
