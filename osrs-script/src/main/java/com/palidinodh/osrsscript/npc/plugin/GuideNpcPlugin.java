package com.palidinodh.osrsscript.npc.plugin;

import com.google.inject.Inject;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.NpcPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.GUIDE)
class GuideNpcPlugin implements NpcPlugin {
  @Inject
  private Npc npc;
  private int messageDelay = 50;

  @Override
  public void tick() {
    if (messageDelay-- == 0) {
      messageDelay = 50;
      npc.setForceMessage("Talk to me to give feedback!");
    }
  }
}
