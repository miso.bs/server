package com.palidinodh.osrsscript.npc.combat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.google.inject.Inject;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatMulti;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.setting.Settings;
import lombok.var;

class EventChaosElementalCombat extends NpcCombat {
  @Inject
  private Npc npc;
  private List<Npc> fanatics = new ArrayList<>();
  private boolean summonedFanatics;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop = NpcCombatDrop.builder().underKiller(true).additionalPlayers(3);


    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.CHAOS_ELEMENTAL_610_16016);
    combat.spawn(NpcCombatSpawn.builder().respawnDelay(36000).build());
    combat.hitpoints(
        NpcCombatHitpoints.builder().total(2000).barType(HitpointsBarType.GREEN_RED_120).build());
    combat.stats(NpcCombatStats.builder().attackLevel(270).magicLevel(270).rangedLevel(270)
        .defenceLevel(270).bonus(BonusType.MELEE_DEFENCE, 70).bonus(BonusType.DEFENCE_MAGIC, 70)
        .bonus(BonusType.DEFENCE_RANGED, 70).build());
    combat.aggression(NpcCombatAggression.builder().range(8).build());
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.killCount(NpcCombatKillCount.builder().sendMessage(true).build());
    combat.deathAnimation(3147);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE);
    style.damage(NpcCombatDamage.maximum(28));
    style.animation(3146).attackSpeed(5);
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(28));
    style.animation(3146).attackSpeed(5);
    style.castGraphic(new Graphic(556, 100)).targetGraphic(new Graphic(558, 100));
    style.projectile(NpcCombatProjectile.id(557));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(28));
    style.animation(3146).attackSpeed(5);
    style.castGraphic(new Graphic(556, 100)).targetGraphic(new Graphic(558, 100));
    style.projectile(NpcCombatProjectile.id(557));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    combat.style(style.build());


    return Arrays.asList(combat.build());
  }

  @Override
  public void spawnHook() {
    summonedFanatics = false;
  }

  @Override
  public void despawnHook() {
    npc.getWorld().removeNpcs(fanatics);
    fanatics.clear();
  }

  @Override
  public void tickStartHook() {
    if (fanatics.isEmpty()) {
      return;
    }
    for (var it = fanatics.iterator(); it.hasNext();) {
      var npc2 = it.next();
      if (npc2.getCombat().isDead()) {
        it.remove();
      }
    }
  }

  @Override
  public void npcApplyHitEndHook(Hit hit) {
    if (npc.getCombat().isDead()
        || npc.getCombat().getHitpoints() > npc.getCombat().getMaxHitpoints() / 2) {
      return;
    }
    if (summonedFanatics) {
      return;
    }
    summonedFanatics = true;
    fanatics.add(npc.getController().addNpc(new NpcSpawn(NpcId.CHAOS_FANATIC_202,
        new Tile(npc.getX() - 1, npc.getY() + 2, npc.getHeight()))));
    fanatics.add(npc.getController().addNpc(new NpcSpawn(NpcId.CHAOS_FANATIC_202,
        new Tile(npc.getX() + 1, npc.getY() - 2, npc.getHeight()))));
    for (var npc2 : fanatics) {
      npc2.getSpawn().moveDistance(4);
    }
  }

  @Override
  public void attackedActionsOpponentHook(Entity opponent) {
    if (!opponent.isPlayer()) {
      return;
    }
    var player = opponent.asPlayer();
    player.getCombat().setPKSkullDelay(PCombat.SKULL_DELAY);
  }

  @Override
  public boolean canBeAttackedHook(Entity opponent, boolean sendMessage,
      HitStyleType hitStyleType) {
    if (!opponent.isPlayer()) {
      return false;
    }
    var player = opponent.asPlayer();
    if (!fanatics.isEmpty()) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("Chaos Elemental is currently immune to attacks.");
      }
      return false;
    }
    if (player.getEquipment().wearingFullVoid(HitStyleType.MELEE)
        || player.getEquipment().wearingFullVoid(HitStyleType.RANGED)
        || player.getEquipment().wearingFullVoid(HitStyleType.MELEE)) {
      player.getGameEncoder()
          .sendMessage("Your armour doesn't seem to be effective against the Chaos Elemental.");
    }
    return true;
  }

  @Override
  public double damageReceivedHook(Entity opponent, double damage, HitStyleType hitStyleType,
      HitStyleType defenceType) {
    if (!opponent.isPlayer()) {
      return damage;
    }
    var player = opponent.asPlayer();
    if ((!npc.getController().inMultiCombat() || !opponent.getController().inMultiCombat())
        && damage > 6) {
      damage = 6;
    }
    if (player.getEquipment().wearingFullVoid(HitStyleType.MELEE)
        || player.getEquipment().wearingFullVoid(HitStyleType.RANGED)
        || player.getEquipment().wearingFullVoid(HitStyleType.MELEE)) {
      damage = 0;
    }
    return damage;
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    if (additionalPlayerLoopCount == 0) {
      npc.getController().addMapItem(new Item(ItemId.BLOODIER_KEY), dropTile, player);
    } else {
      npc.getController().addMapItem(new Item(ItemId.BLOODY_KEY), dropTile, player);
    }
    if (Settings.getInstance().isEconomy() && additionalPlayerLoopCount == 0) {
      player.getInventory().addOrDropItem(ItemId.BLOOD_MONEY, 50_000);
    }
  }
}
