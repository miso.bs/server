package com.palidinodh.osrsscript.npc.plugin;

import com.google.inject.Inject;
import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.cache.id.ObjectId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.NpcPlugin;
import com.palidinodh.osrscore.model.map.TempMapObject;
import com.palidinodh.osrsscript.player.skill.hunter.Hunter;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(NpcId.FERRET)
class HunterFerretNpcPlugin implements NpcPlugin {
  @Inject
  private Npc npc;

  @Override
  public void tick() {
    if (npc.getMovement().getWalkDir() == -1) {
      return;
    }
    if (npc.isLocked()) {
      return;
    }
    var mapObject = npc.getController().getSolidMapObject(npc);
    if (mapObject == null || mapObject.getId() != ObjectId.BOX_TRAP_9380) {
      return;
    }
    if (!(mapObject.getAttachment() instanceof TempMapObject)
        || !(((TempMapObject) mapObject.getAttachment()).getAttachment() instanceof Integer)) {
      return;
    }
    var player = Main.getWorld()
        .getPlayerById((Integer) ((TempMapObject) mapObject.getAttachment()).getAttachment());
    if (player == null || !Hunter.canCaptureTrap(player, ObjectId.SHAKING_BOX_9384)) {
      return;
    }
    if (Hunter.success(player, npc,
        Hunter.getCapturedTrapLevelRequirement(ObjectId.SHAKING_BOX_9384))) {
      mapObject.setId(ObjectId.SHAKING_BOX_9384);
      npc.getCombat().timedDeath(2);
    } else {
      mapObject.setId(ObjectId.BOX_TRAP_9385);
    }
    ((TempMapObject) mapObject.getAttachment()).setTick(Hunter.TRAP_EXPIRIY);
    npc.getController().sendMapObject(mapObject);
  }
}
