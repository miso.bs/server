package com.palidinodh.osrsscript.npc.plugin;

import com.google.inject.Inject;
import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.cache.id.ObjectId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.NpcPlugin;
import com.palidinodh.osrscore.model.map.TempMapObject;
import com.palidinodh.osrsscript.player.skill.hunter.Hunter;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ NpcId.SWAMP_LIZARD, NpcId.ORANGE_SALAMANDER, NpcId.RED_SALAMANDER,
    NpcId.BLACK_SALAMANDER })
class HunterLizardNpcPlugin implements NpcPlugin {
  @Inject
  private Npc npc;

  @Override
  public void tick() {
    if (npc.getMovement().getWalkDir() == -1) {
      return;
    }
    var asObjectId = -1;
    if (npc.getId() == NpcId.SWAMP_LIZARD) {
      asObjectId = ObjectId.NET_TRAP_9004;
    } else if (npc.getId() == NpcId.ORANGE_SALAMANDER) {
      asObjectId = ObjectId.NET_TRAP_8734;
    } else if (npc.getId() == NpcId.RED_SALAMANDER) {
      asObjectId = ObjectId.NET_TRAP_8986;
    } else if (npc.getId() == NpcId.BLACK_SALAMANDER) {
      asObjectId = ObjectId.NET_TRAP_8996;
    }
    if (npc.isLocked() || asObjectId == -1) {
      return;
    }
    var mapObject = npc.getController().getSolidMapObject(npc);
    if (mapObject == null || mapObject.getId() != ObjectId.NET_TRAP_9343) {
      return;
    }
    if (!(mapObject.getAttachment() instanceof TempMapObject)
        || !(((TempMapObject) mapObject.getAttachment()).getAttachment() instanceof Integer)) {
      return;
    }
    var player = Main.getWorld()
        .getPlayerById((Integer) ((TempMapObject) mapObject.getAttachment()).getAttachment());
    if (player == null || !Hunter.canCaptureTrap(player, asObjectId)) {
      return;
    }
    ((TempMapObject) mapObject.getAttachment()).resetMapObject(0);
    if (Hunter.success(player, npc, Hunter.getCapturedTrapLevelRequirement(asObjectId))) {
      ((TempMapObject) mapObject.getAttachment()).getTempMapObject(1).setId(asObjectId);
      npc.getCombat().timedDeath(2);
    } else {
      ((TempMapObject) mapObject.getAttachment()).getTempMapObject(1).setId(ObjectId.NET_TRAP_8973);
    }
    ((TempMapObject) mapObject.getAttachment()).setTick(Hunter.TRAP_EXPIRIY);
    npc.getController().sendMapObject(mapObject);
  }
}
