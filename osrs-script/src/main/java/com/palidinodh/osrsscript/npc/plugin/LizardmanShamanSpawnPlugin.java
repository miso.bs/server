package com.palidinodh.osrsscript.npc.plugin;

import com.google.inject.Inject;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.NpcPlugin;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ NpcId.SPAWN_6768 })
class LizardmanShamanSpawnPlugin implements NpcPlugin {
  @Inject
  private Npc npc;
  private int countdown1;
  private int countdown2;

  public Object script(String name, Object... args) {
    if (name.startsWith("countdown1")) {
      countdown1 = (int) args[0];
    }
    return null;
  }

  @Override
  public void spawn() {
    countdown1 = 5;
    countdown2 = 2;
  }

  @Override
  public void tick() {
    if (countdown1 > 0) {
      countdown1--;
      if (countdown1 == 1) {
        npc.getMovement().setFollowing(null);
        npc.getMovement().clear();
      } else if (countdown1 == 0) {
        npc.getCombat().timedDeath(countdown2);
        npc.setAnimation(7159);
      }
    } else if (countdown2 > 0) {
      countdown2--;
      if (countdown2 == 0) {
        npc.getController().sendMapGraphic(npc, new Graphic(1295));
        for (var player : npc.getController().getPlayers()) {
          if (npc.withinDistance(player, 1)) {
            var hitEvent = new HitEvent(0, player, new Hit(4 + PRandom.randomI(6)));
            player.getCombat().addHitEvent(hitEvent);
          }
        }
      }
    }
  }
}
