package com.palidinodh.osrsscript.npc.combat;

import java.util.Arrays;
import java.util.List;
import com.google.inject.Inject;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.random.PRandom;
import lombok.var;

class YtMejkotCombat extends NpcCombat {
  @Inject
  private Npc npc;
  private int healDelay;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.YT_MEJKOT_180).id(NpcId.YT_MEJKOT_180_3124);
    combat.hitpoints(NpcCombatHitpoints.total(80));
    combat.stats(NpcCombatStats.builder().attackLevel(160).magicLevel(120).rangedLevel(240)
        .defenceLevel(120).build());
    combat.aggression(NpcCombatAggression.PLAYERS);
    combat.deathAnimation(2638).blockAnimation(2635);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(25));
    style.animation(2637).attackSpeed(4);
    combat.style(style.build());


    return Arrays.asList(combat.build());
  }

  @Override
  public void tickStartHook() {
    if (!npc.getCombat().isDead() && npc.isVisible() && npc.getCombat().getHitDelay() == 0
        && npc.getCombat().getHitpoints() < npc.getCombat().getMaxHitpoints() / 2
        && healDelay-- <= 0) {
      healDelay = 2;
      npc.getCombat().setHitDelay(4);
      npc.setAnimation(2639);
      npc.setGraphic(444, 100);
      npc.getCombat().changeHitpoints(1 + PRandom.randomE(9), 0);
    }
  }
}
