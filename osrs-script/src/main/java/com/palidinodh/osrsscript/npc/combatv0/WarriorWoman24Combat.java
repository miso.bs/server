package com.palidinodh.osrsscript.npc.combatv0;

import java.util.Arrays;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.item.RandomItem;
import lombok.var;

class WarriorWoman24Combat extends NpcCombat {
  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop = NpcCombatDrop.builder();
    var dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BONES)));
    drop.table(dropTable.build());


    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.WARRIOR_WOMAN_24);
    combat.stats(NpcCombatStats.builder().attackLevel(22).defenceLevel(22)
        .bonus(BonusType.ATTACK_STAB, 6).bonus(BonusType.ATTACK_SLASH, 6)
        .bonus(BonusType.ATTACK_CRUSH, 6).bonus(BonusType.ATTACK_MAGIC, 6)
        .bonus(BonusType.ATTACK_RANGED, 6).bonus(BonusType.DEFENCE_STAB, 40)
        .bonus(BonusType.DEFENCE_SLASH, 41).bonus(BonusType.DEFENCE_CRUSH, 37)
        .bonus(BonusType.DEFENCE_MAGIC, -10).bonus(BonusType.DEFENCE_RANGED, 38).build());
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(3));
    style.animation(451).attackSpeed(6);
    combat.style(style.build());


    return Arrays.asList(combat.build());
  }
}
