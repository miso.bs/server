package com.palidinodh.osrsscript.npc.combat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.google.inject.Inject;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatMulti;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.special.NpcCombatTargetTile;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import lombok.var;

class VetionCombat extends NpcCombat {
  private static final NpcCombatStyle SHOCKWAVE =
      NpcCombatStyle.builder().type(NpcCombatStyleType.MAGIC).animation(5507).attackSpeed(4)
          .multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE)
          .damage(NpcCombatDamage.builder().maximum(45).ignorePrayer(true).build()).build();

  @Inject
  private Npc npc;
  private int specialAttackDelay;
  private List<Npc> hellhounds = new ArrayList<>();
  private boolean summonedHellhounds;
  private boolean summonedHellhounds2;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop = NpcCombatDrop.builder()
        .clue(NpcCombatDrop.ClueScroll.ELITE, NpcCombatDropTable.CHANCE_1_IN_100)
        .additionalPlayers(3);
    var dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_1_IN_4096)
        .broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.VESTAS_LONGSWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STATIUSS_WARHAMMER)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.VESTAS_SPEAR)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MORRIGANS_JAVELIN, 100)));
    dropTable
        .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MORRIGANS_THROWING_AXE, 100)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ZURIELS_STAFF)));
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_1_IN_2000).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.VETION_JR)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_1_IN_512).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RING_OF_THE_GODS)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_1_IN_256).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_2H_SWORD)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_1_IN_170).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_PICKAXE)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_1_IN_128);
    dropTable
        .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_RANARR_WEED_NOTED, 100)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_2H_SWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CANNONBALL, 250)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAGIC_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PALM_TREE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.YEW_SEED)));
    dropTable
        .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MORT_MYRE_FUNGUS_NOTED, 200)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DARK_FISHING_BAIT, 375)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_STAFF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHAOS_RUNE, 400)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 300)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 200)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SANFEW_SERUM_4_NOTED, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAGIC_LOGS_NOTED, 100)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GOLD_ORE_NOTED, 300)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_BONES_NOTED, 100)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_DRAGONSTONE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.OAK_PLANK_NOTED, 300)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SUPERCOMPOST_NOTED, 100)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_TORSTOL_NOTED, 1, 5)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_PICKAXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DARK_CRAB_NOTED, 8)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SUPER_RESTORE_4, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 15000, 20000)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.OGRE_COFFIN_KEY_NOTED, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LIMPWURT_ROOT_NOTED, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_RUBY_NOTED, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_DIAMOND_NOTED, 10)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BIG_BONES)));
    drop.table(dropTable.build());


    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.VETION_454);
    combat.spawn(NpcCombatSpawn.builder().respawnId(NpcId.VETION_REBORN_454).deathDelay(8).build());
    combat.hitpoints(NpcCombatHitpoints.total(255));
    combat.stats(NpcCombatStats.builder().attackLevel(430).magicLevel(300).defenceLevel(395)
        .bonus(BonusType.DEFENCE_STAB, 201).bonus(BonusType.DEFENCE_SLASH, 200)
        .bonus(BonusType.DEFENCE_MAGIC, 250).bonus(BonusType.DEFENCE_RANGED, 270).build());
    combat.aggression(NpcCombatAggression.builder().range(8).build());
    combat.immunity(NpcCombatImmunity.builder().venom(true).build());
    combat.killCount(NpcCombatKillCount.builder().sendMessage(true).build());
    combat.type(NpcCombatType.UNDEAD).blockAnimation(5508);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(30));
    style.animation(5499).attackSpeed(4);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.builder().hitStyleType(HitStyleType.MAGIC)
        .subHitStyleType(HitStyleType.TYPELESS).build());
    style.damage(NpcCombatDamage.maximum(30));
    style.animation(5499).attackSpeed(4);
    style.targetTileGraphic(new Graphic(749));
    style.projectile(NpcCombatProjectile.builder().id(280).speedMinimumDistance(10).build());
    var targetTile = NpcCombatTargetTile.builder();
    targetTile.breakOff(NpcCombatTargetTile.BreakOff.builder().count(2).distance(2).build());
    style.specialAttack(targetTile.build());
    combat.style(style.build());


    var rebornCombat = NpcCombatDefinition.builder();
    rebornCombat.id(NpcId.VETION_REBORN_454);
    rebornCombat.hitpoints(NpcCombatHitpoints.total(255));
    rebornCombat.stats(NpcCombatStats.builder().attackLevel(430).magicLevel(300).defenceLevel(395)
        .bonus(BonusType.DEFENCE_STAB, 201).bonus(BonusType.DEFENCE_SLASH, 200)
        .bonus(BonusType.DEFENCE_MAGIC, 250).bonus(BonusType.DEFENCE_RANGED, 270).build());
    rebornCombat.aggression(NpcCombatAggression.builder().range(8).build());
    rebornCombat.immunity(NpcCombatImmunity.builder().venom(true).build());
    rebornCombat
        .killCount(NpcCombatKillCount.builder().asName("Vet'ion").sendMessage(true).build());
    rebornCombat.type(NpcCombatType.UNDEAD).deathAnimation(5509).blockAnimation(5508);
    rebornCombat.drop(drop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(30));
    style.animation(5499).attackSpeed(4);
    rebornCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.builder().hitStyleType(HitStyleType.MAGIC)
        .subHitStyleType(HitStyleType.TYPELESS).build());
    style.damage(NpcCombatDamage.maximum(30));
    style.animation(5499).attackSpeed(4);
    style.targetTileGraphic(new Graphic(749));
    style.projectile(NpcCombatProjectile.builder().id(280).speedMinimumDistance(10).build());
    targetTile = NpcCombatTargetTile.builder();
    targetTile.breakOff(NpcCombatTargetTile.BreakOff.builder().count(2).distance(2).build());
    style.specialAttack(targetTile.build());
    rebornCombat.style(style.build());


    return Arrays.asList(combat.build(), rebornCombat.build());
  }

  @Override
  public void spawnHook() {
    specialAttackDelay = 0;
    summonedHellhounds = false;
    summonedHellhounds2 = false;
  }

  @Override
  public void despawnHook() {
    if (!hellhounds.isEmpty()) {
      for (var npc : hellhounds) {
        npc.getWorld().removeNpc(npc);
      }
    }
    hellhounds.clear();
  }

  @Override
  public void tickStartHook() {
    if (specialAttackDelay > 0) {
      specialAttackDelay--;
    }
    if (!hellhounds.isEmpty()) {
      for (var it = hellhounds.iterator(); it.hasNext();) {
        var npc = it.next();
        if (npc.getCombat().isDead()) {
          it.remove();
        }
      }
    }
  }

  @Override
  public HitStyleType attackTickHitStyleTypeHook(HitStyleType hitStyleType, Entity opponent) {
    return npc.withinDistance(opponent, 1) && PRandom.randomE(2) == 0 ? HitStyleType.MELEE
        : hitStyleType;
  }

  @Override
  public void npcApplyHitEndHook(Hit hit) {
    if (!npc.getCombat().isDead()
        && npc.getCombat().getHitpoints() <= npc.getCombat().getMaxHitpoints() / 2) {
      if (npc.getId() == NpcId.VETION_454 && !summonedHellhounds) {
        npc.setForceMessage("Kill, my pets!");
        summonedHellhounds = true;
        hellhounds.add(npc.getController().addNpc(new NpcSpawn(NpcId.SKELETON_HELLHOUND_214,
            new Tile(npc.getX() - 1, npc.getY() - 2, npc.getHeight()))));
        hellhounds.add(npc.getController().addNpc(new NpcSpawn(NpcId.SKELETON_HELLHOUND_214,
            new Tile(npc.getX() + 1, npc.getY() - 2, npc.getHeight()))));
        for (var npc : hellhounds) {
          npc.getSpawn().moveDistance(4);
        }
      } else if (npc.getId() == NpcId.VETION_REBORN_454 && !summonedHellhounds2) {
        npc.setForceMessage("Bahh! Go, Dogs!!");
        summonedHellhounds2 = true;
        hellhounds.add(npc.getController().addNpc(new NpcSpawn(NpcId.GREATER_SKELETON_HELLHOUND_281,
            new Tile(npc.getX() - 1, npc.getY() - 2, npc.getHeight()))));
        hellhounds.add(npc.getController().addNpc(new NpcSpawn(NpcId.GREATER_SKELETON_HELLHOUND_281,
            new Tile(npc.getX() + 1, npc.getY() - 2, npc.getHeight()))));
        for (var npc : hellhounds) {
          npc.getSpawn().moveDistance(4);
        }
      }
    }
  }

  @Override
  public NpcCombatStyle attackTickCombatStyleHook(NpcCombatStyle combatStyle, Entity opponent) {
    if (specialAttackDelay > 0 || PRandom.randomE(16) != 0) {
      return combatStyle;
    }
    specialAttackDelay = 10;
    return SHOCKWAVE;
  }

  @Override
  public void applyAttackEndHook(NpcCombatStyle combatStyle, Entity opponent,
      int applyAttackLoopCount, HitEvent hitEvent) {
    if (combatStyle == SHOCKWAVE && opponent instanceof Player) {
      var player = opponent.asPlayer();
      player.getGameEncoder().sendMessage(
          "Vet'ion pummels the ground sending a shattering earthquake shockwave through you.");
    }
  }

  @Override
  public boolean canBeAttackedHook(Entity opponent, boolean sendMessage,
      HitStyleType hitStyleType) {
    if (!hellhounds.isEmpty()) {
      if (sendMessage && opponent instanceof Player) {
        var player = opponent.asPlayer();
        player.getGameEncoder().sendMessage("Vet'ion is currently immune to attacks.");
      }
      return false;
    }
    return true;
  }
}
