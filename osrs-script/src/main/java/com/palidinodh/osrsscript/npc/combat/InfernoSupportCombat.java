package com.palidinodh.osrsscript.npc.combat;

import java.util.Arrays;
import java.util.List;
import com.google.inject.Inject;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.map.MapObject;
import lombok.var;

class InfernoSupportCombat extends NpcCombat {
  @Inject
  private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.ROCKY_SUPPORT).id(NpcId.ROCKY_SUPPORT_1);
    combat.hitpoints(NpcCombatHitpoints.total(175));
    combat.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());
    combat.deathAnimation(7561);


    return Arrays.asList(combat.build());
  }

  @Override
  public void applyDeadHook() {
    if (npc.getId() == NpcId.ROCKY_SUPPORT_1 && getRespawnDelay() == 1) {
      npc.getController().addMapObject(new MapObject(-1, 10, 0, npc));
      npc.setId(NpcId.ROCKY_SUPPORT);
    }
    if (getRespawnDelay() == 0) {
      for (var player : npc.getWorld().getPlayers()) {
        if (player.isLocked() || !npc.withinDistance(player, 1)) {
          continue;
        }
        var hitEvent = new HitEvent(0, player, new Hit(49));
        player.getCombat().addHitEvent(hitEvent);
      }
      for (var npc2 : npc.getWorld().getNPCs()) {
        if (npc2.isLocked() || !npc.withinDistance(npc2, 1) || npc2 == npc) {
          continue;
        }
        var hitEvent = new HitEvent(0, npc2, new Hit(49));
        npc2.getCombat().addHitEvent(hitEvent);
      }
    }
  }

  @Override
  public boolean canBeAttackedHook(Entity opponent, boolean sendMessage,
      HitStyleType hitStyleType) {
    return false;
  }
}
