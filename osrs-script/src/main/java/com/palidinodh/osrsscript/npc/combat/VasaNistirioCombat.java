package com.palidinodh.osrsscript.npc.combat;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import com.google.inject.Inject;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.cache.id.NullObjectId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitMarkType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.TileHitEvent;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatMulti;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.special.NpcCombatTargetTile;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Magic;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PNumber;
import lombok.var;

class VasaNistirioCombat extends NpcCombat {
  private static final Tile SPAWN_TILE = new Tile(3309, 5293);
  private static final int CRYSTAL_INACTIVE = 0, CRYSTAL_MOVING = 1, CRYSTAL_HEALING = 2;
  private static final Tile[] CRYSTAL_TILES =
      { new Tile(3301, 5285), new Tile(3319, 5285), new Tile(3319, 5302), new Tile(3301, 5302) };
  private static final int SPECIAL_ATTACK_INACTIVE = 0, SPECIAL_ATTACK_START = 1,
      SPECIAL_ATTACK_END = 2;
  private static final Tile[] SPECIAL_ATTACK_TILES =
      { new Tile(3308, 5292), new Tile(3311, 5292), new Tile(3314, 5292), new Tile(3308, 5298),
          new Tile(3311, 5298), new Tile(3314, 5298), new Tile(3308, 5295), new Tile(3314, 5295) };
  private static final Tile[] SPECIAL_ATTACK_AWAY_TILES =
      { new Tile(3316, 5285), new Tile(3300, 5296), new Tile(3322, 5296), new Tile(3311, 5305) };
  private static final Tile[] SPECIAL_ATTACK_CLOSE_TILES =
      { new Tile(3311, 5298), new Tile(3314, 5295), new Tile(3311, 5292), new Tile(3308, 5295) };
  private static final Tile[][] CRYSTAL_PATHS = {
      { new Tile(3309, 5293), new Tile(3307, 5293), new Tile(3307, 5294), new Tile(3299, 5294),
          new Tile(3299, 5293), new Tile(3299, 5289) },
      { new Tile(3309, 5293), new Tile(3309, 5284), new Tile(3310, 5284), new Tile(3314, 5284) },
      { new Tile(3309, 5293), new Tile(3319, 5293), new Tile(3319, 5294), new Tile(3319, 5297) },
      { new Tile(3309, 5293), new Tile(3310, 5293), new Tile(3310, 5294), new Tile(3310, 5302),
          new Tile(3309, 5302), new Tile(3305, 5302) } };

  @Inject
  private Npc npc;
  private boolean loaded;
  private Npc crystalNpc;
  private MapObject[] crystalObjects;
  private int specialAttack;
  private int specialAttackPhase = SPECIAL_ATTACK_START;
  private int specialAttackDamageDelay;
  private int crystalIndex;
  private int crystalPhase = CRYSTAL_INACTIVE;
  private int crystalHealingTime;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop = NpcCombatDrop.builder().additionalPlayers(255);
    var dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ENDARKENED_JUICE, 5)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TWISTED_PLUS_4, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.XERICS_AID_PLUS_4, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TWISTED_PLUS_4, 2)));
    drop.table(dropTable.build());


    var rocksCombat = NpcCombatDefinition.builder();
    rocksCombat.id(NpcId.ROCKS_7565);
    rocksCombat.hitpoints(NpcCombatHitpoints.total(400));
    rocksCombat.stats(NpcCombatStats.builder().magicLevel(230).rangedLevel(230).defenceLevel(175)
        .bonus(BonusType.ATTACK_RANGED, 100).bonus(BonusType.DEFENCE_STAB, 170)
        .bonus(BonusType.DEFENCE_SLASH, 190).bonus(BonusType.DEFENCE_CRUSH, 50)
        .bonus(BonusType.DEFENCE_MAGIC, 400).bonus(BonusType.DEFENCE_RANGED, 60).build());
    rocksCombat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).bind(true).build());
    rocksCombat.focus(NpcCombatFocus.builder().disableFacingOpponent(true)
        .disableFollowingOpponent(true).build());
    rocksCombat.deathAnimation(7415);
    rocksCombat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.builder().hitStyleType(HitStyleType.RANGED).build());
    style.damage(NpcCombatDamage.builder().maximum(30).prayerEffectiveness(0.5).build());
    style.attackSpeed(2).attackRange(7);
    style.targetTileGraphic(new Graphic(1330));
    style.projectile(NpcCombatProjectile.builder().id(1329).speedMinimumDistance(10).build());
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    var targetTile = NpcCombatTargetTile.builder().radius(1);
    style.specialAttack(targetTile.build());
    rocksCombat.style(style.build());


    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.VASA_NISTIRIO).id(NpcId.VASA_NISTIRIO_7567);
    combat.hitpoints(NpcCombatHitpoints.total(400));
    combat.stats(NpcCombatStats.builder().magicLevel(230).rangedLevel(230).defenceLevel(175)
        .bonus(BonusType.ATTACK_RANGED, 100).bonus(BonusType.DEFENCE_STAB, 170)
        .bonus(BonusType.DEFENCE_SLASH, 190).bonus(BonusType.DEFENCE_CRUSH, 50)
        .bonus(BonusType.DEFENCE_MAGIC, 400).bonus(BonusType.DEFENCE_RANGED, 60).build());
    combat.aggression(NpcCombatAggression.builder().range(12).always(true).build());
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).bind(true).build());
    combat.focus(NpcCombatFocus.builder().disableFacingOpponent(true).disableFollowingOpponent(true)
        .build());
    combat.underneathDamage(1).deathAnimation(7415);
    combat.drop(drop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.builder().hitStyleType(HitStyleType.RANGED).build());
    style.damage(
        NpcCombatDamage.builder().maximum(30).ignoreDefence(true).prayerEffectiveness(0.5).build());
    style.attackSpeed(2).attackRange(7);
    style.targetTileGraphic(new Graphic(1330));
    style.projectile(NpcCombatProjectile.builder().id(1329).speedMinimumDistance(10).build());
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    targetTile = NpcCombatTargetTile.builder().radius(1);
    style.specialAttack(targetTile.build());
    combat.style(style.build());


    return Arrays.asList(rocksCombat.build(), combat.build());
  }

  @Override
  public void spawnHook() {
    loadCrystals();
  }

  @Override
  public void tickStartHook() {
    if (!loaded) {
      loadProfile();
      return;
    }
    if (!npc.getController().isRegionLoaded()) {
      return;
    }
    if (--specialAttackDamageDelay == 0) {
      var reducedDamage = false;
      var players = npc.getController().getPlayers();
      for (var player : players) {
        if (npc.withinDistance(player, 1) && player.getPrayer().hasActive("protect from magic")) {
          reducedDamage = true;
          break;
        }
      }
      for (var t : SPECIAL_ATTACK_TILES) {
        var the = new TileHitEvent(0, npc.getController(), t, 0, HitStyleType.MAGIC);
        the.setOffender(npc);
        if (reducedDamage) {
          the.setLeaveHitpointsPercent(65);
        } else {
          the.setLeaveHitpoints(5);
        }
        addEvent(the);
      }
    }
    if (crystalObjects == null) {
      loadCrystals();
    }
    if (npc.getId() == NpcId.ROCKS_7565) {
      var players = npc.getController().getPlayers();
      for (var player : players) {
        if (npc.withinDistance(player, 4)) {
          npc.setId(NpcId.VASA_NISTIRIO);
          npc.setAnimation(7408);
          npc.getCombat().setHitDelay(5);
          npc.setLock(5);
          break;
        }
      }
    } else {
      specialAttackTick();
      crystalTick();
    }
  }

  @Override
  public boolean canAttackEntityHook(NpcCombatStyle combatStyle, Entity opponent) {
    return !npc.isLocked() && specialAttack == 0 && npc.getId() != NpcId.ROCKS_7565
        && npc.getId() != NpcId.VASA_NISTIRIO_7567;
  }

  @Override
  public boolean canBeAttackedHook(Entity opponent, boolean sendMessage,
      HitStyleType hitStyleType) {
    if (npc.isLocked() || npc.getId() == NpcId.ROCKS_7565 || specialAttack > 0) {
      return false;
    }
    return true;
  }

  @Override
  public double damageReceivedHook(Entity opponent, double damage, HitStyleType hitStyleType,
      HitStyleType defenceType) {
    return npc.getId() == NpcId.VASA_NISTIRIO_7567 ? 0 : damage;
  }

  public void loadProfile() {
    if (loaded || npc.getController().getPlayers().isEmpty()) {
      return;
    }
    loaded = true;
    npc.getSpawn().getTile().setTile(SPAWN_TILE);
    npc.setTile(npc.getSpawn().getTile());
    var averageHP = 0;
    var playerMultiplier = 1.0;
    var players = npc.getController().getPlayers();
    for (var player : players) {
      averageHP += player.getCombat().getMaxHitpoints();
      playerMultiplier = PNumber.addDoubles(playerMultiplier, 0.5);
    }
    averageHP /= players.size();
    var hitpoints = (int) ((50 + players.size() * 25 + averageHP * 2) * playerMultiplier);
    npc.getCombat().setMaxHitpoints(hitpoints);
    npc.getCombat().setHitpoints(npc.getCombat().getMaxHitpoints());
  }

  public void loadCrystals() {
    if (crystalObjects == null) {
      crystalObjects = new MapObject[CRYSTAL_TILES.length];
    }
    for (int i = 0; i < crystalObjects.length; i++) {
      var crystal = crystalObjects[i] =
          new MapObject(NullObjectId.NULL_29775, 10, PRandom.randomI(4), CRYSTAL_TILES[i]);
      npc.getController().addMapObject(crystal);
    }
    npc.getWorld().removeNpc(crystalNpc);
  }

  public void specialAttackTick() {
    if (specialAttack > 0) {
      specialAttack--;
    }
    startSpecialAttack();
    endSpecialAttack();
  }

  public void startSpecialAttack() {
    if (!npc.matchesTile(SPAWN_TILE) || npc.getCombat().getHitDelay() > 0
        || specialAttackPhase != SPECIAL_ATTACK_START || specialAttack > 0) {
      return;
    }
    specialAttackPhase = SPECIAL_ATTACK_END;
    specialAttack = 8;
    npc.getCombat().setHitDelay(specialAttack + 8);
    npc.setLock(specialAttack + 4);
    npc.setAnimation(7409);
    var players = npc.getController().getPlayers();
    if (players.size() > 1) {
      Collections.shuffle(players);
    }
    for (var i = 0; i < players.size(); i++) {
      var player = players.get(i);
      if (!player.isVisible() || player.isLocked() || !npc.withinDistance(player, 10)) {
        continue;
      }
      Tile t;
      if (i != 0 && i >= players.size() / 2) {
        t = SPECIAL_ATTACK_AWAY_TILES[PRandom.randomE(SPECIAL_ATTACK_AWAY_TILES.length)];
      } else {
        t = SPECIAL_ATTACK_CLOSE_TILES[PRandom.randomE(SPECIAL_ATTACK_CLOSE_TILES.length)];
        player.getController().setMagicBind(specialAttack + 6, npc);
        player.getPrayer().deactivate("protect from magic");
        player.getPrayer().deactivate("protect from missiles");
        player.getPrayer().deactivate("protect from melee");
        player.getPrayer().setDamageProtectionPrayerBlock(specialAttack + 6);
      }
      player.getMovement().animatedTeleport(t, Magic.ZEAH_MAGIC_ANIMATION_START, -1,
          Magic.ZEAH_MAGIC_GRAPHIC, new Graphic(80, 100), 2);
    }
  }

  public void endSpecialAttack() {
    if (!npc.matchesTile(SPAWN_TILE) || specialAttackPhase != SPECIAL_ATTACK_END
        || specialAttack != 0) {
      return;
    }
    specialAttackPhase = SPECIAL_ATTACK_INACTIVE;
    npc.setAnimation(7410);
    var speed = getProjectileSpeed(10);
    specialAttackDamageDelay = speed.getEventDelay();
    for (var t : SPECIAL_ATTACK_TILES) {
      var projectile = Graphic.Projectile.builder().id(1327).startTile(npc).endTile(t).endHeight(1)
          .speed(speed).build();
      sendMapProjectile(projectile);
      npc.getController().sendMapGraphic(t, new Graphic(1328, 0, projectile.getContactDelay()));
    }
  }

  public void crystalTick() {
    if (crystalObjects == null) {
      return;
    }
    if (crystalHealingTime > 0) {
      crystalHealingTime--;
    }
    startCrystal();
    movingCrystal();
    healingCrystal();
  }

  public void startCrystal() {
    if (!npc.matchesTile(SPAWN_TILE) || npc.getCombat().getHitDelay() > 0
        || crystalPhase != CRYSTAL_INACTIVE || specialAttackPhase != SPECIAL_ATTACK_INACTIVE) {
      return;
    }
    var newCrystalIndex = -1;
    while ((newCrystalIndex = PRandom.randomE(CRYSTAL_PATHS.length)) == crystalIndex) {

    }
    crystalIndex = newCrystalIndex;
    crystalPhase = CRYSTAL_MOVING;
    npc.getMovement().clear();
    for (var t : CRYSTAL_PATHS[crystalIndex]) {
      npc.getMovement().addMovement(t);
    }
  }

  public void movingCrystal() {
    if (crystalPhase != CRYSTAL_MOVING || npc.getCombat().getHitDelay() > 0) {
      return;
    }
    if (!npc.matchesTile(CRYSTAL_PATHS[crystalIndex][CRYSTAL_PATHS[crystalIndex].length - 1])) {
      return;
    }
    crystalPhase = CRYSTAL_HEALING;
    crystalHealingTime = 50 + PRandom.randomI(25);
    npc.setId(NpcId.VASA_NISTIRIO_7567);
    npc.setAnimation(7412);
    crystalObjects[crystalIndex].setId(NullObjectId.NULL_29774);
    npc.getController().addMapObject(crystalObjects[crystalIndex]);
    crystalNpc = npc.getController()
        .addNpc(new NpcSpawn(NpcId.GLOWING_CRYSTAL, CRYSTAL_TILES[crystalIndex]));
    crystalNpc.getCombat().setMaxHitpoints(npc.getCombat().getMaxHitpoints() / 4);
    crystalNpc.getCombat().setHitpoints(crystalNpc.getCombat().getMaxHitpoints());
    crystalNpc.getController().setMultiCombatFlag(true);
  }

  public void healingCrystal() {
    if (crystalPhase != CRYSTAL_HEALING) {
      return;
    }
    for (var player : npc.getController().getPlayers()) {
      if (player.isLocked() || !npc.withinDistance(player, 0)) {
        continue;
      }
      player.getCombat().applyHit(new Hit(npc.getCombatDef().getUnderneathDamage()));
    }
    if (!crystalNpc.getCombat().isDead() && crystalHealingTime > 0) {
      return;
    }
    crystalPhase = CRYSTAL_INACTIVE;
    crystalHealingTime = 0;
    npc.setId(NpcId.VASA_NISTIRIO);
    npc.setAnimation(7414);
    npc.getCombat().setHitDelay(4);
    npc.unlock();
    crystalObjects[crystalIndex].setId(NullObjectId.NULL_29775);
    npc.getController().addMapObject(crystalObjects[crystalIndex]);
    npc.getMovement().clear();
    for (var i = CRYSTAL_PATHS[crystalIndex].length - 1; i >= 0; i--) {
      npc.getMovement().addMovement(CRYSTAL_PATHS[crystalIndex][i]);
    }
    if (crystalNpc.isVisible() && !crystalNpc.getCombat().isDead()) {
      specialAttackPhase = SPECIAL_ATTACK_START;
      npc.getCombat().applyHit(
          new Hit((int) Math.min(npc.getCombat().getMaxHitpoints() - npc.getCombat().getHitpoints(),
              npc.getCombat().getMaxHitpoints() * 0.2), HitMarkType.MAGENTA));
    }
    npc.getWorld().removeNpc(crystalNpc);
  }
}
