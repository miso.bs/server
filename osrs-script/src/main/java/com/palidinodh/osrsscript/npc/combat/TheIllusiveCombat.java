package com.palidinodh.osrsscript.npc.combat;

import java.util.Arrays;
import java.util.List;
import com.google.inject.Inject;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import lombok.var;

class TheIllusiveCombat extends NpcCombat {
  private static final Tile[] BURROWS = { new Tile(1816, 5145), new Tile(1824, 5145),
      new Tile(1834, 5145), new Tile(1816, 5152), new Tile(1824, 5152), new Tile(1834, 5152),
      new Tile(1816, 5160), new Tile(1824, 5160), new Tile(1834, 5160) };

  @Inject
  private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.THE_ILLUSIVE_108);
    combat.hitpoints(NpcCombatHitpoints.total(140));
    combat.aggression(NpcCombatAggression.PLAYERS);
    combat.deathAnimation(6349);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(8));
    style.animation(6342).attackSpeed(6);
    combat.style(style.build());


    return Arrays.asList(combat.build());
  }

  @Override
  public double damageReceivedHook(Entity opponent, double damage, HitStyleType hitStyleType,
      HitStyleType defenceType) {
    if (damage > 0 && PRandom.randomE(4) == 0) {
      opponent.setAttacking(false);
      burrow();
    }
    return damage;
  }

  public void burrow() {
    var teleportTile = new Tile(BURROWS[PRandom.randomE(BURROWS.length)]);
    teleportTile.setHeight(npc.getHeight());
    npc.getMovement().animatedTeleport(teleportTile, 6340, 6341, null, null, 4);
  }
}
