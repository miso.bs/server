package com.palidinodh.osrsscript.npc.combat;

import java.util.Arrays;
import java.util.List;
import com.google.inject.Inject;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.HitMarkType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import lombok.var;

class TheNightmareParasiteCombat extends NpcCombat {
  @Inject
  private Npc npc;
  private Npc theNightmare;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.PARASITE_57).id(NpcId.PARASITE_86);
    combat.hitpoints(NpcCombatHitpoints.total(40));
    combat.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());
    combat.deathAnimation(8559);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.builder().hitStyleType(HitStyleType.MAGIC)
        .hitMarkType(HitMarkType.MAGENTA).build());
    style.damage(NpcCombatDamage.maximum(100));
    style.animation(8554).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(1774));
    combat.style(style.build());


    return Arrays.asList(combat.build());
  }

  @Override
  public boolean canAttackEntityHook(NpcCombatStyle combatStyle, Entity opponent) {
    return opponent == theNightmare && theNightmare.getCombatDef().getHitpoints()
        .getBarType() == HitpointsBarType.CYAN_DARKER_140;
  }

  @Override
  public void setTargetHook(Entity target) {
    if (target == null || !target.isNpc()) {
      return;
    }
    theNightmare = target.asNpc();
    npc.getCombat().startAttacking(target);
    setTarget(null);
  }
}
