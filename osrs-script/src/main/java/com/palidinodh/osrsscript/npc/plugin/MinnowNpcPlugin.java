package com.palidinodh.osrsscript.npc.plugin;

import com.google.inject.Inject;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.NpcPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ NpcId.FISHING_SPOT_7730, NpcId.FISHING_SPOT_7731 })
class MinnowNpcPlugin implements NpcPlugin {
  @Inject
  private Npc npc;
  private int moveDelay;

  @Override
  public void spawn() {
    moveDelay = 25 + (npc.getId() == NpcId.FISHING_SPOT_7731 ? 3 : 0);
  }

  @Override
  public void tick() {
    if (--moveDelay > 0) {
      return;
    }
    moveDelay = 25;
    npc.getMovement().clear();
    if (npc.getY() == 3443 || npc.getY() == 3493) {
      if (npc.getX() == 2609 || npc.getX() == 2617 || npc.getX() == 3119) {
        npc.getMovement().addMovement(npc.getX(), npc.getY() + 1);
      } else {
        npc.getMovement().addMovement(npc.getX() - 1, npc.getY());
      }
    } else if (npc.getY() == 3444 || npc.getY() == 3494) {
      if (npc.getX() == 2612 || npc.getX() == 2620 || npc.getX() == 3122) {
        npc.getMovement().addMovement(npc.getX(), npc.getY() - 1);
      } else {
        npc.getMovement().addMovement(npc.getX() + 1, npc.getY());
      }
    }
  }
}
