package com.palidinodh.osrsscript.npc.combat;

import java.util.Arrays;
import java.util.List;
import com.google.inject.Inject;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatEffect;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatMulti;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.setting.Settings;
import lombok.var;

class EventSotetsegCombat extends NpcCombat {
  @Inject
  private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop = NpcCombatDrop.builder().underKiller(true).additionalPlayers(3);


    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.SOTETSEG_995_16017);
    combat.spawn(NpcCombatSpawn.builder().respawnDelay(36000).build());
    combat.hitpoints(
        NpcCombatHitpoints.builder().total(2000).barType(HitpointsBarType.GREEN_RED_120).build());
    combat.stats(NpcCombatStats.builder().attackLevel(250).magicLevel(250).rangedLevel(250)
        .defenceLevel(200).bonus(BonusType.MELEE_DEFENCE, 70).bonus(BonusType.DEFENCE_MAGIC, 30)
        .bonus(BonusType.DEFENCE_RANGED, 150).build());
    combat.aggression(NpcCombatAggression.builder().range(8).build());
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.killCount(NpcCombatKillCount.builder().sendMessage(true).build());
    combat.deathAnimation(8140);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.builder().maximum(50).delayedPrayerProtectable(true).build());
    style.animation(8138).attackSpeed(5);
    style.projectile(NpcCombatProjectile.builder().id(1607).speedType(HitStyleType.MAGIC)
        .speedMinimumDistance(14).build());
    style.effect(NpcCombatEffect.builder().damageProtectionPrayerBlock(8).build());
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(50).delayedPrayerProtectable(true).build());
    style.animation(8138).attackSpeed(5);
    style.projectile(NpcCombatProjectile.builder().id(1606).speedType(HitStyleType.MAGIC)
        .speedMinimumDistance(14).build());
    style.effect(NpcCombatEffect.builder().damageProtectionPrayerBlock(8).build());
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    combat.style(style.build());


    return Arrays.asList(combat.build());
  }

  @Override
  public void attackedActionsOpponentHook(Entity opponent) {
    if (!opponent.isPlayer()) {
      return;
    }
    var player = opponent.asPlayer();
    player.getCombat().setPKSkullDelay(PCombat.SKULL_DELAY);
  }

  @Override
  public boolean canBeAttackedHook(Entity opponent, boolean sendMessage,
      HitStyleType hitStyleType) {
    if (!opponent.isPlayer()) {
      return false;
    }
    var player = opponent.asPlayer();
    if (player.getEquipment().wearingFullVoid(HitStyleType.MELEE)
        || player.getEquipment().wearingFullVoid(HitStyleType.RANGED)
        || player.getEquipment().wearingFullVoid(HitStyleType.MELEE)) {
      player.getGameEncoder()
          .sendMessage("Your armour doesn't seem to be effective against the Chaos Elemental.");
    }
    return true;
  }

  @Override
  public double damageReceivedHook(Entity opponent, double damage, HitStyleType hitStyleType,
      HitStyleType defenceType) {
    if (!opponent.isPlayer()) {
      return damage;
    }
    var player = opponent.asPlayer();
    if (player.getEquipment().wearingFullVoid(HitStyleType.MELEE)
        || player.getEquipment().wearingFullVoid(HitStyleType.RANGED)
        || player.getEquipment().wearingFullVoid(HitStyleType.MELEE)) {
      damage = 0;
    }
    return damage;
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    if (additionalPlayerLoopCount == 0) {
      npc.getController().addMapItem(new Item(ItemId.BLOODIER_KEY), dropTile, player);
    } else {
      npc.getController().addMapItem(new Item(ItemId.BLOODY_KEY), dropTile, player);
    }
    if (Settings.getInstance().isEconomy() && additionalPlayerLoopCount == 0) {
      player.getInventory().addOrDropItem(ItemId.BLOOD_MONEY, 50_000);
    }
  }
}
