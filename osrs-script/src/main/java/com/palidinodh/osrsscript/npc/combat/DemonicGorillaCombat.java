package com.palidinodh.osrsscript.npc.combat;

import java.util.Arrays;
import java.util.List;
import com.google.inject.Inject;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.TileHitEvent;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import lombok.var;

class DemonicGorillaCombat extends NpcCombat {
  @Inject
  private Npc npc;
  private HitStyleType attackStyle;
  private int lastStyleChange;
  private int misses;
  private int damageTaken;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop = NpcCombatDrop.builder().rareDropTableRate(NpcCombatDropTable.CHANCE_1_IN_256)
        .clue(NpcCombatDrop.ClueScroll.ELITE, NpcCombatDropTable.CHANCE_1_IN_500)
        .clue(NpcCombatDrop.ClueScroll.HARD, NpcCombatDropTable.CHANCE_1_IN_100);
    var dropTable =
        NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_1_IN_1500).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.HEAVY_BALLISTA)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_1_IN_750).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LIGHT_BALLISTA)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_1_IN_300).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ZENYTE_SHARD)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PAPAYA_TREE_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PALM_TREE_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WILLOW_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAPLE_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.YEW_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAGIC_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SPIRIT_SEED, 2)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_BOLTS, 102, 150)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_SCIMITAR)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SHARK, 2, 3)));
    dropTable
        .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_DWARF_WEED_NOTED, 7, 12)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WATERMELON_SEED, 30)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RANARR_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SNAPDRAGON_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TORSTOL_SEED, 1, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DIAMOND_NOTED, 4, 6)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANTITE_BAR_NOTED, 6)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_BAR_NOTED, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_JAVELIN_HEADS, 31, 55)));
    dropTable
        .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_JAVELIN_HEADS, 5, 43)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_PLATELEGS)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_PLATESKIRT)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_CHAINBODY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LAW_RUNE, 50, 75)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 50, 75)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PRAYER_POTION_3, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SARADOMIN_BREW_2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_KWUARM_NOTED, 7, 12)));
    dropTable
        .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_CADANTINE_NOTED, 7, 12)));
    dropTable
        .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_LANTADYME_NOTED, 7, 12)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 5366, 9991)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.JAVELIN_SHAFT, 266, 1238)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ASHES)));
    drop.table(dropTable.build());


    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.DEMONIC_GORILLA_275).id(NpcId.DEMONIC_GORILLA_275_7145)
        .id(NpcId.DEMONIC_GORILLA_275_7146);
    combat.hitpoints(NpcCombatHitpoints.total(380));
    combat.stats(NpcCombatStats.builder().attackLevel(205).magicLevel(195).rangedLevel(195)
        .defenceLevel(200).bonus(BonusType.MELEE_ATTACK, 43).bonus(BonusType.ATTACK_MAGIC, 40)
        .bonus(BonusType.ATTACK_RANGED, 43).bonus(BonusType.MELEE_DEFENCE, 50)
        .bonus(BonusType.DEFENCE_MAGIC, 50).bonus(BonusType.DEFENCE_RANGED, 50).build());
    combat.aggression(NpcCombatAggression.PLAYERS);
    combat.immunity(NpcCombatImmunity.builder().venom(true).build());
    combat.type(NpcCombatType.DEMON).deathAnimation(7229).blockAnimation(7224);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(30));
    style.animation(7226).attackSpeed(5);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(30));
    style.animation(7227).attackSpeed(5);
    style.targetGraphic(new Graphic(1303));
    style.projectile(NpcCombatProjectile.id(1302));
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(30));
    style.animation(7225).attackSpeed(5);
    style.targetGraphic(new Graphic(1305));
    style.projectile(NpcCombatProjectile.id(1304));
    combat.style(style.build());


    return Arrays.asList(combat.build());
  }

  @Override
  public void spawnHook() {
    lastStyleChange = 0;
    misses = 0;
    damageTaken = 0;
    chooseProtectionPrayer(
        HitStyleType.getRandomType(HitStyleType.MELEE, HitStyleType.RANGED, HitStyleType.MAGIC));
    attackStyle =
        HitStyleType.getRandomType(HitStyleType.MELEE, HitStyleType.RANGED, HitStyleType.MAGIC);
  }

  @Override
  public void tickStartHook() {
    if (!npc.isAttacking() && lastStyleChange++ >= 50) {
      chooseAttackStyle();
    }
    if (npc.getCombat().getHitDelay() == 0 && npc.isAttacking() && attackStyle != HitStyleType.MELEE
        && PRandom.randomE(5) == 0
        && npc.getCombat().canAttackEntity(npc.getAttackingEntity(), null)
        && npc.withinDistance(npc.getAttackingEntity(), 10)) {
      addEvent(new TileHitEvent(4, npc.getController(), npc.getAttackingEntity(), 30,
          HitStyleType.TYPELESS));
      npc.getController().sendMapGraphic(npc.getAttackingEntity(), new Graphic(71, 0, 126));
      var tile = new Tile(npc.getAttackingEntity());
      tile.setY(tile.getY() - 1);
      npc.getController().sendMapProjectile(null, tile, npc.getAttackingEntity(), 856, 200, 1, 0,
          126, 16, 64);
      npc.setAnimation(7228);
      npc.getCombat().setHitDelay(5);
    }
  }

  @Override
  public void npcApplyHitEndHook(Hit hit) {
    if (!npc.getCombat().isDead() && hit.getDamage() > 0) {
      damageTaken += hit.getDamage();
      if (damageTaken >= 50) {
        chooseProtectionPrayer(hit.getHitStyleType());
      }
    }
  }

  @Override
  public HitStyleType attackTickHitStyleTypeHook(HitStyleType hitStyleType, Entity opponent) {
    return attackStyle;
  }

  @Override
  public double damageInflictedHook(NpcCombatStyle combatStyle, Entity opponent, double damage) {
    if (damage <= 0 && ++misses >= 3) {
      misses = 0;
      chooseAttackStyle();
    }
    return damage;
  }

  @Override
  public double damageReceivedHook(Entity opponent, double damage, HitStyleType hitStyleType,
      HitStyleType defenceType) {
    if (npc.getId() == NpcId.DEMONIC_GORILLA_275 && hitStyleType == HitStyleType.MELEE) {
      damage = 0;
    } else if (npc.getId() == NpcId.DEMONIC_GORILLA_275_7145
        && hitStyleType == HitStyleType.RANGED) {
      damage = 0;
    } else if (npc.getId() == NpcId.DEMONIC_GORILLA_275_7146
        && hitStyleType == HitStyleType.MAGIC) {
      damage = 0;
    }
    return damage;
  }

  @Override
  public void attackTickEndHook(NpcCombatStyle combatStyle, Entity opponent) {
    lastStyleChange = 0;
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    if (npc.getArea().inWilderness() && PRandom.inRange(
        player.getCombat().getDropRate(NpcCombatDropTable.CHANCE_1_IN_5000, ItemId.DRAGON_CLAWS))) {
      npc.getController().addMapItem(new Item(ItemId.DRAGON_CLAWS), dropTile, player);
    }
  }

  @Override
  public int deathDropItemsRollsHook(Player player, int rolls) {
    if (player.getSkills().isWildernessSlayerTask(npc)) {
      return rolls + 1;
    }
    return rolls;
  }

  public void chooseProtectionPrayer(HitStyleType hitStyleType) {
    damageTaken = 0;
    if (hitStyleType == HitStyleType.MELEE) {
      npc.setId(NpcId.DEMONIC_GORILLA_275);
    } else if (hitStyleType == HitStyleType.RANGED) {
      npc.setId(NpcId.DEMONIC_GORILLA_275_7145);
    } else if (hitStyleType == HitStyleType.MAGIC) {
      npc.setId(NpcId.DEMONIC_GORILLA_275_7146);
    }
  }

  public void chooseAttackStyle() {
    lastStyleChange = 0;
    if (attackStyle == HitStyleType.MELEE) {
      attackStyle = HitStyleType.getRandomType(HitStyleType.RANGED, HitStyleType.MAGIC);
    } else if (attackStyle == HitStyleType.RANGED) {
      attackStyle = HitStyleType.getRandomType(HitStyleType.MELEE, HitStyleType.MAGIC);
    } else if (attackStyle == HitStyleType.MAGIC) {
      attackStyle = HitStyleType.getRandomType(HitStyleType.MELEE, HitStyleType.RANGED);
    }
  }
}
