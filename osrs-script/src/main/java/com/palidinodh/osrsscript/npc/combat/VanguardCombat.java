package com.palidinodh.osrsscript.npc.combat;

import java.util.Arrays;
import java.util.List;
import com.google.inject.Inject;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitMarkType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import lombok.var;

class VanguardCombat extends NpcCombat {
  private static final Tile[] TILES =
      { new Tile(3316, 5329), new Tile(3309, 5333), new Tile(3309, 5324) };

  @Inject
  private Npc npc;
  private boolean loaded;
  private boolean vanguardsLoaded;
  private Npc[] vanguards;
  private HitStyleType hitStyleType;
  private int tileIndex = -1;
  private int moveDelay;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.VANGUARD).id(NpcId.VANGUARD_7526);
    combat.hitpoints(NpcCombatHitpoints.total(133));
    combat.stats(NpcCombatStats.builder().attackLevel(150).magicLevel(150).rangedLevel(150)
        .defenceLevel(210).build());
    combat.aggression(NpcCombatAggression.builder().range(6).always(true).build());
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).bind(true).build());
    combat.deathAnimation(7432);


    var meleeDrop = NpcCombatDrop.builder().additionalPlayers(255);
    var dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.OVERLOAD_PLUS_4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.REVITALISATION_PLUS_4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PRAYER_ENHANCE_PLUS_4)));
    meleeDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ELDER_PLUS_4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.XERICS_AID_PLUS_4, 1, 2)));
    meleeDrop.table(dropTable.build());


    var meleeCombat = NpcCombatDefinition.builder();
    meleeCombat.id(NpcId.VANGUARD_7527);
    meleeCombat.hitpoints(NpcCombatHitpoints.builder().total(133).alwaysShow(true).build());
    meleeCombat.stats(NpcCombatStats.builder().attackLevel(150).magicLevel(150).rangedLevel(150)
        .defenceLevel(210).bonus(BonusType.MELEE_ATTACK, 20).bonus(BonusType.DEFENCE_STAB, 65)
        .bonus(BonusType.DEFENCE_SLASH, 80).bonus(BonusType.DEFENCE_CRUSH, 100)
        .bonus(BonusType.DEFENCE_MAGIC, 80).bonus(BonusType.DEFENCE_RANGED, 400).build());
    meleeCombat.aggression(NpcCombatAggression.builder().range(6).always(true).build());
    meleeCombat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).bind(true).build());
    meleeCombat.deathAnimation(7432);
    meleeCombat.drop(meleeDrop.build());

    var style = NpcCombatStyle.builder();
    style.damage(NpcCombatDamage.builder().maximum(16).prayerEffectiveness(0.5).build());
    style.animation(7441).attackSpeed(4);
    style.attackCount(3);
    meleeCombat.style(style.build());


    var rangedDrop = NpcCombatDrop.builder().additionalPlayers(255);
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.OVERLOAD_PLUS_4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.REVITALISATION_PLUS_4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PRAYER_ENHANCE_PLUS_4)));
    rangedDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TWISTED_PLUS_4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.XERICS_AID_PLUS_4, 1, 2)));
    rangedDrop.table(dropTable.build());


    var rangedCombat = NpcCombatDefinition.builder();
    rangedCombat.id(NpcId.VANGUARD_7528);
    rangedCombat.hitpoints(NpcCombatHitpoints.builder().total(133).alwaysShow(true).build());
    rangedCombat.stats(NpcCombatStats.builder().attackLevel(150).magicLevel(150).rangedLevel(150)
        .defenceLevel(210).bonus(BonusType.ATTACK_RANGED, 40).bonus(BonusType.DEFENCE_STAB, 55)
        .bonus(BonusType.DEFENCE_SLASH, 60).bonus(BonusType.DEFENCE_CRUSH, 100)
        .bonus(BonusType.DEFENCE_MAGIC, 400).bonus(BonusType.DEFENCE_RANGED, 300).build());
    rangedCombat.aggression(NpcCombatAggression.builder().range(6).always(true).build());
    rangedCombat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).bind(true).build());
    rangedCombat.focus(NpcCombatFocus.builder().disableFollowingOpponent(true).build());
    rangedCombat.deathAnimation(7432);
    rangedCombat.drop(rangedDrop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.builder().maximum(16).prayerEffectiveness(0.5).build());
    style.animation(7446).attackSpeed(4);
    style.targetGraphic(new Graphic(305));
    style.projectile(NpcCombatProjectile.builder().id(570).speedMinimumDistance(10).build());
    rangedCombat.style(style.build());


    var magicDrop = NpcCombatDrop.builder().additionalPlayers(255);
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.OVERLOAD_PLUS_4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.REVITALISATION_PLUS_4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PRAYER_ENHANCE_PLUS_4)));
    magicDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.KODAI_PLUS_4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.XERICS_AID_PLUS_4, 1, 2)));
    magicDrop.table(dropTable.build());


    var magicCombat = NpcCombatDefinition.builder();
    magicCombat.id(NpcId.VANGUARD_7529);
    magicCombat.hitpoints(NpcCombatHitpoints.builder().total(133).alwaysShow(true).build());
    magicCombat.stats(NpcCombatStats.builder().attackLevel(150).magicLevel(150).rangedLevel(150)
        .defenceLevel(210).bonus(BonusType.ATTACK_MAGIC, 40).bonus(BonusType.DEFENCE_STAB, 315)
        .bonus(BonusType.DEFENCE_SLASH, 340).bonus(BonusType.DEFENCE_CRUSH, 400)
        .bonus(BonusType.DEFENCE_MAGIC, 110).bonus(BonusType.DEFENCE_RANGED, 50).build());
    magicCombat.aggression(NpcCombatAggression.builder().range(6).always(true).build());
    magicCombat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).bind(true).build());
    magicCombat.deathAnimation(7432);
    magicCombat.drop(magicDrop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(16).prayerEffectiveness(0.5).build());
    style.animation(7436).attackSpeed(4);
    style.targetGraphic(new Graphic(659));
    style.projectile(NpcCombatProjectile.builder().id(1382).speedMinimumDistance(10).build());
    magicCombat.style(style.build());


    return Arrays.asList(combat.build(), meleeCombat.build(), rangedCombat.build(),
        magicCombat.build());
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("loaded")) {
      return loaded;
    } else if (name.equals("tile_index")) {
      return tileIndex;
    } else if (name.equals("vanguards")) {
      vanguards = (Npc[]) args;
    } else if (name.equals("hit_type")) {
      hitStyleType = (HitStyleType) args[0];
    } else if (name.equals("move_delay")) {
      moveDelay = (int) args[0];
    } else if (name.equals("start_combat")) {
      startCombat((boolean) args[0]);
    } else if (name.equals("move_tick")) {
      moveTick((boolean) args[0]);
    }
    return null;
  }

  @Override
  public void spawnHook() {
    loaded = false;
    vanguardsLoaded = false;
    moveDelay = 0;
  }

  @Override
  public void tickStartHook() {
    if (!loaded) {
      loadProfile();
      return;
    }
    if (!npc.getController().isRegionLoaded()) {
      return;
    }
    if (npc == vanguards[0] && !vanguardsLoaded) {
      var loaded1 = (boolean) vanguards[1].getCombat().script("loaded");
      var loaded2 = (boolean) vanguards[1].getCombat().script("loaded");
      if (!loaded1 || !loaded2) {
        return;
      }
      vanguardsLoaded = true;
    }
    if (moveTick(true)) {
    } else if (combatTick()) {
    }
    if (npc.getId() == NpcId.VANGUARD) {
      var players = npc.getController().getPlayers();
      for (var player : players) {
        if (npc.withinDistance(player, 4)) {
          startCombat(true);
          break;
        }
      }
    }
  }

  @Override
  public boolean canAttackEntityHook(NpcCombatStyle combatStyle, Entity opponent) {
    return npc.getId() == NpcId.VANGUARD_7527 || npc.getId() == NpcId.VANGUARD_7528
        || npc.getId() == NpcId.VANGUARD_7529;
  }

  @Override
  public boolean canBeAttackedHook(Entity opponent, boolean sendMessage,
      HitStyleType hitStyleType) {
    return npc.getId() == NpcId.VANGUARD_7527 || npc.getId() == NpcId.VANGUARD_7528
        || npc.getId() == NpcId.VANGUARD_7529;
  }

  public void loadProfile() {
    if (loaded || npc.getController().getPlayers().isEmpty()) {
      return;
    }
    loaded = true;
    var averageHP = 0;
    var playerMultiplier = 1.0;
    var players = npc.getController().getPlayers();
    for (var player : players) {
      averageHP += player.getCombat().getMaxHitpoints();
      playerMultiplier += 0.5;
    }
    averageHP /= players.size();
    var hitpoints = (int) ((50 + players.size() * 25 + averageHP * 2) * playerMultiplier) / 3;
    setMaxHitpoints(hitpoints);
    setHitpoints(getMaxHitpoints());
    npc.setId(NpcId.VANGUARD);
    for (var i = 0; i < TILES.length; i++) {
      if (npc.matchesTile(TILES[i])) {
        tileIndex = i;
        break;
      }
    }
    if (tileIndex == -1) {
      tileIndex = PRandom.randomE(3);
      npc.getSpawn().getTile().setTile(TILES[tileIndex]);
      npc.setTile(npc.getSpawn().getTile());
    }
    if (vanguards == null) {
      var altTileIndex = (tileIndex + 1) % TILES.length;
      vanguards = new Npc[3];
      vanguards[0] = npc;
      vanguards[1] = npc.getController().addNpc(new NpcSpawn(NpcId.VANGUARD, TILES[altTileIndex]));
      vanguards[1].getController().setMultiCombatFlag(true);
      vanguards[1].getSpawn().moveDistance(npc.getSpawn().getMoveDistance());
      altTileIndex = (altTileIndex + 1) % TILES.length;
      vanguards[2] = npc.getController().addNpc(new NpcSpawn(NpcId.VANGUARD, TILES[altTileIndex]));
      vanguards[2].getController().setMultiCombatFlag(true);
      vanguards[2].getSpawn().moveDistance(npc.getSpawn().getMoveDistance());
      vanguards[0].getCombat().script("vanguards", (Object[]) vanguards);
      vanguards[0].getCombat().script("hit_type", HitStyleType.MELEE);
      vanguards[1].getCombat().script("vanguards", (Object[]) vanguards);
      vanguards[1].getCombat().script("hit_type", HitStyleType.RANGED);
      vanguards[2].getCombat().script("vanguards", (Object[]) vanguards);
      vanguards[2].getCombat().script("hit_type", HitStyleType.MAGIC);
    }
  }

  public void startCombat(boolean loadAll) {
    if (vanguards == null) {
      return;
    }
    npc.setId(NpcId.VANGUARD_7526);
    npc.setAnimation(7428);
    npc.setLock(2);
    npc.getController().setMagicBind(2, null);
    if (loadAll) {
      for (var vanguard : vanguards) {
        if (npc == vanguard) {
          continue;
        }
        vanguard.getCombat().script("start_combat", false);
      }
    }
  }

  public void startMove() {
    if (!npc.isVisible() || isDead() || vanguards == null) {
      return;
    }
    npc.setId(NpcId.VANGUARD_7526);
    if (hitStyleType == HitStyleType.MELEE) {
      npc.setAnimation(7442);
    } else if (hitStyleType == HitStyleType.RANGED) {
      npc.setAnimation(7447);
    } else if (hitStyleType == HitStyleType.MAGIC) {
      npc.setAnimation(7437);
    }
    if (hitpointsOutOfRange()) {
      if (vanguards[0].isVisible() && !vanguards[0].getCombat().isDead()) {
        vanguards[0].getCombat().applyHit(new Hit(
            vanguards[0].getCombat().getMaxHitpoints() - vanguards[0].getCombat().getHitpoints(),
            HitMarkType.MAGENTA));
      }
      if (vanguards[1].isVisible() && !vanguards[1].getCombat().isDead()) {
        vanguards[1].getCombat().applyHit(new Hit(
            vanguards[1].getCombat().getMaxHitpoints() - vanguards[1].getCombat().getHitpoints(),
            HitMarkType.MAGENTA));
      }
      if (vanguards[2].isVisible() && !vanguards[2].getCombat().isDead()) {
        vanguards[2].getCombat().applyHit(new Hit(
            vanguards[2].getCombat().getMaxHitpoints() - vanguards[2].getCombat().getHitpoints(),
            HitMarkType.MAGENTA));
      }
    }
    tileIndex = (tileIndex + 1) % TILES.length;
    npc.getController().setMagicBind(2, null);
    npc.getMovement().clear();
    clear();
    clearHitEvents();
    npc.getMovement().addMovement(TILES[tileIndex]);
    npc.setLock(npc.getMovement().getDirections().size() + 2);
  }

  public boolean moveTick(boolean loadAll) {
    if (npc.getId() != NpcId.VANGUARD_7526 || npc.isLocked() || vanguards == null) {
      return false;
    }
    if (!npc.matchesTile(TILES[tileIndex])) {
      return true;
    }
    if (loadAll) {
      for (var vanguard : vanguards) {
        if (npc == vanguard || !vanguard.isVisible() || vanguard.getCombat().isDead()) {
          continue;
        }
        var ti = (int) vanguard.getCombat().script("tile_index");
        if (!vanguard.matchesTile(TILES[ti])) {
          return true;
        }
      }
    }
    if (hitStyleType == HitStyleType.MELEE) {
      npc.setId(NpcId.VANGUARD_7527);
      npc.setAnimation(7438);
    } else if (hitStyleType == HitStyleType.RANGED) {
      npc.setId(NpcId.VANGUARD_7528);
      npc.setAnimation(7443);
    } else if (hitStyleType == HitStyleType.MAGIC) {
      npc.setId(NpcId.VANGUARD_7529);
      npc.setAnimation(7433);
    }
    npc.setLock(2);
    npc.getController().setMagicBind(2, null);
    if (loadAll) {
      moveDelay = 14 + PRandom.randomI(14);
      for (var vanguard : vanguards) {
        if (npc == vanguard || !vanguard.isVisible() || vanguard.getCombat().isDead()) {
          continue;
        }
        vanguard.getCombat().script("move_tick", true);
        vanguard.getCombat().script("move_delay", moveDelay);
      }
    }
    return true;
  }

  public boolean combatTick() {
    if (npc.getId() != NpcId.VANGUARD_7527 && npc.getId() != NpcId.VANGUARD_7528
        && npc.getId() != NpcId.VANGUARD_7529) {
      return false;
    }
    if (moveDelay > 0) {
      moveDelay--;
    }
    if (moveDelay > 1 && hitpointsOutOfRange()) {
      for (var vanguard : vanguards) {
        vanguard.getCombat().script("move_delay", 1);
      }
    }
    if (moveDelay == 0) {
      startMove();
    }
    return true;
  }

  public boolean hitpointsOutOfRange() {
    var hp0 = !vanguards[0].isVisible() ? 0
        : (int) (vanguards[0].getCombat().getHitpoints()
            / (double) vanguards[0].getCombat().getMaxHitpoints() * 100.0);
    var hp1 = !vanguards[1].isVisible() ? 0
        : (int) (vanguards[1].getCombat().getHitpoints()
            / (double) vanguards[0].getCombat().getMaxHitpoints() * 100.0);
    var hp2 = !vanguards[2].isVisible() ? 0
        : (int) (vanguards[2].getCombat().getHitpoints()
            / (double) vanguards[0].getCombat().getMaxHitpoints() * 100.0);
    return hp0 > 0 && hp1 > 0 && Math.abs(hp0 - hp1) > 33
        || hp0 > 0 && hp2 > 0 && Math.abs(hp0 - hp2) > 33
        || hp1 > 0 && hp2 > 0 && Math.abs(hp1 - hp2) > 33;
  }
}
