package com.palidinodh.osrsscript.npc.combat;

import java.util.Arrays;
import java.util.List;
import com.google.inject.Inject;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.random.PRandom;
import lombok.var;

class GelatinnothMotherCombat extends NpcCombat {
  @Inject
  private Npc npc;
  private int changeDelay;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.GELATINNOTH_MOTHER_130).id(NpcId.GELATINNOTH_MOTHER_130_4885)
        .id(NpcId.GELATINNOTH_MOTHER_130_4886).id(NpcId.GELATINNOTH_MOTHER_130_4887)
        .id(NpcId.GELATINNOTH_MOTHER_130_4888).id(NpcId.GELATINNOTH_MOTHER_130_4889);
    combat.hitpoints(NpcCombatHitpoints.total(240));
    combat.stats(NpcCombatStats.builder().attackLevel(78).rangedLevel(50).defenceLevel(81)
        .bonus(BonusType.MELEE_DEFENCE, 150).bonus(BonusType.DEFENCE_MAGIC, 50)
        .bonus(BonusType.DEFENCE_RANGED, 50).build());
    combat.aggression(NpcCombatAggression.PLAYERS);
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.deathAnimation(1342).blockAnimation(1340);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(20));
    style.animation(1341).attackSpeed(4);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(20));
    style.animation(1343).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(599));
    style.attackCount(2);
    combat.style(style.build());


    return Arrays.asList(combat.build());
  }

  @Override
  public void spawnHook() {
    setChangeDelay();
  }

  @Override
  public void tickStartHook() {
    if (npc.isLocked()) {
      return;
    }
    if (changeDelay > 0) {
      changeDelay--;
      if (changeDelay == 0) {
        setChangeDelay();
        if (npc.getId() == NpcId.GELATINNOTH_MOTHER_130) {
          npc.setId(NpcId.GELATINNOTH_MOTHER_130_4886);
        } else if (npc.getId() == NpcId.GELATINNOTH_MOTHER_130_4886) {
          npc.setId(NpcId.GELATINNOTH_MOTHER_130_4885);
        } else if (npc.getId() == NpcId.GELATINNOTH_MOTHER_130_4885) {
          npc.setId(NpcId.GELATINNOTH_MOTHER_130_4889);
        } else if (npc.getId() == NpcId.GELATINNOTH_MOTHER_130_4889) {
          npc.setId(NpcId.GELATINNOTH_MOTHER_130_4887);
        } else if (npc.getId() == NpcId.GELATINNOTH_MOTHER_130_4887) {
          npc.setId(NpcId.GELATINNOTH_MOTHER_130_4888);
        } else if (npc.getId() == NpcId.GELATINNOTH_MOTHER_130_4888) {
          npc.setId(NpcId.GELATINNOTH_MOTHER_130);
        }
      }
    }
  }

  @Override
  public double damageReceivedHook(Entity opponent, double damage, HitStyleType hitStyleType,
      HitStyleType defenceType) {
    if (!opponent.isPlayer()) {
      return damage;
    }
    var player = opponent.asPlayer();
    var spell = player.getMagic().getActiveSpell();
    if (npc.getId() == NpcId.GELATINNOTH_MOTHER_130 && (hitStyleType != HitStyleType.MAGIC
        || spell == null || !spell.getName().startsWith("wind"))) {
      return 0;
    } else if (npc.getId() == NpcId.GELATINNOTH_MOTHER_130_4885
        && hitStyleType != HitStyleType.MELEE) {
      return 0;
    } else if (npc.getId() == NpcId.GELATINNOTH_MOTHER_130_4886
        && (hitStyleType != HitStyleType.MAGIC || spell == null
            || !spell.getName().startsWith("water"))) {
      return 0;
    } else if (npc.getId() == NpcId.GELATINNOTH_MOTHER_130_4887
        && (hitStyleType != HitStyleType.MAGIC || spell == null
            || !spell.getName().startsWith("fire"))) {
      return 0;
    } else if (npc.getId() == NpcId.GELATINNOTH_MOTHER_130_4888
        && hitStyleType != HitStyleType.RANGED) {
      return 0;
    } else if (npc.getId() == NpcId.GELATINNOTH_MOTHER_130_4889
        && (hitStyleType != HitStyleType.MAGIC || spell == null
            || !spell.getName().startsWith("earth"))) {
      return 0;
    }
    return damage;
  }

  public void setChangeDelay() {
    changeDelay = 25 + PRandom.randomI(8);
  }
}
