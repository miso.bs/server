package com.palidinodh.osrsscript.npc.combat;

import java.util.Arrays;
import java.util.List;
import com.google.inject.Inject;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatType;
import com.palidinodh.osrscore.model.entity.player.Player;
import lombok.var;

class VorkathZombifiedSpawnCombat extends NpcCombat {
  @Inject
  private Npc npc;
  private int countdown1;
  private int countdown2;
  private Entity following;
  private int explosionDamage;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.ZOMBIFIED_SPAWN_64);
    combat.hitpoints(NpcCombatHitpoints.total(38));
    combat.stats(NpcCombatStats.builder().attackLevel(82).defenceLevel(6)
        .bonus(BonusType.ATTACK_STAB, 1).bonus(BonusType.ATTACK_SLASH, 1)
        .bonus(BonusType.ATTACK_CRUSH, 1).bonus(BonusType.ATTACK_MAGIC, 1)
        .bonus(BonusType.ATTACK_RANGED, 1).bonus(BonusType.MELEE_DEFENCE, 3)
        .bonus(BonusType.DEFENCE_MAGIC, -100).bonus(BonusType.DEFENCE_RANGED, 3).build());
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());
    combat.type(NpcCombatType.UNDEAD).deathAnimation(7891);


    return Arrays.asList(combat.build());
  }

  @Override
  public void spawnHook() {
    countdown1 = 2;
    countdown2 = 2;
    following = null;
    explosionDamage = 0;
  }

  @Override
  public void tickStartHook() {
    if (npc.getMovement().getFollowing() != null) {
      following = npc.getMovement().getFollowing();
    }
    if (!npc.isLocked() && !npc.withinMapDistance(following)) {
      npc.getCombat().timedDeath();
      return;
    }
    if (!npc.withinDistance(following, 1)) {
      return;
    }
    if (--countdown1 >= 0) {
      if (countdown1 == 1) {
        npc.getMovement().setFollowing(null);
        npc.getMovement().clear();
      } else if (countdown1 == 0) {
        explosionDamage = (int) (npc.getCombat().getHitpoints() * 1.4);
        npc.getCombat().timedDeath(countdown2);
      }
    } else if (--countdown2 >= 0) {
      if (countdown2 == 0) {
        if (npc.withinDistance(following, 1)) {
          var hitEvent = new HitEvent(0, following, new Hit(explosionDamage));
          following.getCombat().addHitEvent(hitEvent);
          following.getController().setMagicBind(0);
        }
      }
    }
  }

  @Override
  public double damageReceivedHook(Entity opponent, double damage, HitStyleType hitStyleType,
      HitStyleType defenceType) {
    if (opponent instanceof Player && hitStyleType == HitStyleType.MAGIC && damage > 0) {
      var player = opponent.asPlayer();
      if (player.getMagic().getActiveSpell() != null
          && player.getMagic().getActiveSpell().getName().contains("crumble undead")) {
        return npc.getCombat().getHitpoints();
      }
    }
    return damage;
  }
}
