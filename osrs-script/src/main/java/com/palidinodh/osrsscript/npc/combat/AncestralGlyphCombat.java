package com.palidinodh.osrsscript.npc.combat;

import java.util.Arrays;
import java.util.List;
import com.google.inject.Inject;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import lombok.var;

class AncestralGlyphCombat extends NpcCombat {
  @Inject
  private Npc npc;
  private int moveDelay;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.ANCESTRAL_GLYPH);
    combat.hitpoints(NpcCombatHitpoints.total(600));
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).bind(true).build());
    combat.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());
    combat.deathAnimation(7569).blockAnimation(7568);


    return Arrays.asList(combat.build());
  }

  @Override
  public void spawnHook() {
    moveDelay = 2;
  }

  @Override
  public void tickStartHook() {
    if (npc.isLocked()) {
      return;
    }
    if (npc.getMovement().isRouting()) {
      return;
    }
    if (moveDelay > 0) {
      moveDelay--;
    }
    if (moveDelay != 0) {
      return;
    }
    moveDelay = 4;
    npc.getMovement().clear();
    if (npc.getY() == npc.getSpawn().getTile().getY()) {
      npc.getMovement().addMovement(npc.getSpawn().getTile().getX(),
          npc.getSpawn().getTile().getY() - 2);
    } else {
      if (npc.getX() < npc.getSpawn().getTile().getX()) {
        npc.getMovement().addMovement(npc.getSpawn().getTile().getX() + 13,
            npc.getSpawn().getTile().getY() - 2);
      } else {
        npc.getMovement().addMovement(npc.getSpawn().getTile().getX() - 13,
            npc.getSpawn().getTile().getY() - 2);
      }
    }
  }

  @Override
  public boolean canBeAttackedHook(Entity opponent, boolean sendMessage,
      HitStyleType hitStyleType) {
    return opponent instanceof Npc && opponent.getId() == NpcId.TZKAL_ZUK_1400;
  }
}
